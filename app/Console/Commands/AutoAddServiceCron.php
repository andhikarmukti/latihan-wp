<?php

namespace App\Console\Commands;

use App\Models\Server;
use GuzzleHttp\Client;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\AutoPriceLog;
use App\Models\ServerAction;
use App\Models\ServerParameter;
use App\Models\ServiceCategory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\ServiceInformationUpdate;

class AutoAddServiceCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoAddServiceCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client();
        $server = Server::where('is_active', true)
        ->where('id', 7)
        ->first();
        $form_params = [];
        $server_action = ServerAction::where([
            ['server_id', $server->id],
            ['action_id', 3]
        ])->first();
        $services = Service::all();
        $server_parameters = ServerParameter::where([
            ['server_id', $server->id],
            ['action_id', 3]
        ])->get();
        foreach($server_parameters as $server_parameter){
            $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
        }

        $responses = $client->request(($server->serverActions->where('action_id', 3)->first()->method), ($server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : '')), [
            'form_params' => $form_params
        ])->getBody();

        $responses = json_decode($responses);
        $responses = property_exists((object)$responses, "data") ? $responses->data : $responses;

        foreach($responses as $response){
            $tambah_service = true;
            foreach($services as $service){
                if($response->service == $service->server_service_id){
                    $tambah_service = false;
                }
            }
            if($tambah_service == true){
                try {
                    DB::beginTransaction();

                    $new_service = Service::create([
                        'service_name' => $response->name,
                        'service_category_id' => ServiceCategory::where('nama_category', $response->category)->first()->id,
                        'type_id' => ServiceType::where('type', $response->type)->first()->id,
                        'server_id' => 7,
                        'server_service_id' => $response->service,
                        'rate' => $response->rate,
                        'hpp' => $response->rate,
                        'min' => $response->min,
                        'max' => $response->max,
                        'refill' => $response->refill,
                        'refill_duration' => $response->refill_duration,
                        'member_info' => $response->member_info,
                        'created_by' => 1,
                    ]);

                    $auto_price_log = AutoPriceLog::create([
                        'service_id' => $new_service->id,
                        'old_hpp' => $new_service->hpp,
                        'new_hpp' => $new_service->hpp,
                        'old_rate' => $new_service->rate,
                        'new_rate' => $new_service->rate,
                        'old_status_active' => 1,
                        'new_status_active' => 1,
                    ]);

                    ServiceInformationUpdate::create([
                        'auto_price_log_id' => $auto_price_log->id,
                        'service_id' => $new_service->id,
                        'info' => 'new service',
                        'harga_naik' => 0,
                        'harga_turun' => 0,
                        'status_active' => 1,
                    ]);

                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    Log::error([$e->getMessage()]);
                }
                Log::info("Berhasil menambahkan service baru : " . $new_service->name . ', server_service_id : ' . $new_service->server_service_id);
            }
        }
    }
}
