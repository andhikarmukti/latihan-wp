<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Topup;
use app\Helpers\Helpers;
use App\Jobs\SendWaJob;
use App\Models\AppConfig;
use App\Models\TopupLog;
use BCAParser\BCAParser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AutoCekMutasiCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AutoCekMutasiCron';
    protected $username;
    protected $password;

    public function __construct()
    {
        parent::__construct();

        $this->username = config('bca.username');
        $this->password = config('bca.password');
    }

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $parser = new BCAParser($this->username, $this->password);
        $start_date = now()->subDays(1)->format('Y-m-d');
        $end_date = now()->format('Y-m-d');
        $topup_pendings = Topup::where('status', 'pending')
        ->where('topup_transfer_type_id', 1)
        ->get();

        if($topup_pendings->count() > 0){
            $all_mutasi = $parser->getListTransaksi($start_date, $end_date);
        foreach($all_mutasi as $mutasi){
            $panjang_array = count($mutasi['description']);
            $nominal_mutasi = (int)str_replace(",", "", $mutasi['description'][$panjang_array - 1]);
            // $nama_pengirim = $mutasi['description'][$panjang_array - 3];
            // $catatan = $mutasi['description'][$panjang_array - 4];
            //Cek di sini kalau ada error pengecekan mutasi, biasanya karna ada history biaya admin
            Log::info([$mutasi]);
            // Log::info(['COUNT', count($mutasi['description'])]);
            // Log::info([$mutasi['date'], $nama_pengirim, $nominal_mutasi, $catatan]);

            foreach($topup_pendings as $topup_pending){
                if($nominal_mutasi == $topup_pending->nominal && $mutasi['date'] == ('PEND' ?? $topup_pending->created_at->format('Y-m-d'))){
                    try{
                        $user = User::where('id', $topup_pending->user_id)->first();
                        DB::beginTransaction();
                        // Update status topup user
                        $topup_pending->update([
                            'status' => 'paid',
                            'note' => 'auto paid',
                            'updated_by' => 1
                        ]);

                        // Create log topup
                        TopupLog::create([
                            'user_id' => $topup_pending->user_id,
                            'topup_id' => $topup_pending->id,
                            'saldo_awal' => $user->saldo,
                            'saldo_ditambahkan' => $topup_pending->nominal + $topup_pending->bonus,
                            'total_saldo' => $user->saldo + $topup_pending->nominal,
                            'bank_id' => $topup_pending->bank_id,
                            'topup_transfer_type_id' => $topup_pending->topup_transfer_type_id,
                            'note' => 'auto paid',
                            'created_by' => $user->id
                        ]);

                        // Log saldo
                        Helpers::logSaldo('topup', $user->saldo, ($user->saldo + $topup_pending->nominal + $topup_pending->bonus), null, $topup_pending);

                        // Tambah saldo user
                        $user->saldo += $topup_pending->nominal + $topup_pending->bonus;
                        $user->save();
                        DB::commit();

                        Log::info("SUKSES PEMBAYARAN TERKONFIRMASI!");
                        $no_wa = $topup_pending->user->no_wa;
                        $message = '*TOPUP ' . AppConfig::find('WEB.NAME')->value . ' BERHASIL*
Topup ID ' . $topup_pending->id . '

_Topup has been confirmed automatically by the system._

Thankyou 😊🙏';
                        dispatch(new SendWaJob($no_wa, $message));

                        $no_wa2 = AppConfig::find('CONTACT.OWNER')->value;
                        $message2 = '*TOPUP ' . AppConfig::find('WEB.NAME')->value . ' BERHASIL*
Topup ID : ' . $topup_pending->id . '
Username : ' . $topup_pending->user->username . '
Nominal : ' . Helpers::format_rupiah($topup_pending->nominal + $topup_pending->bonus) . '
Note : ' . $topup_pending->note . '

_Topup has been confirmed automatically by the system._

Thankyou 😊🙏';
                        dispatch(new SendWaJob($no_wa2, $message2));
                    }catch(\Exception $e){
                        DB::rollBack();
                        Log::info("ADA KESALAHAN PROSES CEK MUTASI!");
                        Log::info([$e->getMessage()]);
                    }
                }
            }
        }
        }

        $parser->logout();
    }
}
