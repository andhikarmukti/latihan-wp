<?php

namespace App\Console\Commands;

use App\Models\Topup;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CancelTopupCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelTopup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $topup_pendings = Topup::whereStatus('pending')->get();
        if($topup_pendings){
            foreach($topup_pendings as $topup_pending){
                $expiredTime = date("Y-m-d H:i:s",strtotime($topup_pending->created_at) + 86400);
                    if(now() > $expiredTime){
                        $topup_pending->update([
                            'status' => 'cancel',
                            'note' => 'expired'
                        ]);
                        Log::info('topup ID : ' . $topup_pending->id . ' has been canceled');
                    }else{
                        Log::info('topup ID : ' . $topup_pending->id . ' still pending');
                    }
            }
        }else{
            Log::info("tidak ada topup yang pending");
        }

    }
}
