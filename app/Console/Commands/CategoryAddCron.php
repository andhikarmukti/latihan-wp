<?php

namespace App\Console\Commands;

use App\Models\Server;
use App\Models\ServiceCategory;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CategoryAddCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categoryAddCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client();
        $server = Server::where('is_active', true)
        ->where('id', 7)
        ->first();
        $form_params = [
            'api_key' => $server->api_key,
            'action' => 'categories'
        ];
        $service_categories = ServiceCategory::all();

        $responses = $client->request('POST', ($server->url . '/' . 'service-categories'), [
            'form_params' => $form_params
        ])->getBody();
        $responses = json_decode($responses);
        $responses = $responses->data;

        foreach($responses as $response){
            $tambah_category = true;
            foreach($service_categories as $service_category){
                if($service_category->nama_category == $response->nama_category){
                    $tambah_category = false;
                }
            }
            if($tambah_category == true){
                ServiceCategory::create([
                    'nama_category' => $response->nama_category,
                    'created_by' => 1
                ]);
                Log::info("Berhasil tambah data category baru : " . $response->nama_category);
            }
        }
    }
}
