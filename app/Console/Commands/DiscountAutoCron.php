<?php

namespace App\Console\Commands;

use App\Models\Service;
use App\Models\Discount;
use Illuminate\Console\Command;

class DiscountAutoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DiscountAutoCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = now()->format('l');
        if($today == 'Friday'){
            $discount_name = 'Jumat Berkah';
            $limit = 5;
        }else if($today == 'Saturday'){
            $discount_name = 'Sabtu Ceria';
            $limit = 5;
        }else if($today == 'Sunday'){
            $discount_name = 'Happy Weekend';
            $limit = 7;
        }else{
            $discount_name = 'Dadakan';
            $limit = 2;
        }

        $services = Service::where('is_active', true)->inRandomOrder()->limit($limit)->get()->pluck('id');

        foreach($services as $service_id){
            $level_member_random = mt_rand(1, 3);
            Discount::create([
                'service_id' => $service_id,
                'discount' => 5,
                'discount_name' => $discount_name,
                'level_member_id' => $level_member_random,
                'start_date' => now(),
                'end_date' => now()->endOfDay(),
                'created_by' => 1
            ]);
        }
    }
}
