<?php

namespace App\Console\Commands;

use App\Models\Server;
use GuzzleHttp\Client;
use App\Models\AppConfig;
use Illuminate\Console\Command;

class ExpiredWebControlCodeCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiredWebControlCodeCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user_keranjangindo = Server::all()->first();

        $client = new Client();
        $user = $client->request('POST', 'https://panel.keranjangindo.com/api/user', [
            'form_params' => [
                'api_key' => $user_keranjangindo->key
            ]
        ])->getBody();
        $user = json_decode($user)->data;

        $control_code = $client->request('POST', 'https://panel.keranjangindo.com/api/fetch-control-code', [
            'form_params' => [
                'user_id' => $user->id
            ]
        ])->getBody();
        $control_code = json_decode($control_code);
        $control_code = json_decode($control_code)->data;

        AppConfig::find('CONTROL.CODE')->update([
            'value' => $control_code
        ]);
    }
}
