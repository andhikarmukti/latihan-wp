<?php

namespace App\Console\Commands;

use App\Models\Server;
use GuzzleHttp\Client;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use App\Models\AppConfig;
use App\Models\ServerAction;
use App\Models\ServerParameter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;

class InfoSaldoServerCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'infoSaldoServerCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $app_config = new AppConfig();
        $min_saldo = $app_config->find('MINIMUM.REMINDER.SALDO')->value;
        $servers = Server::orderBy('id', 'DESC')->get();
        foreach($servers as $server){
            // Tembak order ke API server
            $params = [
                'server_id' => $server->id,
                'action_id' => 4, // check balance action ID nya 4
            ];
            $server_action = ServerAction::where([
                ['server_id', $server->id],
                ['action_id', $params['action_id']]
            ])->first();
            $server_parameters = ServerParameter::where([
                ['server_id', $server->id],
                ['action_id', $params['action_id']]
            ])->get();

            $form_params = [];
            foreach($server_parameters as $server_parameter){
                $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
            }
            // dd($form_params);

            // Push orderan
            $client = New Client();
            try{
                $response = $client->request($server_action->method, $server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : ''), [
                    'form_params' => $form_params
                ])->getBody();
            }catch(GuzzleException $e){
                Log::error('process API order error: ' . $e->getMessage());
                return response()->json($e->getMessage());
            }
            $response = json_decode($response);
            $balance = property_exists((object)$response, "data") ? (int)$response->data->balance : $response->balance;
            if($balance <= $min_saldo){
                $no_wa = $app_config->find('CONTACT.OWNER')->value;
                $message = '*SEGERA ISI SALDO*

Saldo ' . $server->nama_server . ' sisa *' . Helpers::format_rupiah(floor($balance)) . '*';
                dispatch(new SendWaJob($no_wa, $message));
            }

        }
    }
}
