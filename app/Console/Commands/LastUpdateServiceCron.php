<?php

namespace App\Console\Commands;

use App\Models\Service;
use App\Models\HistoryOrder;
use Illuminate\Console\Command;

class LastUpdateServiceCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'LastUpdateServiceCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $services = Service::where('is_active', true)->get();
        foreach($services as $service){
            $history_completed = HistoryOrder::orderBy('id', 'desc')
            ->where('status', 'completed')
            ->where('server_service_id', $service->server_service_id)
            ->first();
            if($history_completed && $history_completed->updated_at){
                $diff_in_seconds = $history_completed->updated_at->diffInSeconds(now());
                $service->last_completed = $diff_in_seconds;
                $service->save();
            }
        }
    }
}
