<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use App\Models\HistoryOrder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PartialRefundCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'partialRefundCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $history_orders = HistoryOrder::where('status', 'Partial')->where('note', '-')->get();
        foreach($history_orders as $history_order){
            $user = $history_order->user;
            $partial = $history_order->remain * ($history_order->harga / $history_order->quantity);
            if($history_order->remain > $history_order->quantity){
                $partial = $history_order->quantity * ($history_order->harga / $history_order->quantity);
            }

            // Log saldo
            Helpers::logSaldo('partial', $user->saldo, ($user->saldo + $partial), $history_order);

            Log::info('Saldo ' . $user->username . ' bertambah karna partial sebesar ' . $partial);
            $user->saldo += $partial;
            $user->saldo_digunakan -= $partial;
            // Pengurangan saldo digunakan jika memiliki upgrade level yang accept
            if($user->upgradeLevelAccept){
                $user->upgradeLevelAccept->update([
                    'saldo_digunakan' => $user->upgradeLevelAccept->saldo_digunakan -= $partial
                ]);
            }
            $user->save();

            if(env('NOTIF_CANCEL') == true){
                if($history_order->notif_cancel_partial == true){
                    $no_wa = $user->no_wa;
                    $message = '*ORDER PARTIAL*

Hallo kak, ' . Helpers::greeting() . ' 😊🙏
mau menginformasikan bahwa ada orderan kakak yang statusnya _Partial_ dengan detail berikut,

Order ID : ' . $history_order->id . '
Service : ' . $history_order->service->service_name . '
Target : ' . $history_order->target . '
Refund Saldo : ' . Helpers::format_rupiah($partial) . '

Orderan partial bisa disebabkan beberapa hal :
1. Server Overload
2. Adanya sekuritas dari social media terkait yang sedang update / ditingkatkan
3. Quantity order sudah mencapai batas maksimal untuk target dan service yang sama

Silahkan dicoba order kembali, atau bisa gunakan service ID yang lain.

Terima kasih 😊🙏
            ';
                    dispatch(new SendWaJob($no_wa, $message));
                }
            }


            $history_order->update([
                'note' => 'Refund ' . Helpers::format_rupiah($partial)
            ]);
        }

    }
}
