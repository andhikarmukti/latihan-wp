<?php

namespace App\Console\Commands;

use App\Models\Topup;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use Illuminate\Console\Command;

class ReminderTopupPendingCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminderTopupPendingCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $topup_pendings = Topup::where('status', 'pending')->get();
        foreach($topup_pendings as $topup_pending){
            $no_wa = $topup_pending->user->no_wa;
            $username = $topup_pending->user->username;
            $message = 'Hallo kak ' . $username . ' 😊
Saat ini ada topup kakak yang masih pending

topup ID : ' . $topup_pending->id . '
nominal : ' . Helpers::format_rupiah($topup_pending->nominal) . '
type : ' . $topup_pending->topupTransferType->type . '
bank : ' . $topup_pending->bank->nama_bank . '

Apakah ada kendala atau bingung saat ingin melakukan pembayarannya kak? 😊🙏';

            dispatch(new SendWaJob($no_wa, $message));
        }
    }
}
