<?php

namespace App\Console\Commands;

use App\Models\ServiceInformationUpdate;
use Illuminate\Console\Command;

class ServiceInformationUpdateDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serviceInformationUpdateDeleteCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ServiceInformationUpdate::where('created_at', '<', now()->subDays(30))->delete();
    }
}
