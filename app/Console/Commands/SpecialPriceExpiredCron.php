<?php

namespace App\Console\Commands;

use App\Models\SpecialPrice;
use Illuminate\Console\Command;

class SpecialPriceExpiredCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'specialPriceExpiredCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $special_prices = SpecialPrice::where('status', 'enable')->get();
        foreach($special_prices as $special_price){
            // Cek jika sudah expired
            if($special_price->expired_date < now()){
                $special_price->status = 'disable';
                $special_price->save();
            }
        }
    }
}
