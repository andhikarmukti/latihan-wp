<?php

namespace App\Console\Commands;

use App\Models\Server;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use App\Models\AppConfig;
use App\Models\HistoryOrder;
use Illuminate\Console\Command;

class SpeedupAutoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SpeedupAutoCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(env('SPEEDUP_AUTO_CRON') ==  true){
            $servers = Server::where('is_active', true)->get();
            $array_order_id_server = [];
            foreach($servers as $server){
                $history_order_by_servers = HistoryOrder::where('server_id', $server->id)
                ->whereNotIn('status', ['Completed', 'Canceled', 'Partial'])
                ->where('pengajuan_speedup', '<', now()->subDay(1))
                ->get();
                foreach($history_order_by_servers as $history_order_by_server){
                    if($history_order_by_server){
                        $array_order_id_server[] = $history_order_by_server->order_id_server;
                    }
                }
                $message = 'Halo kak ' . Helpers::greeting() . ', saya ingin report speedup untuk order ID berikut :

' . implode(", ", $array_order_id_server) . '

Terima kasih 😊🙏

' . AppConfig::find('WEB.NAME')->value;
                $cp = $server->contact_person; // kalau belum diset akan ke nomor pribados

                if($array_order_id_server != []){
                    dispatch(new SendWaJob($cp, $message));
                    foreach($array_order_id_server as $order_id_server){
                        HistoryOrder::where('order_id_server', $order_id_server)->update([
                            'pengajuan_speedup' => now()
                        ]);
                    }
                    $array_order_id_server = [];
                }
            }
        }
    }
}
