<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Topup;
use App\Models\Server;
use GuzzleHttp\Client;
use App\Jobs\SendWaJob;
use App\Models\Service;
use app\Helpers\Helpers;
use App\Models\LogSaldo;
use App\Models\TopupLog;
use BCAParser\BCAParser;
use App\Models\AppConfig;
use App\Models\Discount;
use App\Models\HistoryOrder;
use App\Models\ServerAction;
use App\Models\LogCommission;
use App\Models\ServerParameter;
use App\Models\UserReferral;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Hash;

class TesCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TesCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user_keranjangindo = Server::all()->first();

        $client = new Client();
        $user = $client->request('POST', 'http://127.0.0.1:8000/api/user', [
            'form_params' => [
                'api_key' => $user_keranjangindo->key
            ]
        ])->getBody();
        $user = json_decode($user)->data;

        $control_code = $client->request('POST', 'http://127.0.0.1:8000/api/fetch-control-code', [
            'form_params' => [
                'user_id' => $user->id
            ]
        ])->getBody();
        $control_code = json_decode($control_code);
        $control_code = json_decode($control_code)->data;

        AppConfig::find('CONTROL.CODE')->update([
            'value' => $control_code
        ]);
    }
}
