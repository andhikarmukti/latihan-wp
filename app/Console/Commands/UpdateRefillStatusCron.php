<?php

namespace App\Console\Commands;

use App\Models\Server;
use GuzzleHttp\Client;
use App\Models\RefillStatus;
use App\Models\ServerParameter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;

class UpdateRefillStatusCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateStatusRefill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client();
        $servers = Server::where('is_active', true)->get();
        foreach ($servers as $server) {
            $refill_statuses = new RefillStatus();
            $refill_statuses = $refill_statuses->where('status', 'Pending')->get();
            if (count($refill_statuses) > 0) {
                foreach ($refill_statuses as $refill_status) {
                    if($server->id != 1){
                        $server_parameters = ServerParameter::where([
                            ['server_id', $server->id],
                            ['action_id', 6] // refill_status action id nya 6
                        ])->get();
                        $form_params = [];
                        foreach ($server_parameters as $server_parameter) {
                            $form_params[$server_parameter->parameter] = $refill_status[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
                        }

                        try {
                            $response = $client->request($server->serverActions->where('action_id', 6)->first()->method, $server->url, [
                                'form_params' => $form_params
                            ]);

                            $response = json_decode($response->getBody());
                            RefillStatus::find($refill_status->id)->update([
                                'status' => $response->status
                            ]);
                            Log::info("Berhasil update refill status menjadi " . $response->status . " untuk refill id local : " . $refill_status->id);

                        }
                        catch (ClientException $e) {
                            Log::error("error API Refill Status");
                        }
                    }
                }
            }
        }
    }
}
