<?php

namespace App\Console\Commands;

use app\Helpers\Helpers;
use App\Models\HistoryOrder;
use App\Models\UserReferral;
use App\Models\LogCommission;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateSatusLogCommission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateStatusLogCommissionCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $status_order_log_commissions = LogCommission::whereNotIn('status_order', ['Completed', 'Partial', 'Canceled'])->get();
        if($status_order_log_commissions){
            foreach($status_order_log_commissions as $status_order_log_commission){
                $user_referral = UserReferral::where('user_id', $status_order_log_commission->user_upline_id)->first();
                $history_order = HistoryOrder::where('order_id_server', $status_order_log_commission->order_id_server)->first();
                $user = $history_order->user;
                $persentase_komisi = Helpers::percentCommission($user);

                $status_order_log_commission->status_order = $history_order->status;
                $status_order_log_commission->save();

                if($history_order->status == 'Partial'){
                    $partial_commission = ($history_order->harga - (int)str_replace('.', '', str_replace('Refund Rp ', '', $history_order->note))) * ($persentase_komisi / 100);
                    $status_order_log_commission->commission = $partial_commission;
                    $status_order_log_commission->note = '[PARTIAL] Refund Rp ' . Helpers::format_rupiah((int)str_replace('.', '', str_replace('Refund Rp ', '', $history_order->note)));
                    $status_order_log_commission->save();
                }else if($history_order->status == 'Completed'){
                    $user_referral->total_commission += $status_order_log_commission->commission;
                    $user_referral->save();
                }
                Log::info("Berhasil update status log commission dengan id " . $status_order_log_commission->id);
            }
        }
    }
}
