<?php

namespace App\Console\Commands;

use App\Models\Server;
use GuzzleHttp\Client;
use App\Models\Service;
use App\Models\AutoPriceLog;
use App\Models\ServerAction;
use app\Helpers\ServerHelper;
use App\Models\ServerParameter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\ServiceAutoPriceDisable;
use App\Models\ServiceInformationUpdate;
use GuzzleHttp\Exception\ClientException;

class UpdateServiceInfoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateServiceInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return ServerHelper::updateServiceInfo();
    }
}
