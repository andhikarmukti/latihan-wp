<?php

namespace App\Console\Commands;

use App\Models\Server;
use App\Models\Service;
use App\Models\HistoryOrder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateSpeedCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateSpeedCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $limit = 10;
        $servers = Server::where('is_active', true)->get();

        foreach($servers as $server){
            $services = Service::where('server_id', $server->id)
            ->where('is_active', true)
            ->get();
            foreach($services as $service){
                $total_second = 0;
                $histories = HistoryOrder::orderBy('id', 'DESC')->where('service_id', $service->id)->where('status', 'Completed')->limit($limit)->get();
                foreach($histories as $history){
                    $start_order  = date_create($history->created_at);
                    $completed = date_create($history->updated_at);
                    $diff  = date_diff( $start_order, $completed );

                    $tahun_in_second = $diff->y * 31104000;
                    $bulan_in_second = $diff->m * 2592000;
                    $hari_in_second = $diff->d * 86400;
                    $jam_in_second = $diff->h * 3600;
                    $menit_in_second = $diff->i * 60;
                    $detik_in_second = $diff->s * 1;

                    $total_second += $tahun_in_second + $bulan_in_second + $hari_in_second + $jam_in_second + $menit_in_second + $detik_in_second;
                }

                $service->update([
                    'speed' => $total_second == 0 ? 0 : ($histories->count() == 0 ? 0 : $total_second / $histories->count())
                ]);
            }
        }
    }
}
