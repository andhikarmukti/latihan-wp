<?php

namespace App\Console\Commands;

use app\Helpers\ServerHelper;
use App\Models\HistoryOrder;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateStatusHistoryOrderCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateStatusHistoryOrder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $history_orders = HistoryOrder::where('status', 'Pending')
        ->orWhere('status', 'Processing')
        ->orWhere('status', 'In Progress')
        ->get();
        foreach($history_orders as $history_order){
            ServerHelper::checkStatusOrder($history_order);
        }
    }
}
