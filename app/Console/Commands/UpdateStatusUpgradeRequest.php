<?php

namespace App\Console\Commands;

use App\Models\UpgradeLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateStatusUpgradeRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateStatusUpgradeRequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $upgrade_requests = UpgradeLevel::where('status', 'pending')->get();

        foreach($upgrade_requests as $upgrade_request){
            if($upgrade_request->created_at < now()->subDays(1)){
                $upgrade_request->status = 'cancel';
                $upgrade_request->save();
                Log::info("Status request upgrade level berhasil dicancel dengan id " . $upgrade_request->id);
            }
        }
    }
}
