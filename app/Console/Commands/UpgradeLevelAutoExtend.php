<?php

namespace App\Console\Commands;

use App\Models\User;
use app\Helpers\Helpers;
use App\Models\UpgradeLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpgradeLevelAutoExtend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upgradeLevelAutoExtend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $accepted_upgrades = UpgradeLevel::where([
            ['status', 'accept'],
            ['end_date', '<=', now()],
        ])->get();

        foreach($accepted_upgrades as $accepted_upgrade){
            $perpanjang_auto = $accepted_upgrade->user->levelMember->perpanjang_auto;
            $saldo_digunakan = $accepted_upgrade->saldo_digunakan;

            if($saldo_digunakan < $perpanjang_auto){
                $accepted_upgrade->update([
                    'status' => 'expired'
                ]);
                User::where('id', $accepted_upgrade->user->id)->update([
                    'level_member_id' => 1
                ]);
                Log::info('Upgrade Level ' . $accepted_upgrade->levelMember->level_name . ' untuk user ' . $accepted_upgrade->user->username . ' telah expired');
            }else{
                UpgradeLevel::create([
                    'user_id' => $accepted_upgrade->user->id,
                    'level_member_id' => $accepted_upgrade->level_member_id,
                    'nominal' => $accepted_upgrade->nominal,
                    'start_date' => now(),
                    'end_date' => now()->addDays(365),
                    'status' => 'accept',
                    'note' => 'extend'
                ]);
                $accepted_upgrade->update([
                    'status' => 'expired',
                    'note' => 'Auto Extend [' . Helpers::format_rupiah($accepted_upgrade->saldo_digunakan) . ']'
                ]);
                $accepted_upgrade->user->level_member_id = $accepted_upgrade->level_member_id;
                $accepted_upgrade->user->save();

                Log::info('Upgrade Level ' . $accepted_upgrade->levelMember->level_name . ' untuk user ' . $accepted_upgrade->user->username . ' telah automatis diperpanjang dengan perolehan saldo sebesar ' . $saldo_digunakan);
            }
        }
    }
}
