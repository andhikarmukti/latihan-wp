<?php

namespace App\Console\Commands;

use App\Models\Service;
use app\Helpers\Helpers;
use App\Models\AppConfig;
use App\Models\WaBroadcast;
use App\Jobs\SendBroadcastJob;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WaBroadcastCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'WaBroadcastCron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */

    public $greeting;
    public $attention;

    public function __construct()
    {
        parent::__construct($this);

        $this->greeting = Helpers::greeting();

//         if(Carbon::now()->format('H:i:s') <= "14:00" && Carbon::now()->format('H:i:s') >= "11:00"){
//             $this->attention = "Waktu jam makan siang hampir tiba nih kak, jangan lupa makan siang ya kak 😉";
//         }elseif(Carbon::now()->format('H:i:s') <= "10:59" && Carbon::now()->format('H:i:s') >= "04:00"){
//             $this->attention = "Bagaimanapun kondisi cuaca dan suasana hati kakak saat ini, jangan lupa sarapan pagi dan berolahraga ya kak sebelum memulai rutinitasnya.
// Supaya badan kakak selalu sehat dan fresh 😊";
//         }elseif(Carbon::now()->format('H:i:s') <= "18:00" && Carbon::now()->format('H:i:s') >= "15:00"){
//             $this->attention = "Kakak pasti lelah ya setelah bekerja dan beraktivitas seharian? Istirahat yang cukup ya kak supaya kakak tetap sehat selalu 🥰";
//         }else{
//             $this->attention = "Saat ini pasti kakak sedang beristirahat ya? Atau sedang berkumpul dengan teman-teman? Mungkin juga sedang bekerja atau mau berangkat kerja? Apapun kondisi kakak saat ini, saya selalu mendoakan agar kakak diberikan kesehatan dan kelancaran dalam segala urusannya ya kak. aamiin 😇🤲";
//         }
    }

    private function message_satu(
        $followers_tiktok,
        $views_tiktok,
        $likes_tiktok,
        $followers_instagram_indo,
        $followers_instagram_bule,
        $likes_instagram_indo,
        $likes_instagram_bule,
        $followers_twiiter,
        $likes_youtube,
        // $followers_shopee,
        $views_youtube
    )
    {
        // Pengiriman pesan broadcast message_satu
        $calon_client_m_satu = WaBroadcast::orderBy('id', 'DESC')
        ->where('message_satu', null)
        ->where('is_active', true)
        ->where('regis', null)
        ->first();
        $mark_up = $calon_client_m_satu && ($calon_client_m_satu->type == 'enduser') ? 2 : 1;
        $link = $calon_client_m_satu && ($calon_client_m_satu->type == 'enduser') ?  url('') : url('');
        $keterangan_harga = $calon_client_m_satu && ($calon_client_m_satu->type == 'enduser') ? '' : 'AGENT';

        return $message = "Hallo kak " . $this->greeting . "

Gimana kabarnya hari ini? Semoga sehat selalu ya kak 😊

Oiya kak, apakah saat ini kakak sedang mencari layanan jasa penambahan followers instagram, likes instagram atau followers tiktok, likes tiktok? 😊

HARGA " . $keterangan_harga . ":

500 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 500 * $mark_up) . "
1000 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate * $mark_up) . "
1500 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 1500 * $mark_up) . "
2000 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 2000 * $mark_up) . "
2500 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 2500 * $mark_up) . "

500 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 500 * $mark_up) . "
1000 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate * $mark_up) . "
1500 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 1500 * $mark_up) . "
2000 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 2000 * $mark_up) . "
2500 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 2500 * $mark_up) . "

500 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 500 * $mark_up) . "
1000 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate * $mark_up) . "
1500 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 1500 * $mark_up) . "
2000 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 2000 * $mark_up) . "
2500 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 2500 * $mark_up) . "

500 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 500 * $mark_up) . "
1000 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate * $mark_up) . "
1500 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 1500 * $mark_up) . "
2000 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 2000 * $mark_up) . "
2500 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 2500 * $mark_up) . "

500 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 500 * $mark_up) . "
1000 likes tiktok = Rp " . ceil($likes_tiktok->rate * $mark_up) . "
1500 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 1500 * $mark_up) . "
2000 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 2000 * $mark_up) . "
2500 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 2500 * $mark_up) . "

500 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 500 * $mark_up) . "
1000 views tiktok = Rp " . ceil($views_tiktok->rate * $mark_up) . "
1500 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 1500 * $mark_up) . "
2000 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 2000 * $mark_up) . "
2500 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 2500 * $mark_up) . "

500 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 500 * $mark_up) . "
1000 followers tiktok = Rp " . ceil($followers_tiktok->rate * $mark_up) . "
1500 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 1500 * $mark_up) . "
2000 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 2000 * $mark_up) . "
2500 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 2500 * $mark_up) . "

500 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 500 * $mark_up) . "
1000 followers twitter = Rp " . ceil($followers_twiiter->rate * $mark_up) . "
1500 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 1500 * $mark_up) . "
2000 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 2000 * $mark_up) . "
2500 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 2500 * $mark_up) . "

500 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 500 * $mark_up) . "
1000 likes youtube = Rp " . ceil($likes_youtube->rate * $mark_up) . "
1500 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 1500 * $mark_up) . "
2000 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 2000 * $mark_up) . "
2500 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 2500 * $mark_up) . "

500 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 500 * $mark_up) . "
1000 views youtube = Rp " . ceil($views_youtube->rate * $mark_up) . "
1500 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 1500 * $mark_up) . "
2000 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 2000 * $mark_up) . "
2500 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 2500 * $mark_up) . "

Untuk cek service lainnya silahkan kakak cek di web panel kami yaitu " . $link . " 😊🙏

☝️ _kalau link tidak bisa di klik silahkan save contact dulu nomor ini ya kak_ 😉

Kami juga menyediakan integrasi REST API untuk kakak yang memiliki web panel smm sendiri loh! 🤑🤩

_Harga bisa berubah sewaktu-waktu_

Terima kasih kak, semoga kakak berjodoh dengan web panel " . AppConfig::find('WEB.NAME')->value . " 🥰
dan semoga kakak juga sehat selalu serta dilancarkan segala urusannya 🤗 aamiin 🤲

Salam Hangat,
" . AppConfig::find('WEB.NAME')->value . "


" . now() . "
";
    }

    private function register()
    {
        return "Hallo kak " . $this->greeting . " 😊
Gimana kabarnya hari ini kak? 🤗

Apakah sejauh ini ada kendala dalam menggunakan " . AppConfig::find('WEB.NAME')->value . "? ☺️🙏
Apakah ada yang bisa saya bantu kak terkait cara topup saldo? cara order? atau yang lainnya? 😊";
    }

    private function message_dua(
        $followers_tiktok,
        $views_tiktok,
        $likes_tiktok,
        $followers_instagram_indo,
        $followers_instagram_bule,
        $likes_instagram_indo,
        $likes_instagram_bule,
        $followers_twiiter,
        $likes_youtube,
        // $followers_shopee,
        $views_youtube
    )
    {
        return "Hallo kak " . $this->greeting . " 😊
Gimana kabarnya hari ini kak? 🤗

Apakah sejauh ini ada kendala dalam menggunakan " . AppConfig::find('WEB.NAME')->value . "? ☺️🙏
Apakah ada yang bisa saya bantu kak terkait cara registrasi? cara login? atau yang lainnya? 😊

HARGA AGENT :

500 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 500) . "
1000 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate) . "
1500 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 1500) . "
2000 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 2000) . "
2500 likes instagram bule = Rp " . ceil($likes_instagram_bule->rate / 1000 * 2500) . "

500 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 500) . "
1000 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate) . "
1500 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 1500) . "
2000 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 2000) . "
2500 likes instagram indo = Rp " . ceil($likes_instagram_indo->rate / 1000 * 2500) . "

500 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 500) . "
1000 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate) . "
1500 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 1500) . "
2000 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 2000) . "
2500 followers instagram Bule = Rp " . ceil($followers_instagram_bule->rate / 1000 * 2500) . "

500 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 500) . "
1000 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate) . "
1500 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 1500) . "
2000 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 2000) . "
2500 followers instagram Indo = Rp " . ceil($followers_instagram_indo->rate / 1000 * 2500) . "

500 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 500) . "
1000 likes tiktok = Rp " . ceil($likes_tiktok->rate) . "
1500 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 1500) . "
2000 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 2000) . "
2500 likes tiktok = Rp " . ceil($likes_tiktok->rate / 1000 * 2500) . "

500 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 500) . "
1000 views tiktok = Rp " . ceil($views_tiktok->rate) . "
1500 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 1500) . "
2000 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 2000) . "
2500 views tiktok = Rp " . ceil($views_tiktok->rate / 1000 * 2500) . "

500 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 500) . "
1000 followers tiktok = Rp " . ceil($followers_tiktok->rate) . "
1500 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 1500) . "
2000 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 2000) . "
2500 followers tiktok = Rp " . ceil($followers_tiktok->rate / 1000 * 2500) . "

500 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 500) . "
1000 followers twitter = Rp " . ceil($followers_twiiter->rate) . "
1500 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 1500) . "
2000 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 2000) . "
2500 followers twitter = Rp " . ceil($followers_twiiter->rate / 1000 * 2500) . "

500 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 500) . "
1000 likes youtube = Rp " . ceil($likes_youtube->rate) . "
1500 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 1500) . "
2000 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 2000) . "
2500 likes youtube = Rp " . ceil($likes_youtube->rate / 1000 * 2500) . "

500 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 500) . "
1000 views youtube = Rp " . ceil($views_youtube->rate) . "
1500 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 1500) . "
2000 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 2000) . "
2500 views youtube = Rp " . ceil($views_youtube->rate / 1000 * 2500) . "

Untuk cek service lainnya silahkan kakak cek di web panel kami yaitu " . url('') . "/ref/9PJpy 😊🙏

☝️ _kalau link tidak bisa di klik silahkan save contact dulu nomor ini ya kak_ 😊🙏

Kami juga menyediakan integrasi REST API untuk kakak yang memiliki web panel smm sendiri loh! 🤑🤩

_Harga bisa berubah sewaktu-waktu_

Terima kasih kak, semoga kakak berjodoh dengan web panel " . AppConfig::find('WEB.NAME')->value . " 🥰
dan semoga kakak juga sehat selalu serta dilancarkan segala urusannya 🤗 aamiin 🤲";
    }

    private function message_tiga()
    {
        return "Hallo kak " . $this->greeting . " 😊
Gimana kabarnya hari ini kak? 🤗

Apakah sejauh ini kakak sudah coba untuk mendaftar di " . AppConfig::find('WEB.NAME')->value . "? ☺️🙏
Saat ini jika kakak melakukan pendaftaran melalui link yang kami berikan, maka kakak akan mendapatkan free upgrade ke level Agent loh kak! 🤩

Yuk daftar sekarang juga kak! 😉

Untuk registrasi melalui link berikut maka akan langsung upgrade ke level Agent ya kak
" . url('') . "/ref/9PJpy 😊🙏
☝️ _kalau link tidak bisa di klik silahkan save contact dulu nomor ini ya kak_ 😊🙏

Apakah ada yang bisa saya bantu kak terkait cara registrasi? cara login? atau yang lainnya? 😊

Terima kasih kak, semoga kakak berjodoh dengan web panel " . AppConfig::find('WEB.NAME')->value . " 🥰
dan semoga kakak juga sehat selalu serta dilancarkan segala urusannya 🤗 aamiin 🤲";
    }

    public function handle()
    {
        $no_admin = AppConfig::find('CONTACT.OWNER')->value;
        $err = false;

        $followers_tiktok = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%followers tiktok%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $views_tiktok = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%views tiktok%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $likes_tiktok = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%likes tiktok%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $followers_instagram_indo = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%followers instagram%')
        ->where('service_name', 'LIKE', '%indonesia%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $followers_instagram_bule = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%followers instagram%')
        ->where('service_name', 'NOT LIKE', '%indonesia%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $likes_instagram_indo = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%likes instagram%')
        ->where('service_name', 'LIKE', '%indonesia%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $likes_instagram_bule = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%likes instagram%')
        ->where('service_name', 'NOT LIKE', '%indonesia%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $followers_twiiter = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%followers twitter%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $likes_youtube = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%likes youtube%')
        // ->where('is_active', true)
        ->get()
        ->first();

        $views_youtube = Service::orderBy('rate', 'ASC')
        ->where('service_name', 'LIKE', '%views youtube%')
        // ->where('is_active', true)
        ->get()
        ->first();

        // $followers_shopee = Service::orderBy('rate', 'ASC')
        // ->where('service_name', 'LIKE', '%followers shopee%')
        // ->where('is_active', true)
        // ->get()
        // ->first();

        // dd($likes_instagram_bule);

        if($followers_tiktok == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Followers Tiktok tidak ada yang active';
        }else if($views_tiktok == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Views Tiktok tidak ada yang active';
        }else if($likes_tiktok == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Likes Tiktok tidak ada yang active';
        }else if($followers_instagram_indo == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Followers Instagram Indo tidak ada yang active';
        }else if($followers_instagram_bule == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Followers Instagram Bule tidak ada yang active';
        }else if($likes_instagram_indo == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Likes Instagram Indo tidak ada yang active';
        }else if($likes_instagram_bule == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Likes Instagram Bule tidak ada yang active';
        }else if($followers_twiiter == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Followers Twitter tidak ada yang active';
        }else if($likes_youtube == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Likes Youtube tidak ada yang active';
        }else if($views_youtube == null){
            $err = true;
            $message_err = 'broadcast berhenti, service Views Youtube tidak ada yang active';
        }
        // else if($followers_shopee == null){
        //     $err = true;
        //     $message_err = 'broadcast berhenti, service Followers Shopee tidak ada yang active';
        // }

            // Pengiriman pesan broadcast message_satu
            $calon_client_m_satu = WaBroadcast::where('message_satu', null)
            ->where('is_active', true)
            ->where('regis', null)
            ->first();

            if($calon_client_m_satu){
                if($err == true){
                    dispatch(new SendBroadcastJob($no_admin, $message_err));
                }else{
                    // Message_satu
                    $message_satu = $this->message_satu(
                        $followers_tiktok,
                        $views_tiktok,
                        $likes_tiktok,
                        $followers_instagram_indo,
                        $followers_instagram_bule,
                        $likes_instagram_indo,
                        $likes_instagram_bule,
                        $followers_twiiter,
                        $likes_youtube,
                        // $followers_shopee,
                        $views_youtube
                    );
                    $message_dua = $this->message_dua(
                        $followers_tiktok,
                        $views_tiktok,
                        $likes_tiktok,
                        $followers_instagram_indo,
                        $followers_instagram_bule,
                        $likes_instagram_indo,
                        $likes_instagram_bule,
                        $followers_twiiter,
                        $likes_youtube,
                        // $followers_shopee,
                        $views_youtube
                    );
                    $message_tiga = $this->message_tiga();

                dispatch(new SendBroadcastJob($calon_client_m_satu->no_wa, $message_satu));
                $calon_client_m_satu->message_satu = now();
                $calon_client_m_satu->save();
                Log::info("Berhasil mengirimkan message_satu ke " . $calon_client_m_satu->no_wa);
            }

            // // Pengiriman pesan broadcast message_dua
            // $calon_client_m_dua = WaBroadcast::where('message_dua', null)
            // ->where('message_satu', '<',  now()->subDay(3))
            // ->where('is_active', true)
            // ->where('regis', null)
            // ->first();
            // if($calon_client_m_dua){
            //     dispatch(new SendBroadcastJob($calon_client_m_dua->no_wa, $message_dua));
            //     $calon_client_m_dua->message_dua = now();
            //     $calon_client_m_dua->save();
            //     Log::info("Berhasil mengirimkan message_dua ke " . $calon_client_m_dua->no_wa);
            // }

            // // Pengiriman pesan broadcast message_tiga
            // $calon_client_m_tiga = WaBroadcast::where('message_tiga', null)
            // ->where('message_dua', '<',  now()->subDay(3))
            // ->where('is_active', true)
            // ->where('regis', null)
            // ->first();
            // if($calon_client_m_tiga){
            //     dispatch(new SendBroadcastJob($calon_client_m_tiga->no_wa, $message_tiga));
            //     $calon_client_m_tiga->message_tiga = now();
            //     $calon_client_m_tiga->save();
            //     Log::info("Berhasil mengirimkan message_tiga ke " . $calon_client_m_tiga->no_wa);
            // }

        }

    }
}
