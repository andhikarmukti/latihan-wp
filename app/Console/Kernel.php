<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cancelTopup')->everyThirtyMinutes();
        $schedule->command('updateStatusHistoryOrder')->everyFiveMinutes();
        $schedule->command('updateStatusLogCommissionCron')->everyFifteenMinutes();
        $schedule->command('updateServiceInfo')->everyFifteenMinutes();
        // $schedule->command('updateStatusRefill')->everySixHours();
        $schedule->command('updateStatusUpgradeRequest')->daily();
        $schedule->command('upgradeLevelAutoExtend')->daily();
        $schedule->command('updateSpeedCron')->everyThirtyMinutes();
        $schedule->command('infoSaldoServerCron')->everyThirtyMinutes();
        $schedule->command('reminderTopupPendingCron')->twiceDaily(9, 21);
        // $schedule->command('AutoCekMutasiCron')->everyFiveMinutes();
        $schedule->command('partialRefundCron')->everyThirtyMinutes();
        $schedule->command('WaBroadcastCron')->everyThirtyMinutes();
        // $schedule->command('DiscountAutoCron')->daily();
        $schedule->command('LastUpdateServiceCron')->everyThirtyMinutes();
        $schedule->command('SpeedupAutoCron')->everyThreeHours();
        $schedule->command('autoAddServiceCron')->everyFifteenMinutes();
        $schedule->command('serviceInformationUpdateDeleteCron')->monthly();
        $schedule->command('specialPriceExpiredCron')->daily();
        $schedule->command('queue:work --max-time=295')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
