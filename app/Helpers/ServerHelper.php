<?php

namespace app\Helpers;

use App\Jobs\SendWaJob;
use App\Models\User;
use App\Models\Server;
use GuzzleHttp\Client;
use App\Models\Service;
use App\Models\TopupLog;
use App\Models\AutoPriceLog;
use App\Models\HistoryOrder;
use App\Models\ServerAction;
use App\Models\ServerParameter;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\ServiceAutoPriceDisable;
use App\Models\ServiceInformationUpdate;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ServerHelper{

    public static function checkServer($params)
    {
        $server = Server::find($params['server_id']);

        // Cek dulu servernya ada apa engga?
        if(!$server){
            return response()->json('Order gagal, Server not Found', 400);
        }

        return ServerHelper::processOrderApi($server, $params);
    }

    private static function processOrderApi($server, $params)
    {
        $service = Service::where('id', $params['service_id'])->first();

        $server_action = ServerAction::where([
            ['server_id', $server->id],
            ['action_id', $params['action_id']]
        ])->first();

        $server_parameters = ServerParameter::where([
            ['server_id', $server->id],
            ['action_id', $params['action_id']]
        ])->get();
        $form_params = [];
        foreach($server_parameters as $server_parameter){
            $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
        }
        if($service->type_id == 2){ // Comment Instagram
            $form_params['comments'] = $params['comments'];
            unset($form_params['quantity']);
        }
        if($service->type_id == 3){ // Comment Like Instagram
            $form_params['username'] = $params['username'];
        }
        // return $form_params;

        // Push orderan
        $client = New Client();
        try{
            $response = $client->request($server_action->method, $server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : ''), [
                'form_params' => $form_params
            ])->getBody();
        }catch(GuzzleException $e){
            Log::error('process API order error: ' . $e->getMessage());
            return response()->json($e->getMessage());
        }
        $response = json_decode($response);
        return $response;
    }

    public static function checkStatusOrder($history_order)
    {
        $params = [
            'server_id' => $history_order->server_id,
            'service_id' => $history_order->service_id,
            'action_id' => 2,
            'order_id_server' => $history_order->order_id_server,
        ];

        $server = Server::find($history_order->server_id);
        $server_action = ServerAction::where([
            ['server_id', $history_order->server_id],
            ['action_id', 2]
        ])->first();
        $server_parameters = ServerParameter::where([
            ['server_id', $history_order->server_id],
            ['action_id', 2]
        ])->get();
        $form_params = [];
        foreach($server_parameters as $server_parameter){
            $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
        }

        $client = new Client();
        $status_order = $client->request($history_order->server->serverActions->where('action_id', 2)->first()->method, $history_order->server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : ''), [
            'form_params' => $form_params
        ])->getBody();
        $status_order = json_decode($status_order);

        try{
            $status = property_exists($status_order, "data") ? $status_order->data->status : $status_order->status;
            $remains = property_exists($status_order, "data") ? $status_order->data->remains : ($status_order->remains ?? 0);
            $start_count = property_exists($status_order, "data") ? $status_order->data->start_count : $status_order->start_count;
            DB::beginTransaction();
            HistoryOrder::where('order_id_server', $history_order->order_id_server)->update([
                'remain' => $remains,
                'start_count' => $start_count,
                'status' => $status == 'Success' ? 'Completed' : ($status == 'Error' ? 'Canceled' : $status),
                'note' => $status == 'Canceled' ? 'Refund ' . Helpers::format_rupiah($history_order->harga) : ($status == 'Error' ? 'Refund ' . Helpers::format_rupiah($history_order->harga) : '-'),
            ]);
            Log::info('berhasil update status orderan lama dengan order id ' . $history_order->order_id_server . '|' . 'remain : ' . $remains . ' start_count : ' . $start_count . ' status : ' . $status
            );

            $user = User::find($history_order->user_id);
            if($status == 'Canceled' || $status == 'Error'){
                // Log saldo
                Helpers::logSaldo('cancel', $user->saldo, ($user->saldo + $history_order->harga), $history_order);

                Log::info('Saldo ' . $user->username . ' bertambah karna cancel sebesar ' . $history_order->harga);
                $user->saldo += $history_order->harga;
                $user->saldo_digunakan -= $history_order->harga;
                // Pengurangan saldo digunakan jika memiliki upgrade level yang accept
                if($user->upgradeLevelAccept){
                    $user->upgradeLevelAccept->update([
                        'saldo_digunakan' => $user->upgradeLevelAccept->saldo_digunakan -= $history_order->harga
                    ]);
                }
                $user->save();
            }
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            Log::error('error update status orderan lama dengan order id ' . $history_order->order_id_server);
            return $e->getMessage();
        }

        if(env('NOTIF_CANCEL') == true){
            if($status == 'Canceled' || $status == 'Error'){
                if($history_order->notif_cancel_partial == true){
                $no_wa = $user->no_wa;
                $message = '*ORDER CANCELED*

Hallo kak, ' . Helpers::greeting() . ' 😊🙏
mau menginformasikan bahwa ada orderan kakak yang statusnya _Canceled_ dengan detail berikut,

Order ID : ' . $history_order->id . '
Service : ' . $history_order->service->service_name . '
Target : ' . $history_order->target . '
Refund Saldo : ' . Helpers::format_rupiah($history_order->harga) . '

Orderan cancel bisa disebabkan beberapa hal :
1. Format target tidak sesuai
2. Target tidak ditemukan / private akun _(berdasarkan rules system : kemungkinan sangat besar akan hangus, tapi bisa jadi cancel)_
3. Server Overload
4. Adanya sekuritas dari social media terkait yang sedang update / ditingkatkan
5. Quantity order sudah mencapai batas maksimal untuk target dan service yang sama

Silahkan dicoba order kembali, atau bisa gunakan service ID yang lain.

Terima kasih 😊🙏
                ';
                dispatch(new SendWaJob($no_wa, $message));
                }
            }
        }
    }

    public static function updateServiceInfo()
    {
        $client = new Client();
        $servers = Server::where('is_active', true)->get();
        $info = '';
        foreach ($servers as $server) {
            $service = new Service();
            $services = $service->where('server_id', $server->id)->where('auto_price', true)->get();
            $server_parameters = ServerParameter::where([
                ['server_id', $server->id],
                ['action_id', 3] // get services action id nya 3
            ])->get();
            $server_action = ServerAction::where([
                ['server_id', $server->id],
                ['action_id', 3]
            ])->first();
            $form_params = [];
            foreach($server_parameters as $server_parameter){
                $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
            }
            $response = $client->request(($server->serverActions->where('action_id', 3)->first()->method), ($server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : '')), [
                'form_params' => $form_params
            ])->getBody();
            $response = json_decode($response);
            $response = property_exists((object)$response, "data") ? $response->data : $response;

            if (count($services) > 0) {
                foreach ($services as $service) {
                    try {
                        $trigger_service_active = 0; // digunakan ketika jumlahnya sama dengan count($response) berarti tidak ada server service id yang sama (status tidak aktif)
                        for ($i = 0; $i < count($response); $i++) {
                            $server_service_id = $service->server_service_id;
                            $response_service = property_exists($response[$i], "id") ? $response[$i]->id : $response[$i]->service;
                            $response_rate = property_exists($response[$i], "price") ? $response[$i]->price : $response[$i]->rate;
                            if ($response_service == $server_service_id) {
                                $trigger_service_active -= 1;
                                if ($service->hpp != $response_rate) {
                                    $auto_price_log = AutoPriceLog::create([
                                        'service_id' => $service->id,
                                        'old_hpp' => $service->hpp,
                                        'new_hpp' => $response_rate,
                                        'old_rate' => $service->rate,
                                        'new_rate' => $response_rate * $server->mark_up,
                                        'old_status_active' => $service->is_active,
                                        'new_status_active' => 1,
                                    ]);

                                    if($service->is_active == 1){
                                        if ($auto_price_log->new_rate > $auto_price_log->old_rate) {
                                            $info = 'harga NAIK';
                                            $harga_naik = 1;
                                            $harga_turun = 0;
                                        } else {
                                            $info = 'harga TURUN';
                                            $harga_naik = 0;
                                            $harga_turun = 1;
                                        }
                                    }else{
                                        if ($auto_price_log->new_rate > $auto_price_log->old_rate) {
                                            $info = 'comeback';
                                            $harga_naik = 1;
                                            $harga_turun = 0;
                                        } else {
                                            $info = 'comeback';
                                            $harga_naik = 0;
                                            $harga_turun = 1;
                                        }
                                    }

                                    $service_information_update = ServiceInformationUpdate::create([
                                        'auto_price_log_id' => $auto_price_log->id,
                                        'service_id' => $service->id,
                                        'info' => $info,
                                        'harga_naik' => $harga_naik,
                                        'harga_turun' => $harga_turun,
                                        'status_active' => 1,
                                    ]);

                                    $service_auto_price_disable = ServiceAutoPriceDisable::where('service_id', $service->id)->first();
                                    if ($service_auto_price_disable) {
                                        if($info == 'comeback'){
                                            $is_active = 1;
                                        }
                                        $service->where('server_service_id', $response[$i]->id ?? $response[$i]->service)->update([
                                            'rate' => $service_information_update->harga_turun == 1 ? $service->rate : $response_rate * $server->mark_up,
                                            'hpp' => $response_rate,
                                            'min' => $response[$i]->min,
                                            'max' => $response[$i]->max,
                                            'is_active' => $is_active ?? $service->is_active
                                        ]);
                                        if($service_information_update->harga_turun == 1){
                                            Log::info("Service harga turun, rate tidak diubah untuk service id " . $server_service_id->first()->id);
                                        }else{
                                            Log::info("Service harga naik, rate disesuaikan untuk service id " . $server_service_id->first()->id);
                                        }
                                    } else {
                                        if($info == 'comeback'){
                                            $is_active = 1;
                                        }
                                        $service->where('server_service_id', $response[$i]->id ?? $response[$i]->service)->update([
                                            'rate' => $response_rate * $server->mark_up,
                                            'hpp' => $response_rate,
                                            'min' => $response[$i]->min,
                                            'max' => $response[$i]->max,
                                            'is_active' => $is_active ?? $service->is_active
                                        ]);
                                        if($info == 'comeback'){
                                            Log::info("Layanan aktif kembali untuk Service ID " . $service->id);
                                        }else if($service_information_update->harga_turun == 1){
                                            Log::info("Service harga turun, rate disesuaikan untuk service id " . $service->id);
                                        }else{
                                            Log::info("Service harga naik, rate disesuaikan untuk service id " . $service->id);
                                        }
                                    }
                                }else if($service->is_active == 0){
                                    $response_status_is_active = property_exists($response[$i], "status") ? $response[$i]->status : 1; // server id 4 IRVNKD
                                    $service->update([
                                        'is_active' => $response_status_is_active
                                    ]);

                                    if($response_status_is_active == 0 && $service->is_active == 0 && $server->id == 4){
                                        Log::info("Service ID " . $service->id . " still not active");
                                    }else{
                                        $auto_price_log = AutoPriceLog::create([
                                            'service_id' => $service->id,
                                            'old_hpp' => $service->hpp,
                                            'new_hpp' => $response_rate,
                                            'old_rate' => $service->rate,
                                            'new_rate' => $response_rate * $server->mark_up,
                                            'old_status_active' => $service->is_active,
                                            'new_status_active' => 1,
                                        ]);

                                        $service_information_update = ServiceInformationUpdate::create([
                                            'auto_price_log_id' => $auto_price_log->id,
                                            'service_id' => $service->id,
                                            'info' => 'comeback',
                                            'harga_naik' => 0,
                                            'harga_turun' => 0,
                                            'status_active' => 1,
                                        ]);

                                        $service_auto_price_disable = ServiceAutoPriceDisable::where('service_id', $service->id)->first();
                                        if ($service_auto_price_disable) {
                                            $service->where('server_service_id', $response[$i]->id ?? $response[$i]->service)->update([
                                                // 'unit' => $response[$i]->unit,
                                                'rate' => $service_information_update->harga_turun == 1 ? $service->rate : $response_rate * $server->mark_up,
                                                'hpp' => $response_rate,
                                                'min' => $response[$i]->min,
                                                'max' => $response[$i]->max,
                                                // 'speed' => $response[$i]->speed,
                                            ]);
                                            if($service_information_update->harga_turun == 1){
                                                Log::info("Service harga turun, rate tidak diubah untuk service id " . $server_service_id->first()->id);
                                            }else{
                                                Log::info("Service harga naik, rate disesuaikan untuk service id " . $server_service_id->first()->id);
                                            }
                                        } else {
                                            $service->where('server_service_id', $response_service)->update([
                                                // 'unit' => $response[$i]->unit,
                                                'rate' => $response_rate * $server->mark_up,
                                                'hpp' => $response_rate,
                                                'min' => $response[$i]->min,
                                                'max' => $response[$i]->max,
                                                // 'speed' => $response[$i]->speed,
                                            ]);
                                            if($service_information_update->info == 'comeback'){
                                                Log::info("Layanan aktif kembali untuk Service ID " . $service->id);
                                            }else if($service_information_update->harga_turun == 1){
                                                Log::info("Service harga turun, rate disesuaikan untuk service id " . $service->id);
                                            }else{
                                                Log::info("Service harga naik, rate disesuaikan untuk service id " . $service->id);
                                            }
                                        }
                                    }
                                }else if($server->id == 4 && $service->is_active == 1){ // kondisi server id 4 disable layanannya
                                    $response_status_is_active = $response[$i]->status; // server id 4 IRVNKD
                                    $service->update([
                                        'is_active' => $response_status_is_active
                                    ]);

                                    if($response_status_is_active == 0){
                                        $auto_price_log = AutoPriceLog::create([
                                            'service_id' => $service->id,
                                            'old_hpp' => $service->hpp,
                                            'new_hpp' => $response_rate,
                                            'old_rate' => $service->rate,
                                            'new_rate' => $response_rate * $server->mark_up,
                                            'old_status_active' => $service->is_active,
                                            'new_status_active' => 0,
                                        ]);

                                        $service_information_update = ServiceInformationUpdate::create([
                                            'auto_price_log_id' => $auto_price_log->id,
                                            'service_id' => $service->id,
                                            'info' => 'disabled',
                                            'harga_naik' => 0,
                                            'harga_turun' => 0,
                                            'status_active' => 0,
                                        ]);

                                        $service_auto_price_disable = ServiceAutoPriceDisable::where('service_id', $service->id)->first();
                                        if ($service_auto_price_disable) {
                                            $service->where('server_service_id', $response_service)->update([
                                                // 'unit' => $response[$i]->unit,
                                                'rate' => $service_information_update->harga_turun == 1 ? $service->rate : $response_rate * $server->mark_up,
                                                'hpp' => $response_rate,
                                                'min' => $response[$i]->min,
                                                'max' => $response[$i]->max,
                                                // 'speed' => $response[$i]->speed,
                                            ]);
                                            if($service_information_update->harga_turun == 1){
                                                Log::info("Service harga turun, rate tidak diubah untuk service id " . $server_service_id->first()->id);
                                            }else{
                                                Log::info("Service harga naik, rate disesuaikan untuk service id " . $server_service_id->first()->id);
                                            }
                                        } else {
                                            Log::info("Service ID " . $service->id . " is not active, disabled from server");
                                        }
                                    }
                                }
                                // Log::info("tidak ada update data rate dan status active dari service ID " . $service->id);
                            } else{
                                $trigger_service_active += 1;
                                if($trigger_service_active == count($response)){
                                    if($service->is_active == 1){
                                        $auto_price_log = AutoPriceLog::create([
                                            'service_id' => $service->id,
                                            'old_hpp' => $service->hpp,
                                            'new_hpp' => $service->hpp,
                                            'old_rate' => $service->rate,
                                            'new_rate' => $service->rate,
                                            'old_status_active' => $service->is_active,
                                            'new_status_active' => 0,
                                        ]);
                                        ServiceInformationUpdate::create([
                                            'auto_price_log_id' => $auto_price_log->id,
                                            'service_id' => $service->id,
                                            'info' => 'disabled',
                                            'harga_naik' => 0,
                                            'harga_turun' => 0,
                                            'status_active' => 0,
                                        ]);
                                        $service->update([
                                            'is_active' => 0
                                        ]);
                                        Log::info("Service ID " . $service->id . " is not active, disabled from server");
                                    }else{
                                        // Log::info("tidak ada update data rate dan status active dari service ID " . $service->id);
                                    }
                                }
                            }
                        }
                    }
                    catch (ClientException $e) {
                        $response = $e->getResponse()->getStatusCode();
                        // if($response == 404 && $server->id == 1){
                            if($service->is_active != 0){
                                $auto_price_log = AutoPriceLog::create([
                                    'service_id' => $service->id,
                                    'old_hpp' => $service->hpp,
                                    'new_hpp' => $service->hpp,
                                    'old_rate' => $service->rate,
                                    'new_rate' => $service->rate,
                                    'old_status_active' => $service->is_active,
                                    'new_status_active' => 0,
                                ]);
                                ServiceInformationUpdate::create([
                                    'auto_price_log_id' => $auto_price_log->id,
                                    'service_id' => $service->id,
                                    'info' => 'disabled',
                                    'harga_naik' => 0,
                                    'harga_turun' => 0,
                                    'status_active' => 0,
                                ]);
                                $service->update([
                                    'is_active' => 0
                                ]);
                                Log::info("Service ID " . $service->id . " is not active, disabled from server");
                            }
                            Log::info("Service ID " . $service->id . " still not active");
                        // }
                    }
                }
            }
        }
    }
}
