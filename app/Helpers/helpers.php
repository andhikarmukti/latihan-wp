<?php

namespace app\Helpers;

use App\Models\User;
use App\Models\LogSaldo;
use App\Models\AppConfig;
use App\Models\UpgradeLevel;
use Illuminate\Http\Request;
use App\Models\LogCommission;
use Illuminate\Support\Carbon;
use App\Models\UpgradeLevelPromo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Helpers
{
    public static function format_rupiah($nominal)
    {
        return "Rp " . number_format($nominal, 0, ',', '.');
    }

    public static function format_rupiah_per_unit($nominal)
    {
        return "Rp " . number_format($nominal, 1, ',', '.');
    }

    public static function formatWaktu($waktu)
    {
        if (($waktu > 0) and ($waktu < 60)) {
            $lama = number_format($waktu, 0, ',', '.') . " Detik";
            return $lama;
        }
        if (($waktu > 60) and ($waktu < 3600)) {
            $detik = fmod($waktu, 60);
            $menit = $waktu - $detik;
            $menit = $menit / 60;
            $lama = $menit . " Menit";
            return $lama;
        } elseif (($waktu > 3600) && ($waktu < 86400)) {
            $detik = fmod($waktu, 60);
            $tempmenit = ($waktu - $detik) / 60;
            $menit = fmod($tempmenit, 60);
            $jam = ($tempmenit - $menit) / 60;
            $lama = $jam . " Jam";
            return $lama;
        } elseif ($waktu > 86400 && $waktu < 604800) {
            $hari = ceil($waktu / 86400);
            $lama = $hari . " Hari";
            return $lama;
        } elseif ($waktu > 604800 && $waktu < 2419200) {
            $minggu = ceil($waktu / 604800);
            $lama = $minggu . " Minggu";
            return $lama;
        } elseif ($waktu > 2419200 && $waktu < 125798400) {
            $bulan = ceil($waktu / 2419200);
            $lama = $bulan . " Bulan";
            return $lama;
        } elseif ($waktu > 125798400) {
            $tahun = ceil($waktu / 125798400);
            $lama = $tahun . " Tahun";
            return $lama;
        }
    }

    public static function speedColor($waktu)
    {
        if (($waktu > 0) and ($waktu < 60)) {
            $color = "text-green-500"; // Detik
            return $color;
        }
        if (($waktu > 60) and ($waktu < 3600)) {
            $color = "text-green-500"; // Menit
            return $color;
        } elseif (($waktu > 3600) && ($waktu < 86400)) {
            $color = "text-blue-500"; // Jam
            return $color;
        } elseif ($waktu > 86400 && $waktu < 604800) {
            $color = "text-yellow-500"; // Hari
            return $color;
        } elseif ($waktu > 604800 && $waktu < 2419200) {
            $color = "text-red-500"; // Minggu
            return $color;
        } elseif ($waktu > 2419200 && $waktu < 125798400) {
            $color = "text-red-500"; // Bulan
            return $color;
        } elseif ($waktu > 125798400) {
            $color = "text-red-500"; // Tahun
            return $color;
        }
    }

    public static function logSaldo($note, $saldo_awal, $saldo_akhir, $history_order = null, $topup = null)
    {
        switch ($note) {
            case "order":
                $event = '[ORDER] - ' . 'Order ID ' . $history_order->id;
                break;
            case "topup":
                $event = '[TOPUP] - ' . 'Topup ID ' . $topup->id;
                break;
            case "cancel":
                $event = '[ORDER CANCEL] - ' . 'Order ID ' . $history_order->id;
                break;
            case "partial":
                $event = '[ORDER PARTIAL] - ' . 'Order ID ' . $history_order->id;
                break;
            case "komisi":
                $event = '[COMMISSION] - ' . 'Request ID ' . $history_order->id; // masuk ke dalam param history juga untuk komisi
                break;
            case "notif_cancel_partial":
                $event = '[ADDON NOTIF CANCEL/PARTIAL] - ' . 'Order ID ' . $history_order->id;
                break;
            default:
                $event = '[ - ]';
        }

        try {
            $log_saldo  = LogSaldo::create([
                'user_id' => $history_order ? $history_order->user->id : $topup->user_id,
                'note' => $event,
                'saldo_awal' => $saldo_awal,
                'saldo_akhir' => $saldo_akhir,
                'selisih' => $saldo_akhir - $saldo_awal,
            ]);
        } catch (\Exception $e) {
            return 'Log saldo gagal!';
        }

        return 'Berhasil menambahkan log saldo untuk user ' . ($history_order ? $history_order->user->username : $topup->user->username);
    }

    public static function percentCommission($user)
    {
        $level_member = $user->levelMember->level;
        if($level_member == 3){
            $persentase_komisi = AppConfig::find('BENEFIT.REFERRAL.AGENT')->value;

        }else if($level_member == 2){
            $persentase_komisi = AppConfig::find('BENEFIT.REFERRAL.RESELLER')->value;

        }else{
            $persentase_komisi = AppConfig::find('BENEFIT.REFERRAL.END_USER')->value;
        }

        return $persentase_komisi;
    }

    public static function upgradeLevelMember($request)
    {
        try{
            DB::beginTransaction();
            $upgrade_request = UpgradeLevel::where([
                ['id', $request['upgrade_level_id']],
                ['user_id', $request['user_id']],
                ['status', 'pending'],
            ])->first();

            // Ubah status level upgrade sebelumnya
            $upgrade_existing_accept = UpgradeLevel::where([
                ['user_id', $request['user_id']],
                ['status', 'accept'],
            ])
            ->orderBy('id', 'desc')
            ->first();
            if($upgrade_existing_accept){
                $upgrade_existing_accept->update([
                    'status' => 'expired',
                    'note' => 'upgrade to Agent'
                ]);
            }

            $upgrade_request->update([
                'status' => $request['action'],
                'start_date' => now(),
                'end_date' => now()->addDays(365)
            ]);

            User::find($request['user_id'])->update([
                'level_member_id' => $upgrade_request->level_member_id
            ]);

            // Cek apakah ada promo upgrade level saat registrasi atau tidak
            $promo_upgrade = UpgradeLevelPromo::orderBy('id', 'desc')
            ->where('start_date', '<=', date(now()))
            ->where('end_date', '>=', date(now()))
            ->first();

            // Update Log Commission
            if(!array_key_exists('event', $request)){ // Dilakukan ketika event promo ga ada
                $user_upline_id = User::find($upgrade_request->user_id)->user_upline_id;
                if($user_upline_id != null){
                    LogCommission::create([
                        'user_upline_id' => $user_upline_id,
                        'user_downline_id' => $upgrade_request->user_id,
                        'harga_order' => $upgrade_request->nominal,
                        'status_order' => 'Completed',
                        'commission' => $upgrade_request->nominal * (20 / 100),
                        'note' => 'Upgrade Level ' . $upgrade_request->levelMember->level_name . ' [20%]'
                    ]);
                }
            }

            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public static function greeting()
    {
        $greeting = '';
        if(Carbon::now()->format('H:i:s') <= "15:00" && Carbon::now()->format('H:i:s') >= "11:00"){
            $greeting = "Selamat siang";
        }elseif(Carbon::now()->format('H:i:s') <= "10:59" && Carbon::now()->format('H:i:s') >= "04:00"){
            $greeting = "Selamat pagi";
        }elseif(Carbon::now()->format('H:i:s') <= "18:00" && Carbon::now()->format('H:i:s') >= "15:00"){
            $greeting = "Selamat sore";
        }else{
            $greeting = "Selamat malam";
        }

        return $greeting;
    }
}
