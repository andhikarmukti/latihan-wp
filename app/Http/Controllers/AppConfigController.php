<?php

namespace App\Http\Controllers;

use App\Models\AppConfig;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreAppConfigRequest;
use App\Http\Requests\UpdateAppConfigRequest;
use App\Models\LevelMember;
use App\Models\SliderImage;

class AppConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $app_config;

    public function __construct()
    {
        $this->app_config = new AppConfig();
    }

    public function index()
    {

        $title = 'Configuration';
        $level_member = new LevelMember();

        $logo = $this->app_config->find('LOGO')->value;
        $nama_web = $this->app_config->find('WEB.NAME')->value;
        $no_wa = $this->app_config->find('CONTACT.WA');
        $email = $this->app_config->find('CONTACT.EMAIL');
        $device_wa = $this->app_config->find('WHATSAPP.DEVICE.ID')->value;
        $device_wa_broadcast = $this->app_config->find('WHATSAPP.BROADCAST.DEVICE.ID')->value;
        $min_reminder_saldo = $this->app_config->find('MINIMUM.REMINDER.SALDO')->value;
        $contact_owner = $this->app_config->find('CONTACT.OWNER')->value;
        $komisi_enduser = $this->app_config->find('BENEFIT.REFERRAL.END_USER')->value;
        $komisi_reseller = $this->app_config->find('BENEFIT.REFERRAL.RESELLER')->value;
        $komisi_agent = $this->app_config->find('BENEFIT.REFERRAL.AGENT')->value;
        $minimum_penarikan_komisi = $this->app_config->find('MINIMUM.PENARIKAN.KOMISI')->value;
        $bonus_topup_bca = $this->app_config->find('BONUS_TOPUP.BCA')->value;
        $slider_active = AppConfig::find('SLIDER.IMAGE.ACTIVE')->value;

        return view('appConfig.index', compact(
            'title',
            'logo',
            'nama_web',
            'no_wa',
            'email',
            'level_member',
            'device_wa',
            'device_wa_broadcast',
            'min_reminder_saldo',
            'contact_owner',
            'komisi_enduser',
            'komisi_reseller',
            'komisi_agent',
            'minimum_penarikan_komisi',
            'bonus_topup_bca',
            'slider_active'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAppConfigRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function websiteInfoStore(StoreAppConfigRequest $request)
    {
        $rules = [
            'logo' => 'mimes:png|max:1000'
        ];
        $messages = [
            'logo.mimes' => 'Format tidak diizinkan',
            'logo.max' => 'Ukuran file melebihi kapasitas yang ditentukan (1 Mb)',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            Alert::error('Gagal', $validator->errors()->first());
            return back();
        }

        $slider_image = new SliderImage();

        if($request->file('logo')){
            $image_path = $request->file('logo')->storeAs('images/logo', 'logowebsite.png');
            $logo = $this->app_config->find('LOGO');
            $logo->value = 'storage/' . $image_path;
            $logo->save();
        }

        if($request->file('slider_satu')){
            // dd($request->file('slider_satu')->extension());
            $rules = [
                'slider_satu' => 'mimes:png,jpg,jpeg|max:2000'
            ];
            $messages = [
                'slider_satu.mimes' => 'Format tidak diizinkan',
                'slider_satu.max' => 'Ukuran file melebihi kapasitas yang ditentukan (2 Mb)',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                Alert::error('Gagal', $validator->errors()->first());
                return back();
            }
           $image_name = $request->file('slider_satu')->storeAs('images/slider', 'slider1.png');

           // Hapus slider1.png
           $slider_image->where('image_name', $image_name)->delete();

           SliderImage::create([
                'image_name' => $image_name
           ]);
        }

        if($request->file('slider_dua')){
            $rules = [
                'slider_dua' => 'mimes:png,jpg,jpeg|max:2000'
            ];
            $messages = [
                'slider_dua.mimes' => 'Format tidak diizinkan',
                'slider_dua.max' => 'Ukuran file melebihi kapasitas yang ditentukan (2 Mb)',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                Alert::error('Gagal', $validator->errors()->first());
                return back();
            }
           $image_name = $request->file('slider_dua')->storeAs('images/slider', 'slider2.png');

            // Hapus slider2.png
            $slider_image->where('image_name', $image_name)->delete();

            $slider_image->create([
                'image_name' => $image_name
            ]);
        }

        if($request->file('slider_tiga')){
            $rules = [
                'slider_tiga' => 'mimes:png,jpg,jpeg|max:2000'
            ];
            $messages = [
                'slider_tiga.mimes' => 'Format tidak diizinkan',
                'slider_tiga.max' => 'Ukuran file melebihi kapasitas yang ditentukan (2 Mb)',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                Alert::error('Gagal', $validator->errors()->first());
                return back();
            }
           $image_name = $request->file('slider_tiga')->storeAs('images/slider', 'slider3.png');

           // Hapus slider3.png
           $slider_image->where('image_name', $image_name)->delete();

           SliderImage::create([
                'image_name' => $image_name
           ]);
        }

        if($request->web_name){
            $web_name = $this->app_config->find('WEB.NAME');
            $web_name->value = $request->web_name;
            $web_name->save();
        }

        Alert::success('Berhasil', 'Berhasil update Website Info');
        return back();
    }

    public function contactInfoStore(StoreAppConfigRequest $request)
    {
        $rules = [
            'email' => 'email'
        ];
        $messages = [
            'email' => 'Format email tidak diizinkan!',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            Alert::error('Gagal', $validator->errors()->first());
            return back();
        }

        if($request->no_wa){
            $no_wa = str_replace("+", "", $request->no_wa);
            $no_wa_alt = Str::startsWith($no_wa, '0') ? '62' . substr($no_wa, 1) : '62' . substr($no_wa, 2);

            $no_wa = $this->app_config->find('CONTACT.WA');
            $no_wa->value = $request->no_wa;
            $no_wa->info = 'https://api.whatsapp.com/send/?phone=' . $no_wa_alt;
            $no_wa->save();
        }

        $no_wa = $this->app_config->find('CONTACT.EMAIL');
        $no_wa->value = $request->email;
        $no_wa->info = 'mailto:' . $request->email;
        $no_wa->save();

        Alert::success('Berhasil', 'Berhasil update Contact Info');
        return back();
    }

    public function levelInfoStore(StoreAppConfigRequest $request)
    {
        $level_member = LevelMember::where('id', $request->level_member_id)->first();

        if($request->mark_up){
            $mark_up = ($request->mark_up / 100) + 1;
            $level_member->update([
                'mark_up' => $mark_up,
            ]);
        }

        if($request->perpanjang_auto){
            $level_member->update([
                'perpanjang_auto' => $request->perpanjang_auto,
            ]);
        }

        if($request->harga_upgrade){
            $level_member->update([
                'harga_upgrade' => $request->harga_upgrade,
            ]);
        }

        if($request->komisi_referral_enduser){
            $this->app_config->find('BENEFIT.REFERRAL.END_USER')->update([
                'value' => $request->komisi_referral_enduser
            ]);
        }else if($request->komisi_referral_reseller){
            $this->app_config->find('BENEFIT.REFERRAL.RESELLER')->update([
                'value' => $request->komisi_referral_reseller
            ]);
        }else if($request->komisi_referral_agent){
            $this->app_config->find('BENEFIT.REFERRAL.AGENT')->update([
                'value' => $request->komisi_referral_agent
            ]);
        }

        if($request->min_penarikan_komisi){
            $this->app_config->find('MINIMUM.PENARIKAN.KOMISI')->update([
                'value' => $request->min_penarikan_komisi
            ]);
            $message = 'Berhasil update Minimum Penarikan Komisi';
        }

        if($request->bonus_topup_bca){
            $this->app_config->find('BONUS_TOPUP.BCA')->update([
                'value' => $request->bonus_topup_bca
            ]);
            $message = 'Berhasil update Bonus Topup BCA';
        }

        if($request->file('qr_code_bca')){
            $rules = [
                'qr_code_bca' => 'mimes:png|max:1000'
            ];
            $messages = [
                'qr_code_bca.mimes' => 'Format tidak diizinkan',
                'qr_code_bca.max' => 'Ukuran file melebihi kapasitas yang ditentukan (1 Mb)',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()){
                Alert::error('Gagal', $validator->errors()->first());
                return back();
            }

            $request->file('qr_code_bca')->storeAs('images/qrcode', 'qrcode_1.png');
            $message = 'Berhasil update QR Code BCA';
        }

        Alert::success('Berhasil', $message ?? 'Berhasil update Level Member Info');
        return back();
    }

    public function deviceWaStore(StoreAppConfigRequest $request)
    {
        if($request->bc_device_id){
            $this->app_config->find('WHATSAPP.BROADCAST.DEVICE.ID')->update([
                'value' => $request->bc_device_id
            ]);
        }else if($request->device_id){
            $this->app_config->find('WHATSAPP.DEVICE.ID')->update([
                'value' => $request->device_id
            ]);
        }else if($request->min_reminder_saldo){
            $this->app_config->find('MINIMUM.REMINDER.SALDO')->update([
                'value' => $request->min_reminder_saldo
            ]);
        }else if($request->contact_owner){
            $this->app_config->find('CONTACT.OWNER')->update([
                'value' => $request->contact_owner
            ]);
        }else{
            Alert::error('Gagal', 'Tidak ada perubahan yang dilakukan');
            return back();
        }
        Alert::success('Berhasil', 'Berhasil update WhatsApp Notif Info');
        return back();
    }

    public function sliderActive(StoreAppConfigRequest $request)
    {
        $this->app_config->find('SLIDER.IMAGE.ACTIVE')->update([
            'value' => $request->slider_active == 'on' ? true : false
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Berhasil update slider image active'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function show(AppConfig $appConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(AppConfig $appConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAppConfigRequest  $request
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAppConfigRequest $request, AppConfig $appConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AppConfig  $appConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppConfig $appConfig)
    {
        //
    }
}
