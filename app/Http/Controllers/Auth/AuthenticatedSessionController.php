<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Service;
use App\Models\AppConfig;
use App\Models\LevelMember;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ServiceCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Auth\LoginRequest;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $title = 'Login';
        $services = Service::orderBy('service_name', 'ASC')->where('is_active', 1)->get();
        $logo = AppConfig::find('LOGO')->value;
        $level_member = new LevelMember();
        $markup_enduser = $level_member->find(1)->mark_up;
        $markup_reseller = $level_member->find(2)->mark_up;
        $markup_agent = $level_member->find(3)->mark_up;

        return view('auth.login', compact(
            'title',
            'services',
            'logo',
            'markup_enduser',
            'markup_reseller',
            'markup_agent'
        ));
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'g-recaptcha-response' => 'recaptcha'
        ],
        [
            'required.username' => 'tidak boleh kosong!',
            'g-recaptcha-response' => 'google recaptcha required!',
        ]);

        $request->authenticate();

        User::where('username', $request->username)->update([
            'last_login_at' => now(),
            'last_login_ip' => $request->getClientIp(),
        ]);

        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
