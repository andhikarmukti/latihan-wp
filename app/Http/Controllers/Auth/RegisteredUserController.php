<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Jobs\SendWaJob;
use App\Models\Service;
use app\Helpers\Helpers;
use App\Jobs\SendOtpJob;
use App\Models\AppConfig;
use App\Models\LevelMember;
use App\Models\RegisterOtp;
use App\Models\WaBroadcast;
use Illuminate\Support\Str;
use App\Models\ListReferral;
use App\Models\UpgradeLevel;
use App\Models\UserReferral;
use Illuminate\Http\Request;
use App\Models\ServiceCategory;
use Illuminate\Validation\Rules;
use App\Models\UpgradeLevelPromo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Cookie;
use App\Providers\RouteServiceProvider;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public $promo_upgrade;

    public function __construct()
    {
        $this->promo_upgrade = UpgradeLevelPromo::orderBy('id', 'desc')
        ->where('start_date', '<=', date(now()))
        ->where('end_date', '>=', date(now()))
        ->first();
    }

    public function create()
    {
        $title = 'Register';
        $services = Service::orderBy('service_name', 'ASC')->where('is_active', 1)->get();
        $promo_upgrade = $this->promo_upgrade;
        $referral_code = Cookie::get('reff');
        $user_upline = UserReferral::where('referral_code', $referral_code)->first();
        $level_member = new LevelMember();
        $markup_enduser = $level_member->find(1)->mark_up;
        $markup_reseller = $level_member->find(2)->mark_up;
        $markup_agent = $level_member->find(3)->mark_up;
        $logo = AppConfig::find('LOGO')->value;

        return view('auth.register', compact(
            'title',
            'services',
            'promo_upgrade',
            'user_upline',
            'logo',
            'markup_enduser',
            'markup_reseller',
            'markup_agent'
        ));
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $web_name = AppConfig::find('WEB.NAME')->value;
        $no_wa = str_replace("+", "", $request->no_wa);
        $no_wa_alt = Str::startsWith($no_wa, '0') ? '62' . substr($no_wa, 1) : '0' . substr($no_wa, 2);
        $rules = [
            'username' => 'required|alpha_num|max:255|min:4|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'no_wa' => 'required|string|min:8|unique:users,no_wa',
            'no_wa_alt' => 'unique:users,no_wa',
            'otp' => 'required',
            'g-recaptcha-response' => 'recaptcha'
        ];
        $message = [
            'required' => 'tidak boleh kosong!',
            'alpha_num' => 'hanya bisa menggunakan huruf dan angka, tidak bisa menggunakan spasi',
            'g-recaptcha-response' => 'google recaptcha wajib dicentang!',
            'email' => 'narap gunakan email yang valid',
            'username.min' => 'minimal username adalah 4 karakter',
            'password.min' => 'minimal password adalah 6 karakter',
            'password.confirmed' => 'password tidak sesuai',
        ];
        $validator = Validator::make(array_merge($request->all(), ['no_wa_alt' => $no_wa_alt]), $rules, $message);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }

        //Cek OTP - START
        $checkOtp = RegisterOtp::where([
            'no_wa' => $no_wa,
            'otp' => $request->otp
        ])->first();

        // Pengecekan ketika otp tidak sesuai
        if(!$checkOtp){
            Alert::error('OTP Gagal!', 'Kode OTP tidak sesuai');
            return back()->withInput();
        }else{
            // Cek expired otp selama 10 menit batas waktu
            $expiredTime = date("Y-m-d H:i:s",strtotime($checkOtp->created_at) + 600);
            if(now() > $expiredTime){
                Alert::error('OTP Gagal!', 'Kode OTP expired, silahkan send ulang');
                return back()->withInput();
            }
        }
        //Cek OTP - END

        $referral_code = Cookie::get('reff');
        $user_upline_id = UserReferral::where('referral_code', $referral_code)->first();
        $user = User::create([
            'username' => Str::lower($request->username),
            'email' => $request->email,
            'no_wa' => $no_wa,
            'password' => Hash::make($request->password),
            'user_upline_id' => $user_upline_id ? $user_upline_id->user_id : null
        ]);

        // Update List Referral
        if($user_upline_id){
            ListReferral::create([
                'user_upline_id' => $user_upline_id->user_id,
                'user_downline_id' => $user->id,
            ]);
            $no_upline = User::find($user_upline_id->user_id)->no_wa;
            $upline_info = 'Selamat! Kamu berhasil mendapatkan downline baru 🥳
username : ' . $user->username . '

Mohon untuk dibimbing dengan baik ya kak, supaya orderannya semakin lancar dan komisi kakak semakin besar! 😊🥳';
            dispatch(new SendWaJob($no_upline, $upline_info));
        }

        // Info ke admin ada user baru
        $message = 'Ada pendaftar baru dengan username ' . $user->username . ' dengan nomor wa ' . $user->no_wa;
        $no_admin = AppConfig::find('CONTACT.OWNER')->value;
        dispatch(new SendWaJob($no_admin, $message));

        // Ucapan terima kasih untuk user yang baru registrasi
        $message_to_user = 'Hallo kak ' . $user->username . '
Selamat bergabung di ' . $web_name . ' 🥳
Selamat berbelanja dan semoga harimu menyenangkan 🥰

Kalau ada sesuatu yang bingung terkait cara topup saldo, konfirmasi topup, cara order, cara upgrade level dll bisa langsung konsultasikan ke nomor CS kami yang ini ya kak.
Terima kasih 😊🙏';
        $no_user = $user->no_wa;
        dispatch(new SendWaJob($no_user, $message_to_user));

        event(new Registered($user));

        User::where('username', $user->username)->update([
            'last_login_at' => now(),
            'last_login_ip' => $request->getClientIp(),
        ]);

        // Cek apakah ada promo upgrade gratis
        if($user_upline_id && $user_upline_id->user_id == 1){
            $level_member = LevelMember::where('level', 3)->first();

            $upgrade_level = UpgradeLevel::create([
                'user_id' => $user->id,
                'level_member_id' => $level_member->level,
                'nominal' => $level_member->harga_upgrade,
            ]);

            $no_wa = AppConfig::find('CONTACT.OWNER')->value;
            $message = '*REQUEST UPGRADE LEVEL AGENT (AUTOMATIC FROM REFERRAL LINK ADMIN)*
username : ' . $upgrade_level->user->username .'
level : ' . $upgrade_level->levelMember->level_name .'
nomor WA : ' . $upgrade_level->user->no_wa;

            dispatch(new SendWaJob($no_wa, $message));
            Helpers::upgradeLevelMember([
                'user_id' => $user->id,
                'upgrade_level_id' => $upgrade_level->id,
                'action' => 'accept',
                'event' => 'promo'
            ]);
        }else if($this->promo_upgrade){
            $level_member = LevelMember::where('level', $this->promo_upgrade->level_member_id)->first();

            $upgrade_level = UpgradeLevel::create([
                'user_id' => $user->id,
                'level_member_id' => $level_member->level,
                'nominal' => $level_member->harga_upgrade,
            ]);

            $no_wa = AppConfig::find('CONTACT.OWNER')->value;
            $message = '*REQUEST UPGRADE LEVEL from REGISTRASI PROMO*
username : ' . $upgrade_level->user->username .'
level : ' . $upgrade_level->levelMember->level_name .'
nomor WA : ' . $upgrade_level->user->no_wa;

            dispatch(new SendWaJob($no_wa, $message));
            Helpers::upgradeLevelMember([
                'user_id' => $user->id,
                'upgrade_level_id' => $upgrade_level->id,
                'action' => 'accept',
                'event' => 'promo'
            ]);
        }

        // Update field regis di db wa_broadcasts
        $user_broadcast = WaBroadcast::where('no_wa', $user->no_wa)
        ->orWhere('no_wa', $no_wa_alt)
        ->first();
        if($user_broadcast){
            $user_broadcast->regis = now();
            $user_broadcast->save();
        }

        // First Pembuatan Akun lnagung jadi Admin
        $user_check = User::all()->count();
        if($user_check == 1){
            $user->role = 'admin';
            $user->save();

            // AppConfig::find('CONTACT.WA')->update([
            //     'value' => '08123456789'
            // ]);
            // AppConfig::find('CONTACT.OWNER')->update([
            //     'value' => '08123456789'
            // ]);
            // AppConfig::find('CONTACT.EMAIL')->update([
            //     'value' => 'webpanelreseller@gmail.com',
            //     'info' => 'mailto:webpanelreseller@gmail.com'
            // ]);
        }

        Auth::login($user);

        Alert::success('Berhasil!', 'Registrasi berhasil!');
        return redirect(RouteServiceProvider::HOME);
    }
}
