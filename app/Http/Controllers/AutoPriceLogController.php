<?php

namespace App\Http\Controllers;

use App\Models\AutoPriceLog;
use App\Http\Requests\StoreAutoPriceLogRequest;
use App\Http\Requests\UpdateAutoPriceLogRequest;

class AutoPriceLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAutoPriceLogRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAutoPriceLogRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AutoPriceLog  $autoPriceLog
     * @return \Illuminate\Http\Response
     */
    public function show(AutoPriceLog $autoPriceLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AutoPriceLog  $autoPriceLog
     * @return \Illuminate\Http\Response
     */
    public function edit(AutoPriceLog $autoPriceLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAutoPriceLogRequest  $request
     * @param  \App\Models\AutoPriceLog  $autoPriceLog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAutoPriceLogRequest $request, AutoPriceLog $autoPriceLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AutoPriceLog  $autoPriceLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(AutoPriceLog $autoPriceLog)
    {
        //
    }
}
