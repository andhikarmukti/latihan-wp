<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Topup;
use App\Models\TopupTransferType;
use App\Http\Requests\StoreBankRequest;
use App\Http\Requests\UpdateBankRequest;
use RealRashid\SweetAlert\Facades\Alert;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Bank';
        $banks = Bank::all();
        $topup_transfer_types = TopupTransferType::all();
        $topup = new Topup();

        return view('bank.index', compact(
            'title',
            'banks',
            'topup_transfer_types',
            'topup'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBankRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBankRequest $request)
    {
        $request->validate([
            'topup_transfer_type_id' => 'required',
            'nama_bank' => 'required',
            'no_rek' => 'required',
            'atas_nama' => 'required',
        ]);

        Bank::create($request->all());

        Alert::success('Berhasil!', 'Berhasil menambah data bank');
        return redirect('/admin-bank');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        $title = 'Bank Edit';
        $topup_transfer_types = TopupTransferType::all();

        return view('bank.edit', compact(
            'title',
            'bank',
            'topup_transfer_types'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBankRequest  $request
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBankRequest $request, Bank $bank)
    {
        $bank->fill($request->all())->save();

        Alert::success('Berhasil!', 'Berhasil merubah data bank');
        return redirect('/admin-bank');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        //
    }

    public function disable(Bank $bank)
    {
        $bank->update([
            'is_active' => false
        ]);

        Alert::success('Berhasil!', 'Bank telah disable');
        return redirect('/admin-bank');
    }

    public function enable(Bank $bank)
    {
        $bank->update([
            'is_active' => true
        ]);

        Alert::success('Berhasil!', 'Bank telah enable');
        return redirect('/admin-bank');
    }

    public function delete(Bank $bank)
    {
        $used_bank = Topup::where('bank_id', $bank->id)->first();
        if($used_bank){
            Alert::error('Gagal!', 'Bank tidak dapat dihapus, karna terdapat history topup menggunakan bank tersebut');
            return redirect('/admin-bank');
        }

        $bank->delete();
        Alert::success('Berhasil!', 'Bank telah dihapus');
        return redirect('/admin-bank');
    }
}
