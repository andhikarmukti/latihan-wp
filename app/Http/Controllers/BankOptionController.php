<?php

namespace App\Http\Controllers;

use App\Models\BankOption;
use App\Http\Requests\StoreBankOptionRequest;
use App\Http\Requests\UpdateBankOptionRequest;

class BankOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBankOptionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBankOptionRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BankOption  $bankOption
     * @return \Illuminate\Http\Response
     */
    public function show(BankOption $bankOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BankOption  $bankOption
     * @return \Illuminate\Http\Response
     */
    public function edit(BankOption $bankOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBankOptionRequest  $request
     * @param  \App\Models\BankOption  $bankOption
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBankOptionRequest $request, BankOption $bankOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BankOption  $bankOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(BankOption $bankOption)
    {
        //
    }
}
