<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Discount;
use App\Models\LevelMember;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreDiscountRequest;
use App\Http\Requests\UpdateDiscountRequest;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Discount';
        $services = Service::orderBy('service_name', 'ASC')->where('is_active', true)->get();
        $level_members = LevelMember::all();
        $discounts = Discount::orderBy('id', 'desc')->get();

        return view('discount.index', compact(
            'title',
            'services',
            'level_members',
            'discounts'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDiscountRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'service_id' => 'required',
            'discount' => 'required',
            'discount_name' => 'required',
            'level_member_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $request->merge(['created_by' => auth()->user()->id]);
        Discount::create($request->all());
        Alert::success('Berhasil!', 'Berhasil menambahkan discount baru.');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function show(Discount $discount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount $discount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDiscountRequest  $request
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDiscountRequest $request, Discount $discount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Discount  $discount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount $discount)
    {
        $discount->delete();
    }

    public function infoDiscount()
    {
        $discounts = Discount::orderBy('id', 'desc')
        ->with('service:id,service_name')
        ->with('levelMember:id,level_name')
        ->where('start_date', '<=', date(now()))
        ->where('end_date', '>=', date(now()))
        ->get();

        return response()->json($discounts, 200);
    }
}
