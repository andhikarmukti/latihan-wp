<?php

namespace App\Http\Controllers;

use App\Models\Finance;
use App\Http\Requests\StoreFinanceRequest;
use App\Http\Requests\UpdateFinanceRequest;
use App\Models\HistoryOrder;
use App\Models\Topup;
use App\Models\User;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Finance';
        $id_cobacoba = [];

        $topup_saldo = Topup::whereNotIn('user_id', $id_cobacoba)
        ->where('status', 'paid')
        ->get()
        ->pluck('nominal')->sum();

        $total_saldo_user = User::whereNotIn('id', $id_cobacoba)
        ->get()
        ->pluck('saldo')->sum();

        $total_saldo_digunakan_completed = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
        ->where('status', 'Completed')
        ->get()
        ->pluck('harga')->sum();

        $total_saldo_digunakan_pending = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
        ->whereNotIn('status', ['Completed', 'Partial', 'Canceled'])
        ->get()
        ->pluck('harga')->sum();

        if($request->get('month') && $request->get('month') != 'all'){
            $total_omset = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
            ->where('status', 'completed')
            ->whereMonth('created_at', $request->get('month'))
            ->whereYear('created_at', $request->get('year'))
            ->get()
            ->pluck('harga')->sum();

            $partials = HistoryOrder::where('status', 'partial')
            ->whereNotIn('user_id', $id_cobacoba)
            ->whereMonth('created_at', $request->get('month'))
            ->whereYear('created_at', $request->get('year'))
            ->get()
            ->pluck('note');
            $total_partial = 0;
            foreach($partials as $partial){
                $total_partial += (int)str_replace('Refund Rp ', '', $partial);
            }

            $total_hpp = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
            ->where('status', 'completed')
            ->whereMonth('created_at', $request->get('month'))
            ->whereYear('created_at', $request->get('year'))
            ->get()
            ->pluck('hpp')->sum();

            $histories = HistoryOrder::orderBy('id', 'DESC')
            ->whereNotIn('user_id', $id_cobacoba)
            ->whereIn('status', ['completed', 'partial'])
            ->whereMonth('created_at', $request->get('month'))
            ->whereYear('created_at', $request->get('year'))
            ->get();
        }else if($request->get('month') == 'all'){
            $total_omset = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
            ->where('status', 'completed')
            ->whereYear('created_at', $request->get('year'))
            ->get()
            ->pluck('harga')->sum();

            $partials = HistoryOrder::where('status', 'partial')
            ->whereNotIn('user_id', $id_cobacoba)
            ->whereYear('created_at', $request->get('year'))
            ->get()
            ->pluck('note');
            $total_partial = 0;
            foreach($partials as $partial){
                $total_partial += (int)str_replace('Refund Rp ', '', $partial);
            }

            $total_hpp = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
            ->where('status', 'completed')
            ->whereYear('created_at', $request->get('year'))
            ->get()
            ->pluck('hpp')->sum();

            $histories = HistoryOrder::orderBy('id', 'DESC')
            ->whereNotIn('user_id', $id_cobacoba)
            ->whereIn('status', ['completed', 'partial'])
            ->whereYear('created_at', $request->get('year'))
            ->get();
        }else{
            $total_omset = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
            ->where('status', 'completed')
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->get()
            ->pluck('harga')->sum();

            $partials = HistoryOrder::where('status', 'partial')
            ->whereNotIn('user_id', $id_cobacoba)
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->get()
            ->pluck('note');
            $total_partial = 0;
            foreach($partials as $partial){
                $total_partial += (int)str_replace('Refund Rp ', '', $partial);
            }

            $total_hpp = HistoryOrder::whereNotIn('user_id', $id_cobacoba)
            ->where('status', 'completed')
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->get()
            ->pluck('hpp')->sum();

            $histories = HistoryOrder::orderBy('id', 'DESC')
            ->whereNotIn('user_id', $id_cobacoba)
            ->whereIn('status', ['completed', 'partial'])
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->get();
        }

        return view('finance.index', compact(
            'title',
            'topup_saldo',
            'total_saldo_user',
            'total_omset',
            'total_hpp',
            'histories',
            'total_saldo_digunakan_completed',
            'total_saldo_digunakan_pending',
            'total_partial'
            // 'date'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFinanceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFinanceRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function show(Finance $finance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function edit(Finance $finance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFinanceRequest  $request
     * @param  \App\Models\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFinanceRequest $request, Finance $finance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Finance  $finance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Finance $finance)
    {
        //
    }
}
