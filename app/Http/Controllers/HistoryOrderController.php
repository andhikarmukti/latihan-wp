<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Action;
use App\Models\Server;
use GuzzleHttp\Client;
use App\Jobs\SendWaJob;
use App\Models\Service;
use app\Helpers\Helpers;
use App\Models\Discount;
use App\Models\AppConfig;
use App\Models\HistoryOrder;
use App\Models\RefillStatus;
use App\Models\ServerAction;
use App\Models\SpecialPrice;
use Illuminate\Http\Request;
use app\Helpers\ServerHelper;
use App\Models\LogCommission;
use App\Models\ServerParameter;
use App\Models\ServiceCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreHistoryOrderRequest;
use App\Http\Requests\UpdateHistoryOrderRequest;

class HistoryOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function newOrder(Request $request)
    {
        $title = 'New Order';
        $services_category_id = ServiceCategory::orderBy('nama_category', 'ASC')->where('is_active', true)->get();
        $nama_web = AppConfig::find('WEB.NAME')->value;

        // Ajax
        if($request->service_category_id){
            $services = Service::orderBy('service_name', 'ASC')->with('discounts', function($query) use ($request){
                $query->where('start_date', '<=', date(now()))
                ->where('end_date', '>=', date(now()))
                ->where('level_member_id', '<=', auth()->user()->levelMember->level)
                ->first();
            })
            ->where('service_category_id', $request->service_category_id)
            ->where('is_active', true)
            ->get();
            return response()->json($services, 200);
        }
        if($request->service_id){
            $service = Service::orderBy('id', 'DESC')
            ->with('discounts', function($query) use ($request){
                $query->orderBy('level_member_id', 'desc')
                ->where('service_id', $request->service_id)
                ->where('start_date', '<=', date(now()))
                ->where('end_date', '>=', date(now()))
                ->where('level_member_id', '<=', auth()->user()->levelMember->level)
                ->first();
            })
            ->with('specialPrice', function($query) use ($request){
                $query->orderBy('id', 'DESC')
                ->where('status', 'enable')
                ->where('user_id', auth()->user()->id)
                ->where('service_id', $request->service_id)
                ->first();
            })
            ->where('id', $request->service_id)->first();
            return response()->json($service, 200);
        }

        return view('order.newOrder', compact(
            'title',
            'services_category_id',
            'nama_web'
        ));
    }

    public function historyOrder(Request $request)
    {
        $title = 'History Order';
        $history_orders = HistoryOrder::orderBy('id', 'DESC')
        ->where('user_id', auth()->user()->id)
        ->get();

        if(Gate::allows('admin') || Gate::allows('cs')){
            $history_orders = HistoryOrder::orderBy('id', 'DESC')->get();
        }

        return view('order.historyOrder', compact(
            'title',
            'history_orders'
        ));
    }

    public function historyOrderCheckUserRole(Request $request)
    {
        // Pengecekan untuk menentukan target kolom, apakah admin atau user [format rupiahnya]
        if($request->user_role){
            return auth()->user()->role;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreHistoryOrderRequest  $request
     * @return \Illuminate\Http\Response
     */

    public function cekDiscount($service_id, $user)
    {
        $now = date(now());
        $discount = Discount::orderBy('level_member_id', 'desc')
        ->where('service_id', $service_id)
        ->where('start_date', '<=', $now)
        ->where('end_date', '>=', $now)
        ->where('level_member_id', '<=', $user->levelMember->level)
        ->first();
        if($discount){
            return $discount->discount;
        }else{
            return 0;
        }
    }

    public function hitungHarga($service, $quantity, $discount, $user)
    {
        $rate_harga = $service->rate;

        //Cek apakah ada special price
        $special_price = SpecialPrice::orderBy('id', 'DESC')
        ->where('user_id', $user->id)
        ->where('service_id', $service->id)
        ->where('status', 'enable')
        ->first();
        if($special_price){
            $rate_harga = ($special_price->service->rate - ($special_price->service->rate * $special_price->percent / 100) - $special_price->fix);
            Log::info(["rate harga : ", $rate_harga]);
        }

        $total_harga = ($rate_harga * $user->levelMember->mark_up) / $service->unit * $quantity;
        Log::info(["ee", $total_harga]);
        $total_discount = ($total_harga * $discount) / 100;
        $total_harga = $total_harga - $total_discount;
        Log::info(["hitung harga function : ", $total_harga, $special_price]);
        return ceil($total_harga);
    }

    public function cekSaldo($total_harga, $user)
    {
        $cek_saldo = $user->saldo - $total_harga;
        if($cek_saldo < 0){
            return 0;
        }else{
            return true;
        }
    }

    public function apiStore(Request $request)
    {
        return $request->all();
    }

    public function store(Request $request)
    {
        $service = Service::find($request->service_id);
        $user = auth()->user();

        // START Pengecekan API Key user
        if($request->api_key){
            $user = User::where('api_key', $request->api_key)->first();

            if($service->is_active == false){
                return response()->json([
                    'status' => false,
                    'service_not_ready' => true,
                    'data' => 'Layanan Tidak Tersedia, Hubungi CS kami'
                ], 200);
            }

            if(!$user){
                return response()->json([
                    'status' => false,
                    'data' => 'API key not found!'
                ], 400);
            }
        }else{
            $user = auth()->user();
        }
        // Pengecekan action ID
        $action_id = $request->action_id;
        if($request->api_key){
            if($request->action == 'order'){
                $action_id = 1;
            }else{
                return response()->json([
                    'status' => false,
                    'data' => 'Bad Action!'
                ], 400);
            }
        }
        // END Pengecekan API Key user

        $rules = [
            'service_id' => 'required',
            'target' => 'required',
            'quantity' => 'required|numeric|min:' . $service->min . '|max:' . $service->max,
        ];
        $messsage = [
            'required' => 'tidak boleh kosong!',
            'min' => 'Quantity dibawah batas minimum order',
            'max' => 'Quantity diatas batas maksimum order',
        ];

        if($service->type_id == 2){
            $rules = array_merge($rules, array('comments' => 'required'));
        }else if($service->type_id == 3){
            $rules = array_merge($rules, array('username' => 'required'));
        }

        $validator = Validator::make($request->all(), $rules, $messsage);
        if($validator->fails()){
            Log::error('Error pengisian form order : ' . $validator->errors());
            if($request->api_key){ // Response API
                return response()->json([
                    'status' => false,
                    'data' => $validator->errors()->first()
                ]);
            }
            return response()->json($validator->getMessageBag()->add('type_id', $service->type_id), 400);
        }

        $service = Service::find($request->service_id);
        $server = Server::find($service->server_id);
        $hpp = ceil($service->hpp / $service->unit * $request->quantity);

        if($action_id != 5){ // kalau bukan refill, silahkan dicek. tapi kalau refill gaperlu dicek
            // Cek apakah ada diskon
            $discount = $this->cekDiscount($service->id, $user);

            // Hitung harga
            $total_harga = $this->hitungHarga($service, $request->quantity, $discount, $user);
            Log::info(["total harga", $total_harga]);

            // Pengecekan saldo
            $cek_saldo = $this->cekSaldo($total_harga, $user);
            if($cek_saldo == false){
                return response()->json([
                    'status' => false,
                    'saldo_tidak_cukup' => true,
                    'message' => 'Maaf, saldo kamu tidak cukup untuk melakukan orderan ini'
                ], 400);
            }

            // Pengecekan orderan dengan target yang sama, server service id sama dan satus masih pending
            $same_order = HistoryOrder::where([
                ['user_id', $user->id],
                ['target', $request->target],
                ['server_service_id', $service->server_service_id],
            ])
            ->whereIn('status', ['Pending', 'In Progress', 'Processing', 'Waiting'])
            ->first();
            if($same_order){
                return response()->json([
                    'status' => false,
                    'same_order' => true,
                    'message' => 'Terdapat orderan yang masih pending ke target yang sama.'
                ], 400);
            }
        }

        // Tembak order ke API server
            $order_params = [
                'server_id' => $server->id,
                'service_id' => $service->id,
                'server_service_id' => $service->server_service_id,
                'target' => trim($request->target),
                'quantity' => trim($request->quantity),
                'action_id' => $action_id,
                'comments' => $request->comments,
                'username' => $request->username,
                'order_id_server' => $request->order_id_server,
            ];
        $response_order = ServerHelper::checkServer($order_params);
        // return $response_order;

        // Jika error
        if(property_exists($response_order, "error") || property_exists($response_order, "status") || property_exists($response_order, "response")){
            if(property_exists($response_order, "status") && $response_order->status == false){
                return response()->json([
                    'status' => false,
                    'error_api' => true,
                    'message' => $response_order->message
                ], 200);
            }else if(property_exists($response_order, "error")){
                Log::error('order error : ' . $response_order->error);
                return response()->json([
                    'status' => false,
                    'error_api' => true,
                    'message' => $response_order->error
                ], 200);
            }else if(property_exists($response_order, "response") && $response_order->response == false){
                return response()->json([
                    'status' => false,
                    'error_api' => true,
                    'message' => $response_order->data->msg
                ], 200);
            }
        }

        // Jika refill
        if($response_order && property_exists($response_order, "refill")){
            Log::info('refill success dengan refill ID : ' . $response_order->refill);
            return response()->json([
                'refill_id' => $response_order->refill
            ], 200);
        }

        try{
            $response_order_id_server = property_exists($response_order, "data") ? $response_order->data->id : ($response_order ? $response_order->order : null);
            /** @var User $user */
            // Insert data ke table history_orders
            $new_order = HistoryOrder::create([
                'user_id' => $user->id,
                'service_category_id' => $service->service_category_id,
                'service_id' => $service->id,
                'target' => $request->target,
                'server_id' => $service->server->id,
                'server_service_id' => $service->server_service_id,
                'order_id_server' => $response_order_id_server, // mengatasi orderan yang suka responsenya kosong dari server
                'quantity' => $request->quantity,
                'discount' => $discount,
                'harga' => $total_harga,
                'hpp' => $hpp,
                'mark_up_server' => $server->mark_up,
                'mark_up_user_level' => $user->levelMember->mark_up,
                'saldo_awal' => $user->saldo,
                'sisa_saldo' => $user->saldo - $total_harga,
                'comments' => $request->username ?? ($request->comments ?? '-'),
                'refill_end_date' => $service->refill_duration > 0 ? now()->addDays($service->refill_duration) : now(),
                'api_order' => $request->api_key ? 1 : 0,
                'notif_cancel_partial' => $request->notif_cancel_partial ? 1 : 0,
                'created_by' => $user->id,
            ]);

            // Update Log Commission
            $user_upline_id = $user->user_upline_id;
            if($user_upline_id != null){
                $persentase_komisi = Helpers::percentCommission($user);
                LogCommission::create([
                    'user_upline_id' => $user_upline_id,
                    'user_downline_id' => $user->id,
                    'service_id' => $new_order->service_id,
                    'order_id_server' => $response_order_id_server,
                    'harga_order' => $new_order->harga,
                    'discount' => $new_order->discount,
                    'status_order' => 'Pending',
                    'commission' => $new_order->harga * ($persentase_komisi / 100) < 1 ? 1 : $new_order->harga * ($persentase_komisi / 100),
                ]);
            }


            // Update status ordernya
            ServerHelper::checkStatusOrder($new_order);

            // Log saldo
            $log_saldo = Helpers::logSaldo('order', $user->saldo, $user->saldo - $total_harga, $new_order);
            Log::info(['order', $log_saldo]);

            // Pengurangan saldo user dan penambahan saldo digunakan
            $user->saldo -= $total_harga;
            $user->saldo_digunakan += $total_harga;
            $user->save();

            // Addon notif cancel / partial
            if($request->notif_cancel_partial){
                Helpers::logSaldo('notif_cancel_partial', $user->saldo, $user->saldo - 100, $new_order);
                $user->saldo -= 100;
                $user->saldo_digunakan += 100;
                if($user->upgradeLevelAccept){
                    $user->upgradeLevelAccept->update([
                        'saldo_digunakan' => $user->upgradeLevelAccept->saldo_digunakan += 100
                    ]);
                }
                $user->save();
            }

            // Penambahan saldo digunakan jika memiliki upgrade level yang accept
            if($user->upgradeLevelAccept){
                $user->upgradeLevelAccept->update([
                    'saldo_digunakan' => $user->upgradeLevelAccept->saldo_digunakan += $total_harga
                ]);
            }

        }catch(\Exception $e){
            return response()->json($e->getMessage(), 500);
        }

        if($new_order->order_id_server == null){
            $no_wa = AppConfig::find('CONTACT.OWNER')->value;
            $message = 'Ada orderan baru dengan order_id_server yang null. Order id : ' . $new_order->id;
            dispatch(new SendWaJob($no_wa, $message));
        }

        return response()->json([
            'status' => true,
            'data' => [
                'id' => $new_order->id
            ]
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function show(HistoryOrder $historyOrder)
    {
        //
    }

    public function apiStatusOrder(Request $request)
    {
        // Pengecekan API Key user
        if($request->api_key){
            $user = User::where('api_key', $request->api_key)->first();
            if(!$user){
                return response()->json([
                    'status' => false,
                    'data' => 'API key not found!'
                ], 400);
            }
        }
        // Pengecekan action ID
        $action_id = $request->action_id;
        if($request->api_key){
            if($request->action != 'status'){
                return response()->json([
                    'status' => false,
                    'data' => 'Bad Action!'
                ], 400);
            }
        }

        $order = HistoryOrder::where('id', $request->order)
        ->where('user_id', $user->id)
        ->first();

        if($order){
            return response()->json([
                'status' => true,
                'data' => [
                    'charge' => $order->harga,
                    'start_count' => $order->start_count,
                    'status' => $order->status,
                    'remains' => $order->remain
                ]
            ]);
        }else{
            return response()->json([
                'status' => false,
                'data' => 'Bad Request!'
            ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function speedup(Request $request, HistoryOrder $historyOrder)
    {
        $order_id_server = $historyOrder->order_id_server;
        $server_name = $historyOrder->server->nama_server;
        $message = 'Halo kak, saya ingin report speedup untuk order ID : ' . $order_id_server . '. Terima kasih';
        $cp = $historyOrder->server->contact_person; // kalau belum diset akan ke nomor pribados

        dispatch(new SendWaJob($cp, $message));
        try{
            $historyOrder->update([
                'pengajuan_speedup' => now()
            ]);
            Log::info("Berhasil mengirimkan report speedup ke server " . $server_name . " dari username : " . $historyOrder->user->username . ", dengan service ID : " . $historyOrder->service_id . " dan order ID server : " . $historyOrder->order_id_server);

            return response()->json([
                'info' => 'Berhasil mengirimkan report speedup!'
            ], 200);
        }catch(\Exception $e){
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateHistoryOrderRequest  $request
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HistoryOrder  $historyOrder
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, HistoryOrder $historyOrder)
    {
        $user = User::find($historyOrder->user_id);

        try{
            DB::beginTransaction();
            if($request->action == 'cancel'){
                $historyOrder->update([
                    'status' => 'Canceled',
                    'updated_by' => auth()->user()->id,
                    'note' => 'Refund ' . Helpers::format_rupiah($historyOrder->harga)
                ]);
                // Log saldo
                $log_saldo = Helpers::logSaldo('cancel', $user->saldo, ($user->saldo + $historyOrder->harga), $historyOrder);
                Log::info(['cancel', $log_saldo]);
                $user->saldo += $historyOrder->harga;
                $user->saldo_digunakan -= $historyOrder->harga;
                $user->save();

                // Pengurangan saldo digunakan jika memiliki upgrade level yang accept
                if($user->upgradeLevelAccept){
                    $user->upgradeLevelAccept->update([
                        'saldo_digunakan' => $user->upgradeLevelAccept->saldo_digunakan -= $historyOrder->harga
                    ]);
                }

                $info = 'Orderan dengan order ID ' . $historyOrder->id . ' Canceled. Saldo refund sebesar ' . Helpers::format_rupiah($historyOrder->harga);
                Log::info("saldo refund sebesar " . $historyOrder->harga . ' ke user ID ' . $historyOrder->id . ' ('. $historyOrder->user->username .')');
            }else if($request->action == 'completed'){
                $historyOrder->update([
                    'status' => 'completed',
                    'updated_by' => auth()->user()->id
                ]);

                // Tidak perlu dilakukan lagi, karna saat order sudah bertambah. kecuali pengurangan ketika cancel
                // Penambahan saldo digunakan jika memiliki upgrade level yang accept
                // if($user->upgradeLevelAccept){
                //     $user->upgradeLevelAccept->update([
                //         'saldo_digunakan' => $user->upgradeLevelAccept->saldo_digunakan += $historyOrder->harga
                //     ]);
                // }
                // $user->saldo_digunakan += $historyOrder->harga;
                // $user->save();

                $info = 'Orderan dengan order ID ' . $historyOrder->id . ' completed!';
            }
            DB::commit();

            return response()->json([
                'info' => $info
            ], 200);
        }catch(\Exception $e){
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
    }

    public function successOrder(Request $request)
    {
        $info_order = HistoryOrder::join('services', 'services.id', '=', 'history_orders.service_id')
        ->select('history_orders.id', 'services.service_name', 'history_orders.target', 'history_orders.quantity', 'history_orders.status')
        ->where('history_orders.id', $request->order_id)
        ->first();

        return response()->json([
            'success' => true,
            'data' => $info_order
        ]);
    }

    public function commentShowButton(Request $request, HistoryOrder $historyOrder)
    {
        $comments = preg_split('/\r\n|\r|\n/', $historyOrder->comments);
        return $comments;
    }
}
