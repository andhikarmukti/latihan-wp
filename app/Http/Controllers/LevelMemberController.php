<?php

namespace App\Http\Controllers;

use App\Models\LevelMember;
use App\Http\Requests\StoreLevelMemberRequest;
use App\Http\Requests\UpdateLevelMemberRequest;

class LevelMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLevelMemberRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLevelMemberRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LevelMember  $levelMember
     * @return \Illuminate\Http\Response
     */
    public function show(LevelMember $levelMember)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LevelMember  $levelMember
     * @return \Illuminate\Http\Response
     */
    public function edit(LevelMember $levelMember)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLevelMemberRequest  $request
     * @param  \App\Models\LevelMember  $levelMember
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLevelMemberRequest $request, LevelMember $levelMember)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LevelMember  $levelMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(LevelMember $levelMember)
    {
        //
    }
}
