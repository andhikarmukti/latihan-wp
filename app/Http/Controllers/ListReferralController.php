<?php

namespace App\Http\Controllers;

use App\Models\ListReferral;
use App\Http\Requests\StoreListReferralRequest;
use App\Http\Requests\UpdateListReferralRequest;

class ListReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreListReferralRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreListReferralRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListReferral  $listReferral
     * @return \Illuminate\Http\Response
     */
    public function show(ListReferral $listReferral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListReferral  $listReferral
     * @return \Illuminate\Http\Response
     */
    public function edit(ListReferral $listReferral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateListReferralRequest  $request
     * @param  \App\Models\ListReferral  $listReferral
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateListReferralRequest $request, ListReferral $listReferral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListReferral  $listReferral
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListReferral $listReferral)
    {
        //
    }
}
