<?php

namespace App\Http\Controllers;

use App\Models\LogCommission;
use App\Http\Requests\StoreLogCommissionRequest;
use App\Http\Requests\UpdateLogCommissionRequest;

class LogCommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLogCommissionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLogCommissionRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LogCommission  $logCommission
     * @return \Illuminate\Http\Response
     */
    public function show(LogCommission $logCommission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LogCommission  $logCommission
     * @return \Illuminate\Http\Response
     */
    public function edit(LogCommission $logCommission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLogCommissionRequest  $request
     * @param  \App\Models\LogCommission  $logCommission
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLogCommissionRequest $request, LogCommission $logCommission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LogCommission  $logCommission
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogCommission $logCommission)
    {
        //
    }
}
