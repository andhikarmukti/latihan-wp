<?php

namespace App\Http\Controllers;

use App\Models\LogSaldo;
use App\Http\Requests\StoreLogSaldoRequest;
use App\Http\Requests\UpdateLogSaldoRequest;
use Illuminate\Support\Facades\Gate;

class LogSaldoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Log Saldo';
        $log_saldos = LogSaldo::orderBy('id', 'DESC')->where('user_id', auth()->user()->id)->get();
        if(Gate::allows('admin')){
            $log_saldos = LogSaldo::orderBy('id', 'DESC')->get();
        }

        return view('log_saldo.index', compact(
            'title',
            'log_saldos'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLogSaldoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLogSaldoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LogSaldo  $logSaldo
     * @return \Illuminate\Http\Response
     */
    public function show(LogSaldo $logSaldo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LogSaldo  $logSaldo
     * @return \Illuminate\Http\Response
     */
    public function edit(LogSaldo $logSaldo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLogSaldoRequest  $request
     * @param  \App\Models\LogSaldo  $logSaldo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLogSaldoRequest $request, LogSaldo $logSaldo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LogSaldo  $logSaldo
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogSaldo $logSaldo)
    {
        //
    }
}
