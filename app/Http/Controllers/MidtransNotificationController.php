<?php

namespace App\Http\Controllers;

use App\Models\Topup;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use App\Models\TopupLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\MidtransNotification;
use App\Http\Requests\StoreMidtransNotificationRequest;
use App\Http\Requests\UpdateMidtransNotificationRequest;

class MidtransNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMidtransNotificationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMidtransNotificationRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MidtransNotification  $midtransNotification
     * @return \Illuminate\Http\Response
     */
    public function show(MidtransNotification $midtransNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MidtransNotification  $midtransNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(MidtransNotification $midtransNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMidtransNotificationRequest  $request
     * @param  \App\Models\MidtransNotification  $midtransNotification
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMidtransNotificationRequest $request, MidtransNotification $midtransNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MidtransNotification  $midtransNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(MidtransNotification $midtransNotification)
    {
        //
    }

    public function callbackMidtrans(Request $request)
    {
        $topup = Topup::where('id', $request->order_id)->first();
        // $midtransNotification = MidtransNotification::where('topup_id', $topup->id)->first();

        $signature_key_self = hash('SHA512', $topup->id . $request->status_code . $request->gross_amount . env('MIDTRANS_SERVER_KEY'));
        $signature_key_midtrans = $request->signature_key;
        Log::info([
            'signature key dari midtrans' => $request->signature_key,
            'signature key milik sendiri' => $signature_key_self,
            'transaction status' => $request->transaction_status,
            'request ALL' => $request->all()
        ]);

        if($signature_key_self == $signature_key_midtrans){
            Log::info('masuk');
            switch ($request->transaction_status) {
                case "settlement":
                    $status = 'paid';
                    break;
                case "pending":
                    $status = 'pending';
                    break;
                case "cancel":
                    $status = 'cancel';
                    break;
                case "expire":
                    $status = 'expire';
                    break;
                default:
                    $status = 'pending';
                }
                Log::info("status dari midtrans adalah " . $status);

            MidtransNotification::create([
                'topup_id' => $request->order_id,
                'payment_type' => $request->payment_type,
                'gross_amount' => $request->gross_amount,
                'transaction_id' => $request->transaction_id,
                'transaction_status' => $request->transaction_status,
                'transaction_time' => $request->transaction_time,
                'status_code' => $request->status_code,
                'signature_key' => $request->signature_key,
                'va_bank' => $request->va_numbers[0]['bank'] ?? null,
                'va_number' => $request->va_numbers[0]['va_number'] ?? null,
                'json_notif' => json_encode($request->all()),
            ]);

            $user = $topup->user;
            // Update status topup user
            $update_status = $topup->update([
                'status' => $status == 'expire' ? 'cancel' : $status,
                'note' => 'midtrans ' . $status,
                'updated_by' => $user->id
            ]);
            Log::info([$topup, $update_status, 'midtrans ' . $status]);

            if($status == 'paid'){
                // Create log topup
                TopupLog::create([
                    'user_id' => $topup->user_id,
                    'topup_id' => $topup->id,
                    'saldo_awal' => $user->saldo,
                    'saldo_ditambahkan' => $topup->nominal,
                    'total_saldo' => $user->saldo + $topup->nominal,
                    'bank_id' => $topup->bank_id,
                    'topup_transfer_type_id' => $topup->topup_transfer_type_id,
                    'note' => 'paid by midtrans',
                    'created_by' => $user->id
                ]);

                // Log saldo
                Helpers::logSaldo('topup', $user->saldo, ($user->saldo + $topup->nominal), null, $topup);

                // Tambah saldo user
                $user->saldo += $topup->nominal;
                $user->save();

//                 $no_wa = $topup->user->no_wa;
//                         $message = '*TOPUP KERANJANGINDO BERHASIL*
// Topup ID ' . $topup->id . '

// _Topup has been confirmed automatically by the system._

// Thankyou 😊🙏';
//                         dispatch(new SendWaJob($no_wa, $message));

//                         $no_wa2 = '089656911145';
//                         $message2 = '*TOPUP KERANJANGINDO BERHASIL*
// Topup ID : ' . $topup->id . '
// Username : ' . $topup->user->username . '
// Nominal : ' . Helpers::format_rupiah($topup->nominal + $topup->bonus) . '
// Note : ' . $topup->note . '

// _Topup has been confirmed automatically by the system._

// Thankyou 😊🙏';
//                         dispatch(new SendWaJob($no_wa2, $message2));
                return response()->json([
                    'success' => true
                ], 200);
            }
            Log::info([$update_status, $status == 'expire' ? 'cancel' : $status]);
            return response()->json([
                'success' => true
            ], 200);
        }else{
            Log::error('signature key tidak match');
        }
    }

    // Belum dipake, buat ngasih notif. tapi sudah auto dikirimin juga pas client memilih metode pembayaran lewat method callbackMidtrans
    public function midtransNotification(Request $request)
    {
        $signature_key_self = hash('SHA512', $request->order_id . $request->status_code . $request->gross_amount . env('MIDTRANS_SERVER_KEY'));

        try{
            MidtransNotification::create([
                'topup_id' => $request->order_id,
                'payment_type' => $request->payment_type,
                'gross_amount' => $request->gross_amount,
                'transaction_id' => $request->transaction_id,
                'transaction_status' => $request->transaction_status,
                'transaction_time' => $request->transaction_time,
                'status_code' => $request->status_code,
                'signature_key' => $signature_key_self,
                'va_bank' => $request->va_bank,
                'va_number' => $request->va_number,
                'json_notif' => json_encode($request->all()),
            ]);
            return response()->json([
                'success' => true
            ]);
        }catch(\Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }

    }
}
