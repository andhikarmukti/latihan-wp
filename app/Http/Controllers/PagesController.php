<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use App\Models\AppConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ServiceInformationUpdate;
use App\Models\SliderImage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    public $app_config;
    public $expired_date;

    public function __construct()
    {
        $this->app_config = new AppConfig();
        $this->expired_date = $this->app_config->find('WEB.EXPIRED')->value;
    }

    public function dashboard(Request $request)
    {
        // dd(auth()->user()->upgradeLevel->status);
        $title = 'Dashboard';
        $service_information_updates = ServiceInformationUpdate::orderBy('id', 'DESC')
        ->where('updated_at', '>=', now()->subDays(7))
        ->get();
        $discounts = Discount::orderBy('id', 'DESC')
        ->where('start_date', '<=', date(now()))
        ->where('end_date', '>=', date(now()))
        ->where('level_member_id', '<=', auth()->user()->levelMember->level)
        ->get();
        $slider_active = AppConfig::find('SLIDER.IMAGE.ACTIVE')->value;
        $slider_images = SliderImage::all();

        if($request->filter_info){
            if($request->filter_info == 'all'){
                $service_information_updates = ServiceInformationUpdate::with(['service', 'autoPriceLog'])
                ->orderBy('id', 'DESC')
                ->where('updated_at', '>=', now()->subDays(7))
                ->get();
            }else{
                $service_information_updates = ServiceInformationUpdate::with(['service', 'autoPriceLog'])
                ->orderBy('id', 'DESC')
                ->where('updated_at', '>=', now()->subDays(7))
                ->where('info', $request->filter_info)
                ->get();
            }

            return response()->json($service_information_updates);
        }

        return view('dashboard', compact(
            'title',
            'service_information_updates',
            'discounts',
            'slider_active',
            'slider_images'
        ));
    }

    public function info()
    {
        $title = 'Info';
        $nama_web = $this->app_config->find('WEB.NAME')->value;
        $no_wa = $this->app_config->find('CONTACT.WA');
        $email = $this->app_config->find('CONTACT.EMAIL');

        return view('info', compact(
            'title',
            'nama_web',
            'no_wa',
            'email'
        ));
    }

    public function docApi()
    {
        $title = 'Documentasi API';

        return view('api.index', compact(
            'title'
        ));
    }

    public function expiredWeb()
    {
        $title = 'Expired Web';
        if(now() < $this->expired_date){
            return back();
        }else{
            return view('expired.index', compact(
                'title'
            ));
        }

    }

    public function expiredWebApi(Request $request)
    {
        $code = $this->app_config->find('CONTROL.CODE')->value;
        if(!Hash::check($request->code, $code)){
            return response()->json([
                'success' => false,
                'message' => 'Bad Request!'
            ], 400);
        }

        $rules = [
            'expired_date' => 'required'
        ];
        $messages = [
            'required' => "Parameter required, can't be nulled!"
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first()
            ], 400);
        }

        $this->app_config->find('WEB.EXPIRED')->update([
            'value' => $request->expired_date
        ]);

        return response()->json([
            'success' => true,
            'message' => 'expired date updated to ' . $request->expired_date
        ]);
    }
}
