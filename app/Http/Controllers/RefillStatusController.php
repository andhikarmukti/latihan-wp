<?php

namespace App\Http\Controllers;

use App\Models\HistoryOrder;
use App\Models\RefillStatus;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\StoreRefillStatusRequest;
use App\Http\Requests\UpdateRefillStatusRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RefillStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'History Refill';
        $refill_statuses = RefillStatus::orderBy('id', 'DESC')->where('user_id', auth()->user()->id)->get();
        if(Gate::allows('admin')){
            $refill_statuses = RefillStatus::orderBy('id', 'DESC')->get();
        }

        return view('refill.index', compact(
            'title',
            'refill_statuses'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRefillStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRefillStatusRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RefillStatus  $refillStatus
     * @return \Illuminate\Http\Response
     */
    public function show(RefillStatus $refillStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RefillStatus  $refillStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(RefillStatus $refillStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRefillStatusRequest  $request
     * @param  \App\Models\RefillStatus  $refillStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $historyOrder = HistoryOrder::where('id', $request->order_id)->orderBy('id', 'desc')->first();
        $historyOrder->update([
            'refill_status' => 1
        ]);
        Log::info("Berhasil update refill_status dengan order ID :" . $request->order_id);

        RefillStatus::create([
            'user_id' => $historyOrder->user->id,
            'refill_id' => $request->refill_id,
            'pengajuan_refill' => now(),
            'history_order_id' => $historyOrder->id,
            'order_id_server' => $historyOrder->order_id_server,
            'target' => $historyOrder->target,
            'service' => $historyOrder->service->id . ' - ' . $historyOrder->service->service_name,
            'status' => 'Pending',
            'created_by' => auth()->user()->id,
        ]);

        return response()->json('Berhasil melakukan penambahan data refill status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RefillStatus  $refillStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(RefillStatus $refillStatus)
    {
        //
    }
}
