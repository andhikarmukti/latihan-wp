<?php

namespace App\Http\Controllers;

use App\Jobs\SendOtpJob;
use App\Models\RegisterOtp;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreRegisterOtpRequest;
use App\Http\Requests\UpdateRegisterOtpRequest;

class RegisterOtpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Register OTP';
        $otps = RegisterOtp::orderBy('id', 'DESC')->get();

        return view('registerOtp.index', compact(
            'title',
            'otps'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreRegisterOtpRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegisterOtpRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterOtp $registerOtp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterOtp $registerOtp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateRegisterOtpRequest  $request
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRegisterOtpRequest $request, RegisterOtp $registerOtp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RegisterOtp  $registerOtp
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterOtp $registerOtp)
    {
        //
    }

    public function getOtp(Request $request)
    {
        $no_wa = str_replace("+", "", $request->no_wa);
        $hp_alt = Str::startsWith($no_wa, '0') ? '62' . substr($no_wa, 1) : '0' . substr($no_wa, 2);
        $rules = [
            'no_wa' => 'required|string|min:8|unique:users',
            'hp_alt' => 'unique:users,no_wa',
        ];
        $message = [
            'unique' => 'Nomor sudah digunakan'
        ];

        $validator = Validator::make(array_merge($request->all(), ['hp_alt' => $hp_alt]), $rules, $message);
        if ($validator->fails()) {
            return $validator->errors();
        }
        $otp = rand(100000,999999);
        dispatch(new SendOtpJob($otp, $hp_alt));

        RegisterOtp::create([
            'no_wa' => $no_wa,
            'otp'  => $otp,
            'created_at' => now()
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil mengirim otp ke nomor ' . $no_wa
        ], 200);

    }
}
