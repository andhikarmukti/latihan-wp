<?php

namespace App\Http\Controllers;

use App\Models\ResetPasswordManual;
use App\Http\Requests\StoreResetPasswordManualRequest;
use App\Http\Requests\UpdateResetPasswordManualRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class ResetPasswordManualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Reset Password Manual';
        $reset_manuals = ResetPasswordManual::all();

        return view('password.resetManual', compact(
            'title',
            'reset_manuals'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreResetPasswordManualRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreResetPasswordManualRequest $request)
    {
        $user = User::where('username', $request->username)->first();
        if($user){
            $user->update([
                'password' => Hash::make($request->password)
            ]);
            ResetPasswordManual::create([
                'user_id' => $user->id,
                'created_by' => auth()->user()->id
            ]);
            Alert::success('Berhasil reset password!');
            return back();
        }else{
            Alert::error('Username tidak ditemukan!');
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ResetPasswordManual  $resetPasswordManual
     * @return \Illuminate\Http\Response
     */
    public function show(ResetPasswordManual $resetPasswordManual)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ResetPasswordManual  $resetPasswordManual
     * @return \Illuminate\Http\Response
     */
    public function edit(ResetPasswordManual $resetPasswordManual)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateResetPasswordManualRequest  $request
     * @param  \App\Models\ResetPasswordManual  $resetPasswordManual
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateResetPasswordManualRequest $request, ResetPasswordManual $resetPasswordManual)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ResetPasswordManual  $resetPasswordManual
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResetPasswordManual $resetPasswordManual)
    {
        //
    }
}
