<?php

namespace App\Http\Controllers;

use App\Models\ServerAction;
use App\Http\Requests\StoreServerActionRequest;
use App\Http\Requests\UpdateServerActionRequest;

class ServerActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServerActionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServerActionRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServerAction  $serverAction
     * @return \Illuminate\Http\Response
     */
    public function show(ServerAction $serverAction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServerAction  $serverAction
     * @return \Illuminate\Http\Response
     */
    public function edit(ServerAction $serverAction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServerActionRequest  $request
     * @param  \App\Models\ServerAction  $serverAction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServerActionRequest $request, ServerAction $serverAction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServerAction  $serverAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServerAction $serverAction)
    {
        //
    }
}
