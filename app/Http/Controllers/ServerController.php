<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\Models\Server;
use GuzzleHttp\Client;
use App\Models\Service;
use App\Models\ServerAction;
use Illuminate\Http\Request;
use app\Helpers\ServerHelper;
use App\Models\ServerParameter;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreServerRequest;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UpdateServerRequest;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Server';
        $servers = Server::all();
        $actions = Action::all();

        return view('server.index', compact(
            'title',
            'servers',
            'actions'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama_server' => 'required',
            'url' => 'required',
            'key' => 'required',
            'mark_up' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }

        try{
            Server::create([
                'nama_server' => $request->nama_server,
                'url' => $request->url,
                'key' => $request->key,
                'mark_up' => $request->mark_up,
                'contact_person' => $request->contact_person ?? '6289656911145',
                'created_by' => auth()->user()->id
            ]);
            Alert::success("Berhasil!", "Berhasil menambahkan server.");
            return back();
        }catch(\Exception $e){
            Log::error("Create server error : " . $e->getMessage());
            Alert::error("Error!", "Hubungi Developer");
            return back()->withInput();
        }
    }

    public function serverActionPathStore(Request $request)
    {
        $rules = [
            'server_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }

        $server_actions = [
            [
                'action_id' => 1,
                'url_path' => $request->order_url_path,
                'method' => $request->order_method,
            ],
            [
                'action_id' => 2,
                'url_path' => $request->status_url_path,
                'method' => $request->status_method,
            ],
            [
                'action_id' => 3,
                'url_path' => $request->services_url_path,
                'method' => $request->services_method,
            ],
            [
                'action_id' => 4,
                'url_path' => $request->balances_url_path,
                'method' => $request->balances_method,
            ],
            [
                'action_id' => 5,
                'url_path' => $request->refill_url_path,
                'method' => $request->refill_method,
            ],
            [
                'action_id' => 6,
                'url_path' => $request->refill_status_url_path,
                'method' => $request->refill_status_method,
            ],
        ];

        foreach($server_actions as $server_action){
            ServerAction::create([
                'server_id' => $request->server_id,
                'action_id' => $server_action['action_id'],
                'url_path' => $server_action['url_path'],
                'method' => $server_action['method'],
                'created_by' => auth()->user()->id
            ]);
        }

        Alert::success("Berhasil!", "Berhasil menambahkan data service action path");
        return back();
    }

    public function serverStoreParameters(Request $request)
    {
        $rules = [
            'server_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }

        // Insert Action Order
        if($request->parameter_order[1] != null){
            for($i = 1; $i <= $request->length_loop_order; $i++){
                ServerParameter::create([
                    'server_id' => $request->server_id,
                    'action_id' => 1,
                    'parameter' => trim($request->parameter_order[$i]) ?? '',
                    'value' => trim($request->value_order[$i]) ?? '',
                    'created_by' => auth()->user()->id,
                ]);
            }
        }

        // Insert Action Status
        if($request->parameter_status[1] != null){
            for($i = 1; $i <= $request->length_loop_status; $i++){
                ServerParameter::create([
                    'server_id' => $request->server_id,
                    'action_id' => 2,
                    'parameter' => trim($request->parameter_status[$i]) ?? '',
                    'value' => trim($request->value_status[$i]) ?? '',
                    'created_by' => auth()->user()->id,
                ]);
            }
        }

        // Insert Action Services
        if($request->parameter_services[1] != null){
            for($i = 1; $i <= $request->length_loop_services; $i++){
                ServerParameter::create([
                    'server_id' => $request->server_id,
                    'action_id' => 3,
                    'parameter' => trim($request->parameter_services[$i]) ?? '',
                    'value' => trim($request->value_services[$i]) ?? '',
                    'created_by' => auth()->user()->id,
                ]);
            }
        }

        // Insert Action Balances
        if($request->parameter_balances[1] != null){
            for($i = 1; $i <= $request->length_loop_balances; $i++){
                ServerParameter::create([
                    'server_id' => $request->server_id,
                    'action_id' => 4,
                    'parameter' => trim($request->parameter_balances[$i]) ?? '',
                    'value' => trim($request->value_balances[$i]) ?? '',
                    'created_by' => auth()->user()->id,
                ]);
            }
        }

        // Insert Action Refill
        if($request->parameter_refill[1] != null){
            for($i = 1; $i <= $request->length_loop_refill; $i++){
                ServerParameter::create([
                    'server_id' => $request->server_id,
                    'action_id' => 5,
                    'parameter' => trim($request->parameter_refill[$i]) ?? '',
                    'value' => trim($request->value_refill[$i]) ?? '',
                    'created_by' => auth()->user()->id,
                ]);
            }
        }

        // Insert Action Refill Status
        if($request->parameter_refill_status[1] != null){
            for($i = 1; $i <= $request->length_loop_refill_status; $i++){
                ServerParameter::create([
                    'server_id' => $request->server_id,
                    'action_id' => 6,
                    'parameter' => trim($request->parameter_refill_status[$i]) ?? '',
                    'value' => trim($request->value_refill_status[$i]) ?? '',
                    'created_by' => auth()->user()->id,
                ]);
            }
        }

        Alert::success("Berhasil!", "Berhasil menambahkan data service parameters");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function show(Server $server)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function edit(Server $server)
    {
        $title = 'Edit Server';

        return view('server.edit', compact(
            'title',
            'server'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServerRequest  $request
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServerRequest $request, Server $server)
    {
        $server->fill($request->all())->save();
        Alert::success("Berhasil!", "Berhasil merubah data server");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Server  $server
     * @return \Illuminate\Http\Response
     */
    public function destroy(Server $server)
    {
        //
    }

    public function cekLayananServer(Request $request)
    {
        $title = 'Cek Layanan Server';
        $servers = Server::where('is_active', true)->get();

        $layanan_servers = [];
        $layanan_servers_length = 0;
        $services = Service::all();

        if($request->server_id){
            $server = Server::where('id', $request->server_id)->first();

            // Server BO
            // if($server->id == 1){
            //     $url = 'https://panel.bisnison.com/api/services';
            //     $response = Http::get($url);
            //     $layanan_servers = Cache::remember($url, 3 * 60 , function () use ($response){
            //         return $response->json();
            //     });
            //     $layanan_servers_length = count($layanan_servers);
            // }else{
                $url = $server->url;
                $client = new Client();

                $form_params = [];
                $server_parameters = ServerParameter::where('server_id', $server->id)->where('action_id', 3)->get();

                foreach($server_parameters as $server_parameter){
                    $form_params[$server_parameter->parameter] = $server_parameter[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
                }

                $server_action = ServerAction::where('server_id', $server->id)->where('action_id', 3)->first();
                $response = $client->request($server_action->method, $url . (isset($server_action->url_path) ? '/' . $server_action->url_path : ''), [
                    'form_params' => $form_params
                ])->getBody();
                // dd(json_decode($response));

                $layanan_servers = Cache::remember($url, 3 * 60 , function () use ($response){
                    return json_decode($response);
                });

                $layanan_servers = property_exists((object)$layanan_servers, "data") ? $layanan_servers->data : $layanan_servers;
                $layanan_servers_length = count($layanan_servers);
            // }
        }

        return view('server.cekLayananServer', compact(
            'title',
            'servers',
            'layanan_servers',
            'layanan_servers_length',
            'services'
        ));
    }

    public function serverList()
    {
        $title = 'Server List';
        $saldo = [];
        $servers = Server::orderBy('id', 'DESC')->where('is_active', true)->get();
        foreach($servers as $server){
            // Tembak order ke API server
            $params = [
                'server_id' => $server->id,
                'action_id' => 4, // check balance action ID nya 4
            ];
            $server_action = ServerAction::where([
                ['server_id', $server->id],
                ['action_id', $params['action_id']]
            ])->first();
            $server_parameters = ServerParameter::where([
                ['server_id', $server->id],
                ['action_id', $params['action_id']]
            ])->get();

            $form_params = [];
            foreach($server_parameters as $server_parameter){
                $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
            }
            // dd($form_params);

            // Push orderan
            $client = New Client();
            try{
                $response = $client->request($server_action->method, $server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : ''), [
                    'form_params' => $form_params
                ])->getBody();
            }catch(GuzzleException $e){
                Log::error('process API order error: ' . $e->getMessage());
                return response()->json($e->getMessage());
            }
            $response = json_decode($response);
            // dd($response);

            $saldo['server_id_' . $server->id] = property_exists($response, "data") ? $response->data->balance : $response->balance;
        }

        return view('server.list', compact(
            'title',
            'servers',
            'saldo'
        ));
    }
}
