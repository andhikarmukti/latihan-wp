<?php

namespace App\Http\Controllers;

use App\Models\ServerParameter;
use App\Http\Requests\StoreServerParameterRequest;
use App\Http\Requests\UpdateServerParameterRequest;

class ServerParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServerParameterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServerParameterRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServerParameter  $serverParameter
     * @return \Illuminate\Http\Response
     */
    public function show(ServerParameter $serverParameter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServerParameter  $serverParameter
     * @return \Illuminate\Http\Response
     */
    public function edit(ServerParameter $serverParameter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServerParameterRequest  $request
     * @param  \App\Models\ServerParameter  $serverParameter
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServerParameterRequest $request, ServerParameter $serverParameter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServerParameter  $serverParameter
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServerParameter $serverParameter)
    {
        //
    }
}
