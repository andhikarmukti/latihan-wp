<?php

namespace App\Http\Controllers;

use App\Models\ServiceAutoPriceDisable;
use App\Http\Requests\StoreServiceAutoPriceDisableRequest;
use App\Http\Requests\UpdateServiceAutoPriceDisableRequest;

class ServiceAutoPriceDisableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServiceAutoPriceDisableRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServiceAutoPriceDisableRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceAutoPriceDisable  $serviceAutoPriceDisable
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceAutoPriceDisable $serviceAutoPriceDisable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceAutoPriceDisable  $serviceAutoPriceDisable
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceAutoPriceDisable $serviceAutoPriceDisable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServiceAutoPriceDisableRequest  $request
     * @param  \App\Models\ServiceAutoPriceDisable  $serviceAutoPriceDisable
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceAutoPriceDisableRequest $request, ServiceAutoPriceDisable $serviceAutoPriceDisable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceAutoPriceDisable  $serviceAutoPriceDisable
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceAutoPriceDisable $serviceAutoPriceDisable)
    {
        //
    }
}
