<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Server;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\AutoPriceLog;
use Illuminate\Http\Request;
use app\Helpers\ServerHelper;
use App\Models\ServiceCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Models\ServiceInformationUpdate;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use Illuminate\Console\Scheduling\Schedule;
use App\Console\Commands\UpdateServiceInfoCron;
use App\Jobs\UpdateServiceInfoJob;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function apiServices(Request $request)
    {
        // Pengecekan API Key user
        if($request->api_key){
            $user = User::where('api_key', $request->api_key)->first();
            if(!$user){
                return response()->json([
                    'status' => false,
                    'data' => 'API key not found!'
                ], 400);
            }
        }
        // Pengecekan action ID
        $action_id = $request->action_id;
        if($request->api_key){
            if($request->action != 'services'){
                return response()->json([
                    'status' => false,
                    'data' => 'Bad Action!'
                ], 400);
            }
        }

        $services = Service::join('service_types', 'services.type_id', '=', 'service_types.id')
        ->join('service_categories', 'services.service_category_id', '=', 'service_categories.id')
        ->where('services.is_active', 1)
        ->select(
            'services.id as service',
            'services.service_name as name',
            'service_types.type as type',
            'service_categories.nama_category as category',
            'services.rate',
            'services.min',
            'services.max',
            'services.refill',
            'services.refill_duration',
            'services.member_info',
        )
        ->get();

        $services = $services->map(function($service) use ($user){
            return [
                'service' => $service->service,
                'name' => $service->name,
                'type' => $service->type,
                'category' => $service->category,
                'rate' => ceil($service->rate * $user->levelMember->mark_up),
                'min' => $service->min,
                'max' => $service->max,
                'refill' => $service->refill,
                'refill_duration' => $service->refill_duration,
                'member_info' => $service->member_info,
            ];
        });

        return response()->json([
            'status' => true,
            'level_user' => $user->levelMember->level_name,
            'data' => $services
        ], 200);
    }

    public function apiServiceCategories(Request $request)
    {
        // Pengecekan API Key user
        if($request->api_key){
            $user = User::where('api_key', $request->api_key)->first();
            if(!$user){
                return response()->json([
                    'status' => false,
                    'data' => 'API key not found!'
                ], 400);
            }
        }
        // Pengecekan action ID
        $action_id = $request->action_id;
        if($request->api_key){
            if($request->action != 'categories'){
                return response()->json([
                    'status' => false,
                    'data' => 'Bad Action!'
                ], 400);
            }
        }

        $service_categories = ServiceCategory::where('is_active', true)
        ->select('nama_category')
        ->get();
        return response()->json([
            'success' => true,
            'data' => $service_categories
        ]);
    }

    public function index(Request $request)
    {
        $title = 'Daftar Service';
        $service_categories = ServiceCategory::orderBy('nama_category', 'ASC')->where('is_active', true)->get();
        $services = Service::where('is_active', 1)->get();
        if(Gate::allows('admin')){
            $services = Service::orderBy('rate', 'DESC')->get();
        }
        $servers = Server::all();
        $service_types = ServiceType::all();
        $total_services_active = Service::where('is_active', true)->get()->count();

        if($request->service_id){
            $service = Service::join('service_categories', 'services.service_category_id', '=', 'service_categories.id')
            ->where('services.id', $request->service_id)
            ->select(
                'services.id',
                'service_categories.nama_category as category',
                'services.service_name',
                'services.min',
                'services.max',
                'services.speed',
                'services.rate',
                'services.refill',
                'services.refill_duration',
                'services.is_active',
                'services.member_info',
            )
            ->first();
            return response()->json($service, 200);
        }

        return view('service.index', compact(
            'title',
            'service_categories',
            'services',
            'servers',
            'service_types',
            'total_services_active'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'service_name' => 'required',
            'service_category_id' => 'required',
            'server_id' => 'required',
            'server_service_id' => 'required',
            'hpp' => 'required|numeric',
            'min' => 'required|numeric',
            'max' => 'required|numeric',
            'rate' => 'required|numeric',
            'unit' => 'required|numeric',
            'member_info' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return back()->withInput()->withErrors($validator->errors());
        }

        $existing_service = Service::where([
            ['server_id', $request->server_id],
            ['server_service_id', $request->server_service_id]
        ])->first();
        if($existing_service){
            Alert::error('Gagal!', 'Service tersebut sudah ada di database');
            return back();
        }

        $mark_up_server = Server::find($request->server_id)->mark_up;
        $request->merge([
            'created_by' => auth()->user()->id,
            'rate' => $request->rate * $mark_up_server
        ]);

        try {
            DB::beginTransaction();

            $new_service = Service::create($request->all());

            $auto_price_log = AutoPriceLog::create([
                'service_id' => $new_service->id,
                'old_hpp' => $new_service->hpp,
                'new_hpp' => $new_service->hpp,
                'old_rate' => $new_service->rate,
                'new_rate' => $new_service->rate,
                'old_status_active' => 1,
                'new_status_active' => 1,
            ]);

            ServiceInformationUpdate::create([
                'auto_price_log_id' => $auto_price_log->id,
                'service_id' => $new_service->id,
                'info' => 'new service',
                'harga_naik' => 0,
                'harga_turun' => 0,
                'status_active' => 1,
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->withErrors($e->getMessage());
        }

        Alert::success('Berhasil!', 'Berhasil menambah service baru!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $title = 'Edit Service';
        $service_category = $service->serviceCategory;
        $service_types = ServiceType::all();

        return view('service.edit', compact(
            'title',
            'service',
            'service_category',
            'service_types'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServiceRequest  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRequest $request, Service $service)
    {
        // dd($request->all());
        $info = null;
        if($service->rate != $request->rate || $service->hpp != $request->hpp || $service->is_active != $request->is_active){
            try{
                DB::beginTransaction();

                if($request->ajax()){
                    $auto_price_log = AutoPriceLog::create([
                        'service_id' => $service->id,
                        'old_hpp' => $service->hpp,
                        'new_hpp' => $service->hpp,
                        'old_rate' => $service->rate,
                        'new_rate' => $service->rate,
                        'old_status_active' => $service->is_active,
                        'new_status_active' => $request->action == 'enable' ? 1 : 0,
                    ]);
                }else{
                    $auto_price_log = AutoPriceLog::create([
                        'service_id' => $service->id,
                        'old_hpp' => $request->old_hpp,
                        'new_hpp' => $service->hpp,
                        'old_rate' => $request->old_rate,
                        'new_rate' => $service->rate,
                        'old_status_active' => $request->old_is_active,
                        'new_status_active' => $request->is_active,
                    ]);
                }


                if($auto_price_log->new_status_active == 0 && $auto_price_log->old_status_active == 1){
                    $info = 'disabled';
                    $harga_naik = 0;
                    $harga_turun = 0;
                } else if ($auto_price_log->new_status_active == 1 && $auto_price_log->old_status_active == 0) {
                    $info = 'comeback';
                    if ($auto_price_log->new_rate > $auto_price_log->old_rate) {
                        $harga_naik = 1;
                        $harga_turun = 0;
                    } else if($auto_price_log->new_rate < $auto_price_log->old_rate) {
                        $harga_naik = 0;
                        $harga_turun = 1;
                    }else{
                        $harga_naik = 0;
                        $harga_turun = 0;
                    }
                } else if ($auto_price_log->new_rate > $auto_price_log->old_rate) {
                    $info = 'harga NAIK';
                    $harga_naik = 1;
                    $harga_turun = 0;
                } else if($auto_price_log->new_rate < $auto_price_log->old_rate) {
                    $info = 'harga TURUN';
                    $harga_naik = 0;
                    $harga_turun = 1;
                }

                if($request->ajax()){
                    $service->update([
                        'is_active' => $request->action == 'enable' ? 1 : 0,
                        'auto_price' => $request->action == 'enable' ? 1 : 0,
                    ]);
                }else{
                    $service->fill($request->all())->save();
                }

                if($info != null){
                    ServiceInformationUpdate::create([
                        'auto_price_log_id' => $auto_price_log->id,
                        'service_id' => $service->id,
                        'info' => $info,
                        'harga_naik' => $harga_naik,
                        'harga_turun' => $harga_turun,
                        'status_active' => $service->is_active,
                    ]);
                }

                DB::commit();
            }catch(\Exception $e){
                DB::rollBack();
                Alert::error('Gagal!', $e->getMessage());
                return redirect('/service');
            }
        }else{
            if($request->ajax()){
                $service->update([
                    'is_active' => 0,
                    'auto_price' => 0,
                ]);
            }else{
                $service->fill($request->all())->save();
            }
        }

        if($request->ajax()){
            return response()->json([
                'success' => true,
                'message' => 'Berhasil disabled service'
            ], 200);
        }else{
            Alert::success('Berhasil!', 'Berhasil mengubah data service!');
            return redirect('/service');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }

    public function autoPrice()
    {
        dispatch(new UpdateServiceInfoJob);
        Alert::success('Autoprice done!');
        return back();
    }
}
