<?php

namespace App\Http\Controllers;

use App\Models\ServiceInformationUpdate;
use App\Http\Requests\StoreServiceInformationUpdateRequest;
use App\Http\Requests\UpdateServiceInformationUpdateRequest;

class ServiceInformationUpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreServiceInformationUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServiceInformationUpdateRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceInformationUpdate  $serviceInformationUpdate
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceInformationUpdate $serviceInformationUpdate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceInformationUpdate  $serviceInformationUpdate
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceInformationUpdate $serviceInformationUpdate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateServiceInformationUpdateRequest  $request
     * @param  \App\Models\ServiceInformationUpdate  $serviceInformationUpdate
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceInformationUpdateRequest $request, ServiceInformationUpdate $serviceInformationUpdate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceInformationUpdate  $serviceInformationUpdate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceInformationUpdate $serviceInformationUpdate)
    {
        //
    }
}
