<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\SpecialPrice;
use Illuminate\Http\Request;
use App\Models\ServiceCategory;
use App\Http\Requests\StoreSpecialPriceRequest;
use App\Http\Requests\UpdateSpecialPriceRequest;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class SpecialPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Special Price';
        $services_category_id = ServiceCategory::orderBy('nama_category', 'ASC')->where('is_active', true)->get();
        $special_prices = SpecialPrice::orderBy('id', 'DESC')->get();

        if($request->service_category_id){
            $services = Service::orderBy('service_name', 'ASC')->with('discounts', function($query) use ($request){
                $query->where('start_date', '<=', date(now()))
                ->where('end_date', '>=', date(now()))
                ->where('level_member_id', '<=', auth()->user()->levelMember->level)
                ->first();
            })
            ->where('service_category_id', $request->service_category_id)
            ->where('is_active', true)
            ->get();
            return response()->json($services, 200);
        }

        return view('specialPrice.index', compact(
            'title',
            'services_category_id',
            'special_prices'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSpecialPriceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSpecialPriceRequest $request)
    {
        // Validasi form
        $rules = [
            'username' => 'required',
            'service_category_id' => 'required',
            'service_id' => 'required',
            'expired_date' => 'required',
        ];
        $message = [
            'username.required' => 'Username tidak boleh kosong!',
            'service_category_id.required' => 'Service Category tidak boleh kosong!',
            'service_id.required' => 'Service tidak boleh kosong!',
            'expired_date.required' => 'Expired Date tidak boleh kosong!',
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            Alert::error('Gagal', $validator->errors()->first());
            return back();
        }

        // Pengecekan usernamenya ada atau tidak
        $user = User::where('username', $request->username)->first();
        if(!$user){
            Alert::error('Gagal', 'Username tidak ditemukan');
            return back();
        }

        // Cek apakah discount membuat profit menghilang?
        $service = Service::find($request->service_id);
        $rate_user = $service->rate * $user->levelMember->mark_up;
        if($rate_user - ($rate_user * $request->percent / 100) < $service->hpp){
            Alert::error('Gagal', 'Tidak dapat profit jika ditetapkan special price sebesar ' . $request->percent . '%');
            return back();
        }

        // Submit ke tabel Special Price
        SpecialPrice::create([
            'user_id' => $user->id,
            'service_id' => $request->service_id,
            'percent' => $request->percent != null ? $request->percent : 0,
            'fix' => $request->fix != null ? $request->fix : 0,
            'expired_date' => $request->expired_date,
            'created_by' => auth()->user()->id,
        ]);
        Alert::success('Berhasil!', 'Berhasil menambahkan data special price');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SpecialPrice  $specialPrice
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialPrice $specialPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SpecialPrice  $specialPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SpecialPrice $specialPrice)
    {
        $title = 'Edit Special Price';
        $services_category_id = ServiceCategory::orderBy('nama_category', 'ASC')->where('is_active', true)->get();
        $services = Service::where('service_category_id', $specialPrice->service->service_category_id)->get();

        if($request->service_category_id){
            $services = Service::orderBy('service_name', 'ASC')->with('discounts', function($query) use ($request){
                $query->where('start_date', '<=', date(now()))
                ->where('end_date', '>=', date(now()))
                ->where('level_member_id', '<=', auth()->user()->levelMember->level)
                ->first();
            })
            ->where('service_category_id', $request->service_category_id)
            ->where('is_active', true)
            ->get();
            return response()->json($services, 200);
        }

        return view('specialPrice.edit', compact(
            'title',
            'specialPrice',
            'services_category_id',
            'services'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSpecialPriceRequest  $request
     * @param  \App\Models\SpecialPrice  $specialPrice
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSpecialPriceRequest $request, SpecialPrice $specialPrice)
    {
        // dd($request->status);
        // Validasi form
        $rules = [
            'username' => 'required',
            'service_category_id' => 'required',
            'service_id' => 'required',
            'expired_date' => 'required',
            'status' => 'required',
        ];
        $message = [
            'username.required' => 'Username tidak boleh kosong!',
            'service_category_id.required' => 'Service Category tidak boleh kosong!',
            'service_id.required' => 'Service tidak boleh kosong!',
            'expired_date.required' => 'Expired Date tidak boleh kosong!',
            'status.required' => 'Status tidak boleh kosong!',
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            Alert::error('Gagal', $validator->errors()->first());
            return back();
        }

        // Pengecekan usernamenya ada atau tidak
        $user = User::where('username', $request->username)->first();
        if(!$user){
            Alert::error('Gagal', 'Username tidak ditemukan');
            return back();
        }

        // Submit ke tabel Special Price
        $specialPrice->update([
            'user_id' => $user->id,
            'service_id' => $request->service_id,
            'percent' => $request->percent != null ? $request->percent : 0,
            'fix' => $request->fix != null ? $request->fix : 0,
            'expired_date' => $request->expired_date,
            'status' => $request->status,
            'updated_by' => auth()->user()->id,
        ]);
        Alert::success('Berhasil!', 'Berhasil menambahkan data special price');
        return redirect('/admin-special-price');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SpecialPrice  $specialPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialPrice $specialPrice)
    {
        //
    }
}
