<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\User;
use App\Models\Topup;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use App\Models\TopupLog;
use BCAParser\BCAParser;
use App\Models\AppConfig;
use Illuminate\Http\Request;
use App\Models\TopupTransferType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\MidtransNotification;
use Illuminate\Support\Facades\Gate;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\UpdateTopupRequest;
use Illuminate\Support\Facades\Validator;
use App\Services\Midtrans\CreateSnapTokenService;

class TopupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->topup_transfer_type_id){
            $bonus_topup = AppConfig::find('BONUS_TOPUP.BCA')->value;
            $banks = TopupTransferType::where('id', $request->topup_transfer_type_id)
            ->where('is_active', true)
            ->first()
            ->banks;
            return response()->json([
                'banks' => $banks,
                'bonus_topup' => $bonus_topup
            ], 200);
        }

        $title = 'Tambah Saldo';
        $topup_transfer_types = TopupTransferType::where('is_active', 1)->where('id', '!=', 3)->get(); // ID 3 adalah Manual
        $bonus_topup = AppConfig::find('BONUS_TOPUP.BCA')->value;

        return view('saldo.index', compact(
            'title',
            'topup_transfer_types',
            'bonus_topup'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTopupRequest  $request
     * @return \Illuminate\Http\Response
     */

    private function getUniqueNumber($nominal)
    {
        $unique = rand(100, 999);
        $topUp = Topup::where(['nominal' => $unique + $nominal])->where(function ($query) {
            $query->where(['status' => 'Pending'])
            ->orWhereDate('created_at', '>=', today()->subDays(1));
        })->first();
        if (isset($topUp)) {
            // dd('kembar!');
            return $this->getUniqueNumber($nominal);
        } else {
            return $unique;
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'nominal' => 'required|numeric|min:10000',
            'bank_id' => 'required',
            'topup_transfer_type_id' => 'required'
        ];
        $message = [
            'required' => 'tidak boleh kosong',
            'min' => 'minimum topup saldo adalah Rp 10.000'
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        // Pengecekan apakah ada request top up yang pending
        $topup_pending = Topup::where('user_id', auth()->user()->id)->where('status', 'pending')->first();
        if($topup_pending){
            return response()->json([
                'topup_pending' => true
            ], 200);
        }

        $unique = $this->getUniqueNumber($request->nominal);
        $bank = Bank::find($request->bank_id);
        $bonus = 0;
        try{
            if($request->topup_transfer_type_id == 4){ // Midtrans
                $topup = Topup::create([
                    'user_id' => auth()->user()->id,
                    'nominal' => $request->nominal,
                    'bank_id' => $bank->id,
                    'show_qr' => $bank->id == 13 ? 'qr' : 'auto',
                    'topup_transfer_type_id' => $request->topup_transfer_type_id
                ]);

                $app_config = new AppConfig();

                // Menghitung biaya fee admin dan ppn
                $ppn = $app_config->find('PPN')->value;
                $admin_fee = $app_config->find('MIDTRANS.ADMIN.FEE')->value + ($app_config->find('MIDTRANS.ADMIN.FEE')->value * ($ppn / 100));
                if($bank->id == 7 || $bank->id == 13){ // Gopay
                    $admin_fee = $request->nominal * ((2 + (2 * (11 / 100))) / 100);
                }

                $user = $topup->user;
                $midtrans = new CreateSnapTokenService($topup, $user, $admin_fee, $bank->value_midtrans);
                $snapToken = $midtrans->getSnapToken();
                $topup->snap_token = $snapToken;
                $topup->save();
            }else{
                $bonus = $this->bonusTopup($request->nominal + $unique);
                $topup = Topup::create([
                    'user_id' => auth()->user()->id,
                    'nominal' => $request->nominal + $unique,
                    'bonus' => $bonus,
                    'bank_id' => $bank->id,
                    'topup_transfer_type_id' => $request->topup_transfer_type_id
                ]);
            }

            $message = '*NEW REQUEST TOPUP*
nominal : ' . Helpers::format_rupiah($request->nominal + ($topup->bank->nama_bank == 'Midtrans' ? 0 : $unique)) . '
bonus : ' . ( $bonus != 0 ? Helpers::format_rupiah($bonus) : 0) . '
username : ' . auth()->user()->username . '
topup ID : ' . $topup->id . '
no WA : ' . auth()->user()->no_wa . '
type : ' . $topup->topupTransferType->type . '
bank : ' . $topup->bank->nama_bank;
            dispatch(new SendWaJob(AppConfig::find('CONTACT.OWNER')->value, $message));
            return response()->json(['success' => true]);
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topup  $topup
     * @return \Illuminate\Http\Response
     */
    public function show(Topup $topup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topup  $topup
     * @return \Illuminate\Http\Response
     */
    public function edit(Topup $topup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTopupRequest  $request
     * @param  \App\Models\Topup  $topup
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopupRequest $request, Topup $topup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topup  $topup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topup $topup)
    {
        //
    }

    public function history()
    {
        $title = 'Riwayat Isi Saldo';

        $topup = new Topup();
        if(!Gate::allows('admin')){
            $topups = $topup->orderBy('created_at', 'DESC')->where('user_id', auth()->user()->id)->where('note', '!=', 'manual')->get();
        }else{
            $topups = $topup->orderBy('created_at', 'DESC')->get();
        }
        $topup_pending = $topup->where('user_id', auth()->user()->id)->where('status', 'pending')->first();

        return view('saldo.history', compact(
            'title',
            'topup_pending',
            'topups'
        ));
    }

    public function topupManual(Request $request, Topup $topup)
    {
        // Pengambilan data untuk pengecekan topup ID
        if($request->topup_id){
            $topup = Topup::join('banks', 'topups.bank_id', '=', 'banks.id')
            ->join('users', 'topups.user_id', '=', 'users.id')
            ->join('topup_transfer_types', 'topups.topup_transfer_type_id', '=', 'topup_transfer_types.id')
            ->select(
                'topups.id',
                'users.username as username',
                'users.id as user_id',
                'banks.nama_bank as nama_bank',
                'topup_transfer_types.type as transfer_type',
                'topups.nominal',
                'topups.status',
                'banks.id as bank_id',
                'topup_transfer_types.id as topup_transfer_type_id'
            )
            ->where('topups.id', $request->topup_id)
            ->first();
            if($topup){
                if($topup->status == 'paid'){
                    return response()->json([
                        'paid' => true,
                        'message' => 'Topup Id tersebut telah paid'
                    ], 200);
                }
                return $topup;
            }else{
                return response()->json([
                    'topupId' => false,
                    'message' => 'Topup Id tidak ditemukan'
                ], 400);
            }
        }

        $title = 'Topup Manual';
        $topup_logs = TopupLog::orderBy('id', 'DESC')->get();

        return view('saldo.topupManual', compact(
            'title',
            'topup_logs'
        ));
    }

    public function topupManualStore(Request $request)
    {
        $rules = [
            'username' => 'required',
            'nominal' => 'required|numeric|min:5000',
            'note' => 'required',
        ];
        $messages = [
            'required' => 'tidak boleh kosong!',
            'numeric' => 'tidak boleh selain angka!',
            'min' => 'minimal topup manual adalah Rp 5.000'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('username', $request->username)->first();
        if(!$user){
            return response()->json(['message' => 'Username tidak ditemukan!'], 400);
        }
        $last_topup_id = Topup::orderBy('id', 'DESC')->first();
        try{
            DB::beginTransaction();

            TopupLog::create([
                'user_id' => $user->id,
                'topup_id' => $last_topup_id->id + 1,
                'saldo_awal' => $user->saldo,
                'saldo_ditambahkan' => $request->nominal,
                'total_saldo' => $user->saldo + $request->nominal,
                'bank_id' => 5, // Manual
                'topup_transfer_type_id' => 3, // Manual
                'created_by' => auth()->user()->id,
                'note' => $request->note
            ]);

            $topup_created = Topup::create([
                'user_id' => $user->id,
                'topup_transfer_type_id' => 3, // Manual
                'nominal' => $request->nominal,
                'bank_id' => 5, // Manual
                'status' => 'paid',
                'note' => 'manual'
            ]);

            // Log saldo
            Helpers::logSaldo('topup', $user->saldo, ($user->saldo + $request->nominal), null, $topup_created);

            $user->saldo += $request->nominal;
            $user->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            Log::error("error topup manual : " . $e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function topupManualTidakSesuai3Digit(Request $request)
    {
        $rules = [
            'nominal_transfer' => 'required|numeric|min:10000'
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::find($request->user_id);
        $topup = Topup::find($request->topup_id);
        $note = 'Transfer tidak sesuai 3 digit [' . Helpers::format_rupiah($request->nominal) . ' | ' . Helpers::format_rupiah($request->nominal_transfer) .']';
        try{
            DB::beginTransaction();
            // Create log topup
            TopupLog::create([
                'user_id' => $request->user_id,
                'topup_id' => $request->topup_id,
                'saldo_awal' => $user->saldo,
                'saldo_ditambahkan' => $request->nominal_transfer,
                'total_saldo' => $user->saldo + $request->nominal_transfer,
                'bank_id' => $request->bank_id,
                'topup_transfer_type_id' => $request->topup_transfer_type_id,
                'note' => $note,
                'created_by' => auth()->user()->id
            ]);

            // Log saldo
            Helpers::logSaldo('topup', $user->saldo, ($user->saldo + $request->nominal_transfer), null, $topup);

            // Update user saldo
            $user->update([
                'saldo' => $user->saldo + $request->nominal_transfer
            ]);

            // update note di tabel topups
            Topup::find($request->topup_id)->update([
                'status' => 'paid',
                'note' => $note
            ]);
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json($e);
        }

    }

    public function cancelTopup(Topup $topup)
    {
        // Pengecekan request cancel hanya dari si pemilik topup pending request
        if($topup->user_id != auth()->user()->id){
            return 'The cancel topup process cannot be continued';
        }

        try{
            if($topup->status != 'pending'){
                return response()->json([
                    'message' => 'Status topup telah ' . $topup->status
                ], 400);
            }
            $topup->update([
                'status' => 'cancel',
                'note' => 'cancel by ' . auth()->user()->username,
                'updated_by' => auth()->user()->id
            ]);

                $no_wa = AppConfig::find('CONTACT.OWNER')->value;
                $message = '*TOPUP CANCEL MANUAL*
transfer type : ' . $topup->topupTransferType->type . ' - ' . $topup->bank->nama_bank . '
cancel by : ' . $topup->user->username . '
topup ID : ' . $topup->id;
                dispatch(new SendWaJob($no_wa, $message));
        }catch(\Exception $e){
            return response()->json($e->getMessage());
        }
    }

    public function paidTopup(Topup $topup)
    {
        $user = User::where('id', $topup->user_id)->first();
        try{
            DB::beginTransaction();
            // Update status topup user
            $topup->update([
                'status' => 'paid',
                'note' => 'paid by admin',
                'updated_by' => auth()->user()->id
            ]);

            // Create log topup
            $tes = TopupLog::create([
                'user_id' => $topup->user_id,
                'topup_id' => $topup->id,
                'saldo_awal' => $user->saldo,
                'saldo_ditambahkan' => $topup->nominal + $topup->bonus,
                'total_saldo' => $user->saldo + $topup->nominal + $topup->bonus,
                'bank_id' => $topup->bank_id,
                'topup_transfer_type_id' => $topup->topup_transfer_type_id,
                'note' => 'paid by admin',
                'created_by' => auth()->user()->id
            ]);

            // Log saldo
            $log_saldo = Helpers::logSaldo('topup', $user->saldo, ($user->saldo + $topup->nominal + $topup->bonus), null, $topup);

            // Tambah saldo user
            $user->saldo += $topup->nominal + $topup->bonus;
            $user->save();
            DB::commit();

            $no_wa = $topup->user->no_wa;
                        $message = '*TOPUP ' . AppConfig::find('WEB.NAME')->value . ' BERHASIL*
Topup ID' . $topup->id . '

Saldo telah berhasil ditambahkan sebesar : ' . Helpers::format_rupiah($topup->nominal + $topup->bonus) . '

Terima kasih, selamat berbelanja 😊🙏';
                        dispatch(new SendWaJob($no_wa, $message));

                        $no_wa2 = '089656911145';
                        $message2 = '*TOPUP ' . AppConfig::find('WEB.NAME')->value . ' BERHASIL*
Topup ID : ' . $topup->id . '
Username : ' . $topup->user->username . '
Nominal : ' . Helpers::format_rupiah($topup->nominal + $topup->bonus) . '
Note : ' . $topup->note . '

_Topup has been confirmed manually by ' . auth()->user()->username . '._

Thankyou 😊🙏';
                        dispatch(new SendWaJob($no_wa2, $message2));

            return response()->json(['total_saldo' => $user->saldo], 200);
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }

    private function bonusTopup($nominal)
    {
        $persentase_bonus = AppConfig::find('BONUS_TOPUP.BCA')->value;
        $bonus = floor(($nominal * ($persentase_bonus + auth()->user()->levelMember->bonus_topup)) / 100);

        return $bonus;
    }

    public function mutasiCheck()
    {
        if(!Gate::allows('admin')){
            Alert::error('Forbidden Button!');
            return back();
        }

        $parser = new BCAParser(config('bca.username'), config('bca.password'));
        $start_date = now()->subDays(1)->format('Y-m-d');
        $end_date = now()->format('Y-m-d');

        $all_mutasi = $parser->getListTransaksi($start_date, $end_date);
        foreach($all_mutasi as $mutasi){
            $panjang_array = count($mutasi['description']);
            $nominal_mutasi = (int)str_replace(",", "", $mutasi['description'][$panjang_array - 1]);
            // $nama_pengirim = $mutasi['description'][$panjang_array - 3];
            // $catatan = $mutasi['description'][$panjang_array - 4];
            // Log::info([$mutasi]);
            // Log::info(['COUNT', count($mutasi['description'])]);
            // Log::info([$mutasi['date'], $nama_pengirim, $nominal_mutasi, $catatan]);

            $topup_pendings = Topup::where('status', 'Pending')
            ->where('topup_transfer_type_id', 1)
            ->get();
            foreach($topup_pendings as $topup_pending){
                if($nominal_mutasi == $topup_pending->nominal && $mutasi['date'] == ('PEND' ?? $topup_pending->created_at->format('Y-m-d'))){
                    try{
                        $user = User::where('id', $topup_pending->user_id)->first();
                        DB::beginTransaction();
                        // Update status topup user
                        $topup_pending->update([
                            'status' => 'paid',
                            'note' => 'auto paid',
                            'updated_by' => 1
                        ]);

                        // Create log topup
                        TopupLog::create([
                            'user_id' => $topup_pending->user_id,
                            'topup_id' => $topup_pending->id,
                            'saldo_awal' => $user->saldo,
                            'saldo_ditambahkan' => $topup_pending->nominal + $topup_pending->bonus,
                            'total_saldo' => $user->saldo + $topup_pending->nominal,
                            'bank_id' => $topup_pending->bank_id,
                            'topup_transfer_type_id' => $topup_pending->topup_transfer_type_id,
                            'note' => 'auto paid',
                            'created_by' => $user->id
                        ]);

                        // Log saldo
                        Helpers::logSaldo('topup', $user->saldo, ($user->saldo + $topup_pending->nominal + $topup_pending->bonus), null, $topup_pending);

                        // Tambah saldo user
                        $user->saldo += $topup_pending->nominal + $topup_pending->bonus;
                        $user->save();
                        DB::commit();

                        Log::info("SUKSES PEMBAYARAN TERKONFIRMASI!");
                        $no_wa = $topup_pending->user->no_wa;
                        $message = '*TOPUP ' . AppConfig::find('WEB.NAME')->value . ' BERHASIL*
Topup ID' . $topup_pending->id . '

_Topup has been confirmed automatically by the system._

Thankyou 😊🙏';
                        dispatch(new SendWaJob($no_wa, $message));
                    }catch(\Exception $e){
                        DB::rollBack();
                        Log::info("ADA KESALAHAN PROSES CEK MUTASI!");
                        Log::info([$e->getMessage()]);
                    }
                }
            }
        }
        $parser->logout();
        Alert::success('Berhasil cek mutasi!');
        return back();
    }
}
