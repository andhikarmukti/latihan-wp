<?php

namespace App\Http\Controllers;

use App\Models\TopupTransferType;
use App\Http\Requests\StoreTopupTransferTypeRequest;
use App\Http\Requests\UpdateTopupTransferTypeRequest;

class TopupTransferTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTopupTransferTypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTopupTransferTypeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TopupTransferType  $topupTransferType
     * @return \Illuminate\Http\Response
     */
    public function show(TopupTransferType $topupTransferType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TopupTransferType  $topupTransferType
     * @return \Illuminate\Http\Response
     */
    public function edit(TopupTransferType $topupTransferType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTopupTransferTypeRequest  $request
     * @param  \App\Models\TopupTransferType  $topupTransferType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTopupTransferTypeRequest $request, TopupTransferType $topupTransferType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TopupTransferType  $topupTransferType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TopupTransferType $topupTransferType)
    {
        //
    }
}
