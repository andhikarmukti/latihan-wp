<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\User;
use App\Jobs\SendWaJob;
use app\Helpers\Helpers;
use App\Models\AppConfig;
use App\Models\LevelMember;
use App\Models\UpgradeLevel;
use Illuminate\Http\Request;
use App\Models\LogCommission;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreUpgradeLevelRequest;
use App\Http\Requests\UpdateUpgradeLevelRequest;

class UpgradeLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Upgrade Level';
        $nama_web = AppConfig::find('WEB.NAME')->value;
        $level_member = new LevelMember();

        if($request->bank){
            $bank = Bank::find(1);
            $nominal = auth()->user()->upgradeLevel->nominal;
            return response()->json([
                'bank' => $bank,
                'nominal' => $nominal
            ]);
        }

        return view('upgrade_level.index', compact(
            'title',
            'nama_web',
            'level_member'
        ));
    }

    public function listRequest()
    {
        $title = 'List Upgrade Level Request';
        $upgrade_requests = UpgradeLevel::orderBy('created_at', 'DESC')->get();

        return view('upgrade_level.listRequest', compact(
            'title',
            'upgrade_requests'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUpgradeLevelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'level' => 'required',
        ]);

        $level_member = LevelMember::where('level', $request->level)->first();

        $upgrade_level = UpgradeLevel::create([
            'user_id' => auth()->user()->id,
            'level_member_id' => $level_member->level,
            'nominal' => $level_member->harga_upgrade,
        ]);

        $no_wa = AppConfig::find('CONTACT.OWNER')->value;
        $message = '*REQUEST UPGRADE LEVEL*
username : ' . $upgrade_level->user->username .'
level : ' . $upgrade_level->levelMember->level_name .'
nomor WA : ' . $upgrade_level->user->no_wa;

        dispatch(new SendWaJob($no_wa, $message));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpgradeLevel  $upgradeLevel
     * @return \Illuminate\Http\Response
     */
    public function show(UpgradeLevel $upgradeLevel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpgradeLevel  $upgradeLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(UpgradeLevel $upgradeLevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUpgradeLevelRequest  $request
     * @param  \App\Models\UpgradeLevel  $upgradeLevel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUpgradeLevelRequest $request, UpgradeLevel $upgradeLevel)
    {
        if($request->action == 'decline'){
            UpgradeLevel::find($request->upgrade_level_id)->update([
                'status' => $request->action
            ]);
        }else{
            Helpers::upgradeLevelMember($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpgradeLevel  $upgradeLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(UpgradeLevel $upgradeLevel)
    {
        //
    }
}
