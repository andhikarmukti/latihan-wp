<?php

namespace App\Http\Controllers;

use App\Models\LevelMember;
use App\Models\UpgradeLevelPromo;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreUpgradeLevelPromoRequest;
use App\Http\Requests\UpdateUpgradeLevelPromoRequest;

class UpgradeLevelPromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $title = 'Upgrade Level Promo';
        $level_members = LevelMember::all();
        $upgrade_level_promos = UpgradeLevelPromo::all();

        return view('upgrade_level.promo', compact(
            'title',
            'level_members',
            'upgrade_level_promos'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUpgradeLevelPromoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpgradeLevelPromoRequest $request)
    {
        UpgradeLevelPromo::create([
            'level_member_id' => $request->level_member_id,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'created_by' => auth()->user()->id
        ]);

        Alert::success('Berhasil menambahkan promo upgrade level member!');
        return redirect('/admin-upgrade-level-promo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UpgradeLevelPromo  $upgradeLevelPromo
     * @return \Illuminate\Http\Response
     */
    public function show(UpgradeLevelPromo $upgradeLevelPromo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UpgradeLevelPromo  $upgradeLevelPromo
     * @return \Illuminate\Http\Response
     */
    public function edit(UpgradeLevelPromo $upgradeLevelPromo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUpgradeLevelPromoRequest  $request
     * @param  \App\Models\UpgradeLevelPromo  $upgradeLevelPromo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUpgradeLevelPromoRequest $request, UpgradeLevelPromo $upgradeLevelPromo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UpgradeLevelPromo  $upgradeLevelPromo
     * @return \Illuminate\Http\Response
     */
    public function destroy(UpgradeLevelPromo $upgradeLevelPromo)
    {
        $upgradeLevelPromo->delete();
    }
}
