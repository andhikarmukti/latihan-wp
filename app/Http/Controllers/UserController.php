<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Server;
use GuzzleHttp\Client;
use App\Jobs\SendWaJob;
use Illuminate\Support\Str;
use App\Models\ServerAction;
use Illuminate\Http\Request;
use App\Models\ResetPassword;
use App\Models\ServerParameter;
use App\Models\ResetPasswordManual;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Jobs\SendEmailForgotPasswordJob;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Password;
use RealRashid\SweetAlert\Facades\Alert;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function apiBalance(Request $request)
    {
        if($request->api_key){
            $user = User::where('api_key', $request->api_key)->first();
            if(!$user){
                return response()->json([
                    'status' => false,
                    'data' => 'API key not found!'
                ], 400);
            }
        }
        // Pengecekan action ID
        $action_id = $request->action_id;
        if($request->api_key){
            if($request->action != 'balance'){
                return response()->json([
                    'status' => false,
                    'data' => 'Bad Action!'
                ], 400);
            }
        }

        return response()->json([
            'status' => true,
            'data' => [
                'balance' => $user->saldo
            ]
        ], 200);
    }

    public function users()
    {
        $title = 'Users';
        $users = User::all();

        return view('user.users', compact(
            'title',
            'users'
        ));
    }

    public function userEditAdmin(User $user)
    {
        dd($user);
    }

    public function profile()
    {
        $title = 'Profile';
        $server = Server::find(7);
        $client = new Client();
        $params = [
            'server_id' => $server->id,
            'action_id' => 4, // check balance action ID nya 4
        ];
        $server_action = ServerAction::where([
            ['server_id', $server->id],
            ['action_id', $params['action_id']]
        ])->first();
        $server_parameters = ServerParameter::where([
            ['server_id', $server->id],
            ['action_id', $params['action_id']]
        ])->get();

        $form_params = [];
        foreach($server_parameters as $server_parameter){
            $form_params[$server_parameter->parameter] = $params[$server_parameter->value] ?? ($server[$server_parameter->parameter] ?? $server_parameter->value);
        }
        try{
            $response = $client->request($server_action->method, $server->url . (isset($server_action->url_path) ? '/' . $server_action->url_path : ''), [
                'form_params' => $form_params
            ])->getBody();
        }catch(GuzzleException $e){
            Log::error('process API order error: ' . $e->getMessage());
            return response()->json($e->getMessage());
        }
        $response = json_decode($response);

        $saldo_pusat = $response->data->balance;
        $server_url = str_replace("/api", "",$server->url);

        return view('user.profile', compact(
            'title',
            'saldo_pusat',
            'server_url'
        ));
    }

    public function resetPasswordByUser(Request $request, User $user)
    {
        $rules = [
            'current_password' => 'required',
            'new_password' => 'required',
            'password_confirmation' => 'required',
        ];
        $validator = Validator::make($request->only('current_password', 'new_password', 'password_confirmation'), $rules);
        if($validator->fails()){
            Alert::error("Gagal!", "Pastikan password yang Anda masukan sudah benar");
            return back();
        }

        if (Hash::check($request->current_password, $user->password)) {
            if($request->new_password == $request->password_confirmation){
                $user->password = Hash::make($request->new_password);
                $user->save();

                // Cek apakah memiliki history reset password manual by admin sebelumnya
                $reset_manual = ResetPasswordManual::where('user_id', auth()->user()->id)->first();
                if($reset_manual){
                    $reset_manual->updated_by = auth()->user()->id;
                    $reset_manual->save();
                }

                Alert::success("Berhasil!", "Berhasil merubah password");
                return back();
            }else{
                Alert::error("Gagal!", "Konfirmasi password tidak sesuai");
                return back();
            }
        }else{
            Alert::error("Gagal!", "Password saat ini tidak sesuai");
            return back();
        }
    }

    public function forgotPassword()
    {
        $title = 'Forgot Password';

        return view('auth.forgot-password', compact(
            'title'
        ));
    }

    public function forgotPasswordPost(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();
        if($user){
            $token = Str::random(64);
            $reset_password = ResetPassword::create([
                'user_id' => $user->id,
                'token' => $token
            ]);
            $message = "Silahkan cek inbox / spam email Anda untuk melanjutkan proses pergantian password";

            $format = [
                'greeting' => 'Hi '.$user->username.',',
                'body' => 'Silahkan klik tombol Reset untuk melakukan reset password',
                'thanks' => 'Terima kasih, selamat berbelanja kembali di ' . url(''),
                'action_text' => 'Reset',
                'action_url' => url('/reset-password'),
                'token' => $reset_password->token,
                'id' => $user->id
            ];

            dispatch(new SendEmailForgotPasswordJob($user, $format));
            dispatch(new SendWaJob($user->no_wa, $message));
        }else{
            return response()->json([
                'message' => 'Failed! Wrong Email.'
            ], 400);
        }
    }

    public function formResetPassword($token)
    {
        $title = 'Reset Password';

        return view('resetPassword', compact(
            'title',
            'token'
        ));
    }

    public function formResetPasswordPost(Request $request, $token)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]);

        $user = User::where('email', $request->email)->first();
        $reset_password = ResetPassword::where([
            ['user_id', $user->id],
            ['token', $token],
        ])->first();

        // Jika token ngawur
        if(!$reset_password){
            Alert::error('Gagal!', 'Link Expired, silahkan lakukan ulang request reset passwordnya');
            return back();
        }
        // Cek expired date 30 menit
        $expiredTime = date("Y-m-d H:i:s",strtotime($reset_password->created_at) + 1800);
        if(now() > $expiredTime){
            Alert::error('Gagal!', 'Link Expired, silahkan lakukan ulang request reset passwordnya');
            return back();
        }

        $user->password = Hash::make($request->password);
        $user->save();
        Alert::success('Berhasil!', 'Password berhasil diubah');
        return redirect('/login');
    }

    public function generateApiKey(User $user)
    {
        $random_string = Str::random(40);

        $user->update([
            'api_key' => $random_string
        ]);
    }

    public function apiFetchUser(Request $request)
    {
        $user = User::where('api_key' , $request->api_key)->first();
        return response()->json([
            'success' => true,
            'data' => $user
        ]);
    }
}
