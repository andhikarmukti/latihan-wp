<?php

namespace App\Http\Controllers;

use app\Helpers\Helpers;
use App\Models\AppConfig;
use Illuminate\Support\Str;
use App\Models\ListReferral;
use App\Models\UserReferral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Requests\StoreUserReferralRequest;
use App\Http\Requests\UpdateUserReferralRequest;
use App\Jobs\SendWaJob;
use App\Models\BankOption;
use App\Models\LogCommission;
use App\Models\User;
use App\Models\Withdrawal;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class UserReferralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function referralCookie($referral)
    {
        Cookie::queue('reff', $referral, 43200);
        $user_referral = UserReferral::where('referral_code', $referral)->first();
        if($user_referral){
            $user_referral->visitors += 1;
            $user_referral->save();
        }
        return redirect('/register');
    }

    public function index()
    {
        $user_referral = UserReferral::where('user_id', auth()->user()->id)->first();
        $user = auth()->user();
        $title = 'Referral';
        $persentase_komisi = Helpers::percentCommission($user);
        $history_commissions = LogCommission::orderBy('id', 'DESC')
        ->where('user_upline_id', auth()->user()->id)
        ->where('status_order', 'Completed')
        ->get();
        $minimum_penarikan_komisi = AppConfig::find('MINIMUM.PENARIKAN.KOMISI')->value;

        if(Gate::allows('admin')){
            $history_commissions = LogCommission::orderBy('id', 'DESC')
            ->whereIn('status_order', ['Completed', 'Partial'])
            ->get();
        }

        return view('referral.index', compact(
            'title',
            'persentase_komisi',
            'user_referral',
            'history_commissions',
            'minimum_penarikan_komisi'
        ));
    }

    private function strRandom()
    {
        $code = Str::random(5);

        // cek unique referral_code
        $referral_check = UserReferral::where('referral_code', $code)->first();
        if($referral_check){
            $this->strRandom();
        }

        return $code;
    }

    public function generateLink(Request $request)
    {
        $code = $this->strRandom();

        UserReferral::updateOrCreate(
            [
                'user_id' => auth()->user()->id
            ],
            [
                'referral_code' => $code,
                'updated_by' => auth()->user()->id
            ]
    );

        Alert::success('Berhasil melakukan generate link referral');
        return redirect('/referral');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserReferralRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserReferralRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserReferral  $userReferral
     * @return \Illuminate\Http\Response
     */
    public function show(UserReferral $userReferral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserReferral  $userReferral
     * @return \Illuminate\Http\Response
     */
    public function edit(UserReferral $userReferral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserReferralRequest  $request
     * @param  \App\Models\UserReferral  $userReferral
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserReferralRequest $request, UserReferral $userReferral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserReferral  $userReferral
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserReferral $userReferral)
    {
        //
    }

    public function userReferral()
    {
        $title = 'User Referral';
        $user_referrals = UserReferral::orderBy('id', 'DESC')->get();

        return view('referral.userReferral', compact(
            'title',
            'user_referrals'
        ));
    }

    public function tarikKomisi(Request $request)
    {
        $user_referral = UserReferral::where('user_id', auth()->user()->id)->first();
        if(!$user_referral){
            Alert::error('Akses Dilarang, Silahkan generate link terlebih dahulu');
            return back();
        }
        $title = 'Tarik Komisi';
        $bank_option = new BankOption;
        $bank_options = $bank_option->all();
        $withdrawals = Withdrawal::orderBy('id', 'DESC')
        ->where('user_id', auth()->user()->id)->get();
        if(Gate::allows('admin')){
            $withdrawals = Withdrawal::orderBy('id', 'DESC')->get();
        }
        $minimum_penarikan_komisi = AppConfig::find('MINIMUM.PENARIKAN.KOMISI')->value;

        // Ajax Request Bank
        if($request->ajax()){
            if($request->keyword_bank == ''){

            }else{
                $bank_options = $bank_option->where('nama_bank', 'LIKE', '%' . $request->keyword_bank . '%')->get();
            }
            return response($bank_options);
        }

        return view('referral.tarikKomisi', compact(
            'title',
            'bank_options',
            'user_referral',
            'withdrawals',
            'minimum_penarikan_komisi'
        ));
    }

    public function dataBankStore(Request $request)
    {
        $rules = [
            'code_bank' => 'required',
            'atas_nama' => 'required',
            'no_rekening' => 'required',
        ];
        $message = [
            'code_bank.required' => 'Bank tidak boleh kosong',
            'atas_nama.required' => 'Atas nama pemilik rekening tidak boleh kosong',
            'no_rekening.required' => 'Nomor Rekening tidak boleh kosong',
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->first()
            ], 400);
        }

        $user_referral = UserReferral::where('user_id', auth()->user()->id)->first();
        $user_referral->update([
            'bank' => $request->code_bank,
            'no_rekening' => $request->no_rekening,
            'atas_nama' => $request->atas_nama,
            'status_bank' => 'pending',
        ]);

        $no_wa = AppConfig::find('CONTACT.OWNER')->value;
        $message = 'NEW REQUEST BANK

username : ' . $user_referral->user->username . '
        ';
        dispatch(new SendWaJob($no_wa, $message));

        return response()->json([
            'success' => true,
            'message' => 'Berhasil menambahkan data bank'
        ]);
    }

    public function deleteBankAccount(Request $request)
    {
        if(auth()->user()->userReferral->bank == null){
            return response()->json([
                'success' => false,
                'message' => 'Data bank tidak ditemukan!'
            ], 400);
        }

        auth()->user()->userReferral->bank = null;
        auth()->user()->userReferral->no_rekening = null;
        auth()->user()->userReferral->atas_nama = null;
        auth()->user()->userReferral->status_bank = 'pending';
        auth()->user()->userReferral->save();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil menghapus data bank'
        ], 200);
    }

    public function actionBankAccount(UserReferral $userReferral, Request $request)
    {
        $userReferral->status_bank = $request->action;
        $userReferral->save();

        $no_wa = $userReferral->user->no_wa;
        $no_rekening = $userReferral->no_rekening;
        $count = strlen($no_rekening) - 4;
        $no_rekening = substr_replace($no_rekening, str_repeat('⋆', $count), 3, $count);
        if($request->action == 'accept'){
            $message = '*INFO ' . AppConfig::find('WEB.NAME')->value . '*

Terdapat request data bank dari kakak dengan data sebagai berikut,

Bank : ' . $userReferral->bankOption->nama_bank . '
No Rekening : ' . $no_rekening . '
Atas Nama : ' . $userReferral->atas_nama . '
Status : *Accept*

Saat ini data tersebut telah dikonfirmasi oleh Admin kami.

Terima kasih 😊🙏
        ';
        }else{
$message = '*INFO ' . AppConfig::find('WEB.NAME')->value . '*

Terdapat request data bank dari kakak dengan data sebagai berikut,

Bank : ' . $userReferral->bankOption->nama_bank . '
No Rekening : ' . $no_rekening . '
Atas Nama : ' . $userReferral->atas_nama . '
Status : *Decline*

Mohon maaf data bank belum bisa dikonfirmasi oleh Admin. Silahkan ajukan ulang atau hubungi Admin kami.

Terima kasih 😊🙏
        ';
        }
        dispatch(new SendWaJob($no_wa, $message));

        return response()->json([
            'success' => true,
            'message' => 'Berhasil ' . $request->action . ' data bank'
        ], 200);
    }

    public function listDownline()
    {
        $title = 'List Downline';
        $list_downlines = User::where('user_upline_id', auth()->user()->id)->get();

        return view('referral.listDownline', compact(
            'title',
            'list_downlines'
        ));
    }
}
