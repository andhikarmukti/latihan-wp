<?php

namespace App\Http\Controllers;

use App\Models\WaBroadcast;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreWaBroadcastRequest;
use App\Http\Requests\UpdateWaBroadcastRequest;

class WaBroadcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'WA Broadcast';
        $wa_lists = WaBroadcast::orderBy('id', 'DESC')->get();
        $total_nomor = $wa_lists->count();
        $total_message_satu_null = $wa_lists->where('message_satu', null)->count();
        $total_message_dua_null = $wa_lists->where('message_dua', null)->count();
        $total_message_tiga_null = $wa_lists->where('message_tiga', null)->count();

        return view('wa.index', compact(
            'title',
            'wa_lists',
            'total_nomor',
            'total_message_satu_null',
            'total_message_dua_null',
            'total_message_tiga_null'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreWaBroadcastRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWaBroadcastRequest $request)
    {
        $wa_arrays = preg_split('/\r\n|\r|\n/', $request->no_wa);
        foreach($wa_arrays as $wa_array){
            $no_wa = str_replace("+", "", $wa_array);
            $no_wa = str_replace("-", "", $no_wa);
            $hp_alt = Str::startsWith($no_wa, '0') ? '62' . substr($no_wa, 1) : '0' . substr($no_wa, 2);
            $rules = [
                'no_wa' => 'required|string|min:8|unique:wa_broadcasts',
                'hp_alt' => 'unique:wa_broadcasts,no_wa',
            ];
            $message = [
                'unique' => 'Nomor sudah ada'
            ];
            $validator = Validator::make(array_merge($request->all(), ['hp_alt' => $hp_alt, 'no_wa' => $no_wa]), $rules, $message);
            if ($validator->fails()) {
                Log::error($validator->errors()->first());
            }else{
                WaBroadcast::create([
                    'no_wa' => $no_wa,
                    'type' => $request->type
                ]);
            }
        }
        Alert::success('Berhasil menambahkan nomor WA');
        return redirect('/wa-broadcast');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WaBroadcast  $waBroadcast
     * @return \Illuminate\Http\Response
     */
    public function show(WaBroadcast $waBroadcast)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WaBroadcast  $waBroadcast
     * @return \Illuminate\Http\Response
     */
    public function edit(WaBroadcast $waBroadcast)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateWaBroadcastRequest  $request
     * @param  \App\Models\WaBroadcast  $waBroadcast
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWaBroadcastRequest $request, WaBroadcast $waBroadcast)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WaBroadcast  $waBroadcast
     * @return \Illuminate\Http\Response
     */
    public function destroy(WaBroadcast $waBroadcast)
    {
        //
    }

    public function stopBroadcast(UpdateWaBroadcastRequest $request,WaBroadcast $waBroadcast)
    {
        if($request->action == 'stop'){
            $waBroadcast->is_active = 0;
            $waBroadcast->save();

            return response()->json([
                'success' => true,
                'message' => 'Broadcast telah dihentikan'
            ]);
        }else{
            $waBroadcast->is_active = 1;
            $waBroadcast->save();

            return response()->json([
                'success' => true,
                'message' => 'Broadcast telah dijalankan'
            ]);
        }
    }
}
