<?php

namespace App\Http\Controllers;

use app\Helpers\Helpers;
use App\Models\AppConfig;
use App\Models\Withdrawal;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreWithdrawalRequest;
use App\Http\Requests\UpdateWithdrawalRequest;
use App\Jobs\SendWaJob;
use App\Models\UserReferral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreWithdrawalRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWithdrawalRequest $request)
    {
        // Cegah jika masih ada withdrawal accept dalam kurun waktu 30 hari terakhir
        $withdrawal_accept_last_month = Withdrawal::where('user_id', auth()->user()->id)
        ->where('status', 'accept')
        ->where('created_at', '>', now()->subDay(30))
        ->first();
        if($withdrawal_accept_last_month){
            Alert::error('Gagal!', 'Maksimum request withdrawal adalah 1 kali setiap 30 hari');
            return back();
        }

        // Cegah jika ada request withdrawal yang masih pending
        $withdrawal_pending = Withdrawal::where('user_id', auth()->user()->id)
        ->where('status', 'pending')
        ->first();
        if($withdrawal_pending){
            Alert::error('Gagal!', 'Terdapat request withdrawl yang masih pending');
            return back();
        }

        // Cegah jika bank belum accept oleh admin
        if(auth()->user()->userReferral->status_bank != 'accept'){
            Alert::error('Gagal!', 'Tidak ada data Bank yang verified');
            return back();
        }
        $minimum_penarikan_komisi = AppConfig::find('MINIMUM.PENARIKAN.KOMISI')->value;
        $maksimum_penarikan_komisi = auth()->user()->userReferral->total_commission;
        $rules = [
            'nominal' => 'required|numeric|min:' . (int)$minimum_penarikan_komisi . '|max:' . $maksimum_penarikan_komisi,
            'jenis_penarikan' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules, [
            'nominal.min' => 'Minimum penarikan adalah ' . Helpers::format_rupiah($minimum_penarikan_komisi),
            'nominal.max' => 'Maksimum penarikan adalah ' . Helpers::format_rupiah($maksimum_penarikan_komisi)
        ]);
        if($validator->fails()){
            Alert::error('Gagal!', $validator->errors()->first());
            return back();
        }

        $wihtdrawal = Withdrawal::create([
            'user_id' => auth()->user()->id,
            'nominal' => $request->nominal,
            'is_saldo' => $request->jenis_penarikan == 'saldo' ? true : false,
            'is_transfer' => $request->jenis_penarikan == 'transfer' ? true : false,
            'created_by' => auth()->user()->id,
        ]);

        $no_wa = AppConfig::find('CONTACT.OWNER')->value;
        $jenis_withdrawal = $wihtdrawal->is_saldo == true ? 'Saldo' : 'Transfer';
        $message = '*NEW REQUEST WITHDRAWAL*

request ID : ' . $wihtdrawal->id . '
username : ' . $wihtdrawal->user->username . '
nominal : ' . Helpers::format_rupiah($wihtdrawal->nominal) . '
jenis : ' . $jenis_withdrawal . '
        ';
        dispatch(new SendWaJob($no_wa, $message));

        Alert::success('Berhasil!', 'Berhasil melakukan request withdrawal.');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return \Illuminate\Http\Response
     */
    public function show(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return \Illuminate\Http\Response
     */
    public function edit(Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateWithdrawalRequest  $request
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWithdrawalRequest $request, Withdrawal $withdrawal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Withdrawal  $withdrawal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Withdrawal $withdrawal)
    {
        //
    }

    public function acceptWithdrawal(Request $request, Withdrawal $withdrawal)
    {
        $no_wa = $withdrawal->user->no_wa;
        $jenis_wihtdrawal = $withdrawal->is_saldo == true ? 'Saldo' : 'Transfer';

        try{
            DB::beginTransaction();
            $withdrawal->status = $request->action;
            $withdrawal->save();

            if($request->action == 'accept'){
                $withdrawal->userReferral->total_commission -= $withdrawal->nominal;
                $withdrawal->userReferral->total_withdrawal += $withdrawal->nominal;
                $withdrawal->userReferral->save();
                if($withdrawal->is_saldo == true){
                    $withdrawal->user->saldo += $withdrawal->nominal;
                    $withdrawal->user->save();
                }
                $message = '*PENARIKAN KOMISI BERHASIL*

Withdrawal ID : ' . $withdrawal->id . '
Nominal : ' . Helpers::format_rupiah($withdrawal->nominal) . '
Jenis : ' . $jenis_wihtdrawal . '
Status : *Accept*

Terima kasih 😊🙏
                ';
            }else{
                $message = '*PENARIKAN KOMISI GAGAL*

Withdrawal ID : ' . $withdrawal->id . '
Nominal : ' . Helpers::format_rupiah($withdrawal->nominal) . '
Jenis : ' . $jenis_wihtdrawal . '
Status : *Decline*

Silahkan coba lagi atau hubungi Admin kami.
Terima kasih 😊🙏
                ';
            }
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }

        Helpers::logSaldo('komisi', $withdrawal->user->saldo, ($withdrawal->user->saldo + $withdrawal->nominal), $withdrawal, null);
        dispatch(new SendWaJob($no_wa, $message));
        return response()->json([
            'success' => true,
            'message' => ($request->action == 'accept' ? 'Approved' : 'Decline')
        ], 200);
    }
}
