<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\HistoryOrder;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Gate;

class YajraController extends Controller
{
    public function historyOrder()
    {
        $history_orders = HistoryOrder::from('history_orders as ho')
        ->leftJoin('services as s', 's.id', 'ho.service_id')
        ->leftJoin('servers as sv', 'sv.id', 'ho.server_id')
        ->leftJoin('users as u', 'u.id', 'ho.user_id')
        ->select(
            'ho.*',
            's.service_name',
            'sv.nama_server as server_name',
            'u.username',
        )
        ->limit(25);

        if(!Gate::allows('admin')){ // sementara admin dulu, cs nanti diperbaiki lagi role nya
            $history_orders = $history_orders->where('user_id', auth()->user()->id);
        }

        $datatables = DataTables::of($history_orders);
        // ->editColumn('server_id', function($history_orders){
        //     return $history_orders->server->nama_server;
        // })
        // ->editColumn('user_id', function($history_orders){
        //     return $history_orders->user->username;
        // });


        $datatables = $datatables->make(true);

        return $datatables;
    }

    public function usersList()
    {
        $users = User::from('users as u')
        ->leftJoin('level_members as lb', 'lb.id', 'u.level_member_id')
        ->select(
            'u.*',
            'lb.level_name'
        )
        ->limit(30);

        $datatables = DataTables::of($users)
        ->addColumn('status_active', function($users){
            return $users->statusActive($users->id);
        })
        ->addColumn('saldo_level_digunakan', function($users){
            return $users->upgradeLevelAccept ? $users->upgradeLevelAccept->saldo_digunakan :  '-';
        })
        ->editColumn('last_seen', function($users){
            $different_time = Carbon::parse($users->last_seen)->diffForHumans();
            return $users->last_seen ? $different_time : '';
        })
        ->editColumn('created_at', function($users){
            $different_time = Carbon::parse($users->created_at)->diffForHumans();
            return $users->created_at ? $different_time : '';
        })
        ->editColumn('last_login_at', function($users){
            $different_time = Carbon::parse($users->last_login_at)->diffForHumans();
            return $users->last_login_at ? $different_time : '';
        })
        ->editColumn('user_upline_id', function($users){
            $user_upline = $users->where('id', $users->user_upline_id)->first();
            $username = $user_upline ? $user_upline->username : '';
            return $username;
        })
        ->rawColumns(['status_active']);

        $datatables = $datatables->make(true);

        return $datatables;
    }
}
