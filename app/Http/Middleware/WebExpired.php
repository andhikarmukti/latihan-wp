<?php

namespace App\Http\Middleware;

use App\Models\AppConfig;
use Closure;
use Illuminate\Http\Request;

class WebExpired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $expired_date = AppConfig::find('WEB.EXPIRED')->value;

        if($expired_date <= now()){
            return redirect('/expired');
        }
        return $next($request);
    }
}
