<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use App\Models\AppConfig;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SendOtpJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $otp;
    private $hp_alt;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($otp, $hp_alt)
    {
        $this->otp = $otp;
        $this->hp_alt = $hp_alt;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = 'Nomor OTP Registrasi Akun ' . AppConfig::find('WEB.NAME')->value . '

Mohon tidak memberitahukan nomor OTP ini ke siapapun 🤫

OTP : ' . $this->otp;

        $device_id = AppConfig::find('WHATSAPP.DEVICE.ID')->value;
        $url = 'https://app.whacenter.com/api/send';
        $form_params = [
            'device_id' => $device_id,
            'number' => $this->hp_alt,
            'message' => $message
        ];

        $client = New Client();
        try{
            $response = $client->request('POST', $url, [
                'form_params' => $form_params
            ])->getBody();
        }catch(GuzzleException $e){
            Log::error('process send OTP error: ' . $e->getMessage());
            return response()->json($e->getMessage());
        }
    }
}
