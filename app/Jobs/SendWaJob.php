<?php

namespace App\Jobs;

use App\Models\AppConfig;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SendWaJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $no_wa;
    private $message;

    public function __construct($no_wa, $message)
    {
        $this->no_wa = $no_wa;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $device_id = AppConfig::find('WHATSAPP.DEVICE.ID')->value;
        $url = 'https://app.whacenter.com/api/send';
        $form_params = [
            'device_id' => $device_id,
            'number' => $this->no_wa,
            'message' => $this->message
        ];

        $client = New Client();
        $response = $client->request('POST', $url, [
            'form_params' => $form_params
        ])->getBody();
    }
}
