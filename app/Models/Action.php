<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function serverActions()
    {
        return $this->hasMany(ServerAction::class, 'action_id', 'id');
    }

    public function actions()
    {
        return $this->hasMany(Action::class, 'action_id', 'id');
    }
}
