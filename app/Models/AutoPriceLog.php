<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AutoPriceLog extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'product_id');
    }

    public function serviceInformationUpdate()
    {
        return $this->hasMany(ServiceInformationUpdate::class, 'auto_price_log_id', 'id');
    }
}
