<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function topups()
    {
        return $this->hasMany('bank_id', 'id');
    }

    public function transferType()
    {
        return $this->belongsTo(TopupTransferType::class, 'topup_transfer_type_id', 'id');
    }
}
