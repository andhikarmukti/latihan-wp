<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    public function levelMember()
    {
        return $this->hasOne(LevelMember::class, 'id', 'level_member_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
