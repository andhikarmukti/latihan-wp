<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryOrder extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $dates = ['pengajuan_speedup'];

    public function server()
    {
        return $this->hasOne(Server::class, 'id', 'server_id');
    }

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function refillStatus()
    {
        return $this->hasOne(RefillStatus::class, 'history_order_id', 'id')->latest();
    }
}
