<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogCommission extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function userDownline()
    {
        return $this->belongsTo(User::class, 'user_downline_id', 'id');
    }

    public function userUpline()
    {
        return $this->belongsTo(User::class, 'user_upline_id', 'id');
    }
}
