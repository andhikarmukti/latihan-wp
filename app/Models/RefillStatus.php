<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefillStatus extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function historyOrder()
    {
        return $this->hasOne(HistoryOrder::class, 'id', 'history_order_id');
    }
}
