<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function services()
    {
        return $this->hasMany(Service::class, 'server_id', 'id');
    }

    public function historyOrders()
    {
        return $this->hasMany(HistoryOrder::class, 'server_id', 'id');
    }

    public function serverParameters()
    {
        return $this->hasMany(ServerParameter::class, 'server_id', 'id');
    }

    public function serverActions()
    {
        return $this->hasMany(ServerAction::class, 'server_id', 'id');
    }
}
