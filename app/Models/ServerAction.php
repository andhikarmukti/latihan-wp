<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServerAction extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function action()
    {
        return $this->hasOne(Action::class, 'id', 'action_id');
    }
}
