<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServerParameter extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function server()
    {
        return $this->hasOne(Server::class, 'id', 'server_id');
    }

    public function action()
    {
        return $this->hasOne(Action::class, 'id', 'action_id');
    }
}
