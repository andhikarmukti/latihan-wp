<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function server()
    {
        return $this->hasOne(Server::class, 'id', 'server_id');
    }

    public function serviceCategory()
    {
        return $this->hasOne(ServiceCategory::class, 'id', 'service_category_id');
    }

    public function historyOrders()
    {
        return $this->hasMany(ServiceCategory::class, 'service_id', 'id');
    }

    public function discounts()
    {
        return $this->hasMany(Discount::class, 'service_id', 'id');
    }

    public function serviceType()
    {
        return $this->belongsTo(ServiceType::class, 'type_id', 'id');
    }

    public function specialPrice()
    {
        return $this->hasOne(SpecialPrice::class, 'service_id', 'id');
    }
}
