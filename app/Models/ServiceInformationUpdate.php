<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceInformationUpdate extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $casts = [
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }

    public function autoPriceLog()
    {
        return $this->hasOne(AutoPriceLog::class, 'id', 'auto_price_log_id');
    }
}
