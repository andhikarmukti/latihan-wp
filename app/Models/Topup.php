<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Topup extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function bank()
    {
        return $this->hasOne(Bank::class, 'id', 'bank_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function topupTransferType()
    {
        return $this->hasOne(TopupTransferType::class, 'id', 'topup_transfer_type_id');
    }

    public function topupLog()
    {
        return $this->hasOne(TopupLog::class, 'topup_id', 'id');
    }
}
