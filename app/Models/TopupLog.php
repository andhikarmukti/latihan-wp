<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopupLog extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function bank()
    {
        return $this->hasOne(Bank::class, 'id', 'bank_id');
    }

    public function topupTransferType()
    {
        return $this->hasOne(TopupTransferType::class, 'id', 'topup_transfer_type_id');
    }

    public function topup()
    {
        return $this->hasOne(Topup::class, 'id', 'topup_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
