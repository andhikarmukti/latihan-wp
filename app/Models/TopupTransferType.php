<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopupTransferType extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function topups()
    {
        return $this->hasMany(Topup::class, 'topup_transfer_type_id', 'id');
    }

    public function banks()
    {
        return $this->hasMany(Bank::class, 'topup_transfer_type_id', 'id')->where('is_active', true);
    }
}
