<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpgradeLevelPromo extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function levelMember()
    {
        return $this->belongsTo(LevelMember::class, 'level_member_id');
    }
}
