<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    /**
     * The access token the user is using for the current request.
     *
     * @var Laravel\Sanctum\HasApiTokens
     */
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function historyOrders()
    {
        return $this->hasMany(HistoryOrder::class, 'user_id', 'id');
    }

    public function topups()
    {
        return $this->hasMany('user_id', 'id');
    }

    public function topupLogs()
    {
        return $this->hasMany(TopupLog::class, 'user_id', 'id');
    }

    public function levelMember()
    {
        return $this->hasOne(LevelMember::class, 'id', 'level_member_id');
    }

    public function upgradeLevel()
    {
        return $this->hasOne(UpgradeLevel::class, 'user_id', 'id')->where('status', 'pending')->orderBy('id', 'DESC')->latest();
    }

    public function upgradeLevelAccept()
    {
        return $this->hasOne(UpgradeLevel::class, 'user_id', 'id')->where('status', 'accept')->orderBy('id', 'DESC')->latest();
    }

    public function userReferral()
    {
        return $this->hasOne(UserReferral::class, 'user_id', 'id');
    }

    public function downlines()
    {
        return $this->hasMany(ListReferral::class, 'user_downline_id', 'id');
    }

    public function logCommissions()
    {
        return $this->hasMany(LogCommission::class, 'user_upline_id', 'id');
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'user_id', 'id');
    }

    public function specialPrices()
    {
        return $this->hasMany(SpecialPrice::class, 'user_id', 'id')->where('status', 'enable');
    }

    public function statusActive($id)
    {
        $last_seen =  self::whereId($id)->first()->last_seen;
        if($last_seen > date_format(now()->subMinutes(2), "Y-m-d H:i:s")){
            return '<span class="text-green-500">✦</span>';
        }else{
            return '<span class="text-red-500">✦</span>';
        }
    }
}
