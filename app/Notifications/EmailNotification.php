<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailNotification extends Notification
{
    use Queueable;
    protected $format;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($format)
    {
        $this->format = $format;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = $this->format['action_url'];
        return (new MailMessage)
        ->greeting($this->format['greeting'])
        ->line($this->format['body'])
        ->action($this->format['action_text'], $url . '/' . $this->format['token'])
        ->line($this->format['thanks']);
    }

    public function toDatabase($notifiable)
    {
        return [
            'user_id' => $notifiable->id
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
