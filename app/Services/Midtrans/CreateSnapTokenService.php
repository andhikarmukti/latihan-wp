<?php

namespace App\Services\Midtrans;

use Midtrans\Snap;
use App\Models\AppConfig;

class CreateSnapTokenService extends Midtrans
{
    protected $topup;
    protected $user;
    protected $admin_fee;
    protected $midtrans_payment;

    public function __construct($topup, $user, $admin_fee, $midtrans_payment)
    {
        parent::__construct();

        $this->topup = $topup;
        $this->user = $user;
        $this->admin_fee = $admin_fee;
        $this->midtrans_payment = $midtrans_payment;
    }

    public function getSnapToken()
    {
        $params = [
            'transaction_details' => [
                'order_id' => $this->topup['id'],
                'gross_amount' => $this->topup['nominal'],
            ],
            "enabled_payments" => [
                $this->midtrans_payment
            ],
            'item_details' => [
                [
                    'id' => $this->topup['id'],
                    'quantity' => 1,
                    'price' => $this->topup['nominal'],
                    'name' => 'Topup Saldo ' . AppConfig::find('WEB.NAME')->value,
                ],
                [
                    'id' => $this->topup['id'],
                    'quantity' => 1,
                    'price' => $this->admin_fee,
                    'name' => 'Admin Fee Midtrans',
                ]
            ],
            // 'callbacks' => [
            //     'finish' => 'https://keranjangindo.com/riwayat-isi-saldo',
            //     'unfinish' => 'https://keranjangindo.com/riwayat-isi-saldo',
            //     'error' => 'https://keranjangindo.com/riwayat-isi-saldo',
            // ],
            'customer_details' => [
                'first_name' => $this->user['username'],
                'email' => $this->user['email'],
                'phone' => $this->user['no_wa'],
            ]
        ];

        $snapToken = Snap::getSnapToken($params);

        return $snapToken;
    }
}
