<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_logs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('topup_id');
            $table->integer('saldo_awal');
            $table->decimal('saldo_ditambahkan');
            $table->integer('total_saldo');
            $table->foreignId('bank_id');
            $table->foreignId('topup_transfer_type_id');
            $table->foreignId('created_by');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_logs');
    }
};
