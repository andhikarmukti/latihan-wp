<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('service_name');
            $table->foreignId('service_category_id');
            $table->foreignId('type_id');
            $table->foreignId('server_id');
            $table->integer('server_service_id');
            $table->bigInteger('order_id_server');
            $table->integer('min');
            $table->integer('max');
            $table->integer('speed')->nullable();
            $table->integer('unit')->default(1000);
            $table->decimal('hpp', 12,4);
            $table->boolean('refill')->default(0);
            $table->boolean('cancel')->default(0);
            $table->boolean('is_active')->default(1);
            $table->boolean('auto_price')->default(1);
            $table->text('member_info');
            $table->dateTime('last_completed')->nullable();
            $table->foreignId('created_by');
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
};
