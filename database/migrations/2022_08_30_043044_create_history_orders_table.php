<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('service_category_id');
            $table->foreignId('service_id');
            $table->text('target');
            $table->foreignId('server_id');
            $table->integer('server_service_id');
            $table->integer('quantity');
            $table->integer('start_count')->nullable();
            $table->integer('remain');
            $table->integer('harga');
            $table->decimal('discount', 6,3);
            $table->decimal('hpp', 12,4);
            $table->decimal('mark_up_server', 6,3);
            $table->decimal('mark_up_user_level', 6,3);
            $table->integer('saldo_awal');
            $table->integer('sisa_saldo');
            $table->longText('sisa_saldo')->nullable();
            $table->string('note')->default('-');
            $table->enum('status', ['Pending','Processing','In Progress','Completed','Canceled','Partial','Waiting'])->default('Pending');
            $table->dateTime('pengajuan_speedup')->nullable();
            $table->dateTime('refill_date_end')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->boolean('refill_status')->default(0);
            $table->foreignId('created_by');
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_orders');
    }
};
