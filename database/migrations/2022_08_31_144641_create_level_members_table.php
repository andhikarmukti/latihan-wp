<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_members', function (Blueprint $table) {
            $table->id();
            $table->integer('level');
            $table->string('level_name');
            $table->integer('harga_upgrade');
            $table->decimal('mark_up', 6,3);
            $table->integer('perpanjang_auto');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_members');
    }
};
