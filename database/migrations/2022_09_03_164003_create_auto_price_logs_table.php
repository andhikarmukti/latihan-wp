<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_price_logs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('service_id');
            $table->integer('old_hpp');
            $table->integer('new_hpp');
            $table->integer('old_rate');
            $table->integer('new_rate');
            $table->integer('old_status_active');
            $table->integer('new_status_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_price_logs');
    }
};
