<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_information_updates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('auto_price_log_id');
            $table->foreignId('service_id');
            $table->string('info');
            $table->boolean('harga_naik');
            $table->boolean('harga_turun');
            $table->boolean('status_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_information_updates');
    }
};
