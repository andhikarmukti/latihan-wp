<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refill_statuses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->bigInteger('refill_id')->unsigned();
            $table->dateTime('pengajuan_refill');
            $table->foreignId('history_order_id');
            $table->integer('order_id_server');
            $table->string('target');
            $table->string('service');
            $table->string('status');
            $table->foreignId('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refill_statuses');
    }
};
