<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upgrade_levels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('level_member_id');
            $table->integer('nominal');
            $table->dateTime('start_date')->default('1945-08-17 10:00');
            $table->dateTime('end_date')->default('1945-08-17 10:00');
            $table->integer('saldo_digunakan')->default(0)->unsigned();
            $table->string('status')->default('pending');
            $table->string('note')->default('-');
            $table->foreignId('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upgrade_levels');
    }
};
