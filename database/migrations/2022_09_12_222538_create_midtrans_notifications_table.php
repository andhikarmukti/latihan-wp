<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('midtrans_notifications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('topup_id');
            $table->string('payment_type');
            $table->string('gross_amount');
            $table->string('transaction_id');
            $table->string('transaction_status');
            $table->string('transaction_time');
            $table->string('status_code');
            $table->string('signature_key');
            $table->string('va_bank')->nullable();
            $table->string('va_number')->nullable();
            $table->longText('json_notif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('midtrans_notifications');
    }
};
