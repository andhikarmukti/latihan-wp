<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_saldos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->integer('saldo_awal');
            $table->integer('saldo_akhir');
            $table->integer('selisih');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_saldos');
    }
};
