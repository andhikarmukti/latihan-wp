<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_commissions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_upline_id');
            $table->foreignId('user_downline_id');
            $table->foreignId('service_id')->nullable();
            $table->integer('harga_order');
            $table->bigInteger('order_id_server')->nullable();
            $table->decimal('discount', 6,3)->default(0);
            $table->integer('commission');
            $table->string('note')->nullable();
            $table->enum('status_order', ['Pending','Processing','In Progress','Completed','Canceled','Partial','Waiting'])->default('Pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_commissions');
    }
};
