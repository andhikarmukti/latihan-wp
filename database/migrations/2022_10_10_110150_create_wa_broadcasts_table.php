<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wa_broadcasts', function (Blueprint $table) {
            $table->id();
            $table->string('no_wa')->unique();
            $table->dateTime('message_satu')->nullable();
            $table->dateTime('message_dua')->nullable();
            $table->dateTime('message_tiga')->nullable();
            $table->dateTime('regis')->nullable();
            $table->dateTime('first_topup')->nullable();
            $table->dateTime('first_order')->nullable();
            $table->dateTime('promo')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wa_broadcasts');
    }
};
