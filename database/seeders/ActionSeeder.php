<?php

namespace Database\Seeders;

use App\Models\Action;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            [
                'process' => 'order'
            ],
            [
                'process' => 'status'
            ],
            [
                'process' => 'services'
            ],
            [
                'process' => 'balances'
            ],
            [
                'process' => 'refill'
            ],
            [
                'process' => 'refill_status'
            ],
        ];

        foreach($actions as $action){
            Action::create([
                'process' => $action['process']
            ]);
        }
    }
}
