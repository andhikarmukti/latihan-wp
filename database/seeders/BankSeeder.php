<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            [
                'topup_transfer_type_id' => 1,
                'nama_bank' => 'BCA',
                'no_rek' => '1450055260',
                'atas_nama' => 'Andhika Raharja Mukti'
            ],
            [
                'topup_transfer_type_id' => 1,
                'nama_bank' => 'Mandiri',
                'no_rek' => '123456789',
                'atas_nama' => 'Andhika Raharja Mukti'
            ],
            [
                'topup_transfer_type_id' => 2,
                'nama_bank' => 'Gopay',
                'no_rek' => '089656911145',
                'atas_nama' => 'Andhika Raharja Mukti'
            ],
            [
                'topup_transfer_type_id' => 2,
                'nama_bank' => 'Dana',
                'no_rek' => '089656911145',
                'atas_nama' => 'Andhika Raharja Mukti'
            ],
            [
                'topup_transfer_type_id' => 3,
                'nama_bank' => 'Manual',
                'no_rek' => '089656911145',
                'atas_nama' => 'Andhika Raharja Mukti'
            ],
        ];

        foreach ($banks as $bank) {
            Bank::create([
                'topup_transfer_type_id' => $bank['topup_transfer_type_id'],
                'nama_bank' => $bank['nama_bank'],
                'no_rek' => $bank['no_rek'],
                'atas_nama' => $bank['atas_nama']
            ]);
        }

    }
}
