<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'email' => 'admin@keranjangindo.com',
            'no_wa' => '089656911145',
            'role' => 'admin',
            'password' => Hash::make('asdasdasd')
        ]);

        $this->call([
            BankSeeder::class,
            TopupTransferTypeSeeder::class,
            LevelMemberSeeder::class,
            ActionSeeder::class,
            ServerParameterSeeder::class,
        ]);
    }
}
