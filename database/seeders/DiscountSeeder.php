<?php

namespace Database\Seeders;

use App\Models\Discount;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Discount::create([
            'service_id' => 6,
            'discount' => 10,
            'discount_name' => 'Promo Weekend',
            'level_member_id' => 1,
            'start_date' => '2022/08/31 01:00',
            'end_date' => '2022/09/5 01:00',
            'created_by' => 1
        ]);
    }
}
