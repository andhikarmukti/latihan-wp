<?php

namespace Database\Seeders;

use App\Models\LevelMember;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LevelMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level_members = [
            [
                'level' => 1,
                'level_name' => 'End User',
                'harga_upgrade' => 0,
                'mark_up' => 4,
            ],
            [
                'level' => 2,
                'level_name' => 'Reseller',
                'harga_upgrade' => 100000,
                'mark_up' => 2,
            ],
            [
                'level' => 3,
                'level_name' => 'Agent',
                'harga_upgrade' => 250000,
                'mark_up' => 1.5,
            ],
        ];

        foreach($level_members as $level_member){
            LevelMember::create([
                'level' => $level_member['level'],
                'level_name' => $level_member['level_name'],
                'harga_upgrade' => $level_member['harga_upgrade'],
                'mark_up' => $level_member['mark_up']
            ]);
        }
    }
}
