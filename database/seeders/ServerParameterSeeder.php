<?php

namespace Database\Seeders;

use App\Models\ServerParameter;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServerParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parameters = [
            [
                'server_id' => 1,
                'action_id' => 1,
                'parameter' => 'key',
                'value' => '447436594d0e907b36107e05fd81dc82',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 1,
                'parameter' => 'service',
                'value' => 'server_service_id',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 1,
                'parameter' => 'action',
                'value' => 'add',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 1,
                'parameter' => 'link',
                'value' => 'target',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 1,
                'parameter' => 'quantity',
                'value' => 'quantity',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 2,
                'parameter' => 'key',
                'value' => '447436594d0e907b36107e05fd81dc82',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 2,
                'parameter' => 'action',
                'value' => 'status',
                'created_by' => 1
            ],
            [
                'server_id' => 1,
                'action_id' => 2,
                'parameter' => 'order',
                'value' => 'order_id_server',
                'created_by' => 1
            ],
        ];

        foreach($parameters as $parameter){
            ServerParameter::create([
                'server_id' => $parameter['server_id'],
                'action_id' => $parameter['action_id'],
                'parameter' => $parameter['parameter'],
                'value' => $parameter['value'],
                'created_by' => $parameter['created_by'],
            ]);
        }
    }
}
