<?php

namespace Database\Seeders;

use App\Models\ServiceType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'type' => 'default',
            ],
            [
                'type' => 'comment',
            ],
            [
                'type' => 'comment like',
            ],
            [
                'type' => 'subscription',
            ],
            [
                'type' => 'poll',
            ],
        ];

        foreach($types as $type){
            ServiceType::create([
                'type' => $type['type']
            ]);
        }
    }
}
