<?php

namespace Database\Seeders;

use App\Models\TopupTransferType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TopupTransferTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $toptuptransfertypes = [
            [
                'type' => 'Transfer Bank',
                'minimum_topup' => 10000,
            ],
            [
                'type' => 'E-wallet',
                'minimum_topup' => 10000,
            ],
            [
                'type' => 'Manual',
                'minimum_topup' => 1,
            ],
        ];

        foreach($toptuptransfertypes as $toptuptransfertype){
            TopupTransferType::create([
                'type' => $toptuptransfertype['type'],
                'minimum_topup' => $toptuptransfertype['minimum_topup'],
            ]);
        }
    }
}
