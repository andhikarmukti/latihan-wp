@extends('layouts.main')

@section('content')
    <div class="flex flex-col justify-center items-center p-9">
        <div class="flex border border-slate-200 rounded-lg shadow-md bg-slate-200 w-full md:w-1/2 p-5">
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full">
                <h1 class="text-lg italic font-bold">API Docs</h1>
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <tbody>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                HTTP methos
                            </th>
                            <td class="py-4 px-6">
                                POST
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                API URL
                            </th>
                            <td class="py-4 px-6">
                                {{ url('') }}/api
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                API Key
                            </th>
                            <td class="py-4 px-6">
                                Get Your API Key on the <a href="/profile" class="text-blue-500">Profile</a> Page
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                Response Format
                            </th>
                            <td class="py-4 px-6">
                                JSON
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="flex flex-col border border-slate-200 rounded-lg shadow-md bg-yellow-200 w-full md:w-1/2 p-5 mt-5">
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full">
                <h1 class="text-2xl italic font-bold text-center mb-5">Service List</h1>
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs font-bold text-gray-700 uppercase bg-blue-200 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Parameter
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Description
                            </th>
                    </thead>
                    <tbody>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                url
                            </th>
                            <td class="py-4 px-6">
                                {{ url('') }}/api/services
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                api_key
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Your API Key</i>
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                action
                            </th>
                            <td class="py-4 px-6">
                                services
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-green-500">Success Response</h1>
                <pre>
{
    "status": true,
    "data": [
                {
                    "service": "9",
                    "name": "Likes Instagram [MURAH]",
                    "type": "default",
                    "category": "Instagram",
                    "rate": "686",
                    "min": "10",
                    "max": "150000",
                    "refill": "0",
                    "member_info":"<i class="text-purple-800">information about the service</i>"
                },
            ]
}
                </pre>
                <i class="text-xs text-yellow-700">note : contoh "information about the service" merupakan isi lengkap tentang detail service</i>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-red-500">Error Response</h1>
                <pre>
{
    "status": false,
    "data": "API key not found!"
}
                </pre>
            </div>
        </div>

        <div class="flex flex-col border border-slate-200 rounded-lg shadow-md bg-green-200 w-full md:w-1/2 p-5 mt-5">
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full">
                <h1 class="text-2xl italic font-bold text-center mb-5">Order</h1>
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs font-bold text-gray-700 uppercase bg-blue-200 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Parameter
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Description
                            </th>
                    </thead>
                    <tbody>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                url
                            </th>
                            <td class="py-4 px-6">
                                {{ url('') }}/api/order
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                api_key
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Your API Key</i>
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                action
                            </th>
                            <td class="py-4 px-6">
                                order
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                service_id
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Service ID</i>
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                target
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Link or Username to Target</i>
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                quantity
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Quantity Order</i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-green-500">Success Response</h1>
                <pre>
{
    "status": true,
    "data": {
        "id": 202
    }
}
                </pre>
<i class="text-xs text-yellow-700">note : contoh nilai 202 merupakan order id</i>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-red-500">Error Response</h1>
                <pre>
{
    "status": false,
    "data": "API key not found!"
}
                </pre>
            </div>
        </div>

        <div class="flex flex-col border border-slate-200 rounded-lg shadow-md bg-orange-200 w-full md:w-1/2 p-5 mt-5">
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full">
                <h1 class="text-2xl italic font-bold text-center mb-5">Status Order</h1>
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs font-bold text-gray-700 uppercase bg-blue-200 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Parameter
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Description
                            </th>
                    </thead>
                    <tbody>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                url
                            </th>
                            <td class="py-4 px-6">
                                {{ url('') . '/api/status' }}
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                api_key
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Your API Key</i>
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                action
                            </th>
                            <td class="py-4 px-6">
                                status
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                order
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Order ID</i>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-green-500">Success Response</h1>
                <pre>
{
    "status": true,
    "data": {
        "charge": "10810",
        "start_count": "7421",
        "status": "Completed",
        "remains": "0"
    }
}
                </pre>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-red-500">Error Response</h1>
                <pre>
{
    "status": false,
    "data": "API key not found!"
}
                </pre>
            </div>
        </div>

        <div class="flex flex-col border border-slate-200 rounded-lg shadow-md bg-purple-200 w-full md:w-1/2 p-5 mt-5">
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full">
                <h1 class="text-2xl italic font-bold text-center mb-5">Balance</h1>
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs font-bold text-gray-700 uppercase bg-blue-200 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Parameter
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Description
                            </th>
                    </thead>
                    <tbody>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                url
                            </th>
                            <td class="py-4 px-6">
                                {{ url('') }}/api/balance
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                api_key
                            </th>
                            <td class="py-4 px-6">
                                <i class="text-yellow-700">Your API Key</i>
                            </td>
                        </tr>
                        <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                action
                            </th>
                            <td class="py-4 px-6">
                                balance
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-green-500">Success Response</h1>
                <pre>
{
    "status": true,
    "data": {
        "balance": "176058"
    }
}
                </pre>
            </div>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full bg-white mt-4 p-2">
                <h1 class="text-lg italic font-bold text-red-500">Error Response</h1>
                <pre>
{
    "status": false,
    "data": "API key not found!"
}
                </pre>
            </div>
        </div>
    </div>
@endsection
