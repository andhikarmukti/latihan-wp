@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="flex flex-col md:flex-row gap-2">
            <div class="w-full md:w-1/3 flex flex-col gap-2">
                <div class="w-full border border-slate-300 rounded-lg p-5 shadow-md bg-yellow-50">
                    <form action="/websiteInfoStore" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h3 class="text-lg font-bold italic my-5 text-center">Website Info</h3>
                        <div class="mb-6">
                            <label for="web_name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nama Website</label>
                            <input type="text" id="web_name" name="web_name" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $nama_web }}">
                        </div>
                        <div class="flex flex-col md:flex-row gap-5">
                            <div class="mb-0 md:mb-6 w-full md:w-3/4">
                                <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="logo">Upload Logo</label>
                                <input class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="logo" name="logo" type="file">
                            </div>
                            <div class="flex justify-center w-full md:w-1/4">
                                <img src="{{ asset($logo) }}" alt="logo" class="w-24 mb-5">
                            </div>
                        </div>
                        <hr class="mb-10">
                        <div class="flex gap-2 mb-4">
                            <div class="flex items-center pl-4 rounded border border-gray-200 dark:border-gray-700 w-1/2 shadow-md bg-slate-200">
                                <input data-value="off" {{ $slider_active == false ? 'checked' : ''}} id="slider_active-on" type="radio" value="" name="slider_active" class="slider_active w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="slider_active-on" class="py-4 ml-2 w-full text-sm font-medium text-gray-900 dark:text-gray-300">Slider Image OFF</label>
                            </div>
                            <div class="flex items-center pl-4 rounded border border-gray-200 dark:border-gray-700 w-1/2 shadow-md bg-slate-200">
                                <input data-value="on" {{ $slider_active == true ? 'checked' : ''}} id="slider_active-off" type="radio" value="" name="slider_active" class="slider_active w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="slider_active-off" class="py-4 ml-2 w-full text-sm font-medium text-gray-900 dark:text-gray-300">Slider Image ON</label>
                            </div>
                        </div>
                        <div id="radio_slider_active" {{ $slider_active == false ? 'hidden' : '' }}>
                            <div class="flex flex-col md:flex-row gap-5">
                                <div class="mb-0 md:mb-6 w-full md:w-3/4">
                                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="slider_satu">Slider 1</label>
                                    <input class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="slider_satu" name="slider_satu" type="file">
                                </div>
                                <div class="flex justify-center w-full md:w-1/4">
                                    <img src="{{ asset('storage/images/slider/slider1.png') }}" alt="slider1" class="w-24 mb-5">
                                </div>
                            </div>
                            <div class="flex flex-col md:flex-row gap-5">
                                <div class="mb-0 md:mb-6 w-full md:w-3/4">
                                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="slider_dua">Slider 2</label>
                                    <input class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="slider_dua" name="slider_dua" type="file">
                                </div>
                                <div class="flex justify-center w-full md:w-1/4">
                                    <img src="{{ asset('storage/images/slider/slider2.png') }}" alt="slider2" class="w-24 mb-5">
                                </div>
                            </div>
                            <div class="flex flex-col md:flex-row gap-5">
                                <div class="mb-0 md:mb-6 w-full md:w-3/4">
                                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="slider_tiga">Slider 3</label>
                                    <input class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="slider_tiga" name="slider_tiga" type="file">
                                </div>
                                <div class="flex justify-center w-full md:w-1/4">
                                    <img src="{{ asset('storage/images/slider/slider3.png') }}" alt="slider3" class="w-24 mb-5">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                    </form>
                </div>
                <div class="w-full border border-slate-300 rounded-lg p-5 shadow-md bg-green-50">
                    <form action="/contactInfoStore" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h3 class="text-lg font-bold italic my-5 text-center">Contact Info</h3>
                        <div class="flex gap-2">
                            <div class="mb-6 w-full">
                                <label for="no_wa" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nomor WhatsApp</label>
                                <input type="text" id="no_wa" name="no_wa" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $no_wa->value }}">
                            </div>
                            <div class="mb-6 w-full">
                                <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Email</label>
                                <input type="email" id="email" name="email" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $email->value }}">
                            </div>
                        </div>
                        <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purplelue-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                    </form>
                </div>
                <div class="w-full border border-slate-300 rounded-lg p-5 shadow-md bg-green-50">
                    <form action="/deviceWaStore" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h3 class="text-lg font-bold italic my-5 text-center">WhatsApp Notif</h3>
                        <div class="flex flex-col md:flex-row gap-2">
                            <div class="mb-6 w-full">
                                <label for="contact_owner" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">WhatsApp Owner (Info)</label>
                                <input type="text" id="contact_owner" name="contact_owner" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $contact_owner }}">
                            </div>
                            <div class="mb-6 w-full">
                                <label for="min_reminder_saldo" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Minimum Reminder Saldo</label>
                                <input type="text" id="min_reminder_saldo" name="min_reminder_saldo" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ Helpers::format_rupiah($min_reminder_saldo) }}">
                            </div>
                        </div>
                        {{-- <div class="flex flex-col md:flex-row gap-2">
                            <div class="mb-6 w-full">
                                <label for="device_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Device ID</label>
                                <input type="text" id="device_id" name="device_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $device_wa }}">
                            </div>
                            <div class="mb-6 w-full">
                                <label for="bc_device_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Broadcast Device ID</label>
                                <input type="text" id="bc_device_id" name="bc_device_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $device_wa_broadcast }}">
                            </div>
                        </div> --}}
                        <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purplelue-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                    </form>
                </div>
            </div>
            <div class="w-full md:w-2/3 border border-slate-300 rounded-lg p-5 shadow-md bg-purple-50">
                <h3 class="text-lg font-bold italic my-5 text-center">Level Info</h3>
                <div class="flex flex-col md:flex-row gap-5">
                    <div class="w-full md:w-1/3 border border-slate-200 bg-gray-200 shadow-md rounded-lg p-5 h-auto">
                        <form action="/levelInfoStore" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="flex flex-col items-center">
                                <img class="w-20 h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/1.png" alt="levelMember">
                                <h3 class="text-lg font-bold italic mb-5 text-gray-700">End User</h3>
                            </div>
                            <div class="mb-6">
                                <label for="mark_up_enduser" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Markup</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="mark_up_enduser" name="mark_up" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ ($level_member->where('level_name', 'End User')->first()->mark_up - 1) * 100 }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        <input type="hidden" name="level_member_id" value="1">
                                        %
                                    </div>
                                </div>
                                <div class="flex flex-col">
                                    <small data-value="15000" id="contoh_harga_enduser" class="text-xs italic">contoh harga : Rp 15.000</small>
                                    <small id="total_markup_enduser" class="text-xs italic">hasil markup = <span id="hasil_markup_enduser"></span></small>
                                </div>
                            </div>
                            <div class="mb-6">
                                <label for="komisi_referral_enduser" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Komisi Referral</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="komisi_referral_enduser" name="komisi_referral_enduser" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $komisi_enduser }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        %
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                        </form>
                    </div>
                    <div class="w-full md:w-1/3 border border-slate-200 bg-yellow-100 shadow-md rounded-lg p-5 h-auto">
                        <form action="/levelInfoStore" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="flex flex-col items-center">
                                <img class="w-20 h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/2.png" alt="levelMember">
                                <h3 class="text-lg font-bold italic mb-5 text-yellow-700">Reseller</h3>
                            </div>
                            <div class="mb-6">
                                <label for="mark_up_reseller" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Markup</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="mark_up_reseller" name="mark_up" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ ($level_member->where('level_name', 'Reseller')->first()->mark_up - 1) * 100 }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        <input type="hidden" name="level_member_id" value="2">
                                        %
                                    </div>
                                </div>
                                <div class="flex flex-col">
                                    <small data-value="15000" id="contoh_harga_reseller" class="text-xs italic">contoh harga : Rp 15.000</small>
                                    <small id="total_markup_reseller" class="text-xs italic">hasil markup = <span id="hasil_markup_reseller"></span></small>
                                </div>
                            </div>
                            <div class="mb-6">
                                <label for="harga_upgrade" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Harga Upgrade</label>
                                <input type="number" id="harga_upgrade" name="harga_upgrade" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ Helpers::format_rupiah($level_member->where('level_name', 'Reseller')->first()->harga_upgrade) }}">
                            </div>
                            <div class="mb-6">
                                <label for="perpanjang_auto" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Nominal Perpanjang Automatis</label>
                                <input type="number" id="perpanjang_auto" name="perpanjang_auto" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ Helpers::format_rupiah($level_member->where('level_name', 'Reseller')->first()->perpanjang_auto) }}">
                            </div>
                            <div class="mb-6">
                                <label for="komisi_referral_reseller" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Komisi Referral</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="komisi_referral_reseller" name="komisi_referral_reseller" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $komisi_reseller }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        %
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                        </form>
                    </div>
                    <div class="w-full md:w-1/3 border border-slate-200 bg-purple-200 shadow-md rounded-lg p-5 h-auto">
                        <form action="/levelInfoStore" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="flex flex-col items-center">
                                <img class="w-20 h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/3.png" alt="levelMember">
                                <h3 class="text-lg font-bold italic mb-5 text-purple-700">Agent</h3>
                            </div>
                            <div class="mb-6">
                                <label for="mark_up_agent" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Markup</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="mark_up_agent" name="mark_up" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ ($level_member->where('level_name', 'Agent')->first()->mark_up - 1) * 100 }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        <input type="hidden" name="level_member_id" value="3">
                                        %
                                    </div>
                                </div>
                                <div class="flex flex-col">
                                    <small data-value="15000" id="contoh_harga_agent" class="text-xs italic">contoh harga : Rp 15.000</small>
                                    <small id="total_markup_agent" class="text-xs italic">hasil markup = <span id="hasil_markup_agent"></span></small>
                                </div>
                            </div>
                            <div class="mb-6">
                                <label for="harga_upgrade" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Harga Upgrade</label>
                                <input type="number" id="harga_upgrade" name="harga_upgrade" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ Helpers::format_rupiah($level_member->where('level_name', 'Agent')->first()->harga_upgrade) }}">
                            </div>
                            <div class="mb-6">
                                <label for="perpanjang_auto" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Nominal Perpanjang Automatis</label>
                                <input type="number" id="perpanjang_auto" name="perpanjang_auto" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ Helpers::format_rupiah($level_member->where('level_name', 'Agent')->first()->perpanjang_auto) }}">
                            </div>
                            <div class="mb-6">
                                <label for="komisi_referral_agent" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Komisi Referral</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="komisi_referral_agent" name="komisi_referral_agent" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $komisi_agent }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        %
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="flex flex-col md:flex-row gap-5">
                    <div class="w-full md-w-1/2 border border-slate-200 bg-blue-200 shadow-md rounded-lg p-5 h-auto mt-5">
                        <form action="/levelInfoStore" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-6">
                                <label for="min_penarikan_komisi" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Batas Minimum Penarikan Komisi</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="min_penarikan_komisi" name="min_penarikan_komisi" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ Helpers::format_rupiah($minimum_penarikan_komisi) }}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                        </form>
                    </div>
                    <div class="w-full md-w-1/2 border border-slate-200 bg-blue-300 shadow-md rounded-lg p-5 h-auto mt-5">
                        <form action="/levelInfoStore" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-6">
                                <label for="bonus_topup_bca" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Bonus Topup BCA</label>
                                <div class="flex">
                                    <div class="relative w-full">
                                        <input type="number" id="bonus_topup_bca" name="bonus_topup_bca" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="{{ $bonus_topup_bca }}" autocomplete="off">
                                    </div>
                                    <div class="flex items-center px-3 pointer-events-none border border-slate-200 rounded-lg">
                                        %
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="mb-6">
                                <div class="flex flex-col md:flex-row gap-5">
                                    <div class="mb-0 md:mb-6 w-full md:w-3/4">
                                        <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="qr_code_bca">Upload QR Code BCA</label>
                                        <input class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" id="qr_code_bca" name="qr_code_bca" type="file">
                                    </div>
                                    <div class="flex justify-center w-full md:w-1/4">
                                        <img src="{{ asset('storage/images/qrcode/qrcode_1.png') }}" alt="qrcode" class="w-24 mb-5">
                                    </div>
                                </div>
                            </div> --}}
                            <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-xs w-full sm:w-auto px-2 py-1 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function formatRupiahRibuan(angka, prefix){
			var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
		}

        $('#mark_up_enduser').keyup(function(){
            const markup_enduser = $(this).val();
            const contoh_harga = $('#contoh_harga_enduser').attr('data-value');
            // console.log(contoh_harga);

            $('#hasil_markup_enduser').html(formatRupiahRibuan(contoh_harga*1 + (contoh_harga * markup_enduser / 100), 'Rp '));
        });
        $('#mark_up_reseller').keyup(function(){
            const markup_reseller = $(this).val();
            const contoh_harga = $('#contoh_harga_reseller').attr('data-value');
            // console.log(contoh_harga);

            $('#hasil_markup_reseller').html(formatRupiahRibuan(contoh_harga*1 + (contoh_harga * markup_reseller / 100), 'Rp '));
        });
        $('#mark_up_agent').keyup(function(){
            const markup_agent = $(this).val();
            const contoh_harga = $('#contoh_harga_agent').attr('data-value');
            // console.log(contoh_harga);

            $('#hasil_markup_agent').html(formatRupiahRibuan(contoh_harga*1 + (contoh_harga * markup_agent / 100), 'Rp '));
        });

        $('.slider_active').click(function(){
            const slider_active = $(this).data('value');

            if(slider_active == 'off'){
                $('#radio_slider_active').attr('hidden', true);
            }else{
                $('#radio_slider_active').removeAttr('hidden');
            }

            $.ajax({
                url : '/slider-active',
                method : 'POST',
                data : {
                    '_token' : "{{ csrf_token() }}",
                    slider_active : slider_active
                },
                success : function(res){
                    // console.log(res);
                }
            });
        });
    </script>
@endsection
