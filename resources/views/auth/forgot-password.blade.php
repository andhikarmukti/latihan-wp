@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row flex flex-col justify-center items-center min-h-screen w-screen gap-9">
        <div class="flex justify-center">
            <img src="{{ asset('storage\images\logo\logowebsite.png') }}" class="mr-1 w-1/3 md:w-1/12" alt="Logo">
        </div>
        <h1 class="text-center text-4xl font-bold italic">Forgot Password</h1>
        <div class="card border border-sm p-4 w-80 shadow-md rounded-lg">
            <form action="/forgot-password" method="POST" id="formForgotPassword">
                @csrf
                <div class="flex flex-col gap-3">
                    <div>
                        <label for="email" class="block text-sm font-medium text-gray-700">email</label>
                        <input type="email" name="email" id="email" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    </div>
                    <div class="mx-auto">
                        <button id="buttonSend" class="border border-sm rounded-md bg-blue-600 text-white p-2 w-40 hover:bg-blue-500 hover:text-gray-700">Send</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="flex flex-col justify-center items-center">
            <a href="/login" class="text-blue-500 italic hover:text-blue-700 hover:scale-125 transition duration-300">Login here!</a>
            <a href="/register" class="text-blue-500 italic hover:text-blue-700 hover:scale-125 transition duration-300">Register here!</a>
        </div>
    </div>
</div>

<script>
    $('#buttonSend').click(function(){
        $(this).attr('disabled', true);
        $('#errorEmail').empty();
        $('#email').removeClass('ring-1 ring-red-500');

        let error = [];
        const data = $('#formForgotPassword').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        $.ajax({
            url : '/forgot-password',
            method : 'POST',
            data : {
                email : data.email,
                '_token' : '{{ csrf_token() }}'
            },
            beforeSend: function() {
                let timerInterval
                Swal.fire({
                    title: 'Mohon tunggu sebentar',
                    html: 'Sedang mengirim link reset password',
                    timer: 10000,
                    timerProgressBar: true,
                    didOpen: () => {
                        Swal.showLoading()
                        const b = Swal.getHtmlContainer().querySelector('b')
                        timerInterval = setInterval(() => {
                            if(b){
                                b.textContent = Swal.getTimerLeft()
                            }
                        }, 100)
                    },
                willClose: () => {
                        clearInterval(timerInterval)
                    }
                })
            },
            success : function(res){
                // console.log(res);
            },
            error : function(err){
                // console.log(err);
                error = err.responseJSON;
            },
            complete : function() {
                if(error != ''){
                    let errorMessage = '';
                    if(error.email){
                        errorMessage = error.email[0]
                    }else{
                        errorMessage = error.message
                    }
                    $('#buttonSend').removeAttr('disabled');
                    $('#email').addClass('ring-1 ring-red-500');
                    Swal.fire({
                        icon : 'error',
                        title : 'Gagal!',
                        text : errorMessage,
                    }).then(() => {
                        $('#email').after(`
                            <small id="errorEmail" class="text-red-400 italic">${errorMessage}</small>
                        `)
                    });
                }else{
                    Swal.fire({
                        icon : 'success',
                        title : 'Request reset password berhasil!',
                        text : 'Silahkan cek email atau whatsapp kamu untuk membuka link reset password',
                    }).then(() => {
                        window.location = '/login';
                    });
                }
            }
        });
    });
</script>
@endsection
