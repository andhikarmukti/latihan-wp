@extends('layouts.main')

@section('content')
    <div class="container">

        <div class="w-full">
            <div id="accordion-collapse" data-accordion="collapse" class="sm:hidden bg-blue-300">
                <h2 id="accordion-collapse-heading-2">
                    <button type="button"
                        class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                        data-accordion-target="#accordion-collapse-body-2" aria-expanded="false"
                        aria-controls="accordion-collapse-body-2">
                        <span class="text-slate-900">Daftar Harga</span>
                        <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                clip-rule="evenodd"></path>
                        </svg>
                    </button>
                </h2>
                <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2">
                    <div>
                        <small class="text-xs italic sm:hidden text-gray-500">Table dapat digeser <i
                                class="fa fa-long-arrow-right" aria-hidden="true"></i></small>
                    </div>
                    <div class="overflow-x-auto relative border border-slate-300 p-5 rounded-lg shadow-md w-full">
                        <table
                            class="overflow-x-auto relative w-full text-sm text-left text-gray-500 dark:text-gray-400 bg-white"
                            id="tableService">
                            <thead
                                class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400 text-center">
                                <tr>
                                    <th scope="col" class="py-3 px-6">
                                        Service
                                    </th>
                                    <th scope="col" class="py-3 px-6">
                                        Harga <br> Normal <br> / 1000 quantity
                                    </th>
                                    <th scope="col" class="py-3 px-6">
                                        Harga <br> Reseller <br> / 1000 quantity
                                    </th>
                                    <th scope="col" class="py-3 px-6">
                                        Harga <br> Agent <br> / 1000 quantity
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $service)
                                    <tr
                                        class="border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 text-center">
                                        <th scope="row"
                                            class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                            {{ $service->service_name }}
                                        </th>
                                        <th scope="row"
                                            class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                            {{ $service->rate * $markup_enduser }}
                                        </th>
                                        <th scope="row"
                                            class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                            {{ $service->rate * $markup_reseller }}
                                        </th>
                                        <th scope="row"
                                            class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                            {{ $service->rate * $markup_agent }}
                                        </th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row flex flex-col justify-center items-center min-h-screen w-screen gap-9">
                <div class="flex justify-center">
                    <img src="{{ $logo }}" class="mr-1 w-1/3 md:w-1/12"
                        alt="Logo">
                </div>
                <h1 class="text-center text-4xl font-bold italic">Login</h1>
                <div class="card border border-sm p-4 w-80 shadow-md rounded-lg">
                    <form action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="flex flex-col gap-3">
                            <div>
                                <label for="username" class="block text-sm font-medium text-gray-700">Username</label>
                                <input autofocus type="text" name="username" id="username" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @if ($errors->has('username')) ring-1 ring-red-400 @endif">
                                @if ($errors->has('username'))
                                    <small class="text-red-400 italic">{{ $errors->first('username') }}</small>
                                @endif
                            </div>
                            <div>
                                <label for="password" class="block text-sm font-medium text-gray-700">Password</label>
                                <input type="password" name="password" id="password" autocomplete="off"
                                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>
                            <div class="flex justify-center">
                                <div class="col-md-6"> {!! htmlFormSnippet() !!} </div>
                            </div>
                            @if ($errors->has('g-recaptcha-response'))
                                <small class="text-red-400 italic">{{ $errors->first('g-recaptcha-response') }}</small>
                            @endif
                            <div class="flex items-center mr-4">
                                <input id="remember" name="remember" type="checkbox" value="1"
                                    class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                                <label for="remember"
                                    class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Remember Me</label>
                            </div>
                            <div class="mx-auto">
                                <button
                                    class="border border-sm rounded-md bg-blue-600 text-white p-2 w-40 hover:bg-blue-500 hover:text-gray-700">Login</button>
                            </div>
                            <a href="/forgot-password"
                                class="text-center text-sm text-blue-500 italic hover:text-blue-700 hover:scale-125 transition duration-300">Forgot
                                Password?</a>
                        </div>
                    </form>
                </div>
                <a href="/register"
                    class="text-blue-500 italic hover:text-blue-700 hover:scale-125 transition duration-300 text-sm">Belum punya
                    akun? <span class="text-yellow-600 text-xl">Register here!</span></a>
            </div>
        </div>

    </div>

    <script>
        // Datatables
        $(document).ready(function() {
            $('#tableService').DataTable({
                "pageLength": 100,
                // responsive: true,
                "lengthChange": false,
                "ordering": true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: ["_all"]
                    },
                    {
                        target:1,
                        className:'cell-border'
                    },
                    {
                        target: [1, 2, 3],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                ],
                order: [[0, 'asc']],
            });
        });
    </script>
@endsection
