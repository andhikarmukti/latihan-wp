@extends('layouts.main')

@section('content')
<div class="container">
    <div class="w-full">
        <div id="accordion-collapse" data-accordion="collapse" class="sm:hidden bg-blue-300 mb-9">
            <h2 id="accordion-collapse-heading-2">
                <button type="button"
                    class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                    data-accordion-target="#accordion-collapse-body-2" aria-expanded="false"
                    aria-controls="accordion-collapse-body-2">
                    <span class="text-slate-900">Daftar Harga</span>
                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </h2>
            <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2">
                <div>
                    <small class="text-xs italic sm:hidden text-gray-500">Table dapat digeser <i
                            class="fa fa-long-arrow-right" aria-hidden="true"></i></small>
                </div>
                <div class="overflow-x-auto relative border border-slate-300 p-5 rounded-lg shadow-md w-full">
                    <table
                        class="overflow-x-auto relative w-full text-sm text-left text-gray-500 dark:text-gray-400 bg-white"
                        id="tableService">
                        <thead
                            class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400 text-center">
                            <tr>
                                <th scope="col" class="py-3 px-6">
                                    Service
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Harga <br> Normal <br> / 1000 quantity
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Harga <br> Reseller <br> / 1000 quantity
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Harga <br> Agent <br> / 1000 quantity
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($services as $service)
                                <tr
                                    class="border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 text-center">
                                    <th scope="row"
                                        class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $service->service_name }}
                                    </th>
                                    <th scope="row"
                                        class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $service->rate * $markup_enduser }}
                                    </th>
                                    <th scope="row"
                                        class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $service->rate * $markup_reseller }}
                                    </th>
                                    <th scope="row"
                                        class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                        {{ $service->rate * $markup_agent }}
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row flex flex-col justify-center items-center min-h-screen w-screen gap-9">
            <div class="flex justify-center mt-5">
                <img src="{{ $logo }}" class="mr-1 w-1/3 md:w-1/12" alt="Logo">
            </div>
            <h1 class="text-center text-5xl font-bold">Register</h1>
            <div class="card border border-sm p-4 w-96 shadow-md rounded-lg">
                @if ($user_upline && $user_upline->id == 1)
                    <div id="alert-border-3" class="flex p-4 mb-4 bg-green-100 border-t-4 border-green-500 dark:bg-green-200" role="alert">
                        <div class="flex flex-col w-full text-center">
                            <div class="ml-3 text-sm font-bold text-green-700">
                                Registrasi sekarang juga!<br><span class="text-yellow-700 italic">Auto Upgrade level Agent</span>
                            </div>
                        </div>
                        <button type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 dark:bg-green-200 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 dark:hover:bg-green-300 inline-flex h-8 w-8"  data-dismiss-target="#alert-border-3" aria-label="Close">
                            <span class="sr-only">Dismiss</span>
                            <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </div>
                @elseif ($promo_upgrade)
                    <div id="alert-border-3" class="flex p-4 mb-4 bg-green-100 border-t-4 border-green-500 dark:bg-green-200" role="alert">
                        <div class="flex flex-col w-full text-center">
                            {{-- <div class="ml-3 text-lg font-bold text-green-700">
                                PROMO OKTOBER!
                            </div> --}}
                            <div class="ml-3 text-sm font-bold text-green-700">
                                Registrasi sekarang juga!<br><span class="text-yellow-700 italic">Auto Upgrade level {{ $promo_upgrade->levelMember->level_name }}</span>
                            </div>
                            <div class="ml-3 text-xs font-bold text-purple-700 italic mt-2">
                                Promo hingga : {{ $promo_upgrade->end_date }}
                            </div>
                        </div>
                        <button type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 dark:bg-green-200 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 dark:hover:bg-green-300 inline-flex h-8 w-8"  data-dismiss-target="#alert-border-3" aria-label="Close">
                            <span class="sr-only">Dismiss</span>
                            <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </div>
                @endif
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="flex flex-col gap-4">
                        <div>
                            <label for="username" class="block text-sm font-medium text-gray-700">Username</label>
                            <input type="text" name="username" id="username" autocomplete="off" class="mt-1 @if($errors->has('username')) ring-1 ring-red-400 @endif focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{ old('username') }}">
                            @if($errors->has('username'))
                            <small class="text-red-400 italic">{{ $errors->first('username') }}</small>
                            @endif
                        </div>
                        <div>
                            <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                            <input type="text" name="email" id="email" autocomplete="off" class="mt-1 @if($errors->has('email')) ring-1 ring-red-400 @endif  focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{ old('email') }}">
                            @if($errors->has('email'))
                            <small class="text-red-400 italic">{{ $errors->first('email') }}</small>
                            @endif
                        </div>
                        <div>
                            <label for="no_wa" class="block text-sm font-medium text-gray-700">Nomor WhatsApp</label>
                            <div class="grid grid-cols-4 gap-1">
                                <div class="col-span-3">
                                    <input type="number" name="no_wa" id="no_wa" autocomplete="off" class="mt-1 @if($errors->has('no_wa')) ring-1 ring-red-400 @endif focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{ old('no_wa') }}">
                                </div>
                                <div class="flex items-center justify-center">
                                    <button id="sendOtp" type="button" class="border border-sm px-1 rounded-lg text-sm italic font-bold bg-blue-600 text-white hover:bg-blue-500 hover:text-gray-700">Send OTP</button>
                                </div>
                                <div class="countdown"></div>
                            </div>
                            @if($errors->has('no_wa'))
                            <small class="text-red-400 italic">{{ $errors->first('no_wa') }}</small>
                            @endif
                        </div>
                        <div>
                            <label for="otp" class="block text-sm font-medium text-gray-700">Nomor OTP</label>
                            <input type="text" name="otp" id="otp" autocomplete="off" class="mt-1 @if($errors->has('otp')) ring-1 ring-red-400 @endif  focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{ old('otp') }}">
                            @if($errors->has('otp'))
                            <small class="text-red-400 italic">{{ $errors->first('otp') }}</small>
                            @endif
                        </div>
                        <div>
                            <label for="password" class="block text-sm font-medium text-gray-700">Password</label>
                            <input type="password" name="password" id="password" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @if($errors->has('password'))
                            <small class="text-red-400 italic">{{ $errors->first('password') }}</small>
                            @endif
                        </div>
                        <div>
                            <label for="password_confirmation" class="block text-sm font-medium text-gray-700">Password Confirmation</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        </div>
                        <div class="flex justify-center">
                            <div class="col-md-6"> {!! htmlFormSnippet() !!} </div>
                        </div>
                        @if ($errors->has('g-recaptcha-response'))
                            <small class="text-red-400 italic">{{ $errors->first('g-recaptcha-response') }}</small>
                        @endif
                        <div class="mx-auto">
                            <button id="buttonRegister" class="border border-sm rounded-md bg-blue-600 text-white p-2 w-40 hover:bg-blue-500 hover:text-gray-700">Register</button>
                        </div>
                    </div>
                </form>
            </div>
            <a href="/login" class="text-blue-500 italic hover:text-blue-700 hover:scale-125 transition duration-300">Sudah punya akun? Login here!</a>
        </div>
    </div>
</div>

<script>
    $('#sendOtp').click(function(){
        const noWa = $('#no_wa').val();
        $(this).after(`<span class="sending italic text-slate-500">Sending...</span>`);
        $(this).attr('hidden', true);
        $.ajax({
            url : '/check-otp',
            method : 'POST',
            data : {
                no_wa : noWa,
                '_token' : "{{ csrf_token() }}"
            },
            success : function(res){
                // console.log(res);
                if(res.no_wa || res.hp_alt){
                    Swal.fire({
                        icon : 'error',
                        text : res.no_wa ?? res.hp_alt
                    });
                    $('#sendOtp').removeAttr('hidden');
                    $('.sending').remove();
                    $(this).attr('hidden', false);
                }else if(res.status == 'success'){
                    $('#sendOtp').attr('disabled', true);
                    $('#sendOtp').attr('hidden', true);
                    Swal.fire({
                        icon : 'success',
                        title : 'Berhasil Mengirim OTP!',
                        text : res.message
                    }).then(() => {
                        var timer2 = "0:59";
                        var interval = setInterval(function() {
                            var timer = timer2.split(':');
                            //by parsing integer, I avoid all extra string processing
                            var minutes = parseInt(timer[0], 10);
                            var seconds = parseInt(timer[1], 10);
                            --seconds;
                            minutes = (seconds < 0) ? --minutes : minutes;
                            if (minutes < 0) clearInterval(interval);
                            seconds = (seconds < 0) ? 59 : seconds;
                            seconds = (seconds < 10) ? '0' + seconds : seconds;
                            //minutes = (minutes < 10) ?  minutes : minutes;
                            $('.sending').html(minutes + ':' + seconds);
                            timer2 = minutes + ':' + seconds
                            if(seconds == 0){
                                clearInterval(interval);
                                // console.log('WAKTU HABIS');
                                $('#sendOtp').removeAttr('hidden');
                                $('#sendOtp').removeAttr('disabled');
                                $('#sendOtp').html('kirim ulang');
                                $('.sending').attr('hidden', true);
                            }
                        }, 1000);
                    });
                }
            }
        });
    });

    $('#buttonRegister').click(function(){
        $(this).after('Mohon menunggu...');
        $(this).attr('hidden', true);
    });
</script>

<script>
    // Datatables
    $(document).ready(function() {
        $('#tableService').DataTable({
            "pageLength": 100,
            // responsive: true,
            "lengthChange": false,
            "ordering": true,
            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: ["_all"]
                },
                {
                    target:1,
                    className:'cell-border'
                },
                {
                    target: [1, 2, 3],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                },
            ],
            order: [[0, 'asc']],
        });
    });
</script>
@endsection
