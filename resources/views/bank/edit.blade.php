@extends('layouts.main')

@section('content')
<div class="p-9">
    <h3 class="font-bold italic text-2xl mb-5">Bank Edit</h3>
    <a href="/admin-bank" class="p-2 rounded-lg bg-blue-200 ring-1 ring-blue-300">kembali</a>
    <div class="p-4 rounded-lg border border-slate-300 mt-5 shadow-md w-full md:w-1/3">
        <form action="/admin-bank/update/{{ $bank->id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-6">
                <label for="topup_transfer_type_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Transfer Type</label>
                <select id="topup_transfer_type_id" name="topup_transfer_type_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Trasnfer Type</option>
                    @foreach ($topup_transfer_types as $topup_transfer_type)
                    <option value="{{ $topup_transfer_type->id }}" {{ $topup_transfer_type->id == $bank->topup_transfer_type_id ? 'selected' : '' }}>{{ $topup_transfer_type->type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-6">
                <label for="nama_bank" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nama Bank</label>
                <input value="{{ old('nama_bank', $bank->nama_bank) }}" type="text" id="nama_bank" name="nama_bank" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            </div>
            <div class="mb-6">
                <label for="value_midtrans" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value Midtrans</label>
                <input value="{{ old('value_midtrans', $bank->value_midtrans) }}" type="text" id="value_midtrans" name="value_midtrans" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            </div>
            <div class="mb-6">
                <label for="no_rek" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">No Rekening</label>
                <input value="{{ old('no_rek', $bank->no_rek) }}" type="text" id="no_rek" name="no_rek" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            </div>
            <div class="mb-6">
                <label for="atas_nama" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Atas Nama</label>
                <input value="{{ old('atas_nama', $bank->atas_nama) }}" type="text" id="atas_nama" name="atas_nama" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            </div>
            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
        </form>
    </div>
</div>
@endsection
