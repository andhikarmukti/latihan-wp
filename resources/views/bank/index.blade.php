@extends('layouts.main')

@section('content')
<div class="p-9">
    <h3 class="font-bold italic text-2xl mb-5">Bank</h3>
    <button type="button" id="buttonTambahBank" class="p-2 rounded-lg bg-blue-200 ring-1 ring-blue-300 mb-7">Tambah Bank</button>
    <div class="p-4 rounded-lg border border-slate-300 mb-5 shadow-md w-full md:w-1/3" hidden id="formTambahBank">
        <form action="/admin-bank" method="POST">
            @csrf
            <div class="mb-6">
                <label for="topup_transfer_type_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Transfer Type</label>
                <select id="topup_transfer_type_id" name="topup_transfer_type_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Trasnfer Type</option>
                    @foreach ($topup_transfer_types as $topup_transfer_type)
                    <option value="{{ $topup_transfer_type->id }}">{{ $topup_transfer_type->type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-6">
                <label for="nama_bank" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nama Bank</label>
                <input value="{{ old('nama_bank', '') }}" type="text" id="nama_bank" name="nama_bank" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
            </div>
            <div class="mb-6">
                <label for="value_midtrans" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value Midtrans</label>
                <input value="{{ old('value_midtrans', '') }}" type="text" id="value_midtrans" name="value_midtrans" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
            </div>
            <div class="mb-6">
                <label for="no_rek" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">No Rekening</label>
                <input value="{{ old('no_rek', '') }}" type="text" id="no_rek" name="no_rek" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
            </div>
            <div class="mb-6">
                <label for="atas_nama" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Atas Nama</label>
                <input value="{{ old('atas_nama', '') }}" type="text" id="atas_nama" name="atas_nama" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
            </div>
            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
        </form>
    </div>
    <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400 text-center">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        Transfer Type
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Nama Bank
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Value Midtrans
                    </th>
                    <th scope="col" class="py-3 px-6">
                        No. Rekening
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Atas Nama
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Status
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($banks as $bank)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 text-center">
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $bank->transferType->type }}
                    </th>
                    <td class="py-4 px-6">
                        {{ $bank->nama_bank }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $bank->value_midtrans }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $bank->no_rek }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $bank->atas_nama }}
                    </td>
                    <td class="py-4 px-6 {{ $bank->is_active == 1 ? 'text-green-500' : '' }}">
                        {{ $bank->is_active == 1 ? 'active' : '-' }}
                    </td>
                    <td class="py-4 px-6">
                        <div class="flex flex-col gap-2">
                            <a href="/admin-bank/edit/{{ $bank->id }}" class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-800">edit</a>
                            <a href="/admin-bank/{{ $bank->is_active == true ? 'disable' : 'enable' }}/{{ $bank->id }}" class="{{ $bank->is_active == true ? 'bg-red-100 text-red-800' : 'bg-blue-100 text-blue-800' }} text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-800">{{ $bank->is_active == true ? 'disable' : 'enable' }}</a>
                            @if (!$topup->where('bank_id', $bank->id)->first())
                            <a href="/admin-bank/delete/{{ $bank->id }}" class="bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-800">X</a>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $('#buttonTambahBank').click(function(){
        $(this).attr('hidden', true);
        $('#formTambahBank').removeAttr('hidden');
    });
</script>
@endsection
