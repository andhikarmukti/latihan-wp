@extends('layouts.main')

@section('content')
<div class="bg-blue-200 rounded-b-3xl shadow-xl shadow-transparent md:hidden">
    @if ($slider_active)
    <div id="default-carousel" class="relative bg-yellow-100" data-carousel="slide">
        <!-- Carousel wrapper -->
        <div class="relative h-56 overflow-hidden rounded-lg md:h-96">
             <!-- Item 1 -->
            <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item="">
                <span class="absolute text-2xl font-semibold text-white -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 sm:text-3xl dark:text-gray-800">First Slide</span>
                <a href="storage/images/slider/slider1.png" target="_blank">
                    <img src="{{ asset('storage/images/slider/slider1.png') }}" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-full" alt="slider1">
                </a>
            </div>
            <!-- Item 2 -->
            <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-full z-10" data-carousel-item="">
                <a href="storage/images/slider/slider2.png" target="_blank">
                    <img src="{{ asset('storage/images/slider/slider2.png') }}" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-full" alt="slider2">
                </a>
            </div>
            <!-- Item 3 -->
            <div class="duration-700 ease-in-out absolute inset-0 transition-all transform -translate-x-full z-10" data-carousel-item="">
                <a href="storage/images/slider/slider3.png" target="_blank">
                    <img src="{{ asset('storage/images/slider/slider3.png') }}" class="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-full" alt="slider3">
                </a>
            </div>
        </div>
        <!-- Slider indicators -->
        <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
            <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
            <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
            <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
        </div>
        <!-- Slider controls -->
        <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
            <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>
                <span class="sr-only">Previous</span>
            </span>
        </button>
        <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
            <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                <span class="sr-only">Next</span>
            </span>
        </button>
    </div>
    @endif
    <div class="w-full md:w-1/5">
        <a class="p-2 flex flex-col items-center bg-blue-100 rounded-b-lg shadow-md hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
            <div class="w-1/3 flex justify-center items-center">
                <img class="w-20 h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/{{ auth()->user()->levelMember->level . '.png' }}" alt="levelMember">
            </div>
            <div class="flex flex-col gap-1 w-full items-center">
                <div class="flex flex-col justify-between items-center p-1 leading-normal w-3/4 bg-blue-200 rounded-xl">
                    <h5 class="mb-2 text-sm font-bold tracking-tight text-gray-900 dark:text-white">Hallo, {{ auth()->user()->username }}!</h5>
                    <p class="text-md font-normal dark:text-gray-400">
                        Level :
                        <span class="font-bold text-lg {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                            {{ auth()->user()->levelMember->level_name }}
                        </span>
                    </p>
                </div>
                @if (auth()->user()->levelMember->level < 3)
                <div class="flex justify-center">
                    @if (auth()->user()->upgradeLevel && auth()->user()->upgradeLevel->status)
                        <div class="flex flex-col gap-1">
                            <small class="text-orange-800 text-xs text-center">Upgrade {{ auth()->user()->upgradeLevel->levelMember->level_name }} requested</small>
                            <small class="text-red-500 text-xs">Expired at {{ auth()->user()->upgradeLevel->created_at->addDays(1) }}</small>
                            <button type="button" class="buttonInfoPembayaran bg-blue-300 text-blue-800 hover:bg-blue-400 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">cek info pembayaran</button>
                        </div>
                    @endif
                </div>
                @endif
                @if(auth()->user()->upgradeLevelAccept)
                <div class="text-sm flex flex-col justify-between items-center p-1 leading-normal w-3/4">
                    <div class="flex justify-start w-full">
                        <p class="text-xs font-normal dark:text-gray-400">
                            Masa Berlaku {{ auth()->user()->levelMember->level_name }} :
                            <br>
                            <span class="font-bold text-xs {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                                {{ auth()->user()->upgradeLevelAccept->start_date }} <span class="text-gray-900">s/d</span> {{ auth()->user()->upgradeLevelAccept->end_date }}
                            </span>
                        </p>
                    </div>
                    <div class="flex justify-start w-full">
                        <p class="text-xs font-normal dark:text-gray-400">
                            Total saldo digunakan :
                            <span class="font-bold text-sm {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                                {{ Helpers::format_rupiah(auth()->user()->upgradeLevelAccept->saldo_digunakan   ) }}
                            </span>
                        </p>
                    </div>
                </div>
                @endif
            </div>
        </a>
    </div>

    {{-- Pengumuman No WA Terblokir --}}
    {{-- <div id="alert-additional-content-2" class="p-4 mb-4 border border-red-300 rounded-lg bg-red-50 dark:bg-red-200" role="alert">
        <div class="flex items-center">
          <svg aria-hidden="true" class="w-5 h-5 mr-2 text-red-900 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
          <span class="sr-only">Info</span>
          <h3 class="text-lg font-medium text-red-900 dark:text-red-800">Pengumuman</h3>
        </div>
        <div class="mt-2 mb-4 text-sm text-red-900 dark:text-red-800">
          Mohon maaf bagi para member yang kesulitan menghubungi CS.
          <br>
          <br>
          Saat ini nomor WhatsApp CS Keranjangindo sedang terblokir dan sedang mengajukan banding ke pihak WhatsApp, sehingga WA sementara dinonaktifkan.
          <br>
          <br>
          Untuk dapat menghubungi CS sementara bisa ke WhatsApp kami di nomor <a class="text-purple-800 hover:cursor-pointer" href="https://api.whatsapp.com/send/?phone=6287777493049" target="_blank">087777 49 30 49</a>
          Terima kasih
        </div>
        <div class="flex">
            <a href="https://api.whatsapp.com/send/?phone=6287777493049" target="_blank" class="text-white bg-green-900 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-green-800 dark:hover:bg-green-900">
                <i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>
                WhatsApp
            </a>
          <button type="button" class="buttonClosePengumuman text-red-900 bg-transparent border border-red-900 hover:bg-red-900 hover:text-white focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-xs px-3 py-1.5 text-center dark:border-red-800 dark:text-red-800 dark:hover:text-white" data-dismiss-target="#alert-additional-content-2" aria-label="Close">
            Close
          </button>
        </div>
    </div> --}}

    <div class="flex gap-5 overflow-x-auto p-2 justify-between">
        <div class="flex flex-col p-1 text-green-700 justify-center w-full">
            <a class=" shadow-md text-xs bg-green-200 ring-1 ring-green-300 p-1 rounded-xl flex flex-col items-center text-center" href="/tambah-saldo">
                <i class="fa fa-plus-circle fa-xl" aria-hidden="true"></i>
                {{ Helpers::format_rupiah(auth()->user()->saldo) }}
            </a>
        </div>
        <div class="flex flex-col p-1 text-yellow-700 justify-center w-full">
            <a class=" shadow-md text-xs bg-yellow-200 ring-1 ring-yellow-300 p-1 rounded-xl flex flex-col items-center text-center" href="/new-order">
                <i class="fa fa-cart-plus fa-xl" aria-hidden="true"></i>
                New Order
            </a>
        </div>
        <div class="flex flex-col p-1 text-purple-700 justify-center w-full">
            <a class=" shadow-md text-xs bg-purple-200 ring-1 ring-purple-300 p-1 rounded-xl flex flex-col items-center text-center" href="/service">
                <i class="fa fa-list-ul fa-xl" aria-hidden="true"></i>
                Services
            </a>
        </div>
    </div>
    <div class="flex gap-5 p-2 justify-between">
        <div class="hover:scale-110 duration-300 flex flex-col p-1 text-green-700 justify-center w-full">
            <a class="shadow-md text-xs bg-green-100 ring-1 ring-green-300 p-1 rounded-xl flex flex-col items-center text-center" href="/riwayat-isi-saldo">
                <i class="fa fa-list-alt fa-xl" aria-hidden="true"></i>
                History Saldo
            </a>
        </div>
        <div class="hover:scale-110 duration-300 flex flex-col p-1 text-yellow-700 justify-center w-full">
            <a class="shadow-md text-xs bg-yellow-100 ring-1 ring-yellow-300 p-1 rounded-xl flex flex-col items-center text-center" href="/history-order">
                <i class="fa fa-history fa-xl" aria-hidden="true"></i>
                History Order
            </a>
        </div>
        <div class="hover:scale-110 duration-300 flex flex-col p-1 text-yellow-700 justify-center w-full">
            <a class="shadow-md text-xs bg-purple-100 ring-1 ring-purple-300 p-1 rounded-xl flex flex-col items-center text-center" href="/info">
                <i class="fa fa-info-circle fa-xl" aria-hidden="true"></i>
                Info
            </a>
        </div>
    </div>
</div>
    <div class="flex flex-col p-9 justify-center items-center">
        <div class="hidden md:block w-1/2">
            @if ($slider_active)
            <div id="default-carousel" class="relative bg-purple-100 shadow-xl mb-1" data-carousel="slide">
                <!-- Carousel wrapper -->
                <div class="relative h-56 overflow-hidden rounded-lg md:h-96">
                     <!-- Item 1 -->
                    <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-0 z-20" data-carousel-item="">
                        <span class="absolute text-2xl font-semibold text-white -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 sm:text-3xl dark:text-gray-800">First Slide</span>
                        <a href="storage/images/slider/slider1.png" target="_blank">
                            <img src="{{ asset('storage/images/slider/slider1.png') }}" class="sliderImage hover:cursor-pointer absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-full" alt="slider1">
                        </a>
                    </div>
                    <!-- Item 2 -->
                    <div class="duration-700 ease-in-out absolute inset-0 transition-all transform translate-x-full z-10" data-carousel-item="">
                        <a href="storage/images/slider/slider2.png" target="_blank">
                            <img src="{{ asset('storage/images/slider/slider2.png') }}" class="sliderImage hover:cursor-pointer absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-full" alt="slider2">
                        </a>
                    </div>
                    <!-- Item 3 -->
                    <div class="duration-700 ease-in-out absolute inset-0 transition-all transform -translate-x-full z-10" data-carousel-item="">
                        <a href="storage/images/slider/slider3.png" target="_blank">
                            <img src="{{ asset('storage/images/slider/slider3.png') }}" class="sliderImage hover:cursor-pointer absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2 h-full" alt="slider3">
                        </a>
                    </div>
                </div>
                <!-- Slider indicators -->
                <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                    <button type="button" class="w-3 h-3 rounded-full bg-white dark:bg-gray-800" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                    <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                    <button type="button" class="w-3 h-3 rounded-full bg-white/50 dark:bg-gray-800/50 hover:bg-white dark:hover:bg-gray-800" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                </div>
                <!-- Slider controls -->
                <button type="button" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev="">
                    <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>
                        <span class="sr-only">Previous</span>
                    </span>
                </button>
                <button type="button" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next="">
                    <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                        <span class="sr-only">Next</span>
                    </span>
                </button>
            </div>
            @endif
            <div class="w-full">
                <a class="p-2 flex flex-col items-center bg-blue-100 rounded-b-lg shadow-md hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
                    <div class="w-1/3 flex justify-center items-center">
                        <img class="w-20 h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/{{ auth()->user()->levelMember->level . '.png' }}" alt="levelMember">
                    </div>
                    <div class="flex flex-col gap-1 w-full items-center">
                        <div class="flex flex-col justify-between items-center p-1 leading-normal w-3/4 bg-blue-200 rounded-xl">
                            <h5 class="mb-2 text-sm font-bold tracking-tight text-gray-900 dark:text-white">Hallo, {{ auth()->user()->username }}!</h5>
                            <p class="text-md font-normal dark:text-gray-400">
                                Level :
                                <span class="font-bold text-lg {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                                    {{ auth()->user()->levelMember->level_name }}
                                </span>
                            </p>
                        </div>
                        @if (auth()->user()->levelMember->level < 3)
                        <div class="flex justify-center">
                            @if (auth()->user()->upgradeLevel && auth()->user()->upgradeLevel->status)
                                <div class="flex flex-col gap-1">
                                    <small class="text-orange-800 text-xs text-center">Upgrade {{ auth()->user()->upgradeLevel->levelMember->level_name }} requested</small>
                                    <small class="text-red-500 text-xs">Expired at {{ auth()->user()->upgradeLevel->created_at->addDays(1) }}</small>
                                    <button type="button" class="buttonInfoPembayaran bg-blue-300 text-blue-800 hover:bg-blue-400 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">cek info pembayaran</button>
                                </div>
                            @endif
                        </div>
                        @endif
                        @if(auth()->user()->upgradeLevelAccept)
                        <div class="text-sm flex flex-col justify-between items-center p-1 leading-normal w-3/4">
                            <div class="flex justify-start w-full">
                                <p class="text-xs font-normal dark:text-gray-400">
                                    Masa Berlaku {{ auth()->user()->levelMember->level_name }} :
                                    <span class="font-bold text-sm {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                                        {{ auth()->user()->upgradeLevelAccept->start_date }} <span class="text-gray-900">s/d</span> {{ auth()->user()->upgradeLevelAccept->end_date }}
                                    </span>
                                </p>
                            </div>
                            <div class="flex justify-start w-full">
                                <p class="text-xs font-normal dark:text-gray-400">
                                    Total saldo digunakan :
                                    <span class="font-bold text-sm {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                                        {{ Helpers::format_rupiah(auth()->user()->upgradeLevelAccept->saldo_digunakan   ) }}
                                    </span>
                                </p>
                            </div>
                        </div>
                        @endif
                    </div>
                </a>
            </div>
            {{-- Pengumuman No WA Terblokir --}}
            {{-- <div id="alert-additional-content-1" class="mt-2 p-4 mb-4 border border-red-300 rounded-lg bg-red-50 dark:bg-red-200" role="alert">
                <div class="flex items-center">
                  <svg aria-hidden="true" class="w-5 h-5 mr-2 text-red-900 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                  <span class="sr-only">Info</span>
                  <h3 class="text-lg font-medium text-red-900 dark:text-red-800">Pengumuman</h3>
                </div>
                <div class="mt-2 mb-4 text-sm text-red-900 dark:text-red-800">
                  Mohon maaf bagi para member yang kesulitan menghubungi CS.
                  <br>
                  <br>
                  Saat ini nomor WhatsApp CS Keranjangindo sedang terblokir dan sedang mengajukan banding ke pihak WhatsApp, sehingga WA sementara dinonaktifkan.
                  <br>
                  <br>
                  Untuk dapat menghubungi CS sementara bisa ke WhatsApp kami di nomor <a class="text-purple-800 hover:cursor-pointer" href="https://api.whatsapp.com/send/?phone=6287777493049" target="_blank">087777 49 30 49</a>
                  Terima kasih
                </div>
                <div class="flex">
                  <a href="https://api.whatsapp.com/send/?phone=6287777493049" target="_blank" class="text-white bg-green-900 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-green-800 dark:hover:bg-green-900">
                    <i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>
                    WhatsApp
                  </a>
                  <button type="button" class="buttonClosePengumuman text-red-900 bg-transparent border border-red-900 hover:bg-red-900 hover:text-white focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-xs px-3 py-1.5 text-center dark:border-red-800 dark:text-red-800 dark:hover:text-white" data-dismiss-target="#alert-additional-content-1" aria-label="Close">
                    Close
                  </button>
                </div>
            </div> --}}

            <div class="flex gap-5 overflow-x-auto p-2 justify-between">
                <div class="flex flex-col p-1 text-green-700 justify-center w-full">
                    <a class=" shadow-md text-xs bg-green-200 ring-1 ring-green-300 p-1 rounded-xl flex flex-col items-center text-center" href="/tambah-saldo">
                        <i class="fa fa-plus-circle fa-xl" aria-hidden="true"></i>
                        {{ Helpers::format_rupiah(auth()->user()->saldo) }}
                    </a>
                </div>
                <div class="flex flex-col p-1 text-yellow-700 justify-center w-full">
                    <a class=" shadow-md text-xs bg-yellow-200 ring-1 ring-yellow-300 p-1 rounded-xl flex flex-col items-center text-center" href="/new-order">
                        <i class="fa fa-cart-plus fa-xl" aria-hidden="true"></i>
                        New Order
                    </a>
                </div>
                <div class="flex flex-col p-1 text-purple-700 justify-center w-full">
                    <a class=" shadow-md text-xs bg-purple-200 ring-1 ring-purple-300 p-1 rounded-xl flex flex-col items-center text-center" href="/service">
                        <i class="fa fa-list-ul fa-xl" aria-hidden="true"></i>
                        Services
                    </a>
                </div>
            </div>
            <div class="flex gap-5 p-2 justify-between">
                <div class="hover:scale-110 duration-300 flex flex-col p-1 text-green-700 justify-center w-full">
                    <a class="shadow-md text-xs bg-green-100 ring-1 ring-green-300 p-1 rounded-xl flex flex-col items-center text-center" href="/riwayat-isi-saldo">
                        <i class="fa fa-list-alt fa-xl" aria-hidden="true"></i>
                        History Saldo
                    </a>
                </div>
                <div class="hover:scale-110 duration-300 flex flex-col p-1 text-yellow-700 justify-center w-full">
                    <a class="shadow-md text-xs bg-yellow-100 ring-1 ring-yellow-300 p-1 rounded-xl flex flex-col items-center text-center" href="/history-order">
                        <i class="fa fa-history fa-xl" aria-hidden="true"></i>
                        History Order
                    </a>
                </div>
                <div class="hover:scale-110 duration-300 flex flex-col p-1 text-yellow-700 justify-center w-full">
                    <a class="shadow-md text-xs bg-purple-100 ring-1 ring-purple-300 p-1 rounded-xl flex flex-col items-center text-center" href="/info">
                        <i class="fa fa-info-circle fa-xl" aria-hidden="true"></i>
                        Info
                    </a>
                </div>
            </div>
        </div>

        <div id="alert-additional-content-1" class="flex flex-col justify-center items-center p-4 mb-4 border border-blue-100 rounded-lg bg-blue-100 dark:bg-blue-300 shadow-lg shadow-blue-300" role="alert">
            <div class="flex items-center justify-center gap-2 hover:scale-105 duration-500">
                <i class="fa fa-angle-double-up" aria-hidden="true"></i>
                <h3 class="text-md md:text-xl font-medium text-blue-900">Yuk upgrade level member!</h3>
                <i class="fa fa-angle-double-up" aria-hidden="true"></i>
            </div>
            <div class="flex flex-col items-center mt-2 mb-4 text-sm text-gray-900">
                <p class="text-center">Harga service lebih murah </p>
                <p class="text-center"><span class="font-bold text-yellow-800 text-lg">&</span> </p>
                <p class="text-center">Dapatkan berbagai <span class="font-bold text-purple-800 text-md md:text-2xl">discount menarik</span> khusus level member tertentu!</p>
            </div>
            <div class="flex">
              <a href="/upgrade-level" class="text-white bg-blue-900 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-200 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-blue-800 dark:hover:bg-blue-900">
                Cek Selengkapnya
              </a>
            </div>
        </div>
        <div class="flex flex-col justify-center w-full md:w-1/2 gap-5">
            {{-- Service Information Update - START--}}
            <div id="accordion-collapse" data-accordion="collapse">
                <h2 id="accordion-collapse-heading-2">
                  <button type="button" class="shadow-md flex items-center justify-between w-full p-5 font-medium text-left text-gray-800 border border-b-0 border-gray-300 rounded-lg focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-800" data-accordion-target="#accordion-collapse-body-2" aria-expanded="false" aria-controls="accordion-collapse-body-2">
                    <span>Informasi Update Service</span>
                    <svg data-accordion-icon class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                  </button>
                </h2>
                <div id="accordion-collapse-body-2" class="shadow-md hidden" aria-labelledby="accordion-collapse-heading-2">
                  <div class="h-auto p-5 font-light border border-b-0 border-gray-300 rounded-lg dark:border-gray-700">
                    <div>
                        @if ($service_information_updates->count() > 0)
                        <h1 class="text-start text-lg md:text-2xl mb-5">Informasi Update Service</h1>
                        @endif
                        <div class="w-full md:w-1/3 mb-5">
                            <label for="filter_info" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Filter Info</label>
                            <select id="filter_info" name="filter_info" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected disabled value="">Select Filter</option>
                                <option value="all">All</option>
                                <option value="new service">New Service</option>
                                <option value="harga NAIK">Harga Naik</option>
                                <option value="harga TURUN">Harga Turun</option>
                                <option value="disabled">Disabled</option>
                                <option value="comeback">Comeback</option>
                            </select>
                        </div>
                        <div hidden role="status" class="max-w-sm animate-pulse" id="loading-image">
                            <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="grid grid-cols-1 gap-1" id="serviceInformationUpdate">
                            @foreach ($service_information_updates as $siu)
                            <div id="alert-additional-content-1" class="col-span-5 xl:col-span-1 w-full p-4 mb-4 border {{ $siu->info == 'comeback' ? 'border-yellow-300' : ($siu->info == 'harga NAIK' ? 'border-orange-300' : ($siu->info == 'harga TURUN' ? 'border-green-300' : ($siu->info == 'disabled' ? 'border-red-300' : 'border-blue-300'))) }} rounded-lg {{ $siu->info == 'comeback' ? 'bg-yellow-300' : ($siu->info == 'harga NAIK' ? 'bg-orange-300' : ($siu->info == 'harga TURUN' ? 'bg-green-300' : ($siu->info == 'disabled' ? 'bg-red-300' : 'bg-blue-300'))) }} {{ $siu->info == 'comeback' ? 'dark:bg-yellow-300' : ($siu->info == 'harga NAIK' ? 'dark:bg-orange-300' : ($siu->info == 'harga TURUN' ? 'dark:bg-green-300' : ($siu->info == 'disabled' ? 'dark:bg-red-300' : 'dark:bg-blue-300'))) }}" role="alert">
                                <div class="flex items-center">
                                  <svg aria-hidden="true" class="w-5 h-5 mr-2 {{ $siu->info == 'comeback' ? 'text-yellow-800' : ($siu->info == 'harga NAIK' ? 'text-orange-800' : ($siu->info == 'harga TURUN' ? 'text-green-800' : ($siu->info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }}" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                                  <span class="sr-only">Info</span>
                                  <h3 class="text-lg font-medium {{ $siu->info == 'comeback' ? 'text-yellow-800' : ($siu->info == 'harga NAIK' ? 'text-orange-800' : ($siu->info == 'harga TURUN' ? 'text-green-800' : ($siu->info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }}">{{ ucfirst($siu->info) }}</h3>
                                </div>
                                <div class="mt-2 mb-4 text-sm {{ $siu->info == 'comeback' ? 'text-yellow-800' : ($siu->info == 'harga NAIK' ? 'text-orange-800' : ($siu->info == 'harga TURUN' ? 'text-green-800' : ($siu->info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }}">
                                    {{ $siu->service_id }} - {{ $siu->service->service_name }}
                                </div>
                                @if ($siu->info != 'disabled')
                                <hr>
                                <div class="mt-2 mb-4 text-sm {{ $siu->info == 'comeback' ? 'text-yellow-800' : ($siu->info == 'harga NAIK' ? 'text-orange-800' : ($siu->info == 'harga TURUN' ? 'text-green-800' : ($siu->info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }} flex flex-col">
                                    @if ($siu->info == 'new service')
                                    <div>
                                        Harga : <br class="md:hidden"> <span class="font-bold">{{ Helpers::format_rupiah_per_unit($siu->autoPriceLog->new_rate * auth()->user()->levelMember->mark_up / 1000) }} <p class="text-xs inline">/ quantity</p></span>
                                    </div>
                                    @else
                                    <div>
                                        Harga baru : <br class="md:hidden"> <span class="font-bold text-purple-500">{{ Helpers::format_rupiah_per_unit($siu->autoPriceLog->new_rate * auth()->user()->levelMember->mark_up / 1000) }} <p class="text-xs inline">/ quantity</p></span>
                                    </div>
                                    <div>
                                        Harga lama : <br class="md:hidden"> <span class="font-bold text-yellow-800">{{ Helpers::format_rupiah_per_unit($siu->autoPriceLog->old_rate * auth()->user()->levelMember->mark_up / 1000) }} <p class="text-xs inline">/ quantity</p></span>
                                    </div>
                                    @endif
                                </div>
                                @endif
                                <div class="mt-2 mb-4 text-sm {{ $siu->info == 'comeback' ? 'text-yellow-800' : ($siu->info == 'harga NAIK' ? 'text-orange-800' : ($siu->info == 'harga TURUN' ? 'text-green-800' : ($siu->info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }} flex flex-col">
                                    <div>
                                        Update : <span class="italic">{{ $siu->updated_at }}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                  </div>
                </div>
            </div>
            {{-- Service Information Update - END--}}

            {{-- Service Information Discount - START--}}
            <div id="accordion-collapse" data-accordion="collapse">
                <h2 id="accordion-collapse-heading-3">
                  <button type="button" class=" shadow-md flex items-center justify-between w-full p-5 font-medium text-left text-gray-800 border border-b-0 border-gray-300 rounded-lg focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-800" data-accordion-target="#accordion-collapse-body-3" aria-expanded="false" aria-controls="accordion-collapse-body-3">
                    <span>Informasi Discount</span>
                    <svg data-accordion-icon class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                  </button>
                </h2>
                <div id="accordion-collapse-body-3" class="shadow-md hidden" aria-labelledby="accordion-collapse-heading-3">
                    <div class="p-5 font-light border border-b-0 border-gray-300 rounded-lg dark:border-gray-700">
                      <div>
                        @if($discounts->count() > 0)
                          <h1 class="text-start text-lg md:text-2xl mb-5">Informasi Discount</h1>
                        @endif
                          <div class="flex flex-col">
                              @foreach ($discounts as $discount)
                              <div class="hover:scale-105 duration-500 flex p-4 mb-4 text-sm text-blue-700 bg-blue-200 ring-1 ring-blue-300 rounded-lg dark:bg-blue-200 dark:text-blue-800" role="alert">
                                <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                                <span class="sr-only">Info</span>
                                <div class="flex flex-col">
                                  <span class="font-bold">{{ $discount->discount_name }}</span>
                                  <span>{{ $discount->service->id }} - {{ $discount->service->service_name }}</span>
                                  <span class="text-yellow-800">Discount : {{ ceil($discount->discount) }}%</span>
                                  <span class="text-yellow-800">Level Member : {{ $discount->levelMember->level_name }} ++</span>
                                  <span>Start : {{ $discount->start_date }}</span>
                                  <span class="mb-2">End : {{ $discount->end_date }}</span>
                                  <div>
                                      <a href="/new-order" class="bg-blue-300 text-blue-800 text-xs font-semibold px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">order now</a>
                                  </div>
                                </div>
                              </div>
                              @endforeach
                          </div>
                      </div>
                    </div>
                  </div>
            </div>
            {{-- Service Information Discount - END--}}
        </div>
    </div>

    <script>
        function formatRupiah(angka, prefix){
			var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? (Math.ceil(rupiah * 100) / 100).toFixed(1).replace('.', ',') : ((Math.ceil(rupiah * 100) / 100).toFixed(1).replace('.', ',') ? 'Rp ' + (Math.ceil(rupiah * 100) / 100).toFixed(1).replace('.', ',') : '');
		}
        function formatRupiahRibuan(angka, prefix){
			var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
		}
    </script>

    <script>
        function addHours(numOfHours, date = new Date()) {
            date.setTime(date.getTime() + numOfHours * 60 * 60 * 1000);

            return date;
        }
        $('#filter_info').change(function(){
            $('#serviceInformationUpdate').empty();
            $.ajax({
                url : '/dashboard',
                data : {
                    filter_info : $(this).val()
                },
                beforeSend: function() {
                    $("#loading-image").removeAttr('hidden');
                },
                success : function(res){
                    // console.log(res);
                    $.each(res, function(index, value){
                        $("#loading-image").attr('hidden', true);
                        $('#serviceInformationUpdate').append(`
                            <div id="alert-additional-content-1" class="col-span-5 xl:col-span-1 w-full p-4 mb-4 border ${ value.info == 'comeback' ? 'border-yellow-300' : (value.info == 'harga NAIK' ? 'border-orange-300' : (value.info == 'harga TURUN' ? 'border-green-300' : (value.info == 'disabled' ? 'border-red-300' : 'border-blue-300'))) } rounded-lg ${ value.info == 'comeback' ? 'bg-yellow-300' : (value.info == 'harga NAIK' ? 'bg-orange-300' : (value.info == 'harga TURUN' ? 'bg-green-300' : (value.info == 'disabled' ? 'bg-red-300' : 'bg-blue-300'))) } ${ value.info == 'comeback' ? 'dark:bg-yellow-300' : (value.info == 'harga NAIK' ? 'dark:bg-orange-300' : (value.info == 'harga TURUN' ? 'dark:bg-green-300' : (value.info == 'disabled' ? 'dark:bg-red-300' : 'dark:bg-blue-300'))) }" role="alert">
                                <div class="flex items-center">
                                    <svg aria-hidden="true" class="w-5 h-5 mr-2 ${ value.info == 'comeback' ? 'text-yellow-800' : (value.info == 'harga NAIK' ? 'text-orange-800' : (value.info == 'harga TURUN' ? 'text-green-800' : (value.info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                                    <span class="sr-only">Info</span>
                                    <h3 class="text-lg font-medium ${ value.info == 'comeback' ? 'text-yellow-800' : (value.info == 'harga NAIK' ? 'text-orange-800' : (value.info == 'harga TURUN' ? 'text-green-800' : (value.info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }">${ value.info }</h3>
                                </div>
                                <div class="mt-2 mb-4 text-sm ${ value.info == 'comeback' ? 'text-yellow-800' : (value.info == 'harga NAIK' ? 'text-orange-800' : (value.info == 'harga TURUN' ? 'text-green-800' : (value.info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) }">
                                    ${ value.service_id } - ${ value.service.service_name }
                                </div>
                                ${value.info != 'disabled' ? `<hr>
                                <div class="mt-2 mb-4 text-sm ${ value.info == 'comeback' ? 'text-yellow-800' : (value.info == 'harga NAIK' ? 'text-orange-800' : (value.info == 'harga TURUN' ? 'text-green-800' : (value.info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) } flex flex-col">
                                    ${value.info == 'new service' ? `<div>
                                        Harga : <br class="md:hidden"> <span class="font-bold">${ formatRupiah(value.auto_price_log.new_rate * '{{ auth()->user()->levelMember->mark_up }}', 'Rp ') } <p class="text-xs inline">/ quantity</p></span>
                                    </div>` : `<div>
                                        Harga baru : <br class="md:hidden"> <span class="font-bold text-purple-500">${ formatRupiah(value.auto_price_log.new_rate * '{{ auth()->user()->levelMember->mark_up }}', 'Rp ') } <p class="text-xs inline">/ quantity</p></span>
                                    </div>
                                    <div>
                                        Harga lama : <br class="md:hidden"> <span class="font-bold text-yellow-800">${ formatRupiah(value.auto_price_log.old_rate * '{{ auth()->user()->levelMember->mark_up }}', 'Rp ') } <p class="text-xs inline">/ quantity</p></span>
                                    </div>`}
                                </div>` : ``}
                                <div class="mt-2 mb-4 text-sm ${ value.info == 'comeback' ? 'text-yellow-800' : (value.info == 'harga NAIK' ? 'text-orange-800' : (value.info == 'harga TURUN' ? 'text-green-800' : (value.info == 'disabled' ? 'text-red-800' : 'text-blue-800'))) } flex flex-col">
                                    <div>
                                        Update : <span class="italic">${addHours(7, new Date(value.updated_at)).toLocaleString('en-CA', {hour12:false})}</span>
                                    </div>
                                </div>
                            </div>
                        `);
                    })
                }
            });
        })
    </script>

    <script>
        $('.buttonInfoPembayaran').click(function(){
        $.ajax({
            url : '/upgrade-level',
            data : {
                bank : true
            },
            success : function(res){
                // console.log(res);
                Swal.fire({
                title: '<strong>Upgrade ke level Reseller</strong>',
                icon: 'info',
                html:
                    'Segera Selesaikan pembayarannya sebesar ' + '<br>' + `${formatRupiahRibuan(res.nominal, 'Rp ')}` + '<br>' +
                    `Rekening BCA a/n ${res.bank.atas_nama}` + '<br>' +
                    `No. Rekening : ${res.bank.no_rek}`,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false
            })
            }
        });
    });
    </script>

    <script>
        // $('.sliderImage').click(function(){
        //     const src = $(this).attr('data-src');

        //     console.log(src);
        // });
    </script>
@endsection
