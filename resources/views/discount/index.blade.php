@extends('layouts.main')

@section('content')
    <div class="p-9">
        <h1 class="italic text-2xl mb-5">Discount</h1>
        <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full md:w-1/2">
            <form action="/admin-discount" method="POST">
                @csrf
                <div class="mb-6">
                    <div class="flex flex-col md:flex-row gap-1">
                        <div class="md:w-3/4">
                            <label for="service_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service</label>
                            <select id="service_id" name="service_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected disabled>Pilih Service</option>
                                @foreach ($services as $service)
                                <option value="{{ $service->id }}">{{ $service->id }} - {{ $service->service_name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('service_id'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('service_id') }}</small>
                            @endif
                        </div>
                        <div class="md:w-1/4">
                            <label for="discount" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Discount</label>
                            <div class="flex">
                                <input type="number" id="discount"  name="discount" class="rounded-none rounded-l-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 rounded-r-md border border-r-0 border-gray-300 dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">%</span>
                            </div>
                            @if ($errors->has('discount'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('discount') }}</small>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="mb-6">
                    <div class="flex flex-col md:flex-row gap-1">
                        <div class="w-full">
                            <label for="discount_name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Discount Name</label>
                            <input type="text" id="discount_name" name="discount_name" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('discount_name'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('discount_name') }}</small>
                            @endif
                        </div>
                        <div class="w-full">
                            <label for="level_member_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Level Member</label>
                            <select id="level_member_id" name="level_member_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected disabled>Pilih Level Member</option>
                                @foreach ($level_members as $level_member)
                                <option value="{{ $level_member->id }}">{{ $level_member->level_name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('level_member_id'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('level_member_id') }}</small>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="mb-6">
                    <div class="flex flex-col md:flex-row gap-1">
                        <div class="w-full">
                            <label for="start_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Start Date</label>
                            <input type="datetime-local" id="start_date" name="start_date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('start_date'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('start_date') }}</small>
                            @endif
                        </div>
                        <div class="w-full">
                            <label for="end_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">End Date</label>
                            <input type="datetime-local" id="end_date" name="end_date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('end_date'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('end_date') }}</small>
                            @endif
                        </div>
                    </div>
                </div>
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
            </form>
        </div>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg mt-10 p-4">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableDiscount">
                <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Nama Discount
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Discount
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Service
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Level Member
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Start Date
                        </th>
                        <th scope="col" class="py-3 px-6">
                            End Date
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Create By
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Status
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($discounts as $discount)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $discount->discount_name }}
                        </th>
                        <td class="py-4 px-6">
                            {{ floor($discount->discount) . ' %' }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $discount->service->service_name }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $discount->levelMember->level_name }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $discount->start_date }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $discount->end_date }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $discount->user->username }}
                        </td>
                        <td class="py-4 px-6 {{ $discount->end_date > now() ? 'text-green-500' : 'text-red-500' }}">
                            {{ $discount->end_date > now() ? 'ongoing' : 'done' }}
                        </td>
                        <td class="py-4 px-6 text-right">
                            <span data-id="{{ $discount->id }}" class="deleteDiscount hover:cursor-pointer bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">
                                delete
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#tableDiscount').DataTable({
                "pageLength": 20,
                // responsive: true,
                "lengthChange": false,
                // "ordering": true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: ["_all"]
                    },
                    {
                        target:1,
                        className:'cell-border'
                    }
                ],
                order: [[1, 'asc']],
            });
        });
    </script>

    <script>
        $('.deleteDiscount').click(function(){
            const id = $(this).attr('data-id');
            Swal.fire({
                title: 'Kamu yakin?',
                text: "Discount akan dihapus",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/admin-discount/' + id,
                        method : 'DELETE',
                        data : {
                            '_token' : '{{ csrf_token() }}'
                        },
                        success : function(res){
                            console.log(res);
                            Swal.fire({
                                text: 'Discount Berhasil dihapus!',
                                icon : 'success'
                            }).then(() => {
                                window.location = '/admin-discount';
                            });
                        }
                    });

                }
            })
        })
    </script>
@endsection
