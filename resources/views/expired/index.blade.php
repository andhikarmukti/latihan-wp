<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- Favicon --}}
    <link rel="icon" href="{{ asset('storage\images\logo\logowebsite.png') }}">
    {{-- Tailwind CSS --}}
    <link rel="stylesheet" href="/css/tailwindstyle.css">
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    {{-- DataTable CSS --}}
    <link rel="stylesheet" type="text/css" href="/vendor/dataTables/datatables.css"/>
    {{-- Font Awesome --}}
    <script src="https://use.fontawesome.com/cc283b5013.js"></script>
    {{-- Font --}}
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet">
    {{-- OG image --}}
    <meta property="og:image" content="{{ asset('storage\images\logo\logowebsite.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('storage\images\logo\logowebsite.png') }}" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="400" />
    <meta property="og:image:height" content="300" />
    <meta property="og:image:alt" content="Website untuk tempat mencari tambahan income di era digital yang bisa dilakukan di rumah aja." />
    {{-- Midtrans --}}
    <script type="text/javascript" src="{{ config('midtrans.snap_url') }}" data-client-key="{{ config('midtrans.client_key') }}"></script>
    {{-- Animate CSS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <style>
        html{
            font-family: 'Montserrat', sans-serif;
        }
    </style>
    <title>Expired Web</title>
</head>
<body>
    <div class="flex flex-col items-center justify-center h-screen">
        <h1 class="text-red-700 text-2xl md:text-4xl text-center">Website telah Expired</h1>
        <h2>Silahkan lakukan perpanjangan website Anda.</h2>
    </div>
</body>
</html>
