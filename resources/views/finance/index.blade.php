@extends('layouts.main')

@section('content')
<div class="p-9">
    {{-- <div class="w-1/5 mb-5">
        <form class="flex gap-2" action="/admin-finance" method="GET">
            <div class="mb-6">
                <input type="date" name="date" id="date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@flowbite.com" required="">
            </div>
            <div>
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
            </div>
        </form>
    </div> --}}

    <p>Total Topup Saldo : {{ Helpers::format_rupiah($topup_saldo) }}</p>
    <p>Total Saldo Digunakan [COMPLETED] : {{ Helpers::format_rupiah($total_saldo_digunakan_completed) }}</p>
    <p>Total Saldo Digunakan [PENDING] : {{ Helpers::format_rupiah($total_saldo_digunakan_pending) }}</p>
    <p>Total Saldo User : {{ Helpers::format_rupiah($total_saldo_user) }}</p>
    <div class="border border-slate-400 rounded-lg shadow-md p-5 my-4 w-full md:w-1/5">
        <form action="/admin-finance" method="GET">
            <div class="flex gap-5">
                <div class="w-full">
                    <label for="month" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Bulan</label>
                    <select id="month" name="month" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected value="all">Select All</option>
                        <option {{ (app('request')->input('month') == 1 || !app('request')->input('month') && now()->format('m') == 1 ) ? 'selected' : '' }} value="1">Januari</option>
                        <option {{ (app('request')->input('month') == 2 || !app('request')->input('month') && now()->format('m') == 2 ) ? 'selected' : '' }} value="2">Februari</option>
                        <option {{ (app('request')->input('month') == 3 || !app('request')->input('month') && now()->format('m') == 3 ) ? 'selected' : '' }} value="3">Maret</option>
                        <option {{ (app('request')->input('month') == 4 || !app('request')->input('month') && now()->format('m') == 4 ) ? 'selected' : '' }} value="4">April</option>
                        <option {{ (app('request')->input('month') == 5 || !app('request')->input('month') && now()->format('m') == 5 ) ? 'selected' : '' }} value="5">Mei</option>
                        <option {{ (app('request')->input('month') == 6 || !app('request')->input('month') && now()->format('m') == 6 ) ? 'selected' : '' }} value="6">Juni</option>
                        <option {{ (app('request')->input('month') == 7 || !app('request')->input('month') && now()->format('m') == 7 ) ? 'selected' : '' }} value="7">Juli</option>
                        <option {{ (app('request')->input('month') == 8 || !app('request')->input('month') && now()->format('m') == 8 ) ? 'selected' : '' }} value="8">Agustus</option>
                        <option {{ (app('request')->input('month') == 9 || !app('request')->input('month') && now()->format('m') == 9 ) ? 'selected' : '' }} value="9">September</option>
                        <option {{ (app('request')->input('month') == 10 ||!app('request')->input('month') &&  now()->format('m') == 10 ) ? 'selected' : '' }} value="10">Oktober</option>
                        <option {{ (app('request')->input('month') == 11 ||!app('request')->input('month') &&  now()->format('m') == 11 ) ? 'selected' : '' }} value="11">November</option>
                        <option {{ (app('request')->input('month') == 12 ||!app('request')->input('month') &&  now()->format('m') == 12 ) ? 'selected' : '' }} value="12">Desember</option>
                    </select>
                </div>
                <div class="w-full">
                    <label for="year" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Tahun</label>
                    <select id="year" name="year" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected disabled>Select Year</option>
                        <option {{ (app('request')->input('year') == 2022 || !app('request')->input('year') && now()->format('Y') == 2022) ? 'selected' : '' }} value="2022">2022</option>
                        <option {{ (app('request')->input('year') == 2023 || !app('request')->input('year') && now()->format('Y') == 2023) ? 'selected' : '' }} value="2023">2023</option>
                        <option {{ (app('request')->input('year') == 2024 || !app('request')->input('year') && now()->format('Y') == 2024) ? 'selected' : '' }} value="2024">2024</option>
                        <option {{ (app('request')->input('year') == 2025 || !app('request')->input('year') && now()->format('Y') == 2025) ? 'selected' : '' }} value="2025">2025</option>
                    </select>
                </div>
            </div>
            <div class="w-full bg-yellow-100 mt-2">
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-1 py-1 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
            </div>
        </form>
    </div>
    <div class="border border-slate-400 rounded-lg shadow-md p-5 my-4 w-max">
        <p>Total Omset : {{ Helpers::format_rupiah($total_omset - $total_partial)}}</p>
        <p>Total Modal : {{ Helpers::format_rupiah($total_hpp) }}</p>
        <p class="font-bold text-xl text-green-500">Total Profit : {{ Helpers::format_rupiah($total_omset - $total_hpp) }}</p>
    </div>

    <div class="overflow-x-auto relative shadow-md sm:rounded-lg p-5">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableProfit">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        Username
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Order ID
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Service ID
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Discount
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Harga
                    </th>
                    <th scope="col" class="py-3 px-6">
                        HPP
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Profit
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Note
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Tanggal
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($histories as $history)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $history->user->username }}
                    </th>
                    <td class="py-4 px-6">
                        {{ $history->id }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history->service->id }}
                    </td>
                    <td class="py-4 px-6">
                        {{ (int)$history->discount == 0 ? '-' : (int)$history->discount . '%' }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history->note == '-' ? $history->harga : $history->harga - (int)str_replace('.', '', str_replace('Refund Rp ', '', $history->note)) }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history->note == '-' ? $history->hpp : ($history->harga - (int)str_replace('.', '', str_replace('Refund Rp ', '', $history->note))) / ($history->harga / $history->hpp)}}
                    </td>
                    <td class="py-4 px-6 text-green-500">
                        {{ $history->note == '-' ? $history->harga - $history->hpp : $history->harga - (int)str_replace('.', '', str_replace('Refund Rp ', '', $history->note)) - ($history->harga - (int)str_replace('.', '', str_replace('Refund Rp ', '', $history->note))) / ($history->harga / $history->hpp) }}
                    </td>
                    <td class="py-4 px-6 text-red-500">
                        {{ $history->note }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history->updated_at }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    // Datatables
    $(document).ready(function() {
        $('#tableProfit').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'colvis'
            ],
            "pageLength": 15,
            // responsive: true,
            // "lengthChange": false,
            "ordering": true,
            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: ["_all"]
                },
                {
                    target:1,
                    className:'cell-border'
                },
                {
                    target: [4, 5, 6],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                }
            ],
            order: [[1, 'desc']],
        });
    });
</script>
@endsection
