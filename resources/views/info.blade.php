@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full md:w-1/4 mb-5 bg-blue-100">
            <h3 class="text-2xl font-bol italic mb-5">Kontak Kami</h3>
            <p>WA : <a href="{{ $no_wa->info }}" class="text-blue-500 hover:text-blue-800" target="_blank">{{ $no_wa->value }}</a></p>
            <p>Email : <a href="{{ $email->info }}" class="text-blue-500 hover:text-blue-800" target="_blank">{{ $email->value }}</a></p>
            {{-- <p>Instagram : <a href="https://instagram.com/keranjangindo" class="text-blue-500 hover:text-blue-800" target="_blank">keranjangindo</a></p> --}}
        </div>
        <div id="accordion-open" data-accordion="open" class="shadow-md">
            <h2 id="accordion-open-heading-1">
              <button type="button" class="flex items-center justify-between w-full p-5 font-medium text-left border border-b-0 border-gray-200 rounde3-t-xl focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 hover:bg-gray-100 dark:hover:bg-gray-800 bg-gray-100 dark:bg-gray-800 text-gray-900 dark:text-white" data-accordion-target="#accordion-open-body-1" aria-expanded="true" aria-controls="accordion-open-body-1">
                <span class="flex items-center text-2xl"><svg class="w-5 h-5 mr-2 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path></svg>Kebijakan Pengiriman</span>
                <svg data-accordion-icon="" class="w-6 h-6 rotate-180 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
              </button>
            </h2>
            <div id="accordion-open-body-1" class="bg-blue-200 p-9" aria-labelledby="accordion-open-heading-1">
                <p>Penggunaan layanan yang disediakan oleh {{ $nama_web }} menetapkan kesepakatan dengan persyaratan ini. Dengan mendaftar atau menggunakan layanan {{ $nama_web }}, Anda berarti setuju bahwa Anda telah membaca dan memahami sepenuhnya persyaratan Layanan berikut dan {{ $nama_web }} tidak akan bertanggung jawab atas kerugian dengan cara apa pun bagi pengguna yang belum membaca persyaratan layanan di bawah ini.</p>
                <br>
                <p class="font-bold italic text-2xl">Kebijakan Pengiriman</p>
                <ul class="list-disc list-inside">
                    <li>
                        Dengan melakukan pemesanan dengan {{ $nama_web }}, Anda secara otomatis menerima semua persyaratan layanan yang tercantum di bawah ini baik Anda membacanya atau tidak.
                    </li>
                    <li>
                        Kami berhak mengubah persyaratan layanan ini tanpa pemberitahuan. Anda diharapkan untuk membaca semua persyaratan layanan sebelum melakukan setiap pesanan untuk memastikan Anda mengetahui setiap perubahan atau perubahan di masa mendatang.
                    </li>
                    <li>
                        Tarif {{ $nama_web }} dapat berubah sewaktu-waktu tanpa pemberitahuan. Ketentuan tetap berlaku jika terjadi perubahan tarif.
                    </li>
                    <li>
                        {{ $nama_web }} tidak menjamin waktu pengiriman untuk layanan apa pun karena tergantung pada jumlah pesanan layanan. Kami menawarkan estimasi terbaik kami untuk kapan pesanan akan dikirimkan. Ini hanya perkiraan dan {{ $nama_web }} tidak akan mengembalikan uang pesanan yang sedang diproses jika Anda merasa terlalu lama.
                    </li>
                    <li>
                        {{ $nama_web }} berusaha keras untuk memberikan apa yang diharapkan dari kami oleh customer, reseller atau agent kami. Dalam hal ini, kami berhak untuk mengubah jenis layanan jika kami menganggap perlu untuk menyelesaikan pesanan.
                    </li>
                    <li>
                        Informasi Drop Rate bisa dilihat dibagian deskripsi layanan. Jika terdapat tanda "QUALITY : -" maka kita tidak menyediakan informasi Drop Rate. Informasi pada bagian deskripsi hanya sebagai estimasi, tidak ada jaminan penuh bahwa layanan tidak akan mengalami drop dikemudian hari meskipun memiliki tanda Non Drop. Semua layanan yang ada di {{ $nama_web }} bisa mengalami Drop, tergantung pada update / kebijakan setiap platform media sosial.
                    </li>
                </ul>
                <br>
                <p class="font-bold italic text-2xl">Pelanggaran</p>
                <p>Jika Kami menemukan aktivitas yang tidak biasa atau jika {{ $nama_web }} menganggap Anda melanggar persyaratan {{ $nama_web }} atau membahayakan bisnis {{ $nama_web }}, dalam hal ini {{ $nama_web }} dapat berhenti berbisnis dengan pelanggan mana pun kapan saja. Sayangnya, untuk privasi bisnis kami, kami tidak akan dapat membagikan alasan penutupan akun atau bisnis dengan Anda. Keputusannya akan menjadi Parmanent. Anda dapat menggunakan sisa dana Anda untuk memesan layanan kami atau Anda dapat meminta penarikan dana Anda ke metode pembayaran yang Anda gunakan untuk mendepositkan dana tersebut.</p>
                <br>
                <p class="font-bold italic text-2xl">Disclaimer</p>
                <ul class="list-disc list-inside">
                    <li>
                        {{ $nama_web }} tidak akan bertanggung jawab atas segala kerusakan yang mungkin diderita Anda atau bisnis Anda.
                    </li>
                    <li>
                        {{ $nama_web }} sama sekali tidak bertanggung jawab atas penangguhan akun atau penghapusan konten yang dilakukan oleh YouTube atau Instagram atau Tiktok serta Media Sosial Lainnya.
                    </li>
                </ul>
            </div>
            <h2 id="accordion-open-heading-2">
              <button type="button" class="text-2xl flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-300 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-2" aria-expanded="false" aria-controls="accordion-open-body-2">
                <span class="flex items-center"><svg class="w-5 h-5 mr-2 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path></svg>Kebijakan Layanan</span>
                <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
              </button>
            </h2>
            <div id="accordion-open-body-2" class="hidden bg-blue-200 p-9" aria-labelledby="accordion-open-heading-2">
                <ul class="list-disc list-inside">
                    <li>
                        {{ $nama_web }} hanya akan digunakan untuk mempromosikan akun Youtube atau Instagram atau Tiktok atau Sosial Media Anda lainnya dan membantu meningkatkan "Penampilan" Anda saja.
                    </li>
                    <li>
                        Kami TIDAK menjamin followers baru Anda akan berinteraksi dengan Anda, kami hanya menjamin Anda untuk mendapatkan followers yang Anda bayar. Namun jika mengalami drop maka itu sudah menjadi bagian dari kesepakatan dan persetujuan Anda dalam melakukan order di {{ $nama_web }}.
                    </li>
                    <li>
                        Kami TIDAK menjamin 100% akun kami akan memiliki gambar profil, bio lengkap, dan gambar yang diunggah, meskipun kami berusaha untuk mewujudkannya untuk semua akun.
                    </li>
                    <li>
                        Anda tidak akan mengunggah apa pun ke situs {{ $nama_web }} termasuk ketelanjangan atau materi apa pun yang tidak diterima atau tidak sesuai untuk komunitas Youtube atau Instagram atau Tiktok atau Media Sosial lainnya.
                    </li>
                    <li>
                        Jika sosial media Anda sudah memiliki 100k+ followers / likes / views / lainnya sebelum Anda memesan pada kami, itu tidak akan mencakup perlindungan isi ulang, bahkan jika Anda memesan server isi ulang dari kami, karena tidak mungkin untuk memahami dari mana Anda kehilangan followers / likes / views / lainnya.
                    </li>
                </ul>
            </div>
            <h2 id="accordion-open-heading-3">
              <button type="button" class="text-2xl flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-gray-200 focus:r3ng-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-3" aria-expanded="false" aria-controls="accordion-open-body-3">
                <span class="flex items-center"><svg class="w-5 h-5 mr-2 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path></svg> Kebijakan Pembayaran dan Pengembalian Dana</span>
                <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
              </button>
            </h2>
            <div id="accordion-open-body-3" class="hidden bg-blue-200 p-9" aria-labelledby="accordion-open-heading-3">
                <ul class="list-disc list-inside">
                    <li>
                        Di {{ $nama_web }} Anda perlu menyetor kredit atau dana sebelum Anda menempatkan atau mengirimkan pesanan Anda. Kami menerima metode pembayaran utama seperti Dompet Digital, Bank Transfer, dan yang lainnya bergantung pada ketersediaan metode pembayaran. Anda harus mendaftar, pergi ke halaman Saldo, pilih metode pembayaran Anda dan setorkan kredit Anda. Setelah pembayaran Berhasil, Anda perlu melakukan konfirmasi ke CS kami dengan menyertakan Topup ID dan bukti transfer (bila diperlukan).
                    </li>
                    <li>
                        Di {{ $nama_web }} Setelah deposit, Anda dapat meminta pengembalian dana ke metode pembayaran Anda kapan pun Anda mau. Kami akan mengembalikan dana ke metode pembayaran Anda dalam waktu paling cepat 3x24 jam. Untuk mendapatkan uang ke kartu atau bank Anda bergantung pada jenis bank Anda. Namun, hal ini hanya akan dilakukan jika memang ada kesalahan yang dilakukan oleh pihak CS atau admin kami, tidak berlaku jika kesalahan seperti salah transfer atau salah order atau hanya ingin melakukan penarikan saja tanpa ada alasan yang berkaitan dengan teknis di {{ $nama_web }} atau kesalahan lainnya yang dilakukan oleh Anda.
                    </li>
                    <li>
                        Jika Anda mengajukan sengketa atau menagih kembali kepada kami setelah deposit tanpa alasan yang sah, kami berhak untuk menghentikan semua pesanan di masa mendatang, melarang Anda dari situs kami. Kami juga berhak untuk mengambil followers atau likes yang kami kirimkan ke Instagram/Facebook/Twitter/Tiktok Anda atau klien Anda atau akun media sosial lainnya.
                    </li>
                    <li>
                        Pesanan yang ditempatkan di {{ $nama_web }}, akan dikembalikan atau dibatalkan jika pelanggan memintanya. Anda akan menerima kredit pengembalian dana ke akun {{ $nama_web }} Anda jika pesanan tidak terkirim. Poin ini hanya berlaku pada layanan tertentu.
                    </li>
                    <li>
                        Jika halaman pesanan atau nama pengguna/URL/Tautan berubah, rusak, dihapus, atau akun ditangguhkan karena melanggar syarat & ketentuan media sosial, {{ $nama_web }} tidak bertanggung jawab atas hal ini dan jika status pesanan selesai dan orderan tidak masuk sesuai jumlah yang dipesan, {{ $nama_web }} tidak akan mengembalikan uang pesanan.
                    </li>
                    <li>
                        Pesanan akun yang salah Input atau Private, tidak akan memenuhi syarat untuk pengembalian dana. Pastikan untuk mengkonfirmasi setiap pesanan sebelum menempatkannya.
                    </li>
                    <li>
                        Aktivitas penipuan seperti menggunakan kartu kredit yang tidak sah atau dicuri akan menyebabkan penghentian akun Anda. Tidak ada pengecualian.
                    </li>
                    <li>
                        Harap jangan menggunakan lebih dari satu server secara bersamaan untuk target yang sama. Kami tidak dapat memberi Anda jumlah followers / likes yang benar dalam kasus itu. Kami tidak akan mengembalikan uang pesanan ini.
                    </li>
                </ul>
            </div>
            <h2 id="accordion-open-heading-4">
              <button type="button" class="text-2xl flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-gray-200 focus:r3ng-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-4" aria-expanded="false" aria-controls="accordion-open-body-4">
                <span class="flex items-center"><svg class="w-5 h-5 mr-2 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path></svg> Kebijakan Privasi</span>
                <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
              </button>
            </h2>
            <div id="accordion-open-body-4" class="hidden bg-blue-200 p-9" aria-labelledby="accordion-open-heading-3">
                <p class="font-bold italic text-lg">DEAR PELANGGAN {{ $nama_web }} - KAMI MENGHORMATI PRIVASI ANDA! KAMI BENCI PELANGGARAN PRIVASI. TAPI SELAMA PROSES BISNIS DENGAN ANDA, KAMI MUNGKIN PERLU MEMBAGIKAN BEBERAPA DATA ANDA DENGAN PIHAK KETIGA. RINCIAN KEBIJAKAN PRIVASI KAMI DIBERIKAN DI BAWAH INI. HARAP DICATAT BAHWA, DENGAN MELAKUKAN PEMESANAN DENGAN {{ $nama_web }}, ANDA SETUJU DENGAN SYARAT DAN KETENTUAN DAN KEBIJAKAN PRIVASI KAMI. KAMI DAPAT MENGUBAH KEBIJAKAN PRIVASI KAPANPUN.</p>
                <br>
                <p class="font-bold italic text-2xl">Pengumpulan data</p>
                <ul class="list-disc list-inside">
                    <li>
                        Dokumen ini menjelaskan kebijakan privasi resmi {{ $nama_web }}. Pengguna disarankan untuk membaca kebijakan privasi dan memastikan bahwa dia tidak memiliki ambiguitas dengan mengacu pada poin mana pun dari Kebijakan Privasi.
                    </li>
                    <li>
                        Pengguna diberitahu bahwa dengan mengunjungi, menggunakan {{ $nama_web }} atau berlangganan paket Layanan apa pun, dianggap bahwa pengguna telah menerima semua poin dari Kebijakan Privasi. Jika Anda tidak setuju dengan Kebijakan Privasi, mohon untuk tidak menggunakan {{ $nama_web }}.
                    </li>
                </ul>
                <br>
                <p class="font-bold italic text-2xl">Informasi yang kami kumpulkan</p>
                <p class="">Kami mungkin mengumpulkan informasi berikut dari pengguna kami dan menggunakannya untuk tujuan menyelesaikan transaksi, pekerjaan, dan pemeliharaan informasi keuangan atau strategis kami.</p>
                <ul class="list-disc list-inside">
                    <li>
                        Nama
                    </li>
                    <li>
                        Nama Bisnis
                    </li>
                    <li>
                        Nama pengguna akun media sosial klien
                    </li>
                    <li>
                        Surel
                    </li>
                    <li>
                        Nomor Telpon
                    </li>
                    <li>
                        Preferensi pengguna dan riwayat penggunaan internet untuk optimalisasi layanan dengan menggunakan cookie.
                    </li>
                </ul>
                <br>
                <p class="font-bold italic text-2xl">Prihal Cookie</p>
                <ul class="list-disc list-inside">
                    <li>
                        Cookie adalah sepotong kecil data yang terdiri dari string informasi teks yang diperkenalkan oleh situs web tertentu dan disimpan di hard disk mesin. Mereka mengumpulkan data yang berkaitan dengan preferensi dan riwayat penelusuran, dan informasi lain yang diberikan pengguna ke situs web.
                    </li>
                    <li>
                        Cookie kami bertujuan untuk memberikan pengguna penggunaan {{ $nama_web }} yang optimal. Informasi yang dikumpulkan juga digunakan untuk pemasaran umum dan analisis statistik. Cookie kami tidak mengumpulkan data pribadi pengguna. Beberapa cookie dikomunikasikan ke hard disk pengguna oleh pihak ketiga. {{ $nama_web }} tidak memiliki afiliasi dengan pihak tersebut. {{ $nama_web }} melepaskan tanggung jawab apa pun yang timbul dari cookie tersebut. Klien disarankan untuk membaca kebijakan masing-masing pihak ketiga atau memblokir pintu masuk mereka.
                    </li>
                    <li>
                        Secara default, browser diatur untuk mengizinkan cookie. Pengguna dapat memilih untuk menonaktifkan cookie jika dia tidak ingin cookie tersebut diproses di mesin pengguna dengan mengubah pengaturan browser. Dimungkinkan juga untuk mengonfigurasi pengaturan browser yang memungkinkan beberapa dan menolak yang lain untuk memiliki akses ke komputer pengguna atau mengatur browser untuk memberi tahu ketika cookie meminta akses.
                    </li>
                </ul>
                <br>
                <p class="font-bold italic text-2xl">Keamanan dan Kerahasiaan Informasi</p>
                <p class="">{{ $nama_web }} menghormati privasi pengguna. {{ $nama_web }} menggunakan sistem keamanan yang canggih untuk memastikan bahwa informasi tersebut berada dalam staf {{ $nama_web }} yang tepercaya dan menggunakannya dengan 'dasar yang perlu diketahui'. Informasi terkait pembayaran dienkripsi dengan bantuan teknologi SSL yang sangat andal. {{ $nama_web }} telah mengambil semua langkah yang mungkin untuk menjaga keamanan informasi yang berkaitan dengan pengguna. {{ $nama_web }} melepaskan tanggung jawab jika {{ $nama_web }} gagal menjaga keamanan karena penyebab di luar kendali yang wajar.</p>
                <br>
                <p class="font-bold italic text-2xl">Modifikasi</p>
                <p class="">{{ $nama_web }} dapat mengubah, memodifikasi atau mengamandemen Kebijakan Privasi setiap saat tanpa pemberitahuan sebelumnya. Versi Kebijakan Privasi yang diubah akan segera berlaku setelah diposting di {{ $nama_web }}.com</p>
            </div>
            <h2 id="accordion-open-heading-5">
              <button type="button" class="text-2xl flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-gray-200 focus:r3ng-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-open-body-5" aria-expanded="false" aria-controls="accordion-open-body-5">
                <span class="flex items-center"><svg class="w-5 h-5 mr-2 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path></svg> Kebijakan Lainnya</span>
                <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
              </button>
            </h2>
            <div id="accordion-open-body-5" class="hidden bg-blue-200 p-9" aria-labelledby="accordion-open-heading-3">
                <ul class="list-disc list-inside">
                    <li>
                        Segala bentuk komplain dan konfirmasi top up akan dihandle oleh tim Customer Service kami.
                    </li>
                    <li>
                        Mohon hargai Customer Service kami dengan berbicara sopan dan tidak melakukan ancaman. Kami berhak menolak member yang tidak memenuhi kriteria ini di panel kami.
                    </li>
                    <li>
                        Member yang sudah pernah disusspend tidak diperbolehkan mendaftar ulang menggunakan data baru. Kami akan susspend, dan saldo yang tersisa dianggap hangus.
                    </li>
                </ul>
            </div>
          </div>
    </div>
@endsection
