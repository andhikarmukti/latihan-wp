<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- Favicon --}}
    <link rel="icon" href="{{ asset('storage\images\logo\logowebsite.png') }}">
    {{-- Tailwind CSS --}}
    <link rel="stylesheet" href="/css/tailwindstyle.css">
    {{-- Jquery --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    {{-- DataTable CSS --}}
    <link rel="stylesheet" type="text/css" href="/vendor/dataTables/datatables.css"/>
    {{-- Font Awesome --}}
    <script src="https://use.fontawesome.com/cc283b5013.js"></script>
    {{-- Font --}}
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet">

    {{-- OG image --}}
    <meta property="og:title" content="Web Panel SMM {{ env('APP_NAME') }}"/>
    <meta property="og:type" content="web panel SMM"/>
    <meta property="og:url" content="{{ env('APP_URL') }}"/>
    <meta property="og:image" itemprop="image" content="{{ env('APP_URL') }}/storage/images/logo/logowebsite.png"/>
    <meta property="og:site_name" content="web panel SMM"/>
    <meta property="og:description" content="Web Panel SMM Terbaik dan Jasa Sewa Web Panel SMM"/>

    {{-- Midtrans --}}
    <script type="text/javascript" src="{{ config('midtrans.snap_url') }}" data-client-key="{{ config('midtrans.client_key') }}"></script>
    {{-- Animate CSS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <style>
        html{
            font-family: 'Montserrat', sans-serif;
        }
    </style>
    {{-- Recaptcha --}}
    {!! ReCaptcha::htmlScriptTagJsApi() !!}

    <title>{{ $title }}</title>
</head>
<body class="bg-blue-50">
    @include('sweetalert::alert')
    @auth()
        @include('partials.navbar')
    @endauth
    @yield('content')
    <script src="https://unpkg.com/flowbite@1.5.2/dist/flowbite.js"></script>

    {{-- sweetalert2 --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    {{-- Datatables JS --}}
    <script src="/vendor/dataTables/datatables.js"></script>

    {{-- CKEditor --}}
    <script src="//cdn.ckeditor.com/4.19.0/full/ckeditor.js"></script>

</body>
</html>
