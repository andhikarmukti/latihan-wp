@extends('layouts.main')

@section('content')
    <div class="p-9">
        <h3 class="text-2xl font-bold italic mb-7">Data Perubahan Saldo</h3>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableLogSaldo">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Tanggal
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo Awal
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo Akhir
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Perubahan Terakhir
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Note
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($log_saldos as $log_saldo)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $log_saldo->created_at }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $log_saldo->user->username }}
                        </td>
                        <td class="py-4 px-6">
                            {{ Helpers::format_rupiah($log_saldo->saldo_awal) }}
                        </td>
                        <td class="py-4 px-6">
                            {{ Helpers::format_rupiah($log_saldo->saldo_akhir) }}
                        </td>
                        <td class="py-4 px-6 {{ $log_saldo->selisih >= 0 ? 'text-green-500' : 'text-red-500' }}">
                            {{ Helpers::format_rupiah($log_saldo->selisih) }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $log_saldo->note }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        // Datatables
        $(document).ready(function() {
            $('#tableLogSaldo').DataTable({
                "pageLength": 15,
                // responsive: true,
                "lengthChange": false,
                "ordering": true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: ["_all"]
                    },
                    {
                        target:1,
                        className:'cell-border'
                    }
                ],
                order: [[0, 'desc']],
            });
        });
    </script>
@endsection
