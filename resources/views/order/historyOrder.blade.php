@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="flex flex-col md:flex-row gap-2">
            <div class="w-full md:w-1/2">
                <div id="accordion-collapse" data-accordion="collapse" class="border border-slate-300 rounded-lg shadow-lg mb-1 md:mb-9">
                    <h2 id="accordion-collapse-heading-2">
                        <button type="button" class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-collapse-body-2" aria-expanded="false" aria-controls="accordion-collapse-body-2">
                            <span class="text-gray-600">Info Penting Tentang <span class="font-bold text-green-600">SPEEDUP!</span></span>
                            <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </h2>
                    <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2 p-5">
                        <div class="p-8 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="text-2xl text-green-500 font-bold">Speedup</p>
                            <ol>
                                <li type="1" class="font-bold text-gray-700" >
                                    <p class="font-bold text-gray-500">
                                        Definisi speedup adalah proses boost up layanan yang stuck orderannya  / belum jalan sama sekali <span class="text-purple-800 text-md md:text-xl italic font-bold">setelah lebih dari 24 jam.</span> Speed up 24 jam tidak berlaku untuk layanan yang ada tulisan SLOW pada nama layananya atau pada harga layanan kolom speed memang lambat prosesnya.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Request speed up nantinya akan di proses dalam waktu <span class="text-purple-800 text-md md:text-xl italic font-bold">1x24 jam.</span> Batas maksimal request speed up adalah <span class="text-purple-800 text-md md:text-xl italic font-bold">1x dalam 1 hari</span> untuk satu order id yang sama.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Jika dalam 1x24 jam status orderan masih belum completed, silahkan lakukan request speed up untuk yang kedua kalinya. Kemudian <span class="text-purple-800 text-md md:text-xl italic font-bold">tunggu hingga 1x24 jam.</span>
                                    </p>
                                </li>
                            </ol>
                            <br>
                            <p class="font-bold">
                                NOTE :
                            </p>

                            <p class="font-bold text-purple-800 text-md md:text-xl">
                                Kami hanya menerima request, bukan memantau status orderan belum completed secara berkala.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full md:w-1/2">
                <div id="accordion-collapse" data-accordion="collapse" class="border border-slate-300 rounded-lg shadow-lg mb-9">
                    <h2 id="accordion-collapse-heading-3">
                        <button type="button" class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-collapse-body-3" aria-expanded="false" aria-controls="accordion-collapse-body-3">
                            <span class="text-gray-600">Info Penting Tentang <span class="font-bold text-red-600">REFILL!</span></span>
                            <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </h2>
                    <div id="accordion-collapse-body-3" class="hidden" aria-labelledby="accordion-collapse-heading-3">
                        <div class="p-8 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                            <p class="text-2xl text-red-500 font-bold">Refill</p>
                            <ol>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Definisi refill adalah proses upaya pengisian ulang layanan (followers / view /lainnya) yang mengalami drop atau penurunan sejak orderan completed.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Refill hanya berlaku untuk <span class="text-purple-800 text-md md:text-xl italic font-bold">layanan tertentu</span> yang memberikan garansi refill. Setiap layanan memberikan masa garansi berbeda-beda (silahkan lihat di halaman <a href="/service" class="text-blue-600 hover:text-blue-800">Daftar Service</a>).
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Refill tidak berlaku apabila jumlah followers / views / subscribers dll mengalami drop dibawah 30% atau jumlahnya saat ini berada <span class="text-purple-800 text-md md:text-xl italic font-bold">dibawah start count.</span>
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Untuk klaim garansi refill silahkan klik tombol refill di history order, atau bisa hubungi CS kami di nomor yang tertera pada halaman <a href="/info" class="text-blue-600 hover:text-blue-800">info</a>
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Pastikan <span class="text-purple-800 text-md md:text-xl italic font-bold">akun tidak di private</span>, jika private refill tidak dapat diproses.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Proses refill bisa memakan waktu 1x24 jam atau lebih tergantung jenis layanan.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Jika refill belum masuk, silahkan follow up kembali ke CS. Batas maksimal request refill adalah <span class="text-purple-800 text-md md:text-xl italic font-bold">1x dalam 1 hari</span> untuk satu order id yang sama.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Jika tidak ada follow up dari member setelah 1x24 jam sejak request refill kepada CS, maka kami menganggap bahwa request refill sudah masuk.
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Selama proses refill belum selesai, tidak diperbolehkan untuk order ulang ke link yang sama atau <span class="text-purple-800 text-md md:text-xl italic font-bold">garansi hangus.</span>
                                    </p>
                                </li>
                                <li type="1" class="font-bold text-gray-700">
                                    <p class="font-bold text-gray-500">
                                        Jika orderan masih belum sesuai hingga masa garansi habis, berarti dapat disimpulkan bahwa orderan dengan layanan tersebut memang mengalami drop hingga proses refill tidak bisa dilakukan lagi. Tidak ada refund. Kami hanya mengupayakan untuk terisi kembali, bukan menjanjikan sepenuhnya.
                                    </p>
                                </li>
                            </ol>
                            <br>
                            <p class="font-bold">
                                NOTE :
                            </p>
                            <ul>
                                <li class="text-purple-800 text-md md:text-xl font-bold">
                                    Akun private = tidak bisa refill
                                </li>
                                <li class="text-purple-800 text-md md:text-xl font-bold">
                                    Akun diubah usernamenya = tidak bisa refill
                                </li>
                            </ul>
                            <i class="font-bold text-sm">Request Refill hanya dapat dilakukan sebanyak 1 kali pengisian dan tidak dapat dilakukan jika waktu masa garansi telah habis. Sehingga komplain refill setelah expired tidak diterima. Kami hanya menerima request, bukan memantau jumlah refill sudah terpenuhi atau belum secara berkala.</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <small class="text-xs italic sm:hidden text-gray-500">Table dapat digeser <i class="fa fa-long-arrow-right" aria-hidden="true"></i></small>
        </div>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg rounded-lg p-5 border border-slate-300">
            <h4 class="text-start text-2xl font-bold italic mb-9">History Order</h3>
            {{-- <div class="w-full md:w-1/5 my-5 border border-slate-300 rounded-lg shadow-md p-5">
                <form class="flex flex-col gap-2" action="">
                    <div>
                        <label for="history_time" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Select History Time</label>
                        <select id="history_time" name="history_time" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <option {{ app('request')->input('history_time') == 7 ? 'selected' : '' }} value="7">Last 7 days</option>
                            <option {{ app('request')->input('history_time') == 30 ? 'selected' : '' }} value="30">Last 1 Month</option>
                            <option {{ app('request')->input('history_time') == 60 ? 'selected' : '' }} value="60">Last 2 Months</option>
                            <option {{ app('request')->input('history_time') == 180 ? 'selected' : '' }} value="180">Last 6 Months</option>
                            <option {{ app('request')->input('history_time') == 360 ? 'selected' : '' }} value="360">Last 1 Year</option>
                            <option {{ app('request')->input('history_time') == 1080 ? 'selected' : '' }} value="1080">Last 3 Years</option>
                            <option {{ app('request')->input('history_time') == 1800 ? 'selected' : '' }} value="1800">Last 5 Years</option>
                            <option {{ app('request')->input('history_time') == 'all' ? 'selected' : '' }} value="all">All</option>
                        </select>
                    </div>
                    <div>
                        <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
                    </div>
                </form>
            </div> --}}
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableHistoryOrder">
                <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Order ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Status
                        </th>
                        @can('admin')
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Server Service ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Order ID Server
                        </th>
                        @endcan
                        <th scope="col" class="py-3 px-6">
                            Target
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Quantity
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Start Count
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Remain
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Discount
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Harga
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Service
                        </th>
                        @can('admin')
                        <th scope="col" class="py-3 px-6">
                            HPP
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Mark Up User Level
                        </th>
                        @endcan
                        <th scope="col" class="py-3 px-6">
                            Saldo Awal
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Sisa Saldo
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Note
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Tanggal Order
                        </th>
                        @can('admin')
                        <th scope="col" class="py-3 px-6">
                            Last Update
                        </th>
                        @endcan
                        <th scope="col" class="py-3 px-6">
                            Order From
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Notif
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($history_orders as $history_order)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-300 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white orderId">
                            {{ $history_order->id }} <i class="fa fa-files-o iconCopy text-blue-600" aria-hidden="true"></i>
                        </th>
                        <td class="font-bold py-4 px-6 {{ $history_order->status == 'Completed' ? 'text-green-500 ': (($history_order->status == 'Canceled' || $history_order->status == 'Partial') ? 'text-red-500' : ($history_order->status == 'Pending' ? 'text-yellow-800' : ''))}}">
                            {{ $history_order->status }}
                        </td>
                        @can('admin')
                        <td class="py-4 px-6">
                            {{ $history_order->user->username }}
                        </td>
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $history_order->server_service_id }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $history_order->order_id_server }}
                        </td>
                        @endcan
                        <td class="py-4 px-6">
                            <a class="hover:text-blue-600" target="_blank" href="{{ $history_order->target }}">{{ $history_order->target }}</a>
                        </td>
                        <td class="py-4 px-6">
                            {{ $history_order->quantity }}
                        </td>
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $history_order->start_count }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $history_order->remain }}
                        </td>
                        <td class="py-4 px-6">
                            {{ (int)$history_order->discount == 0 ? '-' : (int)$history_order->discount . '%' }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $history_order->harga }}
                        </td>
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $history_order->service->id . ' - ' . $history_order->service->service_name }}
                        </th>
                        @can('admin')
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $history_order->hpp }}
                        </th>
                        <td class="py-4 px-6">
                            {{ number_format($history_order->mark_up_user_level, 2, ',', ',') }}
                        </td>
                        @endcan
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $history_order->saldo_awal }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $history_order->sisa_saldo }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $history_order->note }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $history_order->created_at }}
                        </td>
                        @can('admin')
                        <td class="py-4 px-6">
                            {{ $history_order->updated_at }}
                        </td>
                        @endcan
                        <td class="py-4 px-6">
                            <small class="text-xs italic {{ $history_order->api_order == 1 ? 'text-purple-900 font-bold' : '' }}">{{ $history_order->api_order == 1 ? 'api' : 'web' }}</small>
                        </td>
                        <td class="py-4 px-6">
                            {{ $history_order->notif_cancel_partial == true ? '✓' : '-' }}
                        </td>
                        <td class="py-4 px-6 flex gap-1">
                            @can('admin')
                                @if($history_order->status != 'Canceled')
                                    <button type="button" data-action="cancel" data-orderid="{{ $history_order->id }}" class="buttonCancel bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">Cancel</button>
                                @endif
                                @if($history_order->status != 'Completed' && $history_order->status != 'Canceled')
                                    <button type="button" data-action="completed" data-orderid="{{ $history_order->id }}" class="buttonCompleted bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900">Completed</button>
                                @endif
                            @endcan
                            @if($history_order->status != 'Completed' && $history_order->status != 'Canceled' && $history_order->status != 'Partial')
                                @if ($history_order->pengajuan_speedup->addDays(1) < now())
                                    <button id="speedup_orderid_{{ $history_order->id }}" type="button" data-servicecategoryid="{{ $history_order->service->service_category_id }}" data-quantity="{{ $history_order->service->min }}" data-serviceid="{{ $history_order->service_id }}" data-action="speedup" data-serverid="{{ $history_order->server_id }}" data-orderid="{{ $history_order->id }}" class="buttonSpeedup bg-red-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">Speedup</button>
                                @endif
                            @endif
                            @if($history_order->status == 'Completed')
                                @if ($history_order->service->refill == true)
                                    @if ($history_order->refill_end_date > now())
                                        @if ($history_order->refillStatus && $history_order->refillStatus->created_at > now()->subDays(1))
                                        refill in progress
                                        @else
                                        <button id="refill_orderid_{{ $history_order->id }}" type="button" data-servicecategoryid="{{ $history_order->service->service_category_id }}" data-quantity="{{ $history_order->service->min }}" data-serviceid="{{ $history_order->service_id }}" data-actionid="5" data-action="refill" data-serverid="{{ $history_order->server_id }}" data-orderidserver="{{ $history_order->order_id_server }}" data-orderid="{{ $history_order->id }}" class="buttonRefill bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">Refill</button>
                                        @endif
                                    @endif
                                @endif
                            @endif
                            @if ($history_order->service->type_id == 2)
                            <button data-orderid="{{ $history_order->id }}" type="button" class="buttonComments bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-900">Comments</button>
                            @endif
                        </td>
                    </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
    </div>

    <!-- Main modal -->
  <div id="modalComments" tabindex="-1" class="overflow-y-scroll overflow-x-scroll fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-full md:h-full justify-center items-center hidden" aria-modal="true" role="dialog">
    <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <!-- Modal header -->
            <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                    Detail Comments
                </h3>
                <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white buttonCross" data-modal-toggle="modalComments">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
          <!-- Modal body -->
          <div class="p-6 space-y-6 overflow-x-auto overflow-y-scroll md:h-96" id="bodyDetail">
              <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400" id="memberInfoDetail"></p>
          </div>
          <!-- Modal footer -->
          <div class="flex items-center p-6 space-x-2 rounded-b border-t border-gray-200 dark:border-gray-600">
              <button data-modal-toggle="modalComments" type="button" class="text-gray-500 bg-slate-300 hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600 buttonClose">Close</button>
          </div>
        </div>
    </div>
</div>


    <script>
        // Datatables
        $(document).ready(function(){
            const target_admin = [10, 12, 14, 15];
            const target_user = [7, 9, 10];
            let userRole = '';

            $.ajax({
                url : '/history-order/check-user-role',
                data : {
                    user_role : true
                },
                success : function(res){
                    userRole = res;
                }
            }).then(() => {
                $('#tableHistoryOrder').DataTable({
                    // responsive : true,
                    "lengthChange": false,
                    "ordering" : true,
                    "pageLength": 25,
                    serverSide: true,
                    processing: true,
                    "autoWidth": false,
                    ajax : '/history-order/yajra',
                    columns: [
                        {data : 'id', name : 'ho.id'},
                        {data : 'status', name : 'ho.status'},
                        @can('admin')
                        {data : 'username', name : 'u.username'},
                        {data : 'server_service_id', name : 'ho.server_service_id'},
                        {data : 'order_id_server', name : 'ho.order_id_server'},
                        @endcan
                        {data : 'target', name : 'ho.target'},
                        {data : 'quantity', name : 'ho.quantity'},
                        {data : 'start_count', name : 'ho.start_count'},
                        {data : 'remain', name : 'ho.remain'},
                        {data : 'discount', name : 'ho.discount'},
                        {data : 'harga', name : 'ho.harga'},
                        {data : 'service_name', name : 's.service_name'},
                        @can('admin')
                        {data : 'hpp', name : 'ho.hpp'},
                        {data : 'mark_up_user_level', name : 'ho.mark_up_user_level'},
                        @endcan
                        {data : 'saldo_awal', name : 'ho.saldo_awal'},
                        {data : 'sisa_saldo', name : 'ho.sisa_saldo'},
                        {data : 'note', name : 'ho.note'},
                        {data : 'created_at', name : 'ho.created_at'},
                        @can('admin')
                        {data : 'updated_at', name : 'ho.updated_at'},
                        @endcan
                        {data : 'api_order', name : 'ho.api_order'},
                        {data : 'notif_cancel_partial', name : 'ho.notif_cancel_partial'}
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        'colvis'
                    ],
                    columnDefs: [
                        {
                            className: "dt-head-center",
                            targets: [ "_all" ]
                        },
                        {
                            className: "dt-center",
                            targets: [ "_all" ]
                        },
                        {
                            target: userRole == 'admin' ? target_admin : target_user,  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                        },
                        {
                            target: 1,
                            render: function ( data, type, row ) {
                                if (data == 'Completed') {
                                    return '<span class="text-green-500">'+ data +'</span>';
                                } else if (data == 'Canceled' || data == 'Partial') {
                                    return '<span class="text-red-500">'+ data +'</span>';
                                } else if (data == 'In Progress' || data == 'Processing' || data == 'Waiting') {
                                    return '<span class="text-yellow-300">'+ data +'</span>';
                                } else {
                                    return '<span>'+ data +'</span>';
                                }
                            }
                        },
                        {
                            target: userRole == 'admin' ? 19 : 13,
                            render: function ( data, type, row ) {
                                if (data == 1) {
                                    return 'api';
                                } else {
                                    return 'web';
                                }
                            }
                        },
                        {
                            target: userRole == 'admin' ? 20 : 14,
                            render: function ( data, type, row ) {
                                if (data == 1) {
                                    return '<span class="text-green-700 font-bold">✓</span>';
                                } else {
                                    return '-';
                                }
                            }
                        },
                        {
                            target: userRole == 'admin' ? 5 : 2,
                            render: function ( data, type, row ) {
                                if(data.includes("https://")){
                                    return `<a target="_blank" href='${data}' class="text-blue-400">${data}</a>`;
                                }else{
                                    return data;
                                }
                            }
                        },
                        {
                            target: userRole == 'admin' ? [17, 18] : 12,
                            render: function ( data, type, row ) {
                                const date = new Date(data);
                                const fullDate = date.toLocaleTimeString("en-GB", {
                                    year: "numeric",
                                    month: "short",
                                    day: "2-digit",
                                    hour: "numeric",
                                    minute: "numeric",
                                    second: "numeric",
                                });
                                return fullDate + ' WIB';
                            }
                        },
                        {
                            target: userRole == 'admin' ? 9 : 6,
                            render: function ( data, type, row ) {
                                const discount = data == 0 ? '-' : parseInt(data) + '%'
                                return discount;
                            }
                        },
                    ],
                    order:[[0,"desc"]]
                });
            })
        });
    </script>

    <script>
        $('.iconCopy').click(function(){
            $(this).removeClass('text-blue-600');
            $(this).addClass('text-yellow-800');

            const orderId = $(this).closest('.orderId').text().trim();

            navigator.clipboard.writeText(orderId);
            const Toast = Swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: false,
                timer: 1000,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: 'Text Copied'
            })
        });
    </script>

    <script>
        $('.buttonCancel').click(function(){
            const orderId = $(this).attr('data-orderid');
            const action = $(this).attr('data-action');

            // console.log(orderId);
            Swal.fire({
                title: 'Cancel?',
                text: "Saldo akan refund full jika dicancel",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#808080',
                confirmButtonText: 'Yes',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/cancel/' + orderId,
                        method : 'POST',
                        data : {
                            action : action,
                            '_token' : '{{ csrf_token() }}'
                        },
                        success : function(res){
                            Swal.fire(
                                'Canceled!',
                                res.info,
                                'success'
                            ).then(() => {
                                window.location = '/history-order';
                            })
                        },error : function(err){
                            const error = err.responseJSON;
                            Swal.fire(
                                'Error!',
                                'Hubungi admin untuk melihat errornya di log!',
                                'error'
                            )
                        }
                    });
                }
            })
        });

        $('.buttonCompleted').click(function(){
            const orderId = $(this).attr('data-orderid');
            const action = $(this).attr('data-action');

            Swal.fire({
                title: 'Completed?',
                text: "Orderan akan selesai diproses server jika telah completed",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#808080',
                confirmButtonText: 'Yes',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/cancel/' + orderId,
                        method : 'POST',
                        data : {
                            action : action,
                            '_token' : '{{ csrf_token() }}'
                        },
                        success : function(res){
                            Swal.fire(
                                'Completed!',
                                res.info,
                                'success'
                            ).then(() => {
                                window.location = '/history-order';
                            });
                        },error : function(err){
                            const error = err.responseJSON;
                            Swal.fire(
                                'Error!',
                                'Hubungi admin untuk melihat errornya di log!',
                                'error'
                            )
                        }
                    });
                }
            })
        });

        $('.buttonRefill').click(function(){
            let error = '';
            // data yang dipake hanya orderid dan actionid, sisanya formalitas syarat params masuk ke method aja
            const orderId = $(this).attr('data-orderid');
            const serverId = $(this).attr('data-serverid');
            const serviceId = $(this).attr('data-serviceid');
            const orderIdServer = $(this).attr('data-orderidserver');
            const action = $(this).attr('data-action');
            const actionId = $(this).attr('data-actionid');
            const serviceCategoryId = $(this).attr('data-servicecategoryid');
            const quantity = $(this).attr('data-quantity');

            Swal.fire({
                title: 'Refill Order?',
                text: "Refill akan diajukan untuk order ID " + orderId,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#808080',
                confirmButtonText: 'Yes',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/new-order',
                        method : 'POST',
                        data : {
                            action : action,
                            order_id_server : orderIdServer,
                            server_id : serverId,
                            service_id : serviceId,
                            action_id : actionId,
                            target : 0,
                            quantity : quantity,
                            service_category_id : serviceCategoryId,
                            '_token' : '{{ csrf_token() }}'
                        },
                        beforeSend: function() {
                            let timerInterval
                            Swal.fire({
                                title: 'Mohon tunggu sebentar',
                                html: 'Sedang melakukan request refill',
                                timer: 20000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        if(b){
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }, 100)
                                },
                            willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })
                        },
                        success : function(res){
                            // console.log(res);

                            if(res.status == false){
                                error = {
                                    message : res.message
                                }
                            }

                            $.ajax({
                                url : '/update-refill-status',
                                method : 'POST',
                                data : {
                                    order_id : orderId,
                                    refill_id : res.refill_id,
                                    '_token' : '{{ csrf_token() }}'
                                },
                                success : function(res){
                                    // console.log(res);
                                }
                            })
                        },
                        error : function(err){
                            error = err.responseJSON;
                            // console.log(error);
                        },
                        complete : function(){
                            // Jika error
                            if(error != ''){
                                Swal.fire(
                                    'Gagal!',
                                    error.message + '<br>' + '<br>' + 'Silahkan informasikan order id ke CS kami untuk mengajukan refill' + '<br>' + `order ID : ${orderId}`,
                                    'error'
                                ).then(() => {
                                    // window.location = '/history-order';
                                });
                            }else{
                                Swal.fire(
                                    'Completed!',
                                    'Request refill berhasil',
                                    'success'
                                ).then(() => {
                                    $('#refill_orderid_' + orderId).text('✓');
                                    $('#refill_orderid_' + orderId).removeClass('bg-red-100');
                                    $('#refill_orderid_' + orderId).attr('disabled', true);
                                });
                            }
                        },
                    });
                }
            })
        });

        $('.buttonSpeedup').click(function(){
            const orderId = $(this).attr('data-orderid');
            const serverId = $(this).attr('data-serverid');
            const serviceId = $(this).attr('data-serviceid');
            const orderIdServer = $(this).attr('data-orderidserver');
            const action = $(this).attr('data-action');
            const actionId = $(this).attr('data-actionid');
            const serviceCategoryId = $(this).attr('data-servicecategoryid');
            const quantity = $(this).attr('data-quantity');

            Swal.fire({
                title: 'Speedup Orderan Ini?',
                text: "Report speed up akan automatis dikirim ke admin",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#808080',
                confirmButtonText: 'Yes',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/speedup/' + orderId,
                        method : 'POST',
                        data : {
                            order_id : orderId,
                            '_token' : '{{ csrf_token() }}'
                        },
                        success : function(res){
                            // console.log(res);
                            Swal.fire(
                                'Report Speedup telah dikirim!',
                                res.info,
                                'success'
                            ).then(() => {
                                $('#speedup_orderid_' + orderId).text('✓');
                                $('#speedup_orderid_' + orderId).removeClass('bg-yellow-100');
                                $('#speedup_orderid_' + orderId).attr('disabled', true);
                            });
                        },error : function(err){
                            const error = err.responseJSON;
                            Swal.fire(
                                'Error!',
                                'Hubungi admin untuk melihat errornya di log!',
                                'error'
                            )
                        }
                    });
                }
            })
        });

        // Comments button view
        $('.buttonComments').click(function(){
            const orderIdForComments = $(this).attr('data-orderid');
            $('#memberInfoDetail').empty();

            $.ajax({
                url : '/comments-show/' + orderIdForComments,
                method : 'POST',
                data : {
                    '_token' : "{{ csrf_token() }}",
                },
                beforeSend: function(){
                    // loading skeleton
                    $('#bodyDetail').after(`
                        <div id="loading" role="status" class="max-w-sm animate-pulse my-5">
                            <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                            <span class="sr-only">Loading...</span>
                        </div>
                    `)
                },
                success : function(res){
                    // console.log(res);
                    $('#loading').empty();
                    $.each(res, function(index, value){
                        $('#memberInfoDetail').append(`
                            <span class="font-bold text-yellow-800">${value}</span>
                            <br>
                        `);
                    });
                }
            })
        });
    </script>
@endsection
