@extends('layouts.main')

@section('content')
<div class="p-9 flex flex-col gap-1 items-center justify-center">
    <div>
        {{-- <a href="https://www.youtube.com/watch?v=f7Yqcqcz_uA" class="text-blue-500 italic text-xs hover:text-blue-700 ring-1 ring-orange-300 rounded-lg p-1 hover:bg-blue-200" target="_blank">
            Video Tutorial Cara Order
        </a> --}}
    </div>
    <div class="text-sm border border-slate-300 rounded-lg shadow-md p-5 w-full md:w-1/3">
        <h4 class="text-center text-2xl font-bold italic mb-9">New Order</h3>
            <form id="formOrder">
                @csrf
                <div class="mb-6">
                    <label for="service_category_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Category Service</label>
                    <select id="service_category_id" name="service_category_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected disabled>Pilih Category</option>
                        @foreach ($services_category_id as $service_category_id)
                        <option value="{{ $service_category_id->id }}">{!! $service_category_id->nama_category !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-6" hidden>
                    <label for="service_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service</label>
                    <select id="service_id" name="service_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected disabled>Pilih Service</option>
                    </select>
                </div>
                <div class="mb-6" hidden>
                    <div id="service_slow" class="overflow-auto break-normal relative border border-red-200 bg-red-50 rounded-lg p-5 mb-6 text-red-900 text-sm"></div>
                </div>
                <div class="mb-6" hidden>
                    <label for="member_info" class="block mb-2 text-sm font-medium text-red-900 dark:text-red-300">Info Penting!</label>
                    <div id="member_info" class="overflow-auto break-normal relative border border-yellow-300 bg-yellow-50 rounded-lg p-5 mb-6 text-red-900 text-sm"></div>
                </div>
                <div class="grid grid-cols-1 md:grid-cols-2 gap-2">
                    <div class="mb-6" hidden>
                        <label for="target" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Target</label>
                        <input type="text" id="target" name="target" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required autocomplete="off">
                        <div>
                            <small class="text-sm italic text-red-500" id="targetError"></small>
                        </div>
                        <small class="text-xs italic text-purple-500">Pastikan akun target tidak diprivate!</small>
                    </div>
                    <div class="mb-6" hidden>
                        <label for="quantity" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Quantity</label>
                        <input type="number" id="quantity" name="quantity" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required autocomplete="off">
                        <div class="flex flex-col">
                            <small hidden class="text-sm italic text-red-500" id="quantityError"></small>
                            <small class="italic text-xs" id="minMax"></small>
                        </div>
                    </div>
                </div>
                <div class="mb-6" hidden>
                    <label for="comments" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Comments</label>
                    <textarea id="comments" name="comments" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Pisahkan comment dengan baris baru (enter), setiap baris mewakili jumlah quantity comment"></textarea>
                        <small hidden class="text-sm italic text-red-500" id="commentsError"></small>
                        <small class="italic text-sm" id="infoComments"></small>
                </div>
                <div class="mb-6" hidden>
                    <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Username</label>
                    <input type="text" id="username" name="username" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required>
                        <small hidden class="text-sm italic text-red-500" id="usernameError"></small>
                        <small class="italic text-sm" id="infoUsername">Username pemilik comment [bukan username pemilik postingan]</small>
                </div>
                <div class="mb-5" hidden id="hargaService">
                    <label for="rate" class="block mb-2 text-sm font-medium text-green-900 dark:text-green-300 text-center md:text-start">Harga Service</label>
                    <div id="infoHargaService" class="border border-green-300 bg-green-50 rounded-lg p-5 text-green-900 text-sm text-center md:text-start">
                        <div id="saldoSaatIni"></div>
                        <div id="rate"></div>
                        <div id="discount" class="flex"></div>
                        <div id="totalHarga" hidden></div>
                    </div>
                </div>
                <input type="hidden" name="action_id" value="1">
                <button id="buttonSubmit" type="button" class="mb-3 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>

                {{-- Button Loader --}}
                <button hidden id="buttonLoader" disabled type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    <svg aria-hidden="true" role="status" class="inline mr-3 w-4 h-4 text-white animate-spin" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="#E5E7EB"/>
                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentColor"/>
                    </svg>
                    Submiting Order...
                </button>
                <span hidden id="buttonCompleted" class="focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2"></span>
            </form>
        </div>
    </div>

    <div class="flex justify-center">
        <div class="p-9 mb-44 w-full md:w-1/2">
            <div id="accordion-collapse" data-accordion="collapse" class="border border-slate-300 rounded-lg shadow-lg">
                <h2 id="accordion-collapse-heading-2">
                    <button type="button" class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-collapse-body-2" aria-expanded="false" aria-controls="accordion-collapse-body-2">
                        <span class="text-red-600">Info Penting Sebelum Melakukan Orderan!</span>
                        <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    </button>
                </h2>
                <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2">
                    <div class="p-5 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                        <p><span class="font-bold">1.</span> Pastikan Anda menginput link yang benar sesuai format yang ada di keterangan, karena <span class="font-bold italic text-purple-800 text-xl">kami tidak bisa membatalkan pesanan.</span></p>
                        <br>
                        <p><span class="font-bold">2.</span> <span class="font-bold italic text-purple-800 text-xl">Jangan menggunakan lebih dari satu layanan sekaligus untuk username/link yang sama</span>. Harap tunggu status completed pada orderan sebelumnya baru melakukan orderan kepada username/ link yang sama. Hal ini  tidak akan membantu mempercepat orderan Anda karena kedua orderan bisa jadi berstatus completed tetapi hanya tercapai target dari salah satu orderan dan tidak ada pengembalian dana.</p>
                        <br>
                        <p><span class="font-bold">3.</span> Setelah submit order, jika username/link yang diinput tidak ditemukan (diganti/diprivate/dihapus), orderan akan otomatis menjadi completed dan <span class="font-bold italic text-purple-800 text-xl">tidak ada pengembalian dana.</span></p>
                        <br>
                        <p><span class="font-bold">4.</span> <span class="font-bold italic text-purple-800 text-xl">Kesalahan pembeli, bukan tanggung jawab admin</span>, karena panel ini serba otomatis, jadi <span class="font-bold">hati-hati dan perhatikan link sebelum order</span>. Tidak ada pengembalian dana!</p>
                        <br>
                        <p><span class="font-bold">5.</span> Jika Orderan statusnya partial atau canceled, saldo <span class="font-bold italic text-purple-800 text-xl">otomatis di refund</span> dan bisa order ulang!</p>
                        <br>
                        <p><span class="font-bold">6.</span> Jumlah maks menunjukkan kapasitas layanan tersebut <span class="font-bold italic text-purple-800 text-xl"><span class="font-bold italic text-purple-800 text-xl"></span></span>. Apabila Anda telah menggunakan semua kapasitas maks layanan, Anda tidak bisa menggunakan layanan itu lagi untuk target yang sama dan harus menggunakan layanan yang lain. Oleh karenannya kami menyediakan banyak layanan dengan kapasitas maks yang lebih besar.</p>
                        <br>
                        <p><span class="font-bold">7.</span> Informasi yang terdapat pada kolom keterangan (speed, drop rate) <span class="font-bold italic text-purple-800 text-xl">bersifat estimasi</span> untuk membedakan layanan yang satu dan lainnya. Informasi bisa jadi tidak akurat tergantung dari performa server dan jumlah orderan yang masuk pada server tersebut. Anda dapat report speedup ke CS kami setelah 24 jam orderan dikirim dan statusnya masih Pending.</p>
                        <br>
                        <p><span class="font-bold">8.</span> Dengan melakukan orderan Anda dianggap sudah memahami dan setuju Syarat dan Ketentuan {{ $nama_web }}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        let serviceInfo = {};
        const user_saldo = "{{  Helpers::format_rupiah(auth()->user()->saldo)  }}";
        const user_level_rate = "{{ auth()->user()->levelMember->mark_up }}"

        function removeError(){
            $('#targetError').text('');
            $('#quantityError').text('');
            $('#commentsError').text('');
            $('#usernamesError').text('');
            $('#target').removeClass('ring-1 ring-red-500');
            $('#quantity').removeClass('ring-1 ring-red-500');
            $('#target').removeClass('ring-1 ring-red-500');
            $('#quantity').removeAttr('disabled');
            $('#quantityError').attr('hidden', true);
            $('#commentsError').attr('hidden', true);
            $('#usernameError').attr('hidden', true);
            $('#saldoSaatIni').removeClass('text-red-500 text-lg');
            $('#discountBerakhir').empty();
        }

        function disableAllInput(){
            $('#comments').attr('disabled', true);
            $('#username').attr('disabled', true);
            $('#target').attr('disabled', true);
            $('#quantity').attr('disabled', true);
        }

        function enableAllInput(){
            $('#comments').removeAttr('disabled');
            $('#username').removeAttr('disabled');
            $('#target').removeAttr('disabled');
            $('#quantity').removeAttr('disabled');
        }

        function formatRupiah(angka, prefix){
			var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
		}

        $('#service_category_id').change(function(){
            $('#addOn').remove();
            removeError();

            // loading skeleton
            $(this).after(`
            <div id="loading" role="status" class="max-w-sm animate-pulse mt-10">
                <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                <span class="sr-only">Loading...</span>
            </div>
            `)

            $('#hargaService').attr('hidden', true);
            $('#comments').parent().attr('hidden', true);
            $('#usernameError').parent().attr('hidden', true);
            $("#buttonCompleted").html('') // hilangkan lagi button order gagal yang merahnya
            $("#buttonCompleted").attr('hidden', true); // hilangkan lagi button order gagal yang merahnya

            const serviceCategoryId = $(this).find(':selected').val();
            $.ajax({
                url : '/new-order',
                data : {
                    service_category_id : serviceCategoryId
                },
                success : function(res){
                    $('#loading').empty(); // hilangkan loading skeleton
                    $('#service_id').parent().removeAttr('hidden');

                    $('#service_id').empty(); // Hapus select service id
                    $('#service_slow').empty(); // Hapus member info
                    $('#service_slow').parent().attr('hidden', true); // Hidden member info
                    $('#member_info').empty(); // Hapus member info
                    $('#member_info').parent().attr('hidden', true); // Hidden member info
                    $('#target').parent().attr('hidden', true); // Hidden target
                    $('#quantity').parent().attr('hidden', true); // Hidden quantity

                    $('#service_id').append(`
                        <option selected disabled>Pilih Service</option>
                    `);
                    $.each(res, function(index, value){
                        // console.log(value.discounts);
                        $('#service_id').append(`
                            <option class="${value.service_name.includes('[SLOW]') ? 'text-red-800' : ''}" value="${value.id}"><span class="font-bold italic">${value.id}</span> - ${value.service_name} <span class="font-bold">${value.discounts.length > 0 ? '[PROMO]' : ''}</span></option>
                        `);
                    })
                }
            });
        })

        $('#service_id').change(function(){
            removeError();
            const serviceId = $(this).find(':selected').val();

            // loading skeleton
            $(this).after(`
            <div id="loading2" role="status" class="max-w-sm animate-pulse mt-10">
                <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                <span class="sr-only">Loading...</span>
            </div>
            `)

            $("#buttonCompleted").html('') // hilangkan lagi button order gagal yang merahnya
            $("#buttonCompleted").attr('hidden', true); // hilangkan lagi button order gagal yang merahnya
            $('#service_slow').empty(); // Hapus member info
            $('#service_slow').parent().attr('hidden', true); // Hapus member info
            $('#member_info').empty(); // Hapus member info
            $('#member_info').parent().attr('hidden', true); // Hapus member info
            $('#minMax').empty(); // Hapus min max quantity
            $('#target').val(''); // Hapus input valuenya
            $('#target').parent().attr('hidden', true); // Hidden dulu biar skeleton aja yang muncul
            $('#quantity').val(''); // Hapus input valuenya
            $('#quantity').parent().attr('hidden', true); // Hidden dulu biar skeleton aja yang muncul
            $('#discount').empty(); // Hapus discount
            $('#totalHarga').attr('hidden', true);
            $('#comments').parent().attr('hidden', true);
            $('#username').parent().attr('hidden', true);

            $.ajax({
                url : '/new-order',
                data : {
                    service_id : serviceId
                },
                success : function(res){
                    $('#hargaService').removeAttr('hidden');
                    $('#addOn').remove();

                    $('#hargaService').after(`
                    <div id="addOn" class="border border-blue-300 bg-blue-50 rounded-lg p-5 text-blue-900 text-sm text-center md:text-start my-10">
                        <p class="font-bold italic mb-4">Add Ons</p>
                        <div class="flex items-center">
                            <input id="notif_cancel_partial" name="notif_cancel_partial" type="checkbox" value="yes" class="w-4 h-4 text-blue-600 bg-gray-100 rounded border-gray-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                            <label for="notif_cancel_partial" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Beritahu saya jika orderan mengalami cancel / partial <span class="text-purple-500">[Rp 100]</span></label>
                        </div>
                    </div>
                    `);

                    $('#loading2').empty(); // hilangkan loading skeleton
                    if(res.type_id != 2){ // kalau diganti dan bukan service comment maka quantity di enable
                        $('#quantity').removeAttr('disabled');
                    }
                    serviceInfo = res;

                    $('#service_slow').parent().removeAttr('hidden');
                    $('#member_info').parent().removeAttr('hidden');
                    $('#target').parent().removeAttr('hidden');
                    $('#quantity').parent().removeAttr('hidden');
                    if(serviceInfo.type_id == 2){
                        $('#comments').parent().removeAttr('hidden');
                        $('#quantity').attr('disabled', true);
                        $('#quantity').parent().attr('hidden', true);
                    }
                    if(serviceInfo.type_id == 3){
                        $('#username').parent().removeAttr('hidden');
                    }

                    // Menghitung jumlah baris comment
                    $('#comments').on('input', function(){
                        let text = $.trim($(this).val());
                        let totalComments = text.split("\n");
                        $('#quantity').parent().removeAttr('hidden');
                        $('#quantity').val(totalComments.length).trigger('input');
                    })

                    if(serviceInfo.service_name.includes('[SLOW]')){
                        $('#service_slow').append(`
                            SERVICE SLOW <br><br> Estimasi pengerjaan orderan bisa memakan waktu lebih dari 2 x 24 jam.
                        `);
                    }else{
                        $('#service_slow').parent().attr('hidden', true);
                    }
                    $('#member_info').append(`
                        ${res.member_info}
                    `);
                    $('#minMax').html(`
                        min.order : <span class="font-bold">${(res.min).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</span> | max.order : <span class="font-bold">${(res.max).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</span><br>(minimal dan maksimal order per akun/target)
                    `);
                    $('#rate').html(`⇆ Rate : ${formatRupiah(Math.ceil(user_level_rate * res.rate), 'Rp ')} / ${formatRupiah(res.unit)} quantity`);
                }
            });
        });

        $('#quantity').on('input', function(){
            let quantity = $(this).val();

            function hitungTotalHarga(qty){
                if(serviceInfo.discounts.length > 0){
                    $('#discount').html(`
                        <hr>
                        <div class="flex justify-between gap-1">
                            <div>
                                <small class="text-sm italic text-blue-500">${serviceInfo.discounts[0].discount_name}</small>
                            </div>
                            <div>
                                <span>\xa0Discount\xa0:\xa0</span>
                                <span class="font-bold text-center">${parseFloat(serviceInfo.discounts[0].discount).toFixed(0)+"%"}</span>
                            </div>
                        </div>
                    `);
                    $('#discount').addClass('bg-blue-200 justify-center items-center rounded-lg p-1');
                    $('#totalHarga').html(`
                        <hr>
                        Total Harga :
                        <s class="italic text-sm">
                            ${serviceInfo.special_price == null ? formatRupiah(Math.ceil((user_level_rate * serviceInfo.rate / serviceInfo.unit) * qty), 'Rp ') : formatRupiah(Math.ceil((user_level_rate * (serviceInfo.rate - (serviceInfo.rate * serviceInfo.special_price.percent / 100) - serviceInfo.special_price.fix) / serviceInfo.unit) * qty), 'Rp ')}
                        </s>
                        <span class="italic font-bold text-lg">
                            ${serviceInfo.special_price == null ? formatRupiah(Math.ceil((user_level_rate * serviceInfo.rate / serviceInfo.unit) * qty - (Math.ceil((user_level_rate * serviceInfo.rate / serviceInfo.unit) * qty)) * (serviceInfo.discounts[0].discount / 100)), 'Rp ') : formatRupiah(Math.ceil((user_level_rate * (serviceInfo.rate - (serviceInfo.rate * serviceInfo.special_price.percent / 100) - serviceInfo.special_price.fix) / serviceInfo.unit) * qty - (user_level_rate * (serviceInfo.rate - (serviceInfo.rate * serviceInfo.special_price.percent / 100) - serviceInfo.special_price.fix) / serviceInfo.unit) * qty * (serviceInfo.discounts[0].discount / 100)), 'Rp ')}
                        </span>
                    `).ready(function(){
                        $('#discountBerakhir').empty();
                        $('#infoHargaService').after(`
                            <small class="text-sm italic text-red-500" id="discountBerakhir">Promo berakhir : ${serviceInfo.discounts[0].end_date}</small>
                        `)
                    });
                }else{
                    $('#discount').removeClass('bg-blue-200 justify-center items-center rounded-lg p-1');
                    $('#totalHarga').html(`
                        <hr>
                        Total Harga\xa0:\xa0
                        <span class="italic font-bold text-lg">
                            ${serviceInfo.special_price == null ? formatRupiah(Math.ceil((user_level_rate * serviceInfo.rate / serviceInfo.unit) * qty), 'Rp ') : formatRupiah(Math.ceil((user_level_rate * (serviceInfo.rate - (serviceInfo.rate * serviceInfo.special_price.percent / 100) - serviceInfo.special_price.fix) / serviceInfo.unit) * qty), 'Rp ')}
                        </span>
                    `);
                }
            }

            $('#totalHarga').removeAttr('hidden');

            // Cegah input yang melebihin range min max
            if(quantity < parseInt(serviceInfo.min)){
                $('#totalHarga').html(`
                    <hr>
                    <span class="italic font-bold text-sm text-red-500">
                        Quantity dibawah batas minimum!
                    </span>
                `);
            }else if(quantity > parseInt(serviceInfo.max)){
                $('#totalHarga').html(`
                    <hr>
                    <span class="italic font-bold text-sm text-red-500">
                        Quantity melebihi batas minimum!
                    </span>
                `);
            }else{
                hitungTotalHarga(quantity);
            }

            $('#saldoSaatIni').html(`
                <i class="fa fa-money" aria-hidden="true"></i> Saldo saat ini : ${user_saldo} <hr>
            `);


        });
    </script>

    <script>
        $('#buttonSubmit').click(function(){
            // Hapus tulisan error
            removeError();
            $("#buttonCompleted").attr('hidden', true);

            const data = $('#formOrder').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});
            let errMessage = '';
            let errStatus = false;
            let successMessage = '';

            $.ajax({
                url : '/new-order',
                method : 'POST',
                data : {
                    // service_category_id : data.service_category_id,
                    service_id : data.service_id,
                    target : data.target,
                    quantity : data.quantity,
                    action_id : data.action_id,
                    comments : data.comments,
                    username : data.username,
                    type_id : data.username,
                    notif_cancel_partial : data.notif_cancel_partial,
                    '_token' : '{{ csrf_token() }}'
                },
                beforeSend: function(){
                    disableAllInput();
                    $("#buttonSubmit").attr('hidden', true);
                    $("#buttonLoader").removeAttr('hidden');
                    let timerInterval
                    Swal.fire({
                        title: 'Submit Order',
                        html: 'Mohon menunggu.',
                        timer: 200000,
                        timerProgressBar: true,
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading()
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    })
                },
                success: function(res){
                    // console.log(res);
                    if(res.status == false){
                        $("#buttonSubmit").removeAttr('hidden');
                        enableAllInput()
                        errStatus = true;

                        const error = res;
                        // console.log(error);

                        // Pengecekan orderan dengan target yang sama dan satus masih pending
                        if(error.same_order){
                            errMessage = error.message;
                            Swal.fire({
                                icon : 'error',
                                title : 'Order Gagal!',
                                html : error.message,
                            });
                        }

                        // Error API
                        if(error.error_api){
                            errMessage = error.message;
                            Swal.fire({
                                icon : 'error',
                                title : 'Order Gagal!',
                                html : error.message + '<br><br>' + 'Note : Silahkan gunakan service ID yang lain' + '<br><br>' + 'Harap screenshoot error ini dan hubungi CS' + '<br>' + '<span class="font-bold">(Service ID - '+ data.service_id +')</span>' ,
                            });
                        }

                        // Error saldo tidak cukup
                        if(error.saldo_tidak_cukup){
                            errMessage = 'Saldo tidak cukup';
                            enableAllInput();
                            Swal.fire({
                                icon : 'warning',
                                title : 'Order Gagal!',
                                text : 'Saldo kamu tidak cukup, silahkan lakukan pengisian saldo terlebih dahulu',
                                timer : 5000
                            });

                            // Warning saldo saat ini, dan penambahan button menuju halaman tambah saldo
                            $('#saldoSaatIni').addClass('text-red-500 text-lg');
                            $('#saldoSaatIni').append(`
                                <a href="/tambah-saldo" class="flex justify-center bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">Tambah Saldo</a>
                            `);
                        }

                        // Error target kosong
                        if(error.target){
                            errMessage = 'Target ' + error.target[0];
                            $('#targetError').text(`${error.target[0]}`);
                            $('#target').addClass(`ring-1 ring-red-500`);
                        }
                        // Error quantity kosong
                        if(error.quantity){
                            errMessage = 'Quantity ' + error.quantity[0];
                            if(error.type_id[0] == 2){ // ketika orderan jenisnya comment maka quantity tetap disable
                                $('#quantity').attr(`disabled`, true);
                            }
                            $('#quantityError').removeAttr('hidden');
                            $('#quantityError').text(`${error.quantity[0]}`);
                            $('#quantity').addClass(`ring-1 ring-red-500`);
                        }
                        // Error comment kosong
                        if(error.comments){
                            errMessage = 'Comments ' + error.comments[0];
                            $('#commentsError').removeAttr('hidden');
                            $('#commentsError').text(`${error.comments[0]}`);
                            $('#comments').addClass(`ring-1 ring-red-500`);
                        }
                    }else{
                        $("#buttonCompleted").html('<span class="italic text-green-500"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Berhasil submit order!</span>');
                        $.ajax({
                            url : '/success-order',
                            method : 'POST',
                            data : {
                                order_id : res.data.id,
                                '_token' : "{{ csrf_token() }}"
                            },
                            success : function(success_order){
                                // console.log(success_order);
                                Swal.fire({
                                allowOutsideClick: false,
                                icon : 'success',
                                title : 'Order Berhasil!',
                                text : 'Berhasil melakukan orderan',
                                html : `
                                <div id="alert-3" class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200" role="alert">
                                    <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800 text-left">
                                        <span class="text-lg">Success Order!</span>
                                        <br>
                                        <br>
                                        <ul>
                                            <li>
                                                Order ID : <span class="italic text-purple-800">${success_order.data.id}</span>
                                            <li>
                                            <li>
                                                Service : <span class="italic text-purple-800">${success_order.data.service_name}</span>
                                            <li>
                                            <li>
                                                Target : <span class="italic text-purple-800">${success_order.data.target}</span>
                                            <li>
                                            <li>
                                                Quantity : <span class="italic text-purple-800">${success_order.data.quantity}</span>
                                            <li>
                                            <li>
                                                Status : <span class="italic text-purple-800">${success_order.data.status}</span>
                                            <li>
                                        </ul>
                                        <br>
                                        <p>Mohon untuk menunggu minimal 1 x 24 jam selama proses pengerjaan orderan oleh system</p>
                                    </div>
                                    <i class="fa fa-check-circle-o text-green-700" aria-hidden="true"></i>
                                </div>
                                `,
                                showClass: {
                                    popup: 'animate__animated animate__fadeInDown'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__bounceOutUp'
                                }
                            }).then(() => {
                                window.location = '/new-order';
                            });
                            }
                        });
                    }
                },
                error : function(err){
                    // console.log(err);
                    // Kembalikan tombol submit
                    $("#buttonSubmit").removeAttr('hidden');
                    $("#buttonLoader").attr('hidden', true);
                    $("#buttonComplete").attr('hidden', true);
                    enableAllInput();
                    $("#buttonCompleted").removeAttr('hidden');
                    $("#buttonCompleted").html(`<span class="italic text-red-500">Order Gagal!</span>`);
                    errStatus = true;

                    const error = err.responseJSON;

                    // Pengecekan orderan dengan target yang sama dan satus masih pending
                    if(error.same_order){
                        errMessage = error.message;
                        Swal.fire({
                            icon : 'error',
                            title : 'Order Gagal!',
                            html : errMessage,
                        });
                    }

                    // Error API
                    if(error.error_api){
                        errMessage = error.message;
                        Swal.fire({
                            icon : 'error',
                            title : 'Order Gagal!',
                            html : error.message + '<br><br>' + 'Note : Silahkan gunakan service ID yang lain' + '<br><br>' + 'Harap screenshoot error ini dan hubungi CS' + '<br>' + '<span class="font-bold">(Service ID - '+ data.service_id +')</span>' ,
                        });
                    }

                    // Error saldo tidak cukup
                    if(error.saldo_tidak_cukup){
                        errMessage = 'Saldo tidak cukup';
                        enableAllInput();
                        Swal.fire({
                            icon : 'warning',
                            title : 'Order Gagal!',
                            text : 'Saldo kamu tidak cukup, silahkan lakukan pengisian saldo terlebih dahulu',
                            timer : 5000
                        });

                        // Warning saldo saat ini, dan penambahan button menuju halaman tambah saldo
                        $('#saldoSaatIni').addClass('text-red-500 text-lg');
                        $('#saldoSaatIni').append(`
                            <a href="/tambah-saldo" class="flex justify-center bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">Tambah Saldo</a>
                        `);
                    }

                    // Error target kosong
                    if(error.target){
                        errMessage = 'Target ' + error.target[0];
                        $('#targetError').text(`${error.target[0]}`);
                        $('#target').addClass(`ring-1 ring-red-500`);
                    }
                    // Error quantity kosong
                    if(error.quantity){
                        errMessage = 'Quantity ' + error.quantity[0];
                        if(error.type_id[0] == 2){ // ketika orderan jenisnya comment maka quantity tetap disable
                            $('#quantity').attr(`disabled`, true);
                        }
                        $('#quantityError').removeAttr('hidden');
                        $('#quantityError').text(`${error.quantity[0]}`);
                        $('#quantity').addClass(`ring-1 ring-red-500`);
                    }
                    // Error comment kosong
                    if(error.comments){
                        errMessage = 'Comments ' + error.comments[0];
                        $('#commentsError').removeAttr('hidden');
                        $('#commentsError').text(`${error.comments[0]}`);
                        $('#comments').addClass(`ring-1 ring-red-500`);
                    }
                },
                complete:function(data){
                    $("#buttonLoader").attr('hidden', true);
                    $("#buttonCompleted").removeAttr('hidden');
                    if(errStatus == true){
                        Swal.fire(
                            'Gagal!',
                            errMessage ?? 'Server Error, Hubungi Admin',
                            'error'
                        )
                    }
                }
            });
        })
    </script>
@endsection
