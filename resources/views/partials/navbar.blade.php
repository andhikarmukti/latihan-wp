<nav class="px-2 bg-white border-gray-200 dark:bg-gray-900 dark:border-gray-700 shadow-md sticky top-0 z-50 py-3 md:py-1 h-16 text-sm">
    <div class="container flex flex-wrap items-center mx-auto justify-between md:justify-start">
        <a href="/dashboard" class="xl:flex items-center hidden">
            <img src="{{ asset('storage\images\logo\logowebsite.png') }}" class="mr-1 h-w-40 sm:h-full w-10" alt="logowebsite">
        </a>
        <a href="/dashboard" class="flex items-center xl:hidden">
            <img src="{{ asset('storage\images\logo\logowebsite.png') }}" class="mr-1 h-w-40 sm:h-full w-10" alt="logowebsite">
        </a>
        <button id="buttonOpenCloseMenu" data-collapse-toggle="mobile-menu" type="button"
            class="inline-flex justify-center items-center ml-3 text-gray-400 rounded-lg lg:hidden hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-blue-300 dark:text-gray-400 dark:hover:text-white dark:focus:ring-gray-500"
            aria-controls="mobile-menu-2" aria-expanded="false">
            <span class="sr-only">Open main menu</span>
            <span id="openCloseMenu"><i class="fa fa-bars" aria-hidden="true"></i> MENU</span>
        </button>
        <div class="hidden w-full lg:w-auto lg:flex justify-between" id="mobile-menu">
            <ul class="flex flex-col p-4 mt-2 bg-gray-50 rounded-lg border border-gray-100 lg:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                <li class="sm:hidden">
                    <div class="{{ request()->is('*dashboard*') ? 'text-purple-700 rounded-lg' : 'md:text-blue-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*dashboard*') ? 'text-purple-700 md:text-purple-700' : '' }} fa fa-lg fa-tachometer text-gray-700" aria-hidden="true"></i>
                        <a href="/dashboard" class="{{ request()->is('*dashboard*') ? 'text-purple-700' : 'md:text-blue-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-purple-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Dashboard
                        </a>
                    </div>
                </li>
                @can('admin')
                <li>
                    <div class="{{ request()->is('*admin*') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*admin*') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-lock text-gray-700" aria-hidden="true"></i>
                        <button id="buttonAdmin" data-dropdown-toggle="dropdownAdmin" class="{{ request()->is('*admin*') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Admin
                            <i class="px-2 fa fa-caret-down" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="dropdownAdmin" class="hidden overflow-x-auto z-50 w-full md:w-56 font-normal bg-gray-200 md:bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600" data-popper-reference-hidden="" data-popper-escaped="" data-popper-placement="bottom"
                    style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(383px, 66px);">
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/configuration" class="{{ request()->is('configuration') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-cog" aria-hidden="true"></i>
                                Configuration
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-bank" class="{{ request()->is('admin-bank') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-university" aria-hidden="true"></i>
                                Bank
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-discount" class="{{ request()->is('admin-discount') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-tags" aria-hidden="true"></i>
                                Discount
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-finance" class="{{ request()->is('admin-finance') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-money" aria-hidden="true"></i>
                                Finance
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-upgrade-level-request" class="{{ request()->is('admin-upgrade-level-request') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-list" aria-hidden="true"></i>
                                Upgrade Level Request
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-upgrade-level-promo" class="{{ request()->is('admin-upgrade-level-promo') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-tag" aria-hidden="true"></i>
                                Upgrade Level PROMO
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-users" class="{{ request()->is('admin-users') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                Users
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/admin-special-price" class="{{ request()->is('admin-special-price') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-tags" aria-hidden="true"></i>
                                Special Price
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/wa-broadcast" class="{{ request()->is('wa-broadcast') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-whatsapp" aria-hidden="true"></i>
                                WA Boradcast
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/reset-password-manual" class="{{ request()->is('reset-password-manual') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-key" aria-hidden="true"></i>
                                Reset Password User
                            </a>
                        </ul>
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/register-otp" class="{{ request()->is('register-otp') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-mobile" aria-hidden="true"></i>
                                OTP
                            </a>
                        </ul>
                    </div>
                </li>
                @endcan
                <li>
                    <div class="{{ request()->is('*saldo*') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*saldo*') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-credit-card-alt text-gray-700" aria-hidden="true"></i>
                        <button id="buttonSaldo" data-dropdown-toggle="dropdownSaldo" class="{{ request()->is('*saldo*') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Saldo
                            <i class="px-2 fa fa-caret-down" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="dropdownSaldo" class="hidden z-50 w-full md:w-44 font-normal bg-gray-200 md:bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600"
                        data-popper-reference-hidden="" data-popper-escaped="" data-popper-placement="bottom"
                        style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(383px, 66px);">
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400"
                            aria-labelledby="dropdownLargeButton">
                            <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                                <a href="/tambah-saldo" class="{{ request()->is('*tambah-saldo*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                    Tambah Saldo
                                </a>
                                <a href="/riwayat-isi-saldo" class="{{ request()->is('*riwayat-isi-saldo*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                                    History Saldo
                                </a>
                                @can('admin')
                                <a href="/topup-saldo-manual" class="{{ request()->is('*topup-saldo-manual*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                    <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>
                                    Topup Manual
                                </a>
                                @endcan
                            </ul>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="{{ request()->is('*order*') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*order*') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-cart-arrow-down text-gray-700" aria-hidden="true"></i>
                        <button id="buttonOrder" data-dropdown-toggle="dropdownOrder" class="{{ request()->is('*order*') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Order
                            <i class="px-2 fa fa-caret-down" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="dropdownOrder" class="hidden z-50 w-full md:w-44 font-normal bg-gray-200 md:bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600" data-popper-reference-hidden="" data-popper-escaped="" data-popper-placement="bottom"
                    style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(383px, 66px);">
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/new-order" class="{{ request()->is('*new-order*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                New Order
                            </a>
                            <a href="/history-order" class="{{ request()->is('history-order') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-history" aria-hidden="true"></i>
                                History Order
                            </a>
                            {{-- <a href="/history-order-refill" class="{{ request()->is('*history-order-refill*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                History Refill
                            </a> --}}
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="{{ request()->is('service') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*service') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-folder-open text-gray-700" aria-hidden="true"></i>
                        <a href="/service" class="{{ request()->is('*service') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Service
                        </a>
                    </div>
                </li>
                {{-- @can('admin')
                <li>
                    <div class="{{ request()->is('*server*') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*server*') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-server text-gray-700" aria-hidden="true"></i>
                        <button id="buttonServer" data-dropdown-toggle="dropdownServer" class="{{ request()->is('*server*') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Server
                            <i class="px-2 fa fa-caret-down" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div id="dropdownServer" class="hidden z-50 w-full md:w-56 font-normal bg-gray-200 md:bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600" data-popper-reference-hidden="" data-popper-escaped="" data-popper-placement="bottom"
                    style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(383px, 66px);">
                        <ul class="py-1 text-sm text-gray-700 dark:text-gray-400 p-2" aria-labelledby="dropdownLargeButton">
                            <a href="/server" class="{{ request()->is('server') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Tambah Server
                            </a>
                            <a href="/cek-layanan-server" class="{{ request()->is('*cek-layanan-server*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                                Cek Layanan Server
                            </a>
                            <a href="/server-list" class="{{ request()->is('*server-list*') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-list-alt" aria-hidden="true"></i>
                                Server List
                            </a>
                        </ul>
                    </div>
                </li>
                @endcan --}}
                <li>
                    <div class="{{ request()->is('upgrade-level') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('upgrade-level') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-level-up text-gray-700" aria-hidden="true"></i>
                        <a href="/upgrade-level" class="{{ request()->is('upgrade-level') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Upgrade Level
                        </a>
                    </div>
                </li>
                <li>
                    <div class="{{ request()->is('referral') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*referral') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-link text-gray-700" aria-hidden="true"></i>
                        <a href="/referral" class="{{ request()->is('*referral') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            Referral
                        </a>
                    </div>
                </li>
                <li>
                    <div class="{{ request()->is('doc-api') ? 'text-blue-700 rounded-lg' : 'md:text-gray-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('doc-api') ? 'text-blue-700 md:text-blue-700' : '' }} fa fa-lg fa-file-code-o text-gray-700" aria-hidden="true"></i>
                        <a href="/doc-api" class="{{ request()->is('doc-api') ? 'text-blue-700' : 'md:text-gray-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">
                            API
                        </a>
                    </div>
                </li>
                <li>
                    <div class="{{ request()->is('*info*') ? 'text-purple-700 rounded-lg' : 'md:text-yellow-700' }} flex gap-2 items-center">
                        <i class="{{ request()->is('*info*') ? 'text-purple-700 md:text-purple-700' : '' }} fa fa-lg fa-info-circle text-yellow-700" aria-hidden="true"></i>
                        <a href="/info" class="{{ request()->is('*info*') ? 'text-purple-700' : 'md:text-yellow-700' }} flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-yellow-700 rounded md:hover:bg-transparent md:border-0 md:hover:text-purple-700 md:p-0 md:w-auto dark:text-yellow-400 dark:hover:text-white dark:focus:text-white dark:border-yellow-700 dark:hover:bg-yellow-700 md:dark:hover:bg-transparent">
                            Info
                        </a>
                    </div>
                </li>
                <div class="lg:hidden" id="profileMobile">
                    <div class="flex gap-1 items-center">
                        @if (auth()->user()->levelMember->level == 3)
                        <i class="fa fa-user-secret" aria-hidden="true"></i>
                        @elseif (auth()->user()->levelMember->level == 2)
                        <i class="fa fa-users" aria-hidden="true"></i>
                        @else
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        @endif
                        <button id="profileSmallButton" data-dropdown-toggle="profileSmallDropdown"
                        class="flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">{{ auth()->user()->username }}
                        <svg class="ml-1 w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </div>
                    <div id="profileSmallDropdown"
                        class="hidden z-50 w-full font-normal bg-gray-200 md:bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600"
                        data-popper-reference-hidden="" data-popper-escaped="" data-popper-placement="bottom"
                        style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(383px, 66px);">
                        <div class="py-1">
                            <a href="/profile" class="{{ request()->is('profile') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                @if (auth()->user()->levelMember->level == 3)
                                <i class="fa fa-user-secret" aria-hidden="true"></i>
                                @elseif (auth()->user()->levelMember->level == 2)
                                <i class="fa fa-users" aria-hidden="true"></i>
                                @else
                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                @endif
                                Profile
                            </a>
                            <a href="/log-saldo" class="{{ request()->is('log-saldo') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                                <i class="fa fa-history" aria-hidden="true"></i>
                                Log Saldo
                            </a>
                            <form action="{{ url('logout') }}" method="POST">
                                @csrf
                                <button type="submit" class="block p-2 gap-2 w-full text-start text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i> Sign out
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </ul>
        </div>
        <div class="ml-auto md:flex">
            <a href="/tambah-saldo" class="lg:hidden hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white flex justify-center items-center px-3 gap-1 ml-auto bg-yellow-100 rounded-xl">
                <div>
                    <i class="fa fa-plus-circle text-blue-700 dark:text-blue-800" aria-hidden="true"></i>
                    <span class="text-yellow-800 font-bold">{{ Helpers::format_rupiah(auth()->user()->saldo) }}</span>
                </div>
            </a>
            <div class="hidden lg:flex gap-1 items-center">
                @if (auth()->user()->levelMember->level == 3)
                <i class="fa fa-user-secret" aria-hidden="true"></i>
                @elseif (auth()->user()->levelMember->level == 2)
                <i class="fa fa-users" aria-hidden="true"></i>
                @else
                <i class="fa fa-user-circle" aria-hidden="true"></i>
                @endif
                <button id="profileLargaButton" data-dropdown-toggle="profileLargaNavbar"
                class="flex justify-between items-center py-2 pr-4 pl-3 w-full font-medium text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-gray-400 dark:hover:text-white dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent">{{ auth()->user()->username }}
                <svg class="ml-1 w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                </button>
            </div>
            <div id="profileLargaNavbar"
                class="hidden z-50 w-60 font-normal bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700 dark:divide-gray-600"
                data-popper-reference-hidden="" data-popper-escaped="" data-popper-placement="bottom"
                style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(383px, 66px);">
                <div class="py-156">
                    <a href="/tambah-saldo" class="hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white flex justify-center items-center px-3 gap-1 ml-auto bg-yellow-100 rounded-xl">
                        <div>
                            <i class="fa fa-plus-circle text-blue-700 dark:text-blue-800" aria-hidden="true"></i>
                            <span class="text-yellow-800 font-bold">{{ Helpers::format_rupiah(auth()->user()->saldo) }}</span>
                        </div>
                    </a>
                    <a href="/profile" class="{{ request()->is('profile') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                        @if (auth()->user()->levelMember->level == 3)
                        <i class="fa fa-user-secret" aria-hidden="true"></i>
                        @elseif (auth()->user()->levelMember->level == 2)
                        <i class="fa fa-users" aria-hidden="true"></i>
                        @else
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        @endif
                        Profile
                    </a>
                    <a href="/log-saldo" class="{{ request()->is('log-saldo') ? 'bg-blue-700 rounded-lg md:text-blue-700 text-white md:bg-white' : 'md:text-gray-700' }} flex items-center hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white p-2 gap-2">
                        <i class="fa fa-history" aria-hidden="true"></i>
                        Log Saldo
                    </a>
                    <form action="{{ url('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="block p-2 gap-2 w-full text-start text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-400 dark:hover:text-white">
                            <i class="fa fa-sign-out" aria-hidden="true"></i> Sign out
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="sticky top-16 z-40">
    <marquee direction="up" scrollamount="1" height="50px" onmouseover="this.stop();" onmouseout="this.start();" class="bg-blue-200 text-center items-center flex justify-center text-sm md:text-2xl mx-auto">
        <div class="p-2 text-sm" id="info">
            <p id="selamat datang">Selamat Datang  {{ auth()->user()->username }}</p>
        </div>
    </marquee>
</div>

<script>
    $(document).ready(function(){
        $.ajax({
            url : '/api/info-discount',
            success : function(res){
                $.each(res, function(key, value){
                    $('#info').append(`
                        <p class=""><span class="text-blue-600 italic">${value.discount_name}</span> | <span class="text-orange-500">ID ${value.service.id} - ${value.service.service_name}</span> | <span class="text-red-500 font-bold"> Discount : ${Math.floor(value.discount)} %</span> | <span class="text-yellow-800 font-bold">${value.level_member.level_name}</span></p>
                    `)
                })
            }
        })
    })
</script>
