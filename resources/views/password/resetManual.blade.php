@extends('layouts.main')

@section('content')
    <div class="p-9 flex justify-center">
        <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full md:w-1/3">
            <form method="POST" action="/reset-password-manual">
                @csrf
                <div class="mb-6">
                  <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Username</label>
                  <input type="username" id="username" name="username" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
                </div>
                <div class="mb-6">
                  <label for="password" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">New password</label>
                  <input type="password" id="password" name="password" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
                </div>
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Reset</button>
            </form>

            <div class="p-5 mt-10">
                <h1 class="text-lg italic mb-4">History Reset Password Manual</h1>
                <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="table-reset-password-manual">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="py-3 px-6">
                                    Username
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Created By
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Updated By
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reset_manuals as $reset_manual)
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                                <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {{ $reset_manual->username->username ?? '-' }}
                                </th>
                                <td class="py-4 px-6">
                                    {{ $reset_manual->createdBy->username ?? '-' }}
                                </td>
                                <td class="py-4 px-6">
                                    {{ $reset_manual->updatedBy->username ?? '-' }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
        $('#table-reset-password-manual').DataTable({
            // responsive : true,
            "lengthChange": false,
            "ordering" : true,
            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: [ "_all" ],
                },
            ]
        });
    });
    </script>
@endsection
