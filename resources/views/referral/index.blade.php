@extends('layouts.main')

@section('content')
<div class="p-9">
    <div class="flex justify-center gap-4">
        @can('admin')
        <div class="flex justify-center">
            <a href="/user-referral" class="hover:bg-blue-200 border border-slate-200 p-2 bg-blue-100 rounded-lg mb-4">User Referral</a>
        </div>
        @endcan
    </div>

    <div class="flex gap-12 justify-center">
        <div class="p-3 shadow-md rounded-lg bg-slate-200 w-full md:w-max flex flex-col gap-5">
            @if(auth()->user()->userReferral)
            <div class="overflow-x-auto flex flex-col gap-5">
                <div class="">
                    <span class="text-sm">Referral Link :</span>
                    <div class="flex gap-4 items-center text-purple-800">
                        <span id="linkReferral">{{ auth()->user()->userReferral ? url('') . '/ref' . '/' .
                            auth()->user()->userReferral->referral_code : '-' }}</span>
                        <i class="fa fa-files-o iconCopy text-blue-600 hover:cursor-pointer" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="">
                    <span class="text-sm">Total Visitors :</span>
                    <div class="flex gap-4 items-center text-yellow-800">
                        <span id="totalVisitors">{{ $user_referral ? $user_referral->visitors : 0 }}</span>
                    </div>
                </div>
                <div class="flex flex-col">
                    <span class="text-sm">Persentase Komisi :</span>
                    <span class="text-lg text-yellow-800">{{ $persentase_komisi }}%
                        {{-- @if(auth()->user()->levelMember->level < 3)<br>
                        <span class="text-xs italic text-yellow-800">upgrade ke level Agen untuk mendapatkan komisi sebesar 5%</span>@endif</span> --}}
                </div>
            </div>
            <div class="flex flex-col justify-between border border-t-4 border-slate-400 p-4 gap-5">
                <div class="flex justify-between">
                    <div class="flex flex-col">
                        <span class="text-sm">Total Komisi</span>
                        <span class="text-md text-green-500">{{ Helpers::format_rupiah(($user_referral ? $user_referral->total_commission : 0)) }} </span>
                    </div>
                    <div class="flex flex-col">
                        <span class="text-sm">Total Penarikan</span>
                        <span class="text-md text-orange-500">{{ Helpers::format_rupiah(auth()->user()->userReferral->total_withdrawal) }}</span>
                    </div>
                </div>
                <div class="flex justify-center">
                    <a href="/tarik-komisi" class="hover:bg-green-300 w-full text-center italic text-xs p-1 rounded-lg bg-green-200 border border-green-300">Tarik Komisi</a>
                </div>
                <div class="flex justify-center">
                    <a href="/list-downline" class="hover:bg-yellow-300 w-full text-center italic text-xs p-1 rounded-lg bg-yellow-200 border border-yellow-300">List Downline</a>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="flex flex-col text-center gap-5">
        <h1 class="text-xs italic md:text-md text-purple-800">Untuk dapat mengaktifkan referral system, silahkan lakukan
            Generate Link</h1>
        <form action="/referral-generate-link" method="POST">
            @csrf
            <button type="submit"
                class="text-slate-800 bg-blue-300 rounded-lg hover:text-slate-500 text-sm w-max px-5">Generate Referral
                Link</button>
        </form>
    </div>
    @endif

    <div class="flex justify-center">
        <div id="accordion-collapse" data-accordion="collapse" class="border border-slate-300 rounded-lg shadow-lg mb-1 md:mb-9 mt-5 w-full">
            <h2 id="accordion-collapse-heading-2">
                <button type="button"
                    class="flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800"
                    data-accordion-target="#accordion-collapse-body-2" aria-expanded="false"
                    aria-controls="accordion-collapse-body-2">
                    <span class="text-gray-600">Informasi <span class="font-bold text-green-600">REFERRAL</span></span>
                    <svg data-accordion-icon="" class="w-6 h-6 shrink-0" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </h2>
            <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2 p-5">
                <div class="p-8 font-light border border-b-0 border-gray-200 dark:border-gray-700 dark:bg-gray-900">
                    <p class="text-2xl text-green-500 font-bold">Cara Mendapatkan Downline</p>
                    <ol>
                        <li type="1" class="font-bold text-gray-700">
                            <p class="font-bold text-gray-500">
                                Silahkan lakukan generate link terlebih dahulu, kemudian sebarkan link tersebut kepada
                                calon member baru
                            </p>
                        </li>
                        <li type="1" class="font-bold text-gray-700">
                            <p class="font-bold text-gray-500">
                                Jika member baru berhasil mendaftar menggunakan link Anda, maka Anda akan mendapatkan 1
                                downline baru
                            </p>
                        </li>
                    </ol>
                    <br>
                    <p class="text-2xl text-green-500 font-bold">Komisi Order</p>
                    <ol>
                        <li type="1" class="font-bold text-gray-700">
                            <p class="font-bold text-gray-500">
                                Jika downline kamu melakukan orderan dan status ordernya adalah <span
                                    class="text-purple-800 text-md md:text-lg italic font-bold">completed</span>, maka
                                kamu berhak mendapatkan <span
                                    class="text-purple-800 text-md md:text-xl italic font-bold">komisi sebesar {{
                                    $persentase_komisi }}%</span> dari total harga yang telah diorder.
                            </p>
                        </li>
                        <li type="1" class="font-bold text-gray-700">
                            <p class="font-bold text-gray-500">
                                Jika downline kamu melakukan upgrade level, maka kamu berhak mendapatkan <span
                                    class="text-purple-800 text-md md:text-xl italic font-bold">komisi sebesar 20%
                                </span> dari total harga upgrade.
                            </p>
                        </li>
                    </ol>
                    <br>
                    <p class="font-bold">
                        NOTE :
                    </p>
                    <li class="font-bold text-purple-800 text-md md:text-xl">
                        Penarikan komisi ke rekening bank hanya bisa dilakukan <span class="text-red-800">1 kali</span> setiap 1 bulan
                    </li>
                    <li class="font-bold text-purple-800 text-md md:text-xl">
                        Penarikan komisi dapat dilakukan setelah mendapatkan konfirmasi dari Customer Support
                    </li>
                    <li class="font-bold text-purple-800 text-md md:text-xl">
                        Minimum penarikan komisi adalah <span class="text-red-800">{{ Helpers::format_rupiah($minimum_penarikan_komisi) }}</span>
                    </li>
                    {{-- <li class="font-bold text-purple-800 text-md md:text-xl">
                        Maksimum penarikan komisi adalah <span class="text-red-800">Rp 100.000</span>
                    </li> --}}
                    <li class="text-gray-800 text-xs md:text-xl">
                        Ketentuan dapat berubah sewaktu-waktu
                    </li>
                </div>
            </div>
        </div>
    </div>

    @if(auth()->user()->userReferral)
    <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
        <h3 class="text-lg italic mb-4">History Komisi</h3>
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableLogCommission">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        Tanggal
                    </th>
                    @can('admin')
                    <th scope="col" class="py-3 px-6">
                        Upline
                    </th>
                    @endcan
                    <th scope="col" class="py-3 px-6">
                        Username
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Service ID
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Harga Order
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Discount
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Komisi
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Note
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Status Order
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($history_commissions as $history_commission)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $history_commission->updated_at }}
                    </th>
                    @can('admin')
                    <td class="py-4 px-6">
                        {{ $history_commission->userUpline->username }}
                    </td>
                    @endcan
                    <td class="py-4 px-6">
                        {{ $history_commission->userDownline->username }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history_commission->service_id ?? '-' }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history_commission->harga_order }}
                    </td>
                    <td class="py-4 px-6">
                        {{ (int)$history_commission->discount }}%
                    </td>
                    <td class="py-4 px-6 text-green-500">
                        {{ $history_commission->commission }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history_commission->note ?? '-' }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $history_commission->status_order }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif
</div>

    <script>
        $('.iconCopy').click(function(){
            $('.copiedLink').empty(); // hapus tulisan copied jika ada
            const link = $('#linkReferral').text();
            // console.log(link);
            navigator.clipboard.writeText(link);
            $(this).after(`<span class="copiedLink text-xs text-yellow-800 italic"> Copied!</span>`);
            $('#linkReferral').addClass(`text-blue-800`);
        });
    </script>

    <script>
        $(document).ready(function(){
            const target_admin = [4, 6];
            const target_user = [3, 5];
            $('#tableLogCommission').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ],
                    },
                    {
                        target: "{{ auth()->user()->role }}" == 'admin' ? target_admin : target_user,  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                ],
                order: [[0, 'desc']],
            });
        });
    </script>

    <script>
        // $('#buttonTarikKomisi').click(function(){
        //     Swal.fire(
        //         'Coming Soon!',
        //         'Total komisi akan tetap terhitung hingga Fitur ini siap digunakan',
        //         'info'
        //     )
        // })

        $('#buttonHistoryPenarikan').click(function(){
            Swal.fire(
                'Coming Soon!',
                'Total komisi akan tetap terhitung hingga Fitur ini siap digunakan',
                'info'
            )
        })
    </script>
@endsection
