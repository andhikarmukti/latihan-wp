@extends('layouts.main')

@section('content')
    <div class="p-9 flex flex-col items-center">
        <div class="flex mb-5 w-1/2">
            <div class="flex w-3/4">
                <a href="/referral" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
            </div>
        </div>
        <h3 class="text-lg italic mb-5">List Downline</h3>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-1/2">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableListDownline">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Status
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Total Saldo
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Total Belanja
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Last Seen
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Tanggal Join
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list_downlines as $list_downline)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <td class="py-4 px-6">
                            @if(Cache::has('user-is-online-' . $list_downline->id))
                                <span class="text-green-500">✦</span>
                            @else
                                <span class="text-red-500">✦</span>
                            @endif
                        </td>
                        <td class="py-4 px-6">
                            {{ $list_downline->username }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $list_downline->saldo }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $list_downline->saldo_digunakan }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $list_downline->last_seen }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $list_downline->created_at }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            const target_admin = [4, 6];
            const target_user = [3, 5];
            $('#tableListDownline').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                "pageLength": 20,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ],
                    },
                    {
                        target: [2, 3],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                ],
                order: [[4, 'desc']],
            });
        });
    </script>
@endsection
