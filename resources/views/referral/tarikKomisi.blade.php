@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="flex justify-center mb-5">
            <div class="flex w-3/4">
                <a href="/referral" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
            </div>
        </div>
        @if ($user_referral->bank == null || $user_referral->status_bank == 'decline')
        <div class="flex flex-col items-center">
            <div id="alert-additional-content-1" class="w-full md:w-1/2 p-4 mb-4 border border-red-300 rounded-lg bg-red-50 dark:bg-red-300" role="alert">
                <div class="flex justify-center">
                  <h3 class="text-sm text-center md:text-lg font-medium text-red-900">Data bank wajib diisi</h3>
                </div>
                <div class="mt-2 mb-4 text-sm text-red-900 text-center">
                    Silahkan isi data bank terlebih dahulu, dan tunggu approval dari Admin kami.
                    {{-- <p>Pastikan bank yang digunakan adalah atas nama pribadi sesuai dengan KTP</p> --}}
                </div>
            </div>
            <div id="alert-additional-content-1" class="mt-5 w-full md:w-1/2 p-4 mb-4 border border-blue-300 rounded-lg bg-blue-50 dark:bg-blue-300" role="alert">
                <form>
                    <div class="mb-6">
                      <label for="bank" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Bank</label>
                      <div id="bank" class="hover:bg-gray-200 cursor-pointer bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <div class="flex justify-between">
                            <div data-codebank="" id="pilihBank">
                                Pilih Bank
                            </div>
                            <div>
                                <i id="arrow" class="fa fa-arrow-down text-gray-900" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div id="searchBox" class="hidden border border-slate-200 rounded-lg mt-2">
                        <div class="bg-white">
                            <input id="inputSearch" type="text" class="hover:bg-slate-200 w-full border-t-0 border-transparent shadow-md" placeholder="Search.." autocomplete="nope">
                        </div>
                          <div class="shadow-md overflow-y-scroll max-h-44 w-full bg-white">
                            @foreach ($bank_options as $bank_option)
                            <div id="listBank">
                                <div data-codebank="{{ $bank_option->code_bank }}" class="optionBank p-2 cursor-pointer hover:bg-slate-200 text-sm">
                                    {{ $bank_option->nama_bank }}
                                </div>
                            </div>
                            @endforeach
                          </div>
                        </div>
                    </div>
                    <div class="mb-6">
                      <label for="no_rekening" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">No Rekening</label>
                      <input type="text" id="no_rekening" name="no_rekening" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
                    </div>
                    <div class="mb-6">
                      <label for="atas_nama" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Atas Nama</label>
                      <input type="text" id="atas_nama" name="atas_nama" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
                    </div>
                    <button id="submitBank" type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
            </div>
        </div>
        @else
        <div class="flex flex-col md:flex-row gap-5 mx-auto w-full md:w-3/4 mb-5 p-5 bg-blue-100 border border-slate-200 rounded-lg shadow-md">
            <div class="w-full md:w-1/2">
                <form action="/request-withdrawal" method="POST">
                    @csrf
                    <div class="flex justify-center bg-green-100 border border-blue-300 rounded-lg p-5 mb-5">
                        <p>Total Komisi : {{ Helpers::format_rupiah($user_referral->total_commission) }}</p>
                    </div>
                    <div class="mb-6">
                        <label for="jenis_penarikan" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Jenis Penarikan Komisi</label>
                        <select id="jenis_penarikan" name="jenis_penarikan" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <option selected disabled>Pilih Jenis Penarikan</option>
                            <option value="saldo">Saldo</option>
                            <option value="transfer">Transfer</option>
                        </select>
                    </div>
                    <div class="mb-6">
                      <label for="nominal" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nominal</label>
                      <input type="number" id="nominal" name="nominal" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required="">
                      <small class="italic text-purple-500 text-xs">Minimum penarikan : {{ Helpers::format_rupiah($minimum_penarikan_komisi) }}</small>
                    </div>
                    <button type="submit" class="text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:outline-none focus:ring-purple-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-800">Request</button>
                </form>
            </div>
            <div class="w-full md:w-1/2">
                <div id="alert-additional-content-2" class="p-4 mb-4 border border-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-300 rounded-lg bg-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-50 dark:bg-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-200" role="alert">
                    <div class="flex items-center">
                      <svg aria-hidden="true" class="w-5 h-5 mr-2 text-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-900 dark:text-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                      <span class="sr-only">Info</span>
                      <h3 class="text-lg font-medium text-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-900 dark:text-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-800">Rekening Bank</h3>
                    </div>
                    <div class="mt-2 mb-4 text-sm text-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-900 dark:text-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'red' }}-800">
                      <ul>
                        <li>Bank : {{ $user_referral->bankOption->nama_bank }}</li>
                        <li>No Rekening : {{ $user_referral->no_rekening }}</li>
                        <li>Atas Nama : {{ $user_referral->atas_nama }}</li>
                        @if($user_referral->status_bank == 'pending')
                        <li class="text-2xl">Status : {{ $user_referral->status_bank }}</li>
                        @endif
                      </ul>
                    </div>
                    <div class="flex">
                      <button id="deleteBankAccount" type="button" class="gap-2 text-white bg-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'yellow' }}-900 hover:bg-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'yellow' }}-800 focus:ring-4 focus:outline-none focus:ring-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'yellow' }}-300 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'yellow' }}-800 dark:hover:bg-{{ $user_referral->status_bank == 'accept' ? 'blue' : 'yellow' }}-900">
                        <i class="fa fa-retweet" aria-hidden="true"></i>
                        Delete Bank
                      </button>
                    </div>
                  </div>
            </div>
        </div>
        <div class="w-full flex justify-center">
            <div class="w-full md:w-3/4 overflow-x-auto relative shadow-md sm:rounded-lg p-5">
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableWithdrawal">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Withdrawal ID
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Username
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Nominal
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Saldo
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Transfer
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Status
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Request Date
                            </th>
                            @can('admin')
                            <th scope="col" class="py-3 px-6">
                                Action
                            </th>
                            @endcan
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($withdrawals as $withdrawal)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $withdrawal->id }}
                            </th>
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $withdrawal->user->username }}
                            </th>
                            <td class="py-4 px-6">
                                {{ $withdrawal->nominal }}
                            </td>
                            <td class="py-4 px-6 font-bold">
                                {{ $withdrawal->is_saldo == true ? '✓' : '-' }}
                            </td>
                            <td class="py-4 px-6 font-bold">
                                {{ $withdrawal->is_transfer == true ? '✓' : '-' }}
                            </td>
                            <td class="py-4 px-6 {{ $withdrawal->status == 'pending' ? 'text-yellow-500' : (($withdrawal->status == 'cancel' || $withdrawal->status == 'decline') ? 'text-red-500' : 'text-green-500') }}">
                                {{ $withdrawal->status }}
                            </td>
                            <td class="py-4 px-6">
                                {{ $withdrawal->created_at }}
                            </td>
                            @can('admin')
                            <td class="py-4 px-6">
                                @if ($withdrawal->status == 'pending')
                                <span data-action="accept" data-idwd="{{ $withdrawal->id }}" class="actionWithdrawal cursor-pointer bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900">✓</span>
                                <span data-action="decline" data-idwd="{{ $withdrawal->id }}" class="actionWithdrawal cursor-pointer bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">✘</span>
                                @endif
                            </td>
                            @endcan
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>

    <script>
        $(document).ready(function(){
            $('#tableWithdrawal').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ],
                    },
                    {
                        target: [2],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                ],
                order: [[0, 'desc']],
            });
        });
    </script>

    <script>
        // DROPDOWN BANK
        $('#bank').click(function(){
            $('#searchBox').toggleClass('hidden');
            $('#arrow').toggleClass('fa-arrow-down');
            $('#arrow').toggleClass('fa-arrow-up');
        });

        // INPUT SEARCH
        $('#inputSearch').keyup(function(){
            let keyword_bank = $(this).val();
            fetchOptionBank(keyword_bank)
        });

        // Klik Option Bank
        $('.optionBank').click(function(){
            clickOptionBank($(this))
        });
    </script>

    <script>
        // Function click option Bank
        function clickOptionBank(optionBank){
            const code_bank = optionBank.data('codebank');
            $('#searchBox').addClass('hidden');
            $('#pilihBank').html(optionBank.html());
            $('#pilihBank').attr('data-codebank', code_bank);
        }

        // Function fetch option bank
        function fetchOptionBank(keyword_bank){
            $.ajax({
                url : '/tarik-komisi',
                dataType : 'JSON',
                method : 'GET',
                data : {
                    keyword_bank : keyword_bank
                },
                success : function(res){
                    $('.optionBank').remove();
                    $('#listBank').remove();
                    if(res){
                        $.each(res, function(key, value){
                            $('#listBank').append(
                                `
                                    <div data-codebank="${value.code_bank}" class="optionBank p-2 cursor-pointer hover:bg-slate-200">
                                        ${value.nama_bank}
                                    </div>
                                `
                            ).ready(function(){
                                $('.optionBank').click(function(){
                                    clickOptionBank($(this))
                                });
                            });
                        })
                    }
                }
            })
        }

        // Function disabled form bank
        function disableFormBank(){
            $('#bank').attr('disabled', true);
            $('#atas_nama').attr('disabled', true);
            $('#no_rekening').attr('disabled', true);
        }
        // Function enable form bank
        function enableFormBank(){
            $('#bank').attr('disabled', false);
            $('#atas_nama').attr('disabled', false);
            $('#no_rekening').attr('disabled', false);
        }
    </script>

    <script>
        $('#submitBank').click(function(){
            const codeBank = $('#pilihBank').attr('data-codebank');
            const atasNama = $('#atas_nama').val();
            const noRekening = $('#no_rekening').val();
            let errMessage = '';
            let errStatus = false;
            let successMessage = '';

            Swal.fire({
                title: 'Anda yakin?',
                text: "Pastikan semua data yang diisi sudah benar",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yakin!',
                cancelButtonText: 'Tidak!',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/tarik-komisi',
                        dataType : 'JSON',
                        method : 'POST',
                        data : {
                            code_bank : codeBank,
                            atas_nama : atasNama,
                            no_rekening : noRekening,
                            '_token' : "{{ csrf_token() }}"
                        },
                        beforeSend : function(){
                            let timerInterval
                            Swal.fire({
                                title: 'Submit Data Bank!',
                                html: 'Mohon menunggu.',
                                timer: 20000,
                                timerProgressBar: true,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })
                        },
                        success : function(res){
                            successMessage = res.message;
                        },
                        error : function(err){
                            errMessage = err.responseJSON.message;
                            errStatus = true;
                        },
                        complete : function(){
                            if(errStatus == true){
                                Swal.fire(
                                    'Gagal!',
                                    errMessage ?? 'Server Error, Hubungi Admin',
                                    'error'
                                )
                            }else{
                                Swal.fire(
                                    'Berhasil!',
                                    successMessage,
                                    'success'
                                ).then(() => {
                                    window.location = '/tarik-komisi';
                                });
                            }
                        }
                    });
                }
            })
        });
    </script>

    <script>
        $('#deleteBankAccount').click(function(){
            let errMessage = '';
            let errStatus = false;
            let successMessage = '';

            Swal.fire({
                title: 'Anda yakin?',
                text: "Data bank akan dihapus, Anda perlu input ulang data bank untuk melakukan penarikan komisi",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yakin!',
                cancelButtonText: 'Tidak!',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/delete-bank-account',
                        dataType : 'JSON',
                        method : 'POST',
                        data : {
                            '_token' : "{{ csrf_token() }}"
                        },
                        beforeSend : function(){
                            let timerInterval
                            Swal.fire({
                                title: 'Menghapus data bank',
                                html: 'Mohon menunggu.',
                                timer: 20000,
                                timerProgressBar: true,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })
                        },
                        success : function(res){
                            // console.log(res);
                            successMessage = res.message;
                        },
                        error : function(err){
                            errMessage = err.responseJSON.message;
                            errStatus = true;
                        },
                        complete : function(){
                            if(errStatus == true){
                                Swal.fire(
                                    'Gagal!',
                                    errMessage ?? 'Server Error, Hubungi Admin',
                                    'error'
                                )
                            }else{
                                Swal.fire(
                                    'Berhasil!',
                                    successMessage,
                                    'success'
                                ).then(() => {
                                    window.location = '/tarik-komisi';
                                });
                            }
                        }
                    });
                }
            })
        });
    </script>

    <script>
        $('.actionWithdrawal').click(function(){
            const idWd = $(this).attr('data-idwd');
            const action = $(this).attr('data-action');
            let errMessage = '';
            let errStatus = false;
            let successMessage = '';

            Swal.fire({
                title: 'Anda yakin?',
                text: "Request penarikan komisi akan disetujui",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yakin!',
                cancelButtonText: 'Tidak!',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/accept-withdrawal/' + idWd,
                        dataType : 'JSON',
                        method : 'POST',
                        data : {
                            action : action,
                            '_token' : "{{ csrf_token() }}"
                        },
                        beforeSend : function(){
                            let timerInterval
                            Swal.fire({
                                title: 'Sedang memproses!',
                                html: 'Mohon menunggu.',
                                timer: 20000,
                                timerProgressBar: true,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })
                        },
                        success : function(res){
                            // console.log(res);
                            successMessage = res.message;
                        },
                        error : function(err){
                            errMessage = err.statusText;
                            errStatus = true;
                        },
                        complete : function(){
                            if(errStatus == true){
                                Swal.fire(
                                    'Gagal!',
                                    errMessage ?? 'Server Error, Hubungi Admin',
                                    'error'
                                )
                            }else{
                                Swal.fire(
                                    successMessage,
                                    '',
                                    'success'
                                ).then(() => {
                                    window.location = '/tarik-komisi';
                                });
                            }
                        }
                    });
                }
            })
        });
    </script>
@endsection
