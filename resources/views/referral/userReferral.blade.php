@extends('layouts.main')

@section('content')
    <div class="p-9">
        <h1 class="flex text-2xl italic">User Referral</h1>
        <div class="my-5">
            <a href="/referral" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
        </div>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableReferral">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Referral Code
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Visitors
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Total Commission
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Total Withdrawal
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Bank
                        </th>
                        <th scope="col" class="py-3 px-6">
                            No Rekening
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Atas Nama
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Status Bank
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user_referrals as $user_referral)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $user_referral->user->username }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $user_referral->referral_code }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user_referral->visitors }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user_referral->total_commission }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user_referral->total_withdrawal }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user_referral->bank }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user_referral->no_rekening }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user_referral->atas_nama }}
                        </td>
                        <td class="py-4 px-6 {{ $user_referral->status_bank == 'accept' ? 'text-green-500' : 'text-yellow-800' }}">
                            {{ $user_referral->bank == null ? '-' : $user_referral->status_bank }}
                        </td>
                        <td class="py-4 px-6">
                            @if ($user_referral->bank != null && $user_referral->status_bank == 'pending')
                            <span data-id="{{ $user_referral->id }}" data-action="accept" class="action cursor-pointer bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900">✓</span>
                            <span data-id="{{ $user_referral->id }}" data-action="decline" class="action cursor-pointer bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">✘</span>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#tableReferral').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ],
                    },
                    {
                        target: [3, 4],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                ],
                order: [[3, 'desc']],
            });
        });
    </script>

    <script>
        $('.action').click(function(){
            const action = $(this).attr('data-action');
            const idUserReferral = $(this).attr('data-id');
            let errMessage = '';
            let errStatus = false;
            let successMessage = '';

            Swal.fire({
                title: action + ' data bank?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Tidak!',
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/action-bank-account/' + idUserReferral,
                        dataType : 'JSON',
                        method : 'POST',
                        data : {
                            action : action,
                            '_token' : "{{ csrf_token() }}"
                        },
                        beforeSend : function(){
                            let timerInterval
                            Swal.fire({
                                title: 'Sedang memproses!',
                                html: 'Mohon menunggu.',
                                timer: 20000,
                                timerProgressBar: true,
                                allowOutsideClick: false,
                                didOpen: () => {
                                    Swal.showLoading()
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })
                        },
                        success : function(res){
                            // console.log(res);
                            successMessage = res.message;
                        },
                        error : function(err){
                            errMessage = err.responseJSON.message;
                            errStatus = true;
                        },
                        complete : function(){
                            if(errStatus == true){
                                Swal.fire(
                                    'Gagal!',
                                    errMessage ?? 'Server Error, Hubungi Admin',
                                    'error'
                                )
                            }else{
                                Swal.fire(
                                    'Berhasil!',
                                    successMessage,
                                    'success'
                                ).then(() => {
                                    window.location = '/user-referral';
                                });
                            }
                        }
                    });
                }
            })
        })
    </script>
@endsection
