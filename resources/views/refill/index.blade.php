@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div>
            <small class="text-xs italic sm:hidden text-gray-500">Table dapat digeser <i class="fa fa-long-arrow-right" aria-hidden="true"></i></small>
        </div>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg rounded-lg p-5 border border-slate-300">
            <h3 class="font-bold italic text-start text-2xl mb-5">History Refill</h3>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Refill ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Tanggal Pengajuan Refill
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Order ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Target
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Service
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Status
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($refill_statuses as $refill_status)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $refill_status->refill_id }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $refill_status->created_at }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $refill_status->history_order_id }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $refill_status->target }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $refill_status->service }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $refill_status->status }}
                        </td>
                    @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
