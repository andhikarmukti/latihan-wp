@extends('layouts.main')

@section('content')
    <div class="p-9 flex justify-center">
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg w-full md:w-1/2 p-5">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="table-otp">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            No
                        </th>
                        <th scope="col" class="py-3 px-6">
                            No WA
                        </th>
                        <th scope="col" class="py-3 px-6">
                            OTP
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Created At
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($otps as $otp)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $otp->id }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $otp->no_wa }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $otp->otp }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $otp->created_at }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#table-otp').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ],
                    },
                ],
                order : [0, 'DESC']
            });
        });
    </script>
@endsection
