@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row flex flex-col justify-center items-center min-h-screen w-screen gap-9">
        <h1 class="text-center text-4xl font-bold italic">Reset Password</h1>
        <div class="card border border-sm p-4 w-80 shadow-md rounded-lg">
            <form method="POST" action="/reset-password/{{ $token }}">
                @csrf
                <div class="flex flex-col gap-3">
                    <div>
                        <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                        <input type="text" name="email" id="email" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @if($errors->has('email')) ring-1 ring-red-400 @endif">
                        @if($errors->has('email'))
                            <small class="text-red-400 italic">{{ $errors->first('email') }}</small>
                        @endif
                    </div>
                    <div>
                        <label for="password" class="block text-sm font-medium text-gray-700">Password</label>
                        <input type="password" name="password" id="password" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @if($errors->has('password'))
                            <small class="text-red-400 italic">{{ $errors->first('password') }}</small>
                        @endif
                    </div>
                    <div>
                        <label for="password_confirmation" class="block text-sm font-medium text-gray-700">Password Confirmation</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" autocomplete="off" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @if($errors->has('password_confirmation'))
                            <small class="text-red-400 italic">{{ $errors->first('password_confirmation') }}</small>
                        @endif
                    </div>
                    <div class="mx-auto">
                        <button class="border border-sm rounded-md bg-blue-600 text-white p-2 w-40 hover:bg-blue-500 hover:text-gray-700">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
