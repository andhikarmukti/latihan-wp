@extends('layouts.main')

@section('content')
<div class="p-9">
    @if(!$topup_pending)
    <div class="flex justify-center lg:justify-start">
        <a href="/tambah-saldo" class="p-3 bg-blue-100 ring-1 ring-blue-400 rounded-xl hover:bg-blue-200" id="buttonTambahService">Tambah Saldo</a>
    </div>
    @endif
    @can('admin')
    <div class="flex justify-center lg:justify-start my-3">
        <form action="/mutasi-check" method="POST">
            @csrf
            <button type="submit" class="text-white p-3 bg-blue-500 ring-1 ring-blue-400 rounded-xl hover:bg-blue-200" id="buttonCekMutasi">Cek Mutasi</button>
        </form>
    </div>
    @endcan
    @if($topup_pending)
    <div class="flex flex-col md:flex-row gap-5">
        <div class="w-full md:w-1/2">
            <div id="alert-additional-content-4" class="shadow-md p-4 mb-4 border border-yellow-300 rounded-lg bg-yellow-50 dark:bg-yellow-200" role="alert">
                <div class="flex items-center">
                    <svg aria-hidden="true" class="w-5 h-5 mr-2 text-yellow-700 dark:text-yellow-800" fill="currentColor"
                        viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M11.088,2.542c0.063-0.146,0.103-0.306,0.103-0.476c0-0.657-0.534-1.19-1.19-1.19c-0.657,0-1.19,0.533-1.19,1.19c0,0.17,0.038,0.33,0.102,0.476c-4.085,0.535-7.243,4.021-7.243,8.252c0,4.601,3.73,8.332,8.332,8.332c4.601,0,8.331-3.73,8.331-8.332C18.331,6.562,15.173,3.076,11.088,2.542z M10,1.669c0.219,0,0.396,0.177,0.396,0.396S10.219,2.462,10,2.462c-0.22,0-0.397-0.177-0.397-0.396S9.78,1.669,10,1.669z M10,18.332c-4.163,0-7.538-3.375-7.538-7.539c0-4.163,3.375-7.538,7.538-7.538c4.162,0,7.538,3.375,7.538,7.538C17.538,14.957,14.162,18.332,10,18.332z M10.386,9.26c0.002-0.018,0.011-0.034,0.011-0.053V5.24c0-0.219-0.177-0.396-0.396-0.396c-0.22,0-0.397,0.177-0.397,0.396v3.967c0,0.019,0.008,0.035,0.011,0.053c-0.689,0.173-1.201,0.792-1.201,1.534c0,0.324,0.098,0.625,0.264,0.875c-0.079,0.014-0.155,0.043-0.216,0.104l-2.244,2.244c-0.155,0.154-0.155,0.406,0,0.561s0.406,0.154,0.561,0l2.244-2.242c0.061-0.062,0.091-0.139,0.104-0.217c0.251,0.166,0.551,0.264,0.875,0.264c0.876,0,1.587-0.711,1.587-1.587C11.587,10.052,11.075,9.433,10.386,9.26z M10,11.586c-0.438,0-0.793-0.354-0.793-0.792c0-0.438,0.355-0.792,0.793-0.792c0.438,0,0.793,0.355,0.793,0.792C10.793,11.232,10.438,11.586,10,11.586z" clip-rule="evenodd"></path>
                    </svg>
                    <h3 class="text-lg font-medium text-yellow-700 dark:text-yellow-800">Topup Pending</h3>
                </div>
                <div class="mt-2 mb-4 text-sm text-yellow-700 dark:text-yellow-800">
                    Topup yang masih pending harus diselesaikan terlebih dahulu.
                </div>
                <div class="border border-pink-300 mb-4 px-2 rounded-lg">
                    <div class="mt-2 mb-4 text-sm text-pink-700 dark:text-pink-800">
                        <ul>
                            <li>{{ $topup_pending->topupTransferType->type }} : {{ $topup_pending->bank->nama_bank }}</li>
                            <li>No. Tujuan / Rekening : <span id="noRekening" class="hover:cursor-pointer hover:text-blue-800">{{ $topup_pending->bank->no_rek }}</span></li>
                            <li>Atas Nama : {{ $topup_pending->bank->atas_nama }}</span></li>
                            {{-- @if($topup_pending->topup_transfer_type_id != 4)
                            <div class="mt-1 text-blue-500">
                                <span class="text-xs">Untuk transfer menggunakan scan barcode silahkan klik </span>
                                <a target="_blank" class="hover:text-blue-600 bg-blue-200 p-1 rounded-lg" href="{{ asset('storage/images/qrcode/qrcode_' . $topup_pending->bank->id . '.png') }}">Open QR Code</a>
                            </div>
                            @endif --}}
                        </ul>
                    </div>
                </div>
                <div class="border border-yellow-300 mb-4 px-2 rounded-lg">
                    <div class="mt-2 mb-4 text-sm text-yellow-700 dark:text-yellow-800">
                        <ul>
                            <li>Topup ID : {{ $topup_pending->id }}</li>
                            <li>Transfer Type : {{ $topup_pending->topupTransferType->type }}</li>
                            <li>Bank Tujuan : {{ $topup_pending->bank->nama_bank }}</li>
                            <hr class="my-3">
                            <li>Nominal : <span id="nominal" class="font-bold hover:cursor-pointer hover:text-blue-800" data-nominal="{{ $topup_pending->nominal }}" >{{ Helpers::format_rupiah($topup_pending->nominal) }}</span></li>
                            <li class="text-yellow-400">Bonus : {{ $topup_pending->bonus != 0 ? Helpers::format_rupiah($topup_pending->bonus) : '-' }}</li>
                            <hr class="my-3">
                            <li>Status : {{ $topup_pending->status }}</li>
                            <li>Request Date : <span class="italic">{{ $topup_pending->created_at }}</span></li>
                            <li>Expired Date : <span class="italic">{{ date("Y-m-d H:i:s",strtotime($topup_pending->created_at) + 86400) }}</span></li>
                        </ul>
                    </div>
                    <div class="flex mb-2">
                        <svg aria-hidden="true" class="w-5 h-5 mr-2 text-red-500 dark:text-red-500" fill="currentColor"
                            viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
                                clip-rule="evenodd"></path>
                        </svg>
                        <span class="sr-only">Info</span>
                        <small class="text-red-500 italic dark:text-red-500">Pastikan transfer sesuai dengan nominal yang tertera <span class="font-bold">hingga 3 digit akhir <span class="text-blue-500 italic dark:text-blue-500">agar topup dapat terkonfirmasi secara automatis dan mendapatkan bonus topup</span></span></small>
                    </div>
                </div>
                <div class="flex flex-col">
                    <div>
                        <button data-id="{{ $topup_pending->id }}" id="buttonCancel" type="button" class="text-white bg-yellow-700 hover:bg-yellow-800 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-yellow-800 dark:hover:bg-yellow-900">
                            <svg aria-hidden="true" class="-ml-0.5 mr-2 h-4 w-4" fill="currentColor" viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.185,1.417c-4.741,0-8.583,3.842-8.583,8.583c0,4.74,3.842,8.582,8.583,8.582S18.768,14.74,18.768,10C18.768,5.259,14.926,1.417,10.185,1.417 M10.185,17.68c-4.235,0-7.679-3.445-7.679-7.68c0-4.235,3.444-7.679,7.679-7.679S17.864,5.765,17.864,10C17.864,14.234,14.42,17.68,10.185,17.68 M10.824,10l2.842-2.844c0.178-0.176,0.178-0.46,0-0.637c-0.177-0.178-0.461-0.178-0.637,0l-2.844,2.841L7.341,6.52c-0.176-0.178-0.46-0.178-0.637,0c-0.178,0.176-0.178,0.461,0,0.637L9.546,10l-2.841,2.844c-0.178,0.176-0.178,0.461,0,0.637c0.178,0.178,0.459,0.178,0.637,0l2.844-2.841l2.844,2.841c0.178,0.178,0.459,0.178,0.637,0c0.178-0.176,0.178-0.461,0-0.637L10.824,10z"></path>
                            </svg>
                            Cancel Topup
                        </button>
                    </div>
                    @if($topup_pending->topup_transfer_type_id == 4)
                    <div class="mt-1">
                        <button id="payButton" type="button" class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Bayar Sekarang</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endif
    </div>

    <div>
        <small class="text-xs italic sm:hidden text-gray-500">Table dapat digeser <i class="fa fa-long-arrow-right" aria-hidden="true"></i></small>
    </div>
    <div class="overflow-x-auto relative shadow-md sm:rounded-lg p-5 bg-white">
        <h1 class="text-center md:text-start mb-6 text-2xl md:text-4xl font-bold">Riwayat Isi Saldo</h1>
        <table class="w-full text-sm text-center text-gray-500 dark:text-gray-400 mb-9" id="table">
            <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        Topup ID
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Nominal
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Bonus
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Bank Tujuan
                    </th>
                    @can('admin')
                    <th scope="col" class="py-3 px-6">
                        Username
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Note
                    </th>
                    @endcan
                    <th scope="col" class="py-3 px-6">
                        Request Date
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Status
                    </th>
                    @can('admin')
                    <th scope="col" class="py-3 px-6">
                        Action
                    </th>
                    @endcan
                </tr>
            </thead>
            <tbody>
                @foreach ($topups as $topup)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600">
                    <th scope="row" class=" mb-5 py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white orderId iconCopy w-11 hover:cursor-pointer">
                        {{ $topup->id }}
                    </th>
                    <td class="py-4 px-6">
                        {{ $topup->nominal }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $topup->bonus }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $topup->bank->nama_bank }}
                    </td>
                    @can('admin')
                    <th scope="row" class="py-4 px-6">
                        {{ $topup->user->username }}
                    </th>
                    <th scope="row" class="py-4 px-6">
                        {{ $topup->note }}
                    </th>
                    @endcan
                    <td class="py-4 px-6">
                        {{ $topup->created_at }}
                    </td>
                    <td class="py-4 px-6">
                        <span class="font-bold {{ $topup->status == 'pending' ? 'text-yellow-800' : ($topup->status == 'paid' ? 'text-green-600' : 'text-red-500') }}">{{ $topup->status }}</span>
                    </td>
                    @can('admin')
                    @if($topup->status == 'pending')
                    <td class="py-4 px-6">
                        <button data-id="{{ $topup->id }}" type="button" class="cancel bg-red-300 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-400 dark:text-red-900">cancel</button>
                        <button data-nominal="{{ $topup->nominal }}" data-id="{{ $topup->id }}" type="button" class="paid bg-green-300 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-400 dark:text-green-900">paid</button>
                    </td>
                    @else
                    <td class="py-4 px-6">
                        -
                    </td>
                    @endif
                    @endcan
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div id="form" hidden></div>

<script>
    // Datatables
    $(document).ready(function(){
        $('#table').DataTable({
            // responsive : true,
            "lengthChange": false,
            "ordering" : true,
            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: [ "_all" ],
                },
                {
                    target: [1, 2],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                },
                // {
                //     responsivePriority: 1,
                //     targets: 0,
                // },
                // {
                //     responsivePriority: 2,
                //     targets: 1
                // },
                // {
                //     responsivePriority: 4,
                //     targets: 5
                // },
            ],
            order: [[0, 'desc']],
        });
    });

    // Format rupiah
    function formatRupiah(angka, prefix){
        var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
    }

    // Button cancel top up di alert info
    $('#buttonCancel').click(function(){
        const topupId = $(this).data('id');
        Swal.fire({
            title: 'Cancel Topup?',
            text: `Topup ID ${topupId} akan dicancel`,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: "Tidak",
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/topup-cancel/' + topupId,
                    method : 'POST',
                    data : {
                        "_token" : "{{ csrf_token() }}"
                    },
                    success : function(res){
                        if(!res){
                            Swal.fire({title : 'Canceled!',
                                text : `Topup ID ${topupId} berhasil dicancel`,
                                icon : 'success',
                                timer : 3000
                            }).then(() => {
                                window.location.replace('/riwayat-isi-saldo');
                            });
                        }else{
                            Swal.fire(
                                'Terjadi Kesalahan!',
                                `${res}`,
                                'error'
                            )
                        }
                    },
                    error : function(err){
                        const error = err.responseJSON;
                        Swal.fire(
                            'Gagal!',
                            `${error.message}`,
                            'warning'
                        ).then(() => {
                            window.location = '/riwayat-isi-saldo'
                        });
                    }
                });

            }
        });
    });

    // Action cancel & paid
    $('.cancel').click(function(){
        const topupId = $(this).data('id');

        Swal.fire({
            title: 'Cancel Topup?',
            text: `Topup ID ${topupId} akan dicancel`,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: "Tidak",
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/topup-cancel/' + topupId,
                    method : 'POST',
                    data : {
                        "_token" : "{{ csrf_token() }}"
                    },
                    success : function(res){
                        if(!res){
                            Swal.fire({title : 'Canceled!',
                                text : `Topup ID ${topupId} berhasil dicancel`,
                                icon : 'success',
                                timer : 3000
                            }).then(() => {
                                window.location.replace('/riwayat-isi-saldo');
                            });
                        }else{
                            Swal.fire(
                                'Terjadi Kesalahan!',
                                `${res}`,
                                'error'
                            )
                        }
                    }
                });

            }
        });
    });
    $('.paid').click(function(){
        const topupId = $(this).data('id');
        const nominal = $(this).data('nominal');

        Swal.fire({
            title: 'Paid Topup?',
            text: `Topup ID ${topupId} akan paid, saldo user akan bertambah sebesar ${formatRupiah(Math.ceil(nominal), 'Rp ')}`,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: "Tidak",
            confirmButtonColor: '#008000',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Paid!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/topup-paid/' + topupId,
                    method : 'POST',
                    data : {
                        "_token" : "{{ csrf_token() }}"
                    },
                    success : function(res){
                        Swal.fire(
                            'Paid!',
                            `Topup ID ${topupId} telah paid`,
                            'success'
                        ).then(() => {
                            window.location.replace('/riwayat-isi-saldo');
                        });
                    },
                    error : function(err){
                        // console.log(err);
                        Swal.fire(
                                'Terjadi Kesalahan!',
                                `${err.responseText}`,
                                'error'
                            )
                    }
                });

            }
        });
    });
</script>

<script>
    $('.iconCopy').click(function(){
            $(this).removeClass('text-blue-600');
            $(this).addClass('text-yellow-800');

            const orderId = $(this).closest('.orderId').text().trim();

            navigator.clipboard.writeText(orderId);
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-start',
                showConfirmButton: false,
                timer: 1000,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: 'Topup ID Copied'
            })
        });

        $('#noRekening').click(function(){
            $('.copiedNorek').empty();
            const noRek = $(this).text();
            navigator.clipboard.writeText(noRek);
            $(this).after(`<span class="copiedNorek text-xs text-yellow-800 italic"> Copied!</span>`);
            $(this).addClass(`text-blue-800`);
        });

        $('#nominal').click(function(){
            $('.copiedNominal').empty();
            const nominal = Math.trunc($(this).attr('data-nominal'));
            navigator.clipboard.writeText(nominal);
            $(this).after(`<span class="copiedNominal text-xs text-yellow-800 italic"> Copied!</span>`);
            $(this).addClass(`text-blue-800`);
        });
</script>

<script>
    $('#payButton').click(function(){
        if('{{ $topup_pending && $topup_pending->topup_transfer_type_id == 4 }}'){
            window.snap.pay('{{ $topup_pending->snap_token ?? 123 }}', {
            gopayMode: '{{ $topup_pending->show_qr ?? 123 }}',
            onSuccess: function(result){
                window.location = '/riwayat-isi-saldo';
                /* You may add your own implementation here */
                // alert("payment success!"); console.log(result);
                // $('#form').append(`
                //     <form action="/callback-midtrans" method="POST" id="submit_form">
                //         @csrf
                //         <input type="hidden" name="json_data" id="json_data">
                //     </form>
                // `).ready(function(){
                //     $('#json_data').val(JSON.stringify(result));
                //     $('#submit_form').submit();
                // })
            },
            onPending: function(result){
                /* You may add your own implementation here */
                // console.log(result);
                // $.ajax({
                //     url : '/midtrans-notification',
                //     method : 'POST',
                //     data : {
                //         '_token' : '{{ csrf_token() }}',
                //         status_code : result.status_code,
                //         status_message : result.status_message,
                //         transaction_id : result.transaction_id,
                //         order_id : result.order_id,
                //         gross_amount : result.gross_amount,
                //         payment_type : result.payment_type,
                //         transaction_time : result.transaction_time,
                //         transaction_status : result.transaction_status,
                //         va_bank : result.va_numbers ? result.va_numbers[0]['bank'] : '',
                //         va_number : result.va_numbers ? result.va_numbers[0]['va_number'] : '',
                //     },
                //     success : function(res){
                //         console.log(res);
                //         if(!res){
                //             console.log('berhasil submit data ke midtrans notif table');
                //         }
                //     },
                //     error : function(err){
                //         console.log(err);
                //     }
                // });
            },
            onError: function(result){
                /* You may add your own implementation here */
                // alert("payment failed!"); console.log(result);
                // $('#form').append(`
                //     <form action="/callback-midtrans" method="POST" id="submit_form">
                //         @csrf
                //         <input type="hidden" name="json_data" id="json_data">
                //     </form>
                // `).ready(function(){
                //     $('#json_data').val(JSON.stringify(result));
                //     $('#submit_form').submit();
                // })
            },
            onClose: function(){
                window.location = '/riwayat-isi-saldo';
                /* You may add your own implementation here */
                // alert('you closed the popup without finishing the payment');
            }
            });
        }
    });
</script>
@endsection
