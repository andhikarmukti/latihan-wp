@extends('layouts.main')

@section('content')
<div class="p-9">
    {{-- <div class="flex flex-col gap-1 items-center mb-10">
        <div>
            <a href="https://www.youtube.com/watch?v=vNTZ_OanZQQ" class="text-blue-500 italic text-xs hover:text-blue-700 ring-1 ring-orange-300 rounded-lg p-1 hover:bg-blue-200" target="_blank">
                Video Tutorial Cara Topup Transfer Bank
            </a>
       </div>
       <div>
           <a href="https://www.youtube.com/watch?v=LXLFJf-WnLQ" class="text-blue-500 italic text-xs hover:text-blue-700 ring-1 ring-orange-300 rounded-lg p-1 hover:bg-blue-200" target="_blank">
               Video Tutorial Cara Topup Transfer Midtrans
           </a>
       </div>
    </div> --}}
    <h1 class="text-center mb-5 text-2xl md:text-4xl font-bold">Topup Saldo</h1>
    <div class="flex justify-center">
        <form class="w-full md:w-1/3 border border-slate-200 p-5 shadow-md rounded-lg bg-white">
            <div class="mb-6" id="divTransferType">
                <label for="topup_transfer_type_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Transfer Type</label>
                <select id="topup_transfer_type_id" name="topup_transfer_type_id" class="@if($errors->has('topup_transfer_type_id')) ring-1 ring-red-500 @endif bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Transfer Type</option>
                    @foreach ($topup_transfer_types as $topup_transfer_type)
                        <option value="{{ $topup_transfer_type->id }}" {{ old('topup_transfer_type_id') ? 'selected' : '' }}>{{ $topup_transfer_type->type }}</option>
                    @endforeach
                </select>
                @if ($errors->has('topup_transfer_type_id'))
                    <small class="text-sm text-red-500 italic">{{ $errors->first('topup_transfer_type_id') }}</small>
                @endif
            </div>
            <div class="mb-6" hidden id="optionBank">
                <label for="bank_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Rekening Tujuan</label>
                <select id="bank_id" name="bank_id" class="@if($errors->has('bank_id')) ring-1 ring-red-500 @endif bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                </select>
                @if ($errors->has('bank_id'))
                    <small class="text-sm text-red-500 italic">{{ $errors->first('bank_id') }}</small>
                @endif
            </div>
            <div class="mb-6" hidden id="inputNominal">
                <label for="nominal" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nominal</label>
                <input type="number" id="nominal" name="nominal" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off" value="{{ old('nominal', '') }}">
            </div>
            <div id="buttonSubmit" hidden>
                <button type="button" class="w-1/3 ml-auto flex text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });
    });
</script>

<script>
    const optionBank = $('#bank_id');
    const inputNominal = $('#nominal');
    const buttonSubmit = $('#buttonSubmit');
    const transferType = $('#topup_transfer_type_id');

    $('#topup_transfer_type_id').change(function(){
        const topup_transfer_type_id = $(this).find(':selected').val();

        // info topup auto dan manual
        $('#infoAutoManual').remove();
        $('#divTransferType').after(`
            <div id="infoAutoManual" class="flex p-4 mb-4 bg-yellow-50 border-t-4 border-yellow-200 dark:bg-yellow-200" role="alert">
                <svg class="flex-shrink-0 w-5 h-5 text-yellow-700" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                <div class="ml-3 text-xs italic font-medium text-yellow-700">
                    ${topup_transfer_type_id == 4 ? '<p id="infoTopup">Pembayaran melalui <span class="font-bold italic text-purple-800">Bank Virtual Account</span> akan dikenakan fee Admin Midtrans sebesar Rp 4.440 <br> <br></p>' : ''}
                    Setelah melakukan pembayaran, harap tunggu 10 - 15 menit. <br> Saldo akan masuk secara automatis
                </div>
            </div>
        `);

        $.ajax({
            url : '/tambah-saldo',
            method : 'GET',
            data : {
                topup_transfer_type_id : topup_transfer_type_id
            },
            success : function(res){
                $('#optionBank').removeAttr('hidden');
                $('#bank_id').empty();
                $('#bank_id').append(`<option selected disabled>Pilih Bank</option>`);
                $.each(res.banks, function(index, value){
                    $('#bank_id').append(`
                        <option value="${value.id}" {{ old('bank_id') ? 'selected' : '' }}>${value.nama_bank} ${value.id == 1 ? `[BONUS TOPUP ${res.bonus_topup}%]` : ''}</option>
                    `)
                })
            }
        });
    });

    optionBank.change(function(){
        $('#inputNominal').removeAttr('hidden');
        if($(this).val() == 7 || $(this).val() == 13 ){
            $('#infoTopup').attr('hidden', true);
        }else{
            $('#infoTopup').attr('hidden', false);
        }
    });

    inputNominal.keyup(function(){
        $('#buttonSubmit').removeAttr('hidden');
    });

    buttonSubmit.click(function(e){
        e.preventDefault();
        // clear input error
        inputNominal.removeClass('ring-1 ring-red-500');
        $('.errorMessage').remove();

        // disable button submit menghindari double click
        $('#buttonSubmit').attr('disabled', true);
        $('#buttonSubmit').html(`<span class="text-purplpe-500 italic">Processing...</span>`);

        const data = $('form').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        $.ajax({
            url : '/tambah-saldo',
            method : 'POST',
            data : {
                nominal : data.nominal,
                bank_id : data.bank_id,
                topup_transfer_type_id : data.topup_transfer_type_id,
                '_token' : "{{ csrf_token() }}"
            },
            success : function(res){
                // console.log(res);
                // disable input ketika berhasil
                optionBank.attr('disabled', 'disabled');
                inputNominal.attr('disabled', 'disabled');
                transferType.attr('disabled', 'disabled');

                if(res.topup_pending == true){
                    Swal.fire({
                    icon : 'warning',
                    title : 'Kamu memiliki request topup yang belum diselesaikan',
                    text : 'Silahkan selesaikan pembayarannya terlebih dahulu',
                    showConfirmButton : false,
                    timer : 5000,
                    timerProgressBar : true,
                }).then(() => {
                    window.location.replace('/riwayat-isi-saldo');
                });
                }else{
                    Swal.fire({
                        // toast : true,
                        // position: 'top-end',
                        icon : 'success',
                        text : "Berhasil melakukan topup request!",
                        showConfirmButton : false,
                        timer : 2000,
                        timerProgressBar : true
                    }).then(() => {
                        window.location.replace('/riwayat-isi-saldo');
                    });
                }
            },
            error : function(err){
                const error = err.responseJSON;

                $('#buttonSubmit').html(`<button type="button" class="w-1/3 ml-auto flex text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>`)

                if(error.nominal){
                    inputNominal.addClass('ring-1 ring-red-500');
                    inputNominal.after(`
                        <small class="text-sm text-red-500 italic errorMessage">${error.nominal[0]}</small>
                    `);
                }
            }
        })
    });
</script>
@endsection
