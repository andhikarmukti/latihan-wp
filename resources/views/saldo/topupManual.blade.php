@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="flex flex-col md:flex-row gap-5 md:px-44">
            <div class="border border-blue-300 rounded-lg bg-blue-200 p-5 shadow-md w-full md:w-1/2">
                <p class="text-center text-2xl mb-5 text-md italic">Topup Manual</p>
                <div class="flex justify-center">
                    <form class="w-full md:w-full border border-slate-200 p-5 shadow-md rounded-lg bg-white" id="formTopupManual">
                        <div class="mb-6" id="inputTopupManual">
                            <div class="gap-2 items-center mb-2">
                                <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Username</label>
                                <div class="col-span-3 md:col-span-4">
                                    <input type="text" id="username" name="username" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off" value="{{ old('username', '') }}">
                                </div>
                            </div>
                            <div class="gap-2 items-center mb-2">
                                <label for="nominalTopupManual" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nominal</label>
                                <div class="col-span-3 md:col-span-4">
                                    <input type="number" id="nominalTopupManual" name="nominalTopupManual"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        autocomplete="off" value="{{ old('nominalTopupManual', '') }}">
                                </div>
                            </div>
                            <div class="gap-2 items-center mb-2">
                                <label for="note" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Note</label>
                                <div class="col-span-3 md:col-span-4">
                                    <input type="text" id="note" name="note"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        autocomplete="off" value="{{ old('note', '') }}">
                                </div>
                            </div>
                            <div>
                                <button id="buttonSubmitManualTopup" type="button"
                                    class="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-md shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-3 py-2.5 text-center flex ml-auto">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="border border-red-300 rounded-lg bg-red-200 p-5 shadow-md w-full md:w-1/2">
                <p class="text-center text-xl md:text-2xl mb-5 text-md italic">Transfer Tidak Sesuai 3 Digit</p>
                <div class="flex justify-center">
                    <form class="w-full md:w-full border border-slate-200 p-5 shadow-md rounded-lg bg-white">
                        <div class="mb-6" id="inputtopupId">
                            <label for="topupid" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Topup ID</label>
                            <div class="grid grid-cols-3 md:grid-cols-5 gap-2 items-center">
                                <div class="col-span-2 md:col-span-4">
                                    <input type="text" id="topupid" name="topupid"
                                        class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        autocomplete="off" value="{{ old('nominal', '') }}">
                                </div>
                                <div class="col-span-1">
                                    <button id="buttonCheck" type="button"
                                        class="w-full text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-md shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-3 py-2.5 text-center">Check</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="p-9">
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg border border-slate-300 p-5">
            <p class="text-center text-xl md:text-2xl mb-5 text-md italic">Topup Logs</p>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableTopupLogs">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Topup Id
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo Awal
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo Ditambahkan
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Total Saldo
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Bank
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Transfer Type
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Note
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Created By
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($topup_logs as $topup_log)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $topup_log->topup_id }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $topup_log->user->username }}
                        </td>
                        <td class="py-4 px-6">
                            {{ Helpers::format_rupiah($topup_log->saldo_awal) }}
                        </td>
                        <td class="py-4 px-6">
                            {{ Helpers::format_rupiah($topup_log->saldo_ditambahkan) }}
                        </td>
                        </th>
                        <td class="py-4 px-6">
                            {{ Helpers::format_rupiah($topup_log->total_saldo) }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $topup_log->bank->nama_bank }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $topup_log->topupTransferType->type }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $topup_log->note }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $topup_log->user->username }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <script>
        // Datatables
        $(document).ready(function(){
            $('#tableTopupLogs').DataTable({
                responsive : true,
                "lengthChange": false,
                "ordering" : false,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ]
                    }
                ]
            });
        });

        $('#buttonCheck').click(function() {
            const topupId = $('#topupid').val();
            if (topupId == '') {
                $('#infoTopup').empty();
                $('#topupIdKosong').empty();
                $('#inputtopupId').after(`
            <div id="topupIdKosong" class="text-center">
                <i class="text-red-500">Topup ID tidak boleh kosong!</i>
            </div>
            `)
            } else {
                $.ajax({
                    url: '/topup-saldo-manual',
                    data: {
                        topup_id: topupId
                    },
                    success: function(res) {
                        // console.log(res);
                        if (res.paid == true) {
                            $('#infoTopup').empty();
                            $('#topupIdKosong').empty();
                            $('#inputtopupId').after(`
                            <div id="topupIdKosong" class="text-center">
                                <i class="text-green-500">${res.message}</i>
                            </div>
                        `)
                        }else {
                            $('#infoTopup').empty();
                            $('#inputtopupId').after(`
                            <div id="loading" role="status" class="max-w-sm animate-pulse">
                                <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                                <span class="sr-only">Loading...</span>
                            </div>
                        `);
                            setTimeout(() => {
                                $('#topupIdKosong').empty();
                                $('#loading').empty();
                                $('#inputtopupId').after(`
                                <div id="infoTopup">
                                    <div class="border border-yellow-300 mb-4 px-2 rounded-lg">
                                        <div class="mt-2 mb-4 text-sm text-yellow-700 dark:text-yellow-800">
                                            <ul>
                                                <li id="topupId">Topup ID : ${res.id}</li>
                                                <li id="username">Username : ${res.username}</li>
                                                <li id="transferType">Transfer Type : ${res.transfer_type}</li>
                                                <li id="nominal">Nominal : ${res.nominal}</li>
                                                <li id="bank">Bank : ${res.nama_bank}</li>
                                                <li id="status">Status : ${res.status}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr class="my-3">
                                    <label for="nominalTransfer" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Nominal Yang Ditransfer</label>
                                    <div id="nominalInputDiv" class="grid grid-cols-3 md:grid-cols-5 gap-2 items-center">
                                        <div class="col-span-2 md:col-span-4">
                                            <input type="number" id="nominalTransfer" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off" value="{{ old('nominal_transfer', '') }}">
                                        </div>
                                        <div class="col-span-1">
                                            <button id="buttonSubmit" type="button" class="text-white bg-gradient-to-r from-yellow-500 via-yellow-600 to-yellow-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-yellow-300 dark:focus:ring-yellow-800 shadow-md shadow-yellow-500/50 dark:shadow-lg dark:shadow-yellow-800/80 font-medium rounded-lg text-sm px-3 py-2.5 w-full text-center">Submit</button>
                                        </div>
                                    </div>
                                </div>

                            `).ready(function() {
                                    $('#buttonSubmit').click(function() {
                                        // hapus error
                                        $('#NominalTransfer').empty();
                                        $('#errorNominalTransfer').empty();
                                        $('#nominalTransfer').removeClass('ring-1 ring-red-500');

                                        const nominalTransfer = $(
                                            '#nominalTransfer').val();
                                        $.ajax({
                                            url: '/topup-saldo-tidak-sesuai-3digit',
                                            method: 'POST',
                                            data: {
                                                "_token": "{{ csrf_token() }}",
                                                topup_id: res.id,
                                                bank_id: res.bank_id,
                                                topup_transfer_type_id: res
                                                    .topup_transfer_type_id,
                                                nominal: res.nominal,
                                                user_id: res.user_id,
                                                nominal_transfer: nominalTransfer
                                            },
                                            success: function(res) {
                                                if (!res) {
                                                    Swal.fire({
                                                        icon: 'success',
                                                        text: 'Berhasil menambahkan saldo',
                                                        timer: 3000
                                                    }).then( () => {
                                                        window.location.replace('/topup-saldo-manual');
                                                    });
                                                }
                                            },
                                            error : function(err){
                                                $('#nominalTransfer').addClass('ring-1 ring-red-500');
                                                $('#nominalInputDiv').after(`
                                                    <small id="errorNominalTransfer" class="text-sm italic text-red-500">${err.responseJSON.nominal_transfer}</small>
                                                `);
                                                // console.log(err.responseJSON.nominal_transfer);
                                            }
                                        });
                                    })
                                })
                            }, 100);
                        }
                    },
                    error : function(err){
                        const error = err.responseJSON;
                        $('#infoTopup').empty();
                        $('#topupIdKosong').empty();
                        $('#inputtopupId').after(`
                            <div id="topupIdKosong" class="text-center">
                                <i class="text-red-500">${error.message}</i>
                            </div>
                        `)
                    }
                })
            }
        })
    </script>

    <script>
        $('#buttonSubmitManualTopup').click(function(){
            // Hilangkan error yang muncul sebelumnya
            $('#usernameError').empty();
            $('#nominalTopupManualError').empty();
            $('#noteError').empty();
            $('#username').removeClass('ring-1 ring-red-500')
            $('#nominalTopupManual').removeClass('ring-1 ring-red-500')
            $('#note').removeClass('ring-1 ring-red-500')

            const data = $('#formTopupManual').serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

            $.ajax({
                url : '/topup-saldo-manual',
                method : 'POST',
                data : {
                    username : data.username,
                    nominal : data.nominalTopupManual,
                    note : data.note,
                    '_token' : '{{ csrf_token() }}'
                },
                success : function(res){
                    if (!res) {
                        Swal.fire({
                            icon: 'success',
                            text: 'Berhasil menambahkan saldo',
                            timer: 3000
                        }).then( () => {
                            window.location.replace('/topup-saldo-manual');
                        });
                    }
                },
                error : function(err){
                    // console.log(err);
                    const error = err.responseJSON;

                    if(error.username){
                        $('#username').after(`
                            <div id="usernameError">
                                <i class="text-sm text-red-500">${error.username}</i>
                            </div>
                        `);
                        $('#username').addClass('ring-1 ring-red-500');
                    }

                    if(error.nominal){
                        $('#nominalTopupManual').after(`
                            <div id="nominalTopupManualError">
                                <i class="text-sm text-red-500">${error.nominal}</i>
                            </div>
                        `);
                        $('#nominalTopupManual').addClass('ring-1 ring-red-500');
                    }

                    if(error.note){
                        $('#note').after(`
                            <div id="noteError">
                                <i class="text-sm text-red-500">${error.note}</i>
                            </div>
                        `);
                        $('#note').addClass('ring-1 ring-red-500');
                    }

                    if(error.message){
                        // console.log(error.message);
                        Swal.fire({
                            icon: 'error',
                            text: error.message ?? `Server Error! Hubungi Admin`
                        });
                    }
                }
            });
        });

    </script>
@endsection
