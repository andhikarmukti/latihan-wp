@extends('layouts.main')

@section('content')
<div class="p-9">
    <div class="flex justify-center">
        <div class="w-full md:w-1/3 p-5 border border-slate-400 shadow-lg rounded-lg">
            <form action="/cek-layanan-server" method="GET">
                <div class="mb-6">
                    <label for="server_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Server</label>
                    <select id="server_id" name="server_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected disabled>Pilih Server</option>
                        @foreach ($servers as $server)
                        <option value="{{ $server->id }}" {{ $server->id == request('server_id') ? 'selected' : '' }}>{{ $server->nama_server }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
            </form>
        </div>
    </div>

    <form action="/server-store-service/{{ request('server_id') }}" method="POST" id="formSubmitFromServer">
        @csrf
        <div class="shadow-md sm:rounded-lg mt-9 p-5">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableCekLayanan">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400 sticky top-28">
                    <tr>
                        @foreach ($layanan_servers ? $layanan_servers[0] : $layanan_servers as $index => $value)
                        <th scope="col" class="py-3 px-6">
                            {{ $index }}
                        </th>
                        @endforeach
                        {{-- <th scope="col" class="py-3 px-6">
                            Action
                        </th> --}}
                    </tr>
                </thead>
                <tbody>
                    @for ($i = 0; $i < $layanan_servers_length; $i++)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        @foreach ($layanan_servers[$i] as $index => $value)
                            <td class="py-4 px-6" name="{{ $index }}">
                                {{ $value }}
                            </td>
                        @endforeach
                        {{-- <td class="py-4 px-6">
                            <button type="button" class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900 buttonSubmitFromServer">Add</button>
                        </td> --}}
                    </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </form>

</div>

<script>
    // Datatables
    $(document).ready(function(){
        $('#tableCekLayanan').DataTable({
            responsive : true,
            "lengthChange": false,
            "ordering" : true,
            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: [ "_all" ]
                }
            ]
        });
    });

    $(document).delegate('.buttonSubmitFromServer', 'click', function(e) {
        // console.log('ok');
        e.preventDefault();
    })
</script>
@endsection
