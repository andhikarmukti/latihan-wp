@extends('layouts.main')

@section('content')
    <div class="p-9 flex justify-center">
        <div class="w-1/3 border border-slate-300 rounded-lg p-5 shadow-md">
            <div class="mb-9">
                <a href="/server" class="bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800" aria-hidden="true"></i> Kembali</a>
            </div>
            <form action="/server/update/{{ $server->id }}" method="POST">
                @csrf
                <div class="relative z-0 mb-6 w-full group">
                    <input value="{{ old('nama_server', $server->nama_server) }}" type="text" name="nama_server" id="floating_nama_server" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" autocomplete="off">
                    <label for="floating_nama_server" class="@if($errors->has('nama_server')) text-red-500 @endif peer-focus:font-medium absolute text-sm  dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nama Server</label>
                    @if($errors->has('nama_server'))
                    <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                    @endif
                </div>
                <div class="relative z-0 mb-6 w-full group">
                    <input value="{{ old('url', $server->url) }}" type="text" name="url" id="floating_url" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                    <label for="floating_url" class="@if($errors->has('url')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Url</label>
                    @if($errors->has('url'))
                    <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                    @endif
                </div>
                <div class="relative z-0 mb-6 w-full group">
                    <input value="{{ old('key', $server->key) }}" type="text" name="key" id="floating_key" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                    <label for="floating_key" class="@if($errors->has('key')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Key</label>
                    @if($errors->has('key'))
                    <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                    @endif
                </div>
                <div class="relative z-0 mb-6 w-full group">
                    <input value="{{ old('mark_up', $server->mark_up) }}" type="text" name="mark_up" id="floating_mark_up" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                    <label for="floating_mark_up" class="@if($errors->has('mark_up')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Mark Up</label>
                    @if($errors->has('mark_up'))
                    <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                    @endif
                </div>
                <div class="relative z-0 mb-6 w-full group">
                    <input value="{{ old('contact_person', $server->contact_person) }}" type="text" name="contact_person" id="floating_contact_person" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                    <label for="floating_contact_person" class="@if($errors->has('contact_person')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Contact Person</label>
                </div>
                <button id="buttonSubmitServer" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
            </form>
        </div>
    </div>
@endsection
