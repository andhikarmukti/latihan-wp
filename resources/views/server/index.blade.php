@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="grid grid-cols-1 md:grid-cols-3 gap-2">
            <div class="border border-slate-300 p-5 rounded-lg shadow-md w-full">
                <h4 class="text-start text-2xl font-bold italic mb-9">Tambah Server</h3>
                <form action="/server" method="POST">
                    @csrf
                    <div class="relative z-0 mb-6 w-full group">
                        <input value="{{ old('nama_server', '') }}" type="text" name="nama_server" id="floating_nama_server" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" autocomplete="off">
                        <label for="floating_nama_server" class="@if($errors->has('nama_server')) text-red-500 @endif peer-focus:font-medium absolute text-sm  dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nama Server</label>
                        @if($errors->has('nama_server'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                        @endif
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <input value="{{ old('url', '') }}" type="text" name="url" id="floating_url" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                        <label for="floating_url" class="@if($errors->has('url')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Url</label>
                        @if($errors->has('url'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                        @endif
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <input value="{{ old('key', '') }}" type="text" name="key" id="floating_key" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                        <label for="floating_key" class="@if($errors->has('key')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Key</label>
                        @if($errors->has('key'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                        @endif
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <input value="{{ old('mark_up', '') }}" type="text" name="mark_up" id="floating_mark_up" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                        <label for="floating_mark_up" class="@if($errors->has('mark_up')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Mark Up</label>
                        @if($errors->has('mark_up'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('nama_server') }}</small>
                        @endif
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <input value="{{ old('contact_person', '') }}" type="text" name="contact_person" id="floating_contact_person" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                        <label for="floating_contact_person" class="@if($errors->has('contact_person')) text-red-500 @endif peer-focus:font-medium absolute text-sm dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Contact Person</label>
                    </div>
                    <button id="buttonSubmitServer" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
            </div>
            <div class="border border-slate-300 p-5 rounded-lg shadow-md w-full">
                <h4 class="text-start text-2xl font-bold italic mb-9">Tambah Server Action (Url Path & Method)</h3>
                <form action="/server-action-path" method="POST">
                    @csrf
                    <div class="relative z-0 mb-6 w-full group">
                        <label for="server_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Server Name</label>
                        <select id="server_id" name="server_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <option selected disabled>Pilih Server</option>
                            @foreach ($servers as $server)
                            <option value="{{ $server->id }}">{{ $server->nama_server }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('server_id'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('server_id') }}</small>
                        @endif
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <div>
                                <label for="order_url_path" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Url Path <small class="text-xm italic text-blue-500">Order</small></label>
                                <input value="{{ old('order_url_path', '') }}" type="text" id="order_url_path" name="order_url_path" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div>
                                <label for="order_method" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Method <small class="text-xm italic text-blue-500">Order</small></label>
                                <input value="{{ old('order_method', 'POST') }}" type="text" id="order_method" name="order_method" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <div>
                                <label for="status_url_path" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Url Path <small class="text-xm italic text-blue-500">Status</small></label>
                                <input value="{{ old('status_url_path', '') }}" type="text" id="status_url_path" name="status_url_path" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div>
                                <label for="status_method" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Method <small class="text-xm italic text-blue-500">Status</small></label>
                                <input value="{{ old('status_method', 'POST') }}" type="text" id="status_method" name="status_method" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <div>
                                <label for="services_url_path" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Url Path <small class="text-xm italic text-blue-500">Services</small></label>
                                <input value="{{ old('services_url_path', '') }}" type="text" id="services_url_path" name="services_url_path" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div>
                                <label for="services_method" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Method <small class="text-xm italic text-blue-500">Services</small></label>
                                <input value="{{ old('services_method', 'POST') }}" type="text" id="services_method" name="services_method" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <div>
                                <label for="balances_url_path" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Url Path <small class="text-xm italic text-blue-500">Balances</small></label>
                                <input value="{{ old('balances_url_path', '') }}" type="text" id="balances_url_path" name="balances_url_path" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div>
                                <label for="balances_method" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Method <small class="text-xm italic text-blue-500">Balances</small></label>
                                <input value="{{ old('balances_method', 'POST') }}" type="text" id="balances_method" name="balances_method" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <div>
                                <label for="refill_url_path" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Url Path <small class="text-xm italic text-blue-500">Refill</small></label>
                                <input value="{{ old('refill_url_path', '') }}" type="text" id="refill_url_path" name="refill_url_path" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div>
                                <label for="refill_method" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Method <small class="text-xm italic text-blue-500">Refill</small></label>
                                <input value="{{ old('refill_method', 'POST') }}" type="text" id="refill_method" name="refill_method" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <div>
                                <label for="refill_status_url_path" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Url Path <small class="text-xm italic text-blue-500">Refill Status</small></label>
                                <input value="{{ old('refill_status_url_path', '') }}" type="text" id="refill_status_url_path" name="refill_status_url_path" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div>
                                <label for="refill_status_method" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Method <small class="text-xm italic text-blue-500">Refill Status</small></label>
                                <input value="{{ old('refill_status_method', 'POST') }}" type="text" id="refill_status_method" name="refill_status_method" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <button id="buttonSubmitServer" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
            </div>
            <div class="border border-slate-300 p-5 rounded-lg shadow-md w-full">
                <h4 class="text-start text-2xl font-bold italic mb-9">Tambah Server Parameter</h3>
                <form action="/server-parameters" method="POST">
                    @csrf
                    <div class="relative z-0 mb-6 w-full group">
                        <label for="server_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Server Name</label>
                        <select id="server_id" name="server_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <option selected disabled>Pilih Server</option>
                            @foreach ($servers as $server)
                            <option value="{{ $server->id }}">{{ $server->nama_server }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('server_id'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('server_id') }}</small>
                        @endif
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <input type="hidden" name="action_id" value="1">
                            <div class="col-span-1">
                                <label for="parameter_order[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Key <small class="text-xm italic text-blue-500">Order</small></label>
                                <input value="{{ old('parameter_order[1]', '') }}" type="text" id="parameter_order[1]" name="parameter_order[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div class="col-span-1">
                                <label for="value_order[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value <small class="text-xm italic text-blue-500">Order</small></label>
                                <input value="{{ old('value_order[1]', '') }}" type="text" id="value_order[1]" name="value_order[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <button type="button" class="col-span-2 p-1 bg-yellow-200 text-xs rounded-lg kesel" id="buttonTambahOrder">tambah</button>
                        </div>
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <input type="hidden" name="action_id" value="1">
                            <div class="col-span-1">
                                <label for="parameter_status[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Key <small class="text-xm italic text-blue-500">Status</small></label>
                                <input value="{{ old('parameter_status[1]', '') }}" type="text" id="parameter_status[1]" name="parameter_status[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div class="col-span-1">
                                <label for="value_status[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value <small class="text-xm italic text-blue-500">Status</small></label>
                                <input value="{{ old('value_status[1]', '') }}" type="text" id="value_status[1]" name="value_status[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <button type="button" class="col-span-2 p-1 bg-yellow-200 text-xs rounded-lg kesel" id="buttonTambahStatus">tambah</button>
                        </div>
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <input type="hidden" name="action_id" value="1">
                            <div class="col-span-1">
                                <label for="parameter_services[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Key <small class="text-xm italic text-blue-500">Services</small></label>
                                <input value="{{ old('parameter_services[1]', '') }}" type="text" id="parameter_services[1]" name="parameter_services[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div class="col-span-1">
                                <label for="value_services[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value <small class="text-xm italic text-blue-500">Services</small></label>
                                <input value="{{ old('value_services[1]', '') }}" type="text" id="value_services[1]" name="value_services[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <button type="button" class="col-span-2 p-1 bg-yellow-200 text-xs rounded-lg kesel" id="buttonTambahServices">tambah</button>
                        </div>
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <input type="hidden" name="action_id" value="1">
                            <div class="col-span-1">
                                <label for="parameter_balances[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Key <small class="text-xm italic text-blue-500">Balances</small></label>
                                <input value="{{ old('parameter_balances[1]', '') }}" type="text" id="parameter_balances[1]" name="parameter_balances[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div class="col-span-1">
                                <label for="value_balances[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value <small class="text-xm italic text-blue-500">Balances</small></label>
                                <input value="{{ old('value_balances[1]', '') }}" type="text" id="value_balances[1]" name="value_balances[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <button type="button" class="col-span-2 p-1 bg-yellow-200 text-xs rounded-lg kesel" id="buttonTambahBalances">tambah</button>
                        </div>
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <input type="hidden" name="action_id" value="1">
                            <div class="col-span-1">
                                <label for="parameter_refill[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Key <small class="text-xm italic text-blue-500">Refill</small></label>
                                <input value="{{ old('parameter_refill[1]', '') }}" type="text" id="parameter_refill[1]" name="parameter_refill[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div class="col-span-1">
                                <label for="value_refill[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value <small class="text-xm italic text-blue-500">Refill</small></label>
                                <input value="{{ old('value_refill[1]', '') }}" type="text" id="value_refill[1]" name="value_refill[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <button type="button" class="col-span-2 p-1 bg-yellow-200 text-xs rounded-lg kesel" id="buttonTambahRefill">tambah</button>
                        </div>
                        <div class="mb-6 grid grid-cols-2 gap-1">
                            <input type="hidden" name="action_id" value="1">
                            <div class="col-span-1">
                                <label for="parameter_refill_status[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Key <small class="text-xm italic text-blue-500">Refill Status</small></label>
                                <input value="{{ old('parameter_refill_Refill Status[1]', '') }}" type="text" id="parameter_refill_status[1]" name="parameter_refill_status[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <div class="col-span-1">
                                <label for="value_refill_status[1]" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Value <small class="text-xm italic text-blue-500">Refill Status</small></label>
                                <input value="{{ old('value_refill_status[1]', '') }}" type="text" id="value_refill_status[1]" name="value_refill_status[1]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                            </div>
                            <button type="button" class="col-span-2 p-1 bg-yellow-200 text-xs rounded-lg kesel" id="buttonTambahRefillStatus">tambah</button>
                        </div>
                    </div>
                    <button id="buttonSubmitServer" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
            </div>
        </div>

        <div class="overflow-x-auto relative shadow-md sm:rounded-lg p-5 my-9">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableServer">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Server ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Server Name
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Url
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Key
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Mark Up
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Contact Person
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($servers as $server)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $server->id }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $server->nama_server }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $server->url }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $server->key }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $server->mark_up }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $server->contact_person }}
                        </td>
                        <td class="py-4 px-6">
                            <a href="/server/edit/{{ $server->id }}" class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-800">edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        // Datatables
        $(document).ready(function(){
            $('#tableServer').DataTable({
                responsive : true,
                "lengthChange": false,
                "ordering" : false,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ]
                    }
                ]
            });
        });

        $('#buttonSubmitServer').click(function(e){
            e.preventDefault();
            var form =  $(this).closest("form");

            Swal.fire({
                title: 'Tambah Server?',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: "Tidak",
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya!'
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        })
    </script>

    <script>
        let i = 1;
        $('#buttonTambahOrder').click(function(){
            i += 1;
            $(this).before(`
                <div>
                    <input value="{{ old('parameter_order[${i}]', '') }}" type="text" id="parameter_order[${i}]" name="parameter_order[${i}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <div>
                    <input value="{{ old('value_order[${i}]', '') }}" type="text" id="value_order[${i}]" name="value_order[${i}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <input type="hidden" name="length_loop_order" value="${i}">
            `)
        })

        let j = 1;
        $('#buttonTambahStatus').click(function(){
            j += 1;
            $(this).before(`
                <div>
                    <input value="{{ old('parameter_status[${j}]', '') }}" type="text" id="parameter_status[${j}]" name="parameter_status[${j}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <div>
                    <input value="{{ old('value_status[${j}]', '') }}" type="text" id="value_status[${j}]" name="value_status[${j}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <input type="hidden" name="length_loop_status" value="${j}">
            `)
        })

        let k = 1;
        $('#buttonTambahServices').click(function(){
            k += 1;
            $(this).before(`
                <div>
                    <input value="{{ old('parameter_services[${k}]', '') }}" type="text" id="parameter_services[${k}]" name="parameter_services[${k}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <div>
                    <input value="{{ old('value_services[${k}]', '') }}" type="text" id="value_services[${k}]" name="value_services[${k}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <input type="hidden" name="length_loop_services" value="${k}">
            `)
        })

        let l = 1;
        $('#buttonTambahBalances').click(function(){
            l += 1;
            $(this).before(`
                <div>
                    <input value="{{ old('parameter_balances[${l}]', '') }}" type="text" id="parameter_balances[${l}]" name="parameter_balances[${l}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <div>
                    <input value="{{ old('value_balances[${l}]', '') }}" type="text" id="value_balances[${l}]" name="value_balances[${l}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <input type="hidden" name="length_loop_balances" value="${l}">
            `)
        })

        let m = 1;
        $('#buttonTambahRefill').click(function(){
            m += 1;
            $(this).before(`
                <div>
                    <input value="{{ old('parameter_refill[${n}]', '') }}" type="text" id="parameter_refill[${m}]" name="parameter_refill[${m}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <div>
                    <input value="{{ old('value_refill[${m}]', '') }}" type="text" id="value_refill[${m}]" name="value_refill[${m}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <input type="hidden" name="length_loop_refill" value="${m}">
            `)
        })

        let n = 1;
        $('#buttonTambahRefillStatus').click(function(){
            n += 1;
            $(this).before(`
                <div>
                    <input value="{{ old('parameter_refill_status[${n}]', '') }}" type="text" id="parameter_refill_status[${n}]" name="parameter_refill_status[${n}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <div>
                    <input value="{{ old('value_refill_status[${n}]', '') }}" type="text" id="value_refill_status[${n}]" name="value_refill_status[${n}]" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                <input type="hidden" name="length_loop_refill_status" value="${n}">
            `)
        })
    </script>
@endsection
