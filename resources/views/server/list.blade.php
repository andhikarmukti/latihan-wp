@extends('layouts.main')

@section('content')
    <div class="p-9">
        <h3 class="text-2xl font-bold italic mb-5">List Sever</h3>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Nama Server
                        </th>
                        <th scope="col" class="py-3 px-6">
                            CP
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Sisa Saldo
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($servers as $server)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $server->nama_server }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $server->contact_person }}
                        </td>
                        <td class="py-4 px-6 {{ $saldo['server_id_' . $server->id] < 10000 ? 'text-red-500' : '' }}">
                            {{ Helpers::format_rupiah($saldo['server_id_' . $server->id]) }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>

    </script>
@endsection
