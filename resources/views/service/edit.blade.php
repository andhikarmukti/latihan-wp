@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="mb-9">
            <a href="/service" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
        </div>
        @can('admin')
        <div class="w-full md:w-3/4 mb-9 border border-slate-300 rounded-lg shadow-md p-5">
            <h4 class="text-start text-2xl font-bold italic mb-9">Edit Service</h3>
                <form action="/service/update/{{ $service->id }}" method="POST">
                    @csrf
                    <div class="flex flex-col md:flex-row gap-1">
                        <input type="hidden" name="old_is_active" value="{{ $service->is_active }}">
                        <div class="mb-6 md:w-1/3">
                            <label for="is_active" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Active</label>
                            <select id="is_active" name="is_active" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option value="1"  {{ $service->is_active == 1 ? 'selected' : '' }}>Yes</option>
                                <option value="0"  {{ $service->is_active == 0 ? 'selected' : '' }}>No</option>
                            </select>
                            @if ($errors->has('is_active'))
                                <small
                                    class="italic text-sm text-red-500">{{ $errors->first('is_active') }}</small>
                            @endif
                        </div>
                        <div class="mb-6 md:w-1/3">
                            <label for="auto_price" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Auto Price</label>
                            <select id="auto_price" name="auto_price" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option value="1"  {{ $service->auto_price == 1 ? 'selected' : '' }}>Yes</option>
                                <option value="0"  {{ $service->auto_price == 0 ? 'selected' : '' }}>No</option>
                            </select>
                            @if ($errors->has('auto_price'))
                                <small
                                    class="italic text-sm text-red-500">{{ $errors->first('auto_price') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row gap-1">
                        <div class="mb-6 md:w-1/3">
                            <label for="service_name"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Service Name</label>
                            <input value="{{ $service->service_name }}" type="text" id="service_name" name="service_name"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('service_name'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('service_name') }}</small>
                            @endif
                        </div>
                        <div class="mb-6 md:w-1/3">
                            <label for="service_category_id"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service Category</label>
                                <input disabled value="{{ $service_category->nama_category }}" type="text" id="service_category_id" name="service_category_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('service_category_id'))
                                <small
                                    class="italic text-sm text-red-500">{{ $errors->first('service_category_id') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="flex flex-col md:flex-row gap-1">
                        <div class="mb-6 md:w-1/3">
                            <input type="hidden" name="old_hpp" value="{{ $service->hpp }}">
                            <input type="hidden" name="old_rate" value="{{ $service->rate }}">
                            <label for="hpp"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">HPP (Rate : hpp/1000)</label>
                            <input disabled value="{{ round($service->hpp) }}" type="number" id="hpp" name="hpp"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('hpp'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('hpp') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="mb-6">
                        <label for="member_info"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Member Info</label>
                        <textarea id="member_info" name="member_info"
                            class="ckeditor bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">{{ $service->member_info }}</textarea>
                        @if ($errors->has('member_info'))
                            <small class="italic text-sm text-red-500">{{ $errors->first('member_info') }}</small>
                        @endif
                    </div>
                    <div class="grid grid-cols-2 gap-1">
                        <div class="mb-6">
                            <label for="speed"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Speed</label>
                            <input value="{{ $service->speed }}" type="number" id="speed" name="speed"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('speed'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('speed') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="last_completed"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Last Completed</label>
                            <input value="{{ $service->last_completed }}" type="number" id="last_completed" name="last_completed"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('last_completed'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('last_completed') }}</small>
                            @endif
                        </div>
                    </div>
                    <button type="submit"
                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Edit</button>
                </form>
        </div>
        @endcan
    </div>
@endsection
