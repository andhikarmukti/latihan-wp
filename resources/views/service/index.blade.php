@extends('layouts.main')

@section('content')
    <div class="p-9">
        @can('admin')
        {{-- <button class="p-3 bg-blue-100 ring-1 ring-blue-400 rounded-xl mb-3" id="buttonTambahService">Tambah Service</button> --}}
        <form action="/auto-price" method="POST">
            @csrf
            <button id="buttonAutoPrice" type="submit" class="p-3 bg-green-100 ring-1 ring-green-400 rounded-xl mb-3" id="buttonTambahService">Auto Price</button>
        </form>
        {{-- <div class="hidden w-full md:w-3/4 mb-9 border border-slate-300 rounded-lg shadow-md p-5" id="formTambahService">
            <h4 class="text-start text-2xl font-bold italic mb-9">Tambah Service</h3>
                <form action="/service" method="POST">
                    @csrf
                    <div class="grid md:grid-cols-3 gap-1">
                        <div class="mb-6">
                            <label for="service_name"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Service Name</label>
                            <input type="text" id="service_name" name="service_name"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('service_name'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('service_name') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="server_service_id"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Server Service
                                ID</label>
                            <input type="number" id="server_service_id" name="server_service_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('server_service_id'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('server_service_id') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="hpp"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">HPP</label>
                            <input type="number" id="hpp" name="hpp"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('hpp'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('hpp') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="grid md:grid-cols-3 gap-1">
                        <div class="mb-6">
                            <label for="service_category_id"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service Category</label>
                            <select id="service_category_id" name="service_category_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected disabled>Pilih Service Category</option>
                                @foreach ($service_categories as $service_category)
                                    <option value="{{ $service_category->id }}"
                                        {{ $service_category->id == request('server_id') ? 'selected' : '' }}>
                                        {{ $service_category->nama_category }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('service_category_id'))
                                <small
                                    class="italic text-sm text-red-500">{{ $errors->first('service_category_id') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="type_id"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service Type</label>
                            <select id="type_id" name="type_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected disabled>Select Service Type</option>
                                @foreach ($service_types as $service_type)
                                    <option value="{{ $service_type->id }}" {{ $service_type->id == 1 ? 'selected' : '' }}>{{ $service_type->type }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('type_id'))
                                <small
                                    class="italic text-sm text-red-500">{{ $errors->first('service_category_id') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="server_id"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Server</label>
                            <select id="server_id" name="server_id"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                <option selected disabled>Pilih Server</option>
                                @foreach ($servers as $server)
                                    <option value="{{ $server->id }}"
                                        {{ $server->id == request('server_id') ? 'selected' : '' }}>
                                        {{ $server->nama_server }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('server_id'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('server_id') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="flex flex-col gap-1 w-full md:w-1/2">
                        <div class="flex gap-1 w-full">
                            <div class="mb-6 w-full md:w-1/3">
                                <label for="refill"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Refill</label>
                                <select id="refill" name="refill"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option value="1">Yes</option>
                                    <option selected value="0">No</option>
                                </select>
                                @if ($errors->has('refill'))
                                    <small
                                        class="italic text-sm text-red-500">{{ $errors->first('refill') }}</small>
                                @endif
                            </div>
                            <div class="mb-6 w-full md:w-1/3">
                                <label for="refill_duration"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Durasi Refill (hari)</label>
                                    <input value="0" type="text" id="refill_duration" name="refill_duration"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                @if ($errors->has('refill_duration'))
                                    <small
                                        class="italic text-sm text-red-500">{{ $errors->first('refill_duration') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="flex gap-1 w-full">
                            <div class="mb-6 w-full md:w-1/3">
                                <label for="cancel"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Cancel</label>
                                <select id="cancel" name="cancel"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option value="1">Yes</option>
                                    <option selected value="0">No</option>
                                </select>
                                @if ($errors->has('cancel'))
                                    <small
                                        class="italic text-sm text-red-500">{{ $errors->first('cancel') }}</small>
                                @endif
                            </div>
                            <div class="mb-6 w-full md:w-1/3">
                                <label for="auto_price"
                                    class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Auto Price</label>
                                <select id="auto_price" name="auto_price"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if ($errors->has('auto_price'))
                                    <small
                                        class="italic text-sm text-red-500">{{ $errors->first('cancel') }}</small>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-1">
                        <div class="mb-6">
                            <label for="min"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Min</label>
                            <input type="number" id="min" name="min"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('min'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('min') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="max"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Max</label>
                            <input type="number" id="max" name="max"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('max'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('max') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-1">
                        <div class="mb-6">
                            <label for="rate"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Rate</label>
                            <input type="number" id="rate" name="rate"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('rate'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('rate') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="unit"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Unit</label>
                            <input type="number" id="unit" name="unit"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('unit'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('unit') }}</small>
                            @endif
                        </div>
                    </div>
                    <div class="mb-6">
                        <label for="member_info"
                            class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Member Info</label>
                        <textarea id="member_info" name="member_info"
                            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"></textarea>
                        @if ($errors->has('member_info'))
                            <small class="italic text-sm text-red-500">{{ $errors->first('member_info') }}</small>
                        @endif
                    </div>
                    <div class="grid grid-cols-2 gap-1">
                        <div class="mb-6">
                            <label for="speed"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Speed</label>
                            <input type="number" id="speed" name="speed"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('speed'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('speed') }}</small>
                            @endif
                        </div>
                        <div class="mb-6">
                            <label for="last_completed"
                                class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Last Completed</label>
                            <input type="number" id="last_completed" name="last_completed"
                                class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('last_completed'))
                                <small class="italic text-sm text-red-500">{{ $errors->first('last_completed') }}</small>
                            @endif
                        </div>
                    </div>
                    <button type="submit"
                        class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
        </div> --}}
        @endcan

        <h2 id="accordion-arrow-icon-heading-2" data-accordion="open">
            <button type="button" class="bg-blue-200 flex items-center justify-between w-full p-5 font-medium text-left text-gray-500 border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-800 dark:border-gray-700 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-800" data-accordion-target="#accordion-arrow-icon-body-2" aria-expanded="false" aria-controls="accordion-arrow-icon-body-2">
              <span class="text-blue-600"><span class="text-yellow-800">[</span> INFO SERVICE <span class="text-yellow-800">]</span></span>
              <svg data-accordion-icon class="w-6 h-6 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 13l-3 3m0 0l-3-3m3 3V8m0 13a9 9 0 110-18 9 9 0 010 18z"></path></svg>
            </button>
          </h2>
          <div id="accordion-arrow-icon-body-2" class="hidden" aria-labelledby="accordion-arrow-icon-heading-2">
            <div id="alert-additional-content-1" class="p-4 mb-4 border border-blue-300 rounded-lg bg-blue-100 dark:bg-blue-300" role="alert">
                <div class="mt-2 mb-4 text-sm text-blue-900">
                  <ul class="list-disc list-inside">
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Min :
                        </span>
                        <span class="text-md font-bold">Jumlah minimum pembelian.</span>

                        <div class="text-yellow-700">
                            Contoh : Jika jumlah min yang tertera pada service followers adalah 10, maka minimal order yang bisa dilakukan adalah pembelian sebanyak 10 followers
                        </div>
                    </li>
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Max :
                        </span>
                        <span class="text-md font-bold">Jumlah maksimum pembelian.</span>

                        <div class="text-yellow-700">
                            Contoh : Jika jumlah min yang tertera pada service likes adalah 10.000, maka maksimum order yang bisa dilakukan adalah pembelian sebanyak 10.000 likes
                        </div>
                    </li>
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Harga / Quantity :
                        </span>
                        <span class="text-md font-bold">Harga setiap 1 quantity</span>

                        <div class="text-yellow-700">
                            Contoh : Jika jumlah harga / quantity yang tertera pada service followers adalah Rp 50, maka jika kamu membeli 100 followers akan dikenakan biaya sebesar <span class="text-purple-800">100 x Rp 50 = Rp 5.000</span>
                        </div>
                    </li>
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Harga / 1000 Quantity :
                        </span>
                        <span class="text-md font-bold">Harga setiap 1000 quantity</span>

                        <div class="text-yellow-700">
                            Contoh : Jika jumlah harga / 1000 quantity yang tertera pada service views adalah Rp 9.500, maka jika kamu membeli 1000 views akan dikenakan biaya sebesar <span class="text-purple-800">Rp 9.500</span>
                        </div>
                    </li>
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Estimasi Speed :
                        </span>
                        <span class="text-md font-bold">Perkiraan lamanya waktu proses pengerjaan</span>

                        <div class="text-yellow-700">
                            Waktu yang tertera hanyalah perkiraan saja, berdasarkan history dari orderan-orderan sebelumnya. Prosesnya bisa lebih lambat ataupun lebih cepat dari waktu yang tertera. Jika kosong, maka system kami belum dapat memperkirakan speednya. Tetapi service tetap bisa diorder dan berjalan dengan baik.
                        </div>
                    </li>
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Refill & Garansi
                        </span>

                        <div class="text-yellow-700">
                            Jika terdapat keterangan YES maka service tersebut bisa diupayakan untuk diisi ulang ketika mengalami drop atau status completed dengan jumlah orderan yang masuk tidak sesuai.
                            Info selengkapnya tentang Refill bisa cek di halaman <a href="/history-order" class="text-blue-600 hover:text-blue-800" target="_blank">History Order</a>
                        </div>
                    </li>
                    <li class="mb-5">
                        <span class="text-xl font-bold">
                            Member Info
                        </span>

                        <div class="text-yellow-700">
                            Merupakan bagian paling penting pada setiap service. Berisi tentang informasi lebih detail mengenai service tersebut. Terdapat contoh target pada informasi tersebut, biasanya berisi "Username" atau "Link" yang harus diinputkan saat melakukan orderan. Setiap layanan memiliki contoh target yang berbeda-beda, sangat disarankan untuk selalu membaca "info penting" yang muncul saat melakukan orderan di halaman <a href="/new-order" class="text-blue-600 hover:text-blue-800" target="_blank">New Order</a>
                        </div>
                    </li>
                  </ul>
                </div>
                <div class="flex">
                    <a href="#tableService" type="button" class="text-white bg-yellow-900 hover:bg-yellow-800 focus:ring-4 focus:outline-none focus:ring-yellow-200 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-yellow-800 dark:hover:bg-yellow-900">
                        Cek Service
                    </a>
                    <a href="/new-order" target="_blank" type="button" class="text-white bg-blue-900 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-200 font-medium rounded-lg text-xs px-3 py-1.5 mr-2 text-center inline-flex items-center dark:bg-blue-800 dark:hover:bg-blue-900">
                        Order sekarang
                    </a>
                </div>
            </div>
          </div>


        <div>
            <small class="text-xs italic sm:hidden text-gray-500">Table dapat digeser <i class="fa fa-long-arrow-right" aria-hidden="true"></i></small>
        </div>
        <div class="overflow-x-auto relative border border-slate-300 p-5 rounded-lg shadow-md w-full">
            @can('admin')
            <p>Total service active = {{ $total_services_active }}</p>
            @endcan
            <h4 class="text-start text-2xl font-bold italic mb-9">Services</h3>
            <table class="overflow-x-auto relative w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableService">
                <thead class="text-xs text-gray-700 uppercase bg-gray-200 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Service ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Service Category
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Service
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Min Order
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Max Order
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Harga / quantity
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Harga / 1000 quantity
                        </th>
                        @can('admin')
                        <th scope="col" class="py-3 px-6">
                            Server Service ID
                        </th>
                        @endcan
                        <th scope="col" class="py-3 px-6">
                            Estimasi Speed
                        </th>
                        @can('admin')
                        <th scope="col" class="py-3 px-6">
                            HPP
                        </th>
                        @endcan
                        <th scope="col" class="py-3 px-6">
                            Refill
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Garansi
                        </th>
                        @can('admin')
                        <th scope="col" class="py-3 px-6">
                            Active
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Last Completed
                        </th>
                        @endcan
                        {{-- <th scope="col" class="py-3 px-6">
                            Member Info
                        </th> --}}
                        <th scope="col" class="py-3 px-6">
                            Member Info
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($services as $service)
                        <tr
                            class="border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 text-center">
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->id }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->serviceCategory->nama_category }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->service_name }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->min }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->max }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->rate * auth()->user()->levelMember->mark_up / 1000 }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->rate * auth()->user()->levelMember->mark_up }}
                            </th>
                            @can('admin')
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->server_service_id }}
                            </th>
                            @endcan
                            <th scope="row"
                                class="{{ Helpers::speedColor($service->speed) }} py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ Helpers::formatWaktu($service->speed) }}
                            </th>
                            @can('admin')
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->hpp }}
                            </th>
                            @endcan
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->refill == 1 ? 'Yes' : '-' }}
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $service->refill_duration != 0 ? $service->refill_duration . ' hari' : '-'  }}
                            </th>
                            @can('admin')
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                <span
                                    class="{{ $service->is_active == 1 ? 'text-green-600 font-bold' : 'text-red-600 font-bold' }}">
                                    {{ $service->is_active == 1 ? 'active' : '-' }}
                                </span>
                            </th>
                            <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ Helpers::formatWaktu($service->last_completed) ? Helpers::formatWaktu($service->last_completed) . ' yang lalu' : '-' }}
                            </th>
                            @endcan
                            {{-- <th scope="row"
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white text-start">
                                {!! $service->member_info !!}
                            </th> --}}
                            <th scope="row"
                            @can('admin')
                                class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                <a href="/service/edit/{{ $service->id }}"
                                    class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">edit</a>
                                <button id="button_disable_{{ $service->id }}" type="button" data-action="{{ $service->is_active == 1 ? 'disable' : 'enable' }}" data-serviceid="{{ $service->id }}" class="disableButton bg-{{ $service->is_active == 1 ? 'red' : 'blue' }}-100 text-{{ $service->is_active == 1 ? 'red' : 'blue' }}-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-{{ $service->is_active == 1 ? 'red' : 'blue' }}-200 dark:text-{{ $service->is_active == 1 ? 'red' : 'blue' }}-900">{{ $service->is_active == 1 ? 'disable' : 'enable' }}</button>
                            @endcan
                            <button class="bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 dark:bg-blue-200 dark:text-blue-800 hover:cursor-pointer rounded-full buttonDetail" type="button" data-id="{{ $service->id }}" data-modal-toggle="modalDetail">Info</button>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


  <!-- Main modal -->
  <div id="modalDetail" tabindex="-1" class="overflow-y-scroll overflow-x-scroll fixed top-0 right-0 left-0 z-50 w-full md:inset-0 h-full md:h-full justify-center items-center hidden" aria-modal="true" role="dialog">
      <div class="relative p-4 w-full max-w-2xl h-full md:h-auto">
          <!-- Modal content -->
          <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
              <!-- Modal header -->
              <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                  <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                      Detail Service
                  </h3>
                  <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white buttonCross" data-modal-toggle="modalDetail">
                      <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                      <span class="sr-only">Close modal</span>
                  </button>
              </div>
            <!-- Modal body -->
            <div class="p-6 space-y-6 overflow-x-auto overflow-y-scroll md:h-96" id="bodyDetail">
                <p class="text-base leading-relaxed text-gray-500 dark:text-gray-400" id="memberInfoDetail"></p>
            </div>
            <!-- Modal footer -->
            <div class="flex items-center p-6 space-x-2 rounded-b border-t border-gray-200 dark:border-gray-600">
                <button data-modal-toggle="modalDetail" type="button" class="text-gray-500 bg-slate-300 hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600 buttonClose">Close</button>
            </div>
          </div>
      </div>
  </div>

    <script>
        // Datatables
        $(document).ready(function() {
            const kolom_admin = [6, 9];
            const kolom_user = [6];
            const user_role = "{{ auth()->user()->role }}";
            $('#tableService').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'colvis'
                ],
                "pageLength": 100,
                // responsive: true,
                // "lengthChange": false,
                "ordering": true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: ["_all"]
                    },
                    {
                        target:1,
                        className:'cell-border'
                    },
                    {
                        target: [5],  render: $.fn.dataTable.render.number( '.', ',', 1, 'Rp ' )
                    },
                    {
                        target: user_role == 'admin' ? kolom_admin : kolom_user,  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                    {
                        target: [3, 4],  render: $.fn.dataTable.render.number( '.', ',', 0)
                    }
                ],
                order: [[0, 'asc']],
            });
        });
    </script>

    <script>
        $('.buttonDetail').click(function(){
            const serviceId = $(this).data('id');

            $.ajax({
                url : '/service',
                data : {
                    service_id : serviceId
                },
                beforeSend: function(){
                    // loading skeleton
                    $('#bodyDetail').after(`
                        <div id="loading" role="status" class="max-w-sm animate-pulse my-5">
                            <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                            <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                            <span class="sr-only">Loading...</span>
                        </div>
                    `)
                },
                success : function(res){
                    // console.log(res);
                    $('#loading').empty();
                    $('#memberInfoDetail').html(`
                        <span class="font-bold">Member Info :</span>
                        <br>
                        <span class="font-bold text-yellow-800">${res.member_info}</span>
                    `);
                },
            })
        });

        $('.buttonCross').click(function(){
            $('#memberInfoDetail').empty();
        });
        $('.buttonClose').click(function(){
            $('#memberInfoDetail').empty();
        })

        // $('#buttonTambahService').click(function(){
        //     $(this).addClass('hidden');
        //     $('#formTambahService').removeClass('hidden');
        // })
    </script>

    <script>
        $('#buttonAutoPrice').click(function(){
            let timerInterval
                Swal.fire({
                    title: 'Mohon tunggu sebentar',
                    html: 'Sedang melakukan auto price',
                    timer: 500000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading()
                        const b = Swal.getHtmlContainer().querySelector('b')
                        timerInterval = setInterval(() => {
                            if(b){
                                b.textContent = Swal.getTimerLeft()
                            }
                        }, 100)
                    },
                willClose: () => {
                        clearInterval(timerInterval)
                    }
                })
        });

        // Disable Button
        $('.disableButton').click(function(){
            const serviceId = $(this).attr('data-serviceid');
            const action = $(this).attr('data-action');

            $.ajax({
                url : '/service/update/' + serviceId,
                method : 'POST',
                type : 'JSON',
                data : {
                    "_token" : "{{ csrf_token() }}",
                    action : action
                },
                success : function(res){
                    $('#button_disable_' + serviceId).html('✓')
                }
            });
        });
    </script>
@endsection
