@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="flex gap-2">
            <div class="border border-slate-300 p-5 rounded-lg shadow-md w-1/3">
                <h4 class="text-start text-2xl font-bold italic mb-9">Tambah Category Service</h3>
                <form action="/category-service" method="POST">
                    @csrf
                    <div class="relative z-0 mb-6 w-full group">
                        <input value="{{ old('nama_category', '') }}" type="nama_category" name="nama_category" id="floating_nama_category" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" autocomplete="off">
                        <label for="floating_nama_category" class="@if($errors->has('nama_category')) text-red-500 @else @endif peer-focus:font-medium absolute text-sm  dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Nama Category</label>
                        @if($errors->has('nama_category'))
                        <small class="text-sm italic text-red-500">{{ $errors->first('nama_category') }}</small>
                        @endif
                    </div>
                    <button id="buttonSubmitCategoryService" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                </form>
            </div>
            <div class="border border-slate-300 p-5 rounded-lg shadow-md w-2/3">
                <h4 class="text-start text-2xl font-bold italic mb-9">Category Service</h3>
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableCategoryService">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Nama Category
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Active
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($category_services as $category_service)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $category_service->nama_category }}
                            </th>
                            <td class="py-4 px-6">
                                <span class="{{ $category_service->is_active == 1 ? 'text-green-600 font-bold' : 'text-red-600 font-bold' }}">
                                    {{ $category_service->is_active == 1 ? 'active' : '-' }}
                                </span>
                            </td>
                            <td class="py-4 px-6 text-center">
                                <a href="/category-service/edit/{{ $category_service->id }}" class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">edit</a>
                                <a href="/category-service/disable/{{ $category_service->id }}" class="bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">disable</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        // Datatables
        $(document).ready(function(){
            $('#tableCategoryService').DataTable({
                responsive : true,
                "lengthChange": false,
                "ordering" : false,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ 2 ]
                    }
                ]
            });
        });
    </script>
@endsection
