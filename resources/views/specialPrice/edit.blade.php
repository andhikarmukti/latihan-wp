@extends('layouts.main')

@section('content')
    <div class="p-9">
        <form method="POST" action="/admin-special-price/{{ $specialPrice->id }}" class="border border-slate-200 shadow-md p-5 rounded-lg w-full lg:w-1/2">
            @csrf
            @method('PUT')
            <div class="mb-6">
              <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
              <input value="{{ $specialPrice->user->username }}" type="text" id="username" name="username" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
            </div>
            <div class="mb-6">
                <label for="service_category_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Category Service</label>
                <select id="service_category_id" name="service_category_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Category</option>
                    @foreach ($services_category_id as $service_category_id)
                    <option value="{{ $service_category_id->id }}" {{ $specialPrice->service->service_category_id == $service_category_id->id ? 'selected' : '' }}>{!! $service_category_id->nama_category !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-6">
                <label for="service_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service</label>
                <select id="service_id" name="service_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Service</option>
                    @foreach ($services as $service)
                    <option class="{{ str_contains($service->service_name, '[SLOW]') ? 'text-red-800' : '' }}" {{ $service->id == $specialPrice->service_id ? 'selected' : '' }} value="{{ $service->id == $specialPrice->service_id ? $specialPrice->service_id : $service->id }}">{{ $service->service_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="flex flex-col md:flex-row gap-9 mb-6">
                <div class="md:w-1/2">
                    <label for="percent" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Percent (%)</label>
                    <div class="flex">
                        <input value="{{ (int)$specialPrice->percent ?? 0 }}" type="text" id="percent" name="percent" class="rounded-none rounded-l-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-r-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                          %
                        </span>
                      </div>
                  </div>
                  <div class="md:w-1/2">
                    <div class="">
                        <label for="expired_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Expired Date</label>
                        <input value="{{ $specialPrice->expired_date }}" type="datetime-local" id="expired_date" name="expired_date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                    </div>
                  </div>
                  {{-- <div class="w-1/2">
                    <label for="fix" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Fix (Rp)</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                          Rp
                        </span>
                        <input value="{{ $specialPrice->fix }}" type="text" id="fix" name="fix" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="">
                    </div>
                  </div> --}}
            </div>
            <div class="flex flex-wrap mb-6">
                <div class="flex items-center mr-4">
                    <input {{ $specialPrice->status == 'enable' ? 'checked' : '' }} id="green-radio" type="radio" value="enable" name="status" class="w-4 h-4 text-green-600 bg-gray-100 border-gray-300 focus:ring-green-500 dark:focus:ring-green-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="green-radio" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Enable</label>
                </div>
                <div class="flex items-center mr-4">
                    <input {{ $specialPrice->status == 'disable' ? 'checked' : '' }} id="red-radio" type="radio" value="disable" name="status" class="w-4 h-4 text-red-600 bg-gray-100 border-gray-300 focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600">
                    <label for="red-radio" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Disable</label>
                </div>
            </div>
            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Edit</button>
        </form>
    </div>

    <script>
        $('#service_category_id').change(function(){
            $('#service_id').parent().attr('hidden', true);
            // loading skeleton
            $(this).after(`
            <div id="loading" role="status" class="max-w-sm animate-pulse mt-10">
                <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                <span class="sr-only">Loading...</span>
            </div>
            `)

            const serviceCategoryId = $(this).find(':selected').val();
            $.ajax({
                url : '/admin-special-price/edit/{{ $specialPrice->id }}',
                data : {
                    service_category_id : serviceCategoryId
                },
                success : function(res){
                    $('#loading').remove(); // hilangkan loading skeleton
                    $('#service_id').parent().removeAttr('hidden');

                    $('#service_id').empty(); // Hapus select service id

                    $('#service_id').append(`
                        <option selected disabled>Pilih Service</option>
                    `);
                    $.each(res, function(index, value){
                        // console.log(value.discounts);
                        $('#service_id').append(`
                            <option class="${value.service_name.includes('[SLOW]') ? 'text-red-800' : ''}" value="${value.id}"><span class="font-bold italic">${value.id}</span> - ${value.service_name} <span class="font-bold">${value.discounts.length > 0 ? '[PROMO]' : ''}</span></option>
                        `);
                    })
                }
            });
        })
    </script>
@endsection
