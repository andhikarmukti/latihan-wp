@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div>
            <h1 class="text-2xl italic font-bold mb-5">Special Price</h1>
        </div>
        <form method="POST" action="/admin-special-price" class="border border-slate-200 shadow-md p-5 rounded-lg w-full md:w-1/2">
            @csrf
            <div class="mb-6">
              <label for="username" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
              <input type="text" id="username" name="username" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
            </div>
            <div class="mb-6">
                <label for="service_category_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Category Service</label>
                <select id="service_category_id" name="service_category_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Category</option>
                    @foreach ($services_category_id as $service_category_id)
                    <option value="{{ $service_category_id->id }}">{!! $service_category_id->nama_category !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-6">
                <label for="service_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Service</label>
                <select id="service_id" name="service_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Service</option>
                </select>
            </div>
            <div class="flex flex-col md:flex-row gap-6 md:gap-9 mb-6">
                <div class="md:w-1/2">
                    <label for="percent" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Percent (%)</label>
                    <div class="flex">
                        <input type="text" id="percent" name="percent" class="rounded-none rounded-l-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-r-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                          %
                        </span>
                    </div>
                </div>
                <div class="md:w-1/2">
                    <label for="expired_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Expired Date</label>
                    <input type="datetime-local" id="expired_date" name="expired_date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                </div>
                  {{-- <div class="w-1/2">
                    <label for="fix" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Fix (Rp)</label>
                    <div class="flex">
                        <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                          Rp
                        </span>
                        <input type="text" id="fix" name="fix" class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="">
                      </div>
                  </div> --}}
            </div>
            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
        </form>

        <div class="overflow-x-auto relative shadow-md sm:rounded-lg mt-5">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableSpecialPrice">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Service
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Percent
                        </th>
                        {{-- <th scope="col" class="py-3 px-6">
                            Fix
                        </th> --}}
                        <th scope="col" class="py-3 px-6">
                            Before
                        </th>
                        <th scope="col" class="py-3 px-6">
                            After
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Expired Date
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Status
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($special_prices as $special_price)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $special_price->user->username }}
                        </th>
                        <td class="py-4 px-6">
                            {{ $special_price->service->id }} - {{ $special_price->service->service_name }}
                        </td>
                        <td class="py-4 px-6">
                            {{ (int)$special_price->percent == 0 ? '-' : (int)$special_price->percent . '%' }}
                        </td>
                        {{-- <td class="py-4 px-6">
                            {{ $special_price->fix }}
                        </td> --}}
                        <td class="py-4 px-6">
                            {{ $special_price->service->rate * $special_price->user->levelMember->mark_up }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $special_price->user->levelMember->mark_up * ($special_price->service->rate - ($special_price->service->rate * $special_price->percent / 100) - $special_price->fix) }}
                        </td>
                        <td class="py-4 px-6">
                            {{ date('d-M-y H:i:s', strtotime($special_price->expired_date)) }}
                        </td>
                        <td class="py-4 px-6 {{ $special_price->status == 'enable' ? 'text-green-500' : 'text-red-500'}}">
                            {{ $special_price->status }}
                        </td>
                        <td class="py-4 px-6">
                            <a href="/admin-special-price/edit/{{ $special_price->id }}" class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#tableSpecialPrice').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                "pageLength": 25,
                dom: 'Bfrtip',
                buttons: [
                    'colvis'
                ],
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ]
                    },
                    {
                        target: [3, 4],  render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    },
                ],
                order:[[0,"desc"]]
            });
        });
    </script>

    <script>
        $('#service_category_id').change(function(){
            $('#service_id').parent().attr('hidden', true);
            // loading skeleton
            $(this).after(`
            <div id="loading" role="status" class="max-w-sm animate-pulse mt-10">
                <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                <span class="sr-only">Loading...</span>
            </div>
            `)

            const serviceCategoryId = $(this).find(':selected').val();
            $.ajax({
                url : '/admin-special-price',
                data : {
                    service_category_id : serviceCategoryId
                },
                success : function(res){
                    $('#loading').remove(); // hilangkan loading skeleton
                    $('#service_id').parent().removeAttr('hidden');

                    $('#service_id').empty(); // Hapus select service id

                    $('#service_id').append(`
                        <option selected disabled>Pilih Service</option>
                    `);
                    $.each(res, function(index, value){
                        // console.log(value.discounts);
                        $('#service_id').append(`
                            <option class="${value.service_name.includes('[SLOW]') ? 'text-red-800' : ''}" value="${value.id}"><span class="font-bold italic">${value.id}</span> - ${value.service_name} <span class="font-bold">${value.discounts.length > 0 ? '[PROMO]' : ''}</span></option>
                        `);
                    })
                }
            });
        })
    </script>
@endsection
