@extends('layouts.main')

@section('content')
<div class="p-9">
    <div class="w-full bg-white rounded-lg border shadow-md dark:bg-gray-800 dark:border-gray-700">
        <ul class="flex flex-wrap justify-center text-sm md:text-lg font-medium text-center text-gray-500 bg-gray-50 rounded-t-lg border-b border-gray-200 dark:border-gray-700 dark:text-gray-400 dark:bg-gray-800" id="defaultTab" data-tabs-toggle="#defaultTabContent" role="tablist">
            <li class="mr-2">
                <button id="end-user-tab" data-tabs-target="#end-user" type="button" role="tab" aria-controls="end-user" aria-selected="true" class="inline-block p-4 text-blue-600 rounded-tl-lg hover:bg-gray-100 dark:bg-gray-800 dark:hover:bg-gray-700 dark:text-blue-500 hover:text-blue-600 dark:hover:text-blue-500 border-blue-600 dark:border-blue-500">End User</button>
            </li>
            <li class="mr-2">
                <button id="reseller-tab" data-tabs-target="#reseller" type="button" role="tab" aria-controls="reseller" aria-selected="false" class="inline-block p-4 hover:text-gray-600 hover:bg-gray-100 dark:hover:bg-gray-700 dark:hover:text-gray-300 dark:border-transparent text-gray-500 dark:text-gray-400 border-gray-100 hover:border-gray-300 dark:border-gray-700">Reseller</button>
            </li>
            <li class="mr-2">
                <button id="agent-tab" data-tabs-target="#agent" type="button" role="tab" aria-controls="agent" aria-selected="false" class="inline-block rounded-tr-lg p-4 hover:text-gray-600 hover:bg-gray-100 dark:hover:bg-gray-700 dark:hover:text-gray-300 dark:border-transparent text-gray-500 dark:text-gray-400 border-gray-100 hover:border-gray-300 dark:border-gray-700">Agent</button>
            </li>
        </ul>
        <div id="defaultTabContent">
            {{-- End User --}}
            <div class="p-4 bg-white rounded-lg md:p-8 dark:bg-gray-800" id="end-user" role="tabpanel" aria-labelledby="end-user-tab">
                <div class="flex gap-3 justify-center items-center mb-4">
                    <div class="">
                        <img class="w-10 h-10 md:w-20 md:h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/1.png" alt="levelMember">
                    </div>
                    <div class="">
                        <span class="font-bold text-xl md:text-6xl {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-blue-700') }}">
                            End User
                        </span>
                    </div>
                </div>
                <div class="flex flex-col md:flex-row gap-3">
                    <div class="flex p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg w-full md:w-1/2" role="alert">
                        <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Info</span>
                        <div>
                          <span class="font-medium text-blue-800">Level ini cocok buat kamu dengan kondisi berikut :</span>
                            <ul class="mt-1.5 md:ml-4 text-blue-700 list-disc list-inside">
                              <li>Hanya untuk coba-coba dan digunakan sendiri</li>
                              <li>Tidak ada niat untuk menjadi reseller</li>
                              <li>Harga mahal bukan masalah</li>
                            </ul>
                        </div>
                    </div>
                    <div class="flex p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg w-full md:w-1/2" role="alert">
                        <div>
                          <span class="text-md md:text-xl text-blue-800">Saat pertama daftar, kamu sudah berada di level End User</span>
                            <p class="text-xl">Biaya Upgrade : Tidak ada biaya upgrade</p>
                        </div>
                    </div>
                </div>
                <div class="flex flex-col md:flex-row justify-center">
                    <div class="p-4 mb-4 text-sm text-blue-700 bg-blue-100 rounded-lg md:w-1/2" role="alert">
                        <div class="flex justify-center">
                            <div class="mb-5">
                                <span class="text-md md:text-4xl text-blue-800">Keuntungan yang kamu dapatkan</span>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <span class="text-lg text-blue-800">Harga Normal</span>
                            <span class="text-lg text-blue-800">Discount : </span>
                            <ul class="mt-1.5 md:ml-4 text-blue-700 list-disc list-inside">
                                <li class="text-gray-600">Bonus Topup 5% <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus End User <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus Reseller <i class="fa fa-times text-red-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus Agent <i class="fa fa-times text-red-500" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Reseller --}}
            <div class="hidden p-4 bg-white rounded-lg md:p-8 dark:bg-gray-800" id="reseller" role="tabpanel" aria-labelledby="reseller-tab">
                <div class="flex justify-center items-center gap-3 mb-5">
                    <div class="">
                        <img class="w-10 h-10 md:w-20 md:h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/2.png" alt="levelMember">
                    </div>
                    <div class="">
                        <span class="font-bold text-xl md:text-6xl {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-yellow-700') }}">
                            Reseller
                        </span>
                    </div>
                </div>
                <div class="flex flex-col md:flex-row gap-3">
                    <div class="flex p-4 mb-4 text-sm text-yellow-700 bg-yellow-100 rounded-lg w-full md:w-1/2" role="alert">
                        <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Info</span>
                        <div>
                          <span class="font-medium text-yellow-800">Level ini cocok buat kamu dengan kondisi berikut :</span>
                            <ul class="mt-1.5 md:ml-4 text-yellow-700 list-disc list-inside">
                              <li>Berniat untuk menjual kembali service yang ada di {{ $nama_web }}</li>
                              <li>Mendapatkan keuntungan yang besar dengan harga modal yang lebih murah</li>
                              <li>Bisa digunakan untuk pribadi ataupun untuk dijual kembali</li>
                            </ul>
                        </div>
                    </div>
                    <div class="flex p-4 mb-4 text-sm text-yellow-700 bg-yellow-100 rounded-lg w-full md:w-1/2" role="alert">
                        <div class="flex flex-col">
                            <p class="text-lg md:text-xl">Biaya Upgrade : {{ Helpers::format_rupiah($level_member->find(2)->harga_upgrade) }}</p>
                            <span class="text-xl text-yellow-800">Durasi : 1 tahun</span>
                            <span class="text-md md:text-lg italic text-gray-800">Note : Jika dalam 1 tahun total belanja mencapai <u>lebih dari 1 juta</u>, maka akan automatis perpanjang GRATIS</span>
                            <span class="text-xs md:text-xs italic text-red-500">Total penggunaan saldo dihitung sejak pembelian level upgrade di accept oleh admin</span>
                        </div>
                    </div>
                </div>
                <div class="flex justify-center">
                    <div class="p-4 mb-4 text-sm text-yellow-700 bg-yellow-100 rounded-lg w-full md:w-1/2" role="alert">
                        <div class="flex justify-center">
                            <div class="mb-5">
                                <span class="text-md md:text-4xl text-yellow-800">Keuntungan yang kamu dapatkan</span>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <span class="text-md md:text-lg text-yellow-800">Harga lebih murah untuk semua service</span>
                            <span class="text-lg text-yellow-800">Discount : </span>
                            <ul class="mt-1.5 md:ml-4 text-yellow-700 list-disc list-inside">
                                <li class="text-gray-600">Bonus Topup 5% <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus End User <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus Reseller <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus Agent <i class="fa fa-times text-red-500" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if (auth()->user()->levelMember->level < 2 && auth()->user()->levelMember->level == 1)
                <div class="flex justify-center">
                    @if (auth()->user()->upgradeLevel && auth()->user()->upgradeLevel->level_member_id == 2)
                        <div class="flex flex-col gap-1">
                            <button disabled class="buttonUpgrade p-3 border border-yellow-300 bg-yellow-300 rounded-xl text-orange-800">Requested</button>
                            <button type="button" class="buttonInfoPembayaran bg-blue-100 text-blue-800 hover:bg-blue-200 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">cek info pembayaran</button>
                            <span class="text-red-500">Expired at {{ auth()->user()->upgradeLevel->created_at->addDays(1) }}</span>
                        </div>
                    @else
                    <button data-level="2" data-levelname="Reseller" data-biayaupgrade="{{ $level_member->find(2)->harga_upgrade }}" class="buttonUpgrade p-3 border border-yellow-300 bg-yellow-100 rounded-xl hover:bg-yellow-200">Upgrade ke Reseller</button>
                    @endif
                </div>
                @endif
            </div>
            {{-- Agent --}}
            <div class="hidden p-4 bg-white rounded-lg md:p-8 dark:bg-gray-800" id="agent" role="tabpanel" aria-labelledby="agent-tab">
                <div class="flex justify-center items-center gap-3 mb-5">
                    <div class="">
                        <img class=" w-10 h-10md:w-20 md:h-20 object-cover rounded-t-lg md:rounded-none md:rounded-l-lg" src="images/levelMember/3.png" alt="levelMember">
                    </div>
                    <div class="">
                        <span class="font-bold text-xl md:text-6xl {{ auth()->user()->levelMember->level == 1 ? 'text-gray-700' : (auth()->user()->levelMember->level == 2 ? 'text-blue-700' : 'text-purple-700') }}">
                            Agent
                        </span>
                    </div>
                </div>
                <div class="flex flex-col md:flex-row gap-3">
                    <div class="flex p-4 mb-4 text-sm text-purple-700 bg-purple-100 rounded-lg w-full md:w-1/2" role="alert">
                        <svg aria-hidden="true" class="flex-shrink-0 inline w-5 h-5 mr-3" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Info</span>
                        <div>
                          <span class="font-medium text-purple-800">Level ini cocok buat kamu dengan kondisi berikut :</span>
                            <ul class="mt-1.5 md:ml-4 text-purple-700 list-disc list-inside">
                              <li>Kamu sudah memiliki banyak pelanggan setia</li>
                              <li>Mendapatkan keuntungan yang sangat besar dengan harga modal yang sangat murah</li>
                              <li>{{ $nama_web }} siap support dan memberikan harga terbaik bagi para Agent</li>
                            </ul>
                        </div>
                    </div>
                    <div class="flex p-4 mb-4 text-sm text-purple-700 bg-purple-100 rounded-lg w-full md:w-1/2" role="alert">
                        <div class="flex flex-col">
                            <p class="text-lg md:text-xl">Biaya Upgrade : {{ Helpers::format_rupiah($level_member->find(3)->harga_upgrade) }}</p>
                            <span class="text-xl text-purple-800">Durasi : 1 tahun</span>
                            <hr>
                            <span class="text-md md:text-lg italic text-gray-800">Note : Jika dalam 1 tahun total belanja mencapai <u class="font-bold">lebih dari 5 juta</u>, maka akan automatis perpanjang GRATIS</span>
                            <ul class="mt-1.5 md:ml-4 text-purple-700 list-disc list-inside">
                                <li><span class="text-xs md:text-xs italic text-red-500">Total penggunaan saldo dihitung sejak pembelian level upgrade di accept oleh admin</span></li>
                                <li><span class="text-xs md:text-xs italic text-red-500">Jika kamu upgrade dari Reseller menjadi Agent, maka total penggunaan saldo akan dihitung ulang sejak pembelian upgrade level Agent di accept oleh admin. Dan status level Reseller kamu menjadi expired (upgrade to Agent)</span></li>
                              </ul>
                        </div>
                    </div>
                </div>
                <div class="flex justify-center">
                    <div class="p-4 mb-4 text-sm text-purple-700 bg-purple-100 rounded-lg w-full md:w-1/2" role="alert">
                        <div class="flex justify-center">
                            <div class="mb-5">
                                <span class="text-md md:text-4xl text-purple-800">Keuntungan yang kamu dapatkan</span>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <span class="text-md md:text-lg text-purple-800">Harga lebih murah untuk semua service</span>
                            <span class="text-lg text-purple-800">Discount : </span>
                            <ul class="mt-1.5 ml-4 text-purple-700 list-disc list-inside">
                                <li class="text-gray-600">Bonus Topup 5% <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus End User <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus Reseller <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                                <li class="text-gray-600">Discount Khusus Agent <i class="fa fa-check text-green-500" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @if (auth()->user()->levelMember->level < 3)
                <div class="flex justify-center">
                    @if (auth()->user()->upgradeLevel && auth()->user()->upgradeLevel->level_member_id == 3)
                        <div class="flex flex-col gap-1">
                            <button disabled class="buttonUpgrade p-3 border border-purple-300 bg-purple-300 rounded-xl text-orange-800">Requested</button>
                            <button type="button" class="buttonInfoPembayaran bg-blue-100 text-blue-800 hover:bg-blue-200 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">cek info pembayaran</button>
                            <span class="text-red-500">Expired at {{ auth()->user()->upgradeLevel->created_at->addDays(1) }}</span>
                        </div>
                    @else
                    <div class="flex justify-center">
                        <button data-level="3" data-levelname="Agent" data-biayaupgrade="{{ $level_member->find(3)->harga_upgrade }}" class="buttonUpgrade p-3 border border-purple-300 bg-purple-100 rounded-xl hover:bg-purple-200">Upgrade ke Agent</button>
                    </div>
                    @endif
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

{{-- Format Rupiah --}}
<script>
    function formatRupiah(angka, prefix){
			var number_string = angka.toString().replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
		}
</script>

<script>
    $('.buttonUpgrade').click(function(){
        const level = $(this).attr('data-level');
        const levelName = $(this).attr('data-levelname');
        const biayaUpgrade = $(this).attr('data-biayaupgrade');

        Swal.fire({
            title: 'Upgrade level menjadi ' + levelName,
            text : 'Biaya upgrade sebesar ' + formatRupiah(biayaUpgrade, 'Rp '),
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/upgrade-level',
                    method : 'POST',
                    data : {
                        level : level,
                        '_token' : '{{ csrf_token() }}'
                    },
                    success : function(res){
                        if(!res){
                            Swal.fire(
                                'Berhasil!',
                                'Request upgrade level berhasil dilakukan. Silahkan hubungi Admin untuk konfirmasi pembayaran',
                                'success'
                            ).then(() => {
                                window.location = '/dashboard';
                            });
                        }
                    }
                });
            }
        })
    });

    $('.buttonInfoPembayaran').click(function(){
        $.ajax({
            url : '/upgrade-level',
            data : {
                bank : true
            },
            success : function(res){
                // console.log(res);
                Swal.fire({
                title: '<strong>Upgrade ke level Reseller</strong>',
                icon: 'info',
                html:
                    'Segera Selesaikan pembayarannya sebesar ' + '<br>' + `${formatRupiah(res.nominal, 'Rp ')}` + '<br>' +
                    `Rekening BCA a/n ${res.bank.atas_nama}` + '<br>' +
                    `No. Rekening : ${res.bank.no_rek}`,
                showCloseButton: false,
                showCancelButton: false,
                focusConfirm: false
            })
            }
        });
    });
</script>
@endsection
