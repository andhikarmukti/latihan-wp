@extends('layouts.main')

@section('content')
<div class="p-9">
    <div class="overflow-x-auto relative shadow-md sm:rounded-lg p-5">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableUpgradeRequest">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        Username
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Level Request
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Nominal
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Tanggal Pengajuan
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Status
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Note
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($upgrade_requests as $upgrade_request)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $upgrade_request->user->username }}
                    </th>
                    <td class="py-4 px-6">
                        {{ $upgrade_request->levelMember->level_name }}
                    </td>
                    <td class="py-4 px-6">
                        {{ Helpers::format_rupiah($upgrade_request->nominal) }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $upgrade_request->created_at }}
                    </td>
                    <td class="py-4 px-6 {{ $upgrade_request->status == 'pending' ? 'text-yellow-800' : ($upgrade_request->status == 'decline' ? 'text-red-500' : ($upgrade_request->status == 'expired' ? 'text-red-500' : 'text-green-500')) }}">
                        {{ $upgrade_request->status }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $upgrade_request->note }}
                    </td>
                    @if ($upgrade_request->status == 'pending')
                    <td class="py-4 px-2">
                        <button type="button" data-upgradeid="{{ $upgrade_request->id }}" data-userid="{{ $upgrade_request->user_id }}" data-action="accept" class="buttonAction bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900">Accept</button>
                        <button type="button" data-upgradeid="{{ $upgrade_request->id }}" data-userid="{{ $upgrade_request->user_id }}" data-action="decline" class="buttonAction bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900">Decline</button>
                    </td>
                    @else
                    <td class="py-4 px-2">
                        -
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

{{-- Datatables --}}
<script>
    // Datatables
    $(document).ready(function(){
            $('#tableUpgradeRequest').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                "pageLength": 100,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ]
                    }
                ],
                order:[[3,"desc"]]
            });
        });
</script>

{{-- Button Action --}}
<script>
    function capitalFirst(string) {
       return string.charAt(0).toUpperCase() + string.slice(1);
    }

    $('.buttonAction').click(function(){
        const action = $(this).attr('data-action');
        const userId = $(this).attr('data-userid');
        const upgradeId = $(this).attr('data-upgradeid');

        Swal.fire({
            title: `${capitalFirst(action)} Upgrade level ini?`,
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: "Tidak",
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ya!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/admin-upgrade-level-request',
                    method : 'PUT',
                    data : {
                        action : action,
                        user_id : userId,
                        upgrade_level_id : upgradeId,
                        '_token' : '{{ csrf_token() }}'
                    },
                    success : function(res){
                        // console.log(res);
                        if(!res){
                            Swal.fire({
                                title : 'Berhasil!',
                                text : `Berhasil update data upgrade level`,
                                icon : 'success',
                                timer : 3000
                            }).then(() => {
                                window.location.replace('/admin-upgrade-level-request');
                            });
                        }
                    },
                    error : function(err){
                        const error = err.responseJSON;
                        // console.log(error);
                        Swal.fire({
                            title : 'Gagal!',
                            text : `${error.message}`,
                            icon : 'error'
                        });
                    }
                });
            }
        });
    });
</script>
@endsection
