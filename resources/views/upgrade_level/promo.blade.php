@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full md:w-1/2">
            <form method="POST" action="/admin-upgrade-level-promo">
                @csrf
                <div class="mb-6">
                  <label for="level_member_id" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Level Member</label>
                  <select name="level_member_id" id="level_member_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                    <option selected disabled>Pilih Level Member</option>
                    @foreach ($level_members as $level_member)
                    <option value="{{ $level_member->id }}">{{ $level_member->level_name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="mb-6">
                    <div class="flex flex-col md:flex-row gap-1">
                        <div class="w-full">
                            <label for="start_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Start Date</label>
                            <input type="datetime-local" id="start_date" name="start_date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('start_date'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('start_date') }}</small>
                            @endif
                        </div>
                        <div class="w-full">
                            <label for="end_date" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">End Date</label>
                            <input type="datetime-local" id="end_date" name="end_date" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            @if ($errors->has('end_date'))
                                <small class="text-sm italic text-red-500">{{ $errors->first('end_date') }}</small>
                            @endif
                        </div>
                    </div>
                </div>
                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
            </form>
        </div>

        <div class="p-5 border border-slate-300 shadow-md rounded-lg mt-10 w-full md:w-3/4">
            <h3 class="text-lg italic font-bold mb-5">List Upgrade Level Promo</h3>
            <div class="overflow-x-auto relative shadow-md sm:rounded-lg p-4">
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableUpgradeLevelPromo">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="py-3 px-6">
                                Level Member
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Start Date
                            </th>
                            <th scope="col" class="py-3 px-6">
                                End Date
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Status
                            </th>
                            <th scope="col" class="py-3 px-6">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($upgrade_level_promos as $upgrade_level_promo)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                            <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $upgrade_level_promo->levelMember->level_name }}
                            </th>
                            <td class="py-4 px-6">
                                {{ $upgrade_level_promo->start_date }}
                            </td>
                            <td class="py-4 px-6">
                                {{ $upgrade_level_promo->end_date }}
                            </td>
                            <td class="py-4 px-6 {{ $upgrade_level_promo->end_date > date(now()) ? 'text-green-500' : 'text-red-500' }}">
                                {{ $upgrade_level_promo->end_date > date(now()) ? 'active' : 'done' }}
                            </td>
                            <td class="py-4 px-6">
                                <span data-id="{{ $upgrade_level_promo->id }}" class="delete bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-900 hover:cursor-pointer">delete</span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#tableUpgradeLevelPromo').DataTable({
                // responsive : true,
                "lengthChange": false,
                "ordering" : true,
                columnDefs: [
                    {
                        className: "dt-head-center",
                        targets: [ "_all" ],
                    },
                ],
                order: [[0, 'desc']],
            });
        });
    </script>

<script>
    $('.delete').click(function(){
        const id = $(this).attr('data-id');
        Swal.fire({
            title: 'Kamu yakin?',
            text: "Promo akan dihapus",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url : '/admin-upgrade-level-promo/' + id,
                    method : 'DELETE',
                    data : {
                        '_token' : '{{ csrf_token() }}'
                    },
                    success : function(res){
                        // console.log(res);
                        Swal.fire({
                            text: 'Promo Berhasil dihapus!',
                            icon : 'success'
                        }).then(() => {
                            window.location = '/admin-upgrade-level-promo';
                        });
                    }
                });

            }
        })
    })
</script>
@endsection
