@extends('layouts.main')

@section('content')
    <div class="p-9">
        <div class="flex flex-col">
            <div class="flex justify-center">
                <h3 class="text-2xl font-bold italic">User Profile</h3>
            </div>
            <div class="flex justify-center mt-9">
                <div class="w-full md:w-1/2">
                    <div class="flex gap-12 justify-between shadow-md rounded-lg border border-slate-300 w-full lg:w-1/3 mb-12 p-2 bg-green-100">
                        <div>
                            Saldo Pusat : <br> {{ Helpers::format_rupiah($saldo_pusat) }}
                        </div>
                        <div class="flex align-middle">
                            <a target="_blank" href="{{ $server_url . '/tambah-saldo' }}" class="shadow-md rounded-lg border border-slate-300 p-2 my-auto bg-yellow-100 hover:bg-yellow-200 hover:cursor-pointer">isi saldo</a>
                        </div>
                    </div>
                    <div class="relative z-0 mb-6 w-full group">
                        <input disabled value="{{ auth()->user()->api_key }}" type="text" name="username" id="floating_username" class="block py-2.5 px-0 w-full md:text-md text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                        <label for="floating_username" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">API Key</label>
                        <div class="flex justify-end mt-1">
                            <button id="buttonGenerateApiKey" class="text-xs p-1 bg-yellow-200 rounded-lg shadow-md hover:bg-blue-300">
                                Generate API Key
                            </button>
                        </div>
                    </div>
                    <form action="reset-user-password/{{ auth()->user()->id }}" method="POST">
                        @csrf
                        <div class="relative z-0 mb-6 w-full group">
                            <input disabled value="{{ auth()->user()->username }}" type="text" name="username" id="floating_username" class="block py-2.5 px-0 w-full text-2xl text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                            <label for="floating_username" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Username</label>
                        </div>
                        <div class="relative z-0 mb-6 w-full group">
                            <input type="password" name="current_password" id="current_password" class="block py-2.5 px-0 w-full text-2xl text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="" autocomplete="off">
                            <label for="current_password" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Password Saat Ini</label>
                        </div>
                        <div class="relative z-0 mb-6 w-full group">
                            <input type="password" name="new_password" id="new_password" class="block py-2.5 px-0 w-full text-2xl text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="" autocomplete="off">
                            <label for="new_password" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Password Baru</label>
                        </div>
                        <div class="relative z-0 mb-6 w-full group">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="block py-2.5 px-0 w-full text-2xl text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="" autocomplete="off">
                            <label for="password_confirmation" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Konfirmasi Password Baru</label>
                        </div>
                        <div class="relative z-0 mb-6 w-full group">
                            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-2 py-1.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Reset Password</button>
                        </div>
                        {{-- <div class="grid md:grid-cols-3 md:gap-6">
                            <div class="relative z-0 mb-6 w-full group">
                                <input disabled value="{{ auth()->user()->levelMember->level_name }}" type="text" name="level_member" id="level_member" class="block py-2.5 px-0 w-full text-2xl text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                                <label for="level_member" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Level Member</label>
                            </div>
                            <div class="relative z-0 mb-6 w-full group">
                                <input disabled value="{{ Helpers::format_rupiah(auth()->user()->saldo) }}" type="text" name="saldo" id="saldo" class="block py-2.5 px-0 w-full text-2xl text-yellow-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                                <label for="saldo" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Saldo Saat Ini</label>
                            </div>
                            <div class="relative z-0 mb-6 w-full group">
                                @if(auth()->user()->levelMember->level != 1)
                                <input disabled value="{{ Helpers::format_rupiah(auth()->user()->saldo_digunakan) }}" type="text" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" name="saldo_digunakan" id="saldo_digunakan" class="block py-2.5 px-0 w-full text-2xl text-green-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                                @else
                                <input disabled value="Hanya terhitung untuk level Reseller / Agent" type="text" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" name="saldo_digunakan" id="saldo_digunakan" class="block py-2.5 px-0 w-full text-xs text-green-800 font-bold bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required="">
                                @endif
                                <label for="saldo_digunakan" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Total Saldo Digunakan</label>
                            </div> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#buttonGenerateApiKey').click(function(){
            const userId = "{{ auth()->user()->id }}";
            Swal.fire({
                title: 'Generate API Key?',
                text: `Mohon untuk selalu menjaga kerahasiaan API Key Anda`,
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: "Tidak",
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url : '/user-generate-api-key/' + userId,
                        data : {
                            '_token' : '{{ csrf_token() }}'
                        },
                        method : 'POST',
                        success : function(res){
                            if(!res){
                                Swal.fire({
                                    icon : 'success',
                                    text : 'Berhasil generate API Key',
                                    timer : 800
                                }).then(() => {
                                    window.location = '/profile'
                                });
                            }else{
                                Swal.fire({
                                    icon : 'error',
                                    text : 'Error, Silahkan Hubungi Admin',
                                    timer : 3000
                                })
                            }
                        }
                    });
                }
            });

        });
    </script>
@endsection
