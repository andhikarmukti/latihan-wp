@extends('layouts.main')

@section('content')
    <div class="p-9">
        <h3 class="font-bold italic text-2xl mb-7">{{ $users->count() }} Users</h3>
        <div class="overflow-x-auto relative shadow-md sm:rounded-lg p-5">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableUsers">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            User ID
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Status
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Username
                        </th>
                        <th scope="col" class="py-3 px-6">
                            User Upline
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Email
                        </th>
                        <th scope="col" class="py-3 px-6">
                            No WA
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo Digunakan
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Saldo Level Digunakan
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Tanggal Join
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Last Login
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Last Seen
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Role
                        </th>
                        <th scope="col" class="py-3 px-6">
                            Level
                        </th>
                        {{-- <th scope="col" class="py-3 px-6">
                            Action
                        </th> --}}
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($users as $user)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 text-center">
                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{ $user->id }}
                        </th>
                        <td class="py-4 px-6">
                            @if(Cache::has('user-is-online-' . $user->id))
                                <span class="text-green-500">✦</span>
                            @else
                                <span class="text-red-500">✦</span>
                            @endif
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->username }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->where('id', $user->user_upline_id)->first()->username ?? '' }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->email }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->no_wa }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->saldo }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->saldo_digunakan }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->upgradeLevelAccept ? $user->upgradeLevelAccept->saldo_digunakan : '-'}}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->created_at }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->last_login_at }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->last_seen }}
                        </td>
                        <td class="py-4 px-6">
                            {{ $user->role }}
                        </td>
                        <td class="py-4 px-6 {{ $user->levelMember->level == 2 ? 'text-yellow-500' : ($user->levelMember->level == 3 ? 'text-purple-500' : '') }}">
                            {{ $user->levelMember->level_name }}
                        </td>
                        <td class="py-4 px-6">
                            <a href="/admin-user-edit/{{ $user->id }}" class="bg-yellow-100 text-yellow-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-yellow-200 dark:text-yellow-900">edit</a>
                        </td>
                    </tr>
                    @endforeach --}}
                </tbody>
            </table>
        </div>
    </div>

    <script>
        // Datatables
        $(document).ready(function() {
            $('#tableUsers').DataTable({
                "pageLength": 100,
                // responsive: true,
                "lengthChange": false,
                "ordering": true,
                serverSide: true,
                processing: true,
                "autoWidth": false,
                ajax : '/users-list/yajra',
                columns : [
                    {data : 'id', name : 'u.id'},
                    {data : 'status_active', name : 'status_active'},
                    {data : 'username', name : 'u.username'},
                    {data : 'user_upline_id', name : 'u.user_upline_id'},
                    {data : 'email', name : 'u.email'},
                    {data : 'no_wa', name : 'u.no_wa'},
                    {data : 'saldo', name : 'u.saldo'},
                    {data : 'saldo_digunakan', name : 'u.saldo_digunakan'},
                    {data : 'saldo_level_digunakan', name : 'saldo_level_digunakan', orderable : false},
                    {data : 'created_at', name : 'u.created_at'},
                    {data : 'last_login_at', name : 'u.last_login_at'},
                    {data : 'last_seen', name : 'u.last_seen'},
                    {data : 'role', name : 'u.role'},
                    {data : 'level_name', name : 'lb.level_name'},
                ],
                columnDefs: [
                    {
                        targets: ["_all"],
                        className: "dt-head-center",
                    },
                    {
                        targets: ["_all"],
                        className: "dt-center",
                    },
                    {
                        target: 13,
                        render: function ( data, type, row ) {
                            return data == 'Agent' ? `<span class="text-purple-500">${data}</span>` : (data == 'Reseller' ? `<span class="text-yellow-500">${data}</span>` : `<span class="">${data}</span>`)
                        }
                    },
                    {
                        target: [6, 7, 8],
                        render: $.fn.dataTable.render.number( '.', ',', 0, 'Rp ' )
                    }
                ],
                order : [[11, 'desc']],
            });
        });
    </script>
@endsection
