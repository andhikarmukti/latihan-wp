@extends('layouts.main')

@section('content')
<div class="p-9">
    <div class="w-full md:w-1/3 rounded-lg border border-slate-200 shadow-md mb-5 p-5">
        <form method="POST" action="wa-broadcast">
            @csrf
            <div class="mb-6">
                <label for="no_wa" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">WhatsApp Number</label>
                <textarea id="no_wa" name="no_wa" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off"></textarea>
            </div>
            <div class="mb-6">
                <label for="type" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Broadcast Type</label>
                <select id="type" name="type" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                  <option selected value="agent">Agent</option>
                  {{-- <option value="reseller">Reseller</option> --}}
                  <option value="enduser">End User</option>
                </select>
            </div>
            <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
        </form>
    </div>

    <p>Total Nomor : {{ $total_nomor }}</p>
    <p>Total Message Pending : {{ $total_message_satu_null }}</p>
    {{-- <p>Total Message Dua Null : {{ $total_message_dua_null }}</p>
    <p>Total Message Tiga Null : {{ $total_message_tiga_null }}</p> --}}
    <div class="overflow-x-auto relative shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400" id="tableBroadcast">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400 text-center">
                <tr>
                    <th scope="col" class="py-3 px-6">
                        No
                    </th>
                    <th scope="col" class="py-3 px-6">
                        No WA
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Type
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Message
                    </th>
                    {{-- <th scope="col" class="py-3 px-6">
                        Message 2
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Message 3
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Regis
                    </th>
                    <th scope="col" class="py-3 px-6">
                        First Topup
                    </th>
                    <th scope="col" class="py-3 px-6">
                        First Order
                    </th>
                    <th scope="col" class="py-3 px-6">
                        Promo
                    </th> --}}
                    <th scope="col" class="py-3 px-6">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($wa_lists as $wa_list)
                <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700 text-center">
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $wa_list->id }}
                    </th>
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $wa_list->no_wa }}
                    </th>
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $wa_list->type }}
                    </th>
                    <td class="py-4 px-6">
                        {{ $wa_list->message_satu ?? '-' }}
                    </td>
                    {{-- <td class="py-4 px-6">
                        {{ $wa_list->message_dua ?? '-' }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $wa_list->message_tiga ?? '-' }}
                    </td>
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{ $wa_list->regis ?? '-' }}
                    </th>
                    <td class="py-4 px-6">
                        {{ $wa_list->first_topup ?? '-' }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $wa_list->first_order ?? '-' }}
                    </td>
                    <td class="py-4 px-6">
                        {{ $wa_list->promo ?? '-' }}
                    </td> --}}
                    <td class="py-4 px-6">
                        @if ($wa_list->is_active == 1)
                        <button id="stop_id_{{ $wa_list->id }}" data-action="stop" data-id="{{ $wa_list->id }}" class="actionButton bg-red-100 text-red-500 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-red-200 dark:text-red-600 hover:cursor-pointer">Stop</button>
                        @else
                        <button id="run_id_{{ $wa_list->id }}" data-action="run" data-id="{{ $wa_list->id }}" class="actionButton bg-blue-100 text-blue-500 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-600 hover:cursor-pointer">Run</button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    // Datatables
    $(document).ready(function() {
        $('#tableBroadcast').DataTable({
            "pageLength": 100,
            // responsive: true,
            "lengthChange": false,
            "ordering": true,
            columnDefs: [
                {
                    className: "dt-head-center",
                    targets: ["_all"]
                },
                {
                    target:1,
                    className:'cell-border'
                }
            ],
            order : [[0, 'desc']],
        });
    });
</script>

<script>
    $('.actionButton').click(function(){
        const action = $(this).data('action');
        const idWa = $(this).data('id');
        $.ajax({
            url : '/wa-broadcast/' + idWa,
            method : 'POST',
            data : {
                id : idWa,
                action : action,
                '_method' : 'PUT',
                '_token' : '{{ csrf_token() }}'
            },
            success : function(res){
                Swal.fire({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    text: res.message,
                    icon: "success",
                    timer: 1000
                }).then(() => {
                    if(action == 'stop'){
                        $('#stop_id_' + idWa).text('✓');
                        $('#stop_id_' + idWa).attr('disabled', true);
                        $('#stop_id_' + idWa).removeClass('bg-red-100 hover:cursor-pointer');
                    }else{
                        $('#run_id_' + idWa).text('✓');
                        $('#run_id_' + idWa).attr('disabled', true);
                        $('#run_id_' + idWa).removeClass('bg-blue-100 hover:cursor-pointer');
                    }
                });
            },error : function(err){
                const error = err.responseJSON;
                Swal.fire(
                    'Error!',
                    'Hubungi admin untuk melihat errornya di log!',
                    'error'
                )
            }
        });
    });
</script>
@endsection
