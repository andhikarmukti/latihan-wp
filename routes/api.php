<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\HistoryOrderController;
use App\Http\Controllers\MidtransNotificationController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Midtrans Callback
Route::post('/callback-midtrans', [MidtransNotificationController::class, 'callbackMidtrans']);

// Info Discount Tulisan Berjalan
Route::get('/info-discount', [DiscountController::class, 'infoDiscount']);


Route::group(['middleware' => ['user-banned-api']], function () {
    // REST API for Web Panel
    Route::post('/order', [HistoryOrderController::class, 'store']);
    Route::post('/services', [ServiceController::class, 'apiServices']);
    Route::post('/service-categories', [ServiceController::class, 'apiServiceCategories']);
    Route::post('/status', [HistoryOrderController::class, 'apiStatusOrder']);
    Route::post('/balance', [UserController::class, 'apiBalance']);
    Route::post('/user', [UserController::class, 'apiFetchUser']);
    Route::post('/fetch-control-code', [WebpanelOrderController::class, 'fetchControlCode']);
});

Route::post('/expired-web', [PagesController::class, 'expiredWebApi']);
