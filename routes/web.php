<?php

use Midtrans\Snap;
use App\Models\Server;
use App\Models\RefillStatus;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BankController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\TopupController;
use App\Http\Controllers\ServerController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\LogSaldoController;
use App\Http\Controllers\AppConfigController;
use App\Http\Controllers\WithdrawalController;
use App\Http\Controllers\RegisterOtpController;
use App\Http\Controllers\WaBroadcastController;
use App\Http\Controllers\HistoryOrderController;
use App\Http\Controllers\RefillStatusController;
use App\Http\Controllers\SpecialPriceController;
use App\Http\Controllers\UpgradeLevelController;
use App\Http\Controllers\UserReferralController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\ServiceCategoryController;
use App\Http\Controllers\UpgradeLevelPromoController;
use App\Http\Controllers\ResetPasswordManualController;
use App\Http\Controllers\MidtransNotificationController;
use App\Http\Controllers\YajraController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
})->middleware('guest');

Route::group(['middleware' => ['auth', 'web_expired', 'user-banned']], function () {
    // Admin Only
    Route::group(['middleware' => ['admin-only', 'web_expired']], function () {
        // Saldo
        Route::post('/topup-paid/{topup}', [TopupController::class, 'paidTopup']);
        Route::get('/topup-saldo-manual', [TopupController::class, 'topupManual']);
        Route::post('/topup-saldo-manual', [TopupController::class, 'topupManualStore']);
        Route::post('/topup-saldo-tidak-sesuai-3digit', [TopupController::class, 'topupManualTidakSesuai3Digit']);

        // Server
        // Route::get('/server', [ServerController::class, 'index']);
        // Route::get('/server/edit/{server}', [ServerController::class, 'edit']);
        // Route::post('/server/update/{server}', [ServerController::class, 'update']);
        // Route::post('/server', [ServerController::class, 'store']);
        // Route::post('/server-action-path', [ServerController::class, 'serverActionPathStore']);
        // Route::get('/cek-layanan-server', [ServerController::class, 'cekLayananServer']);
        // Route::get('/server-list', [ServerController::class, 'serverList']);
        // Server Store Auto
        // Route::post('/server-parameters', [ServerController::class, 'serverStoreParameters']);

        // Service Category
        // Route::get('/category-service', [ServiceCategoryController::class, 'index']);
        // Route::get('/category-service/edit/{serviceCategory}', [ServiceCategoryController::class, 'edit']);
        // Route::put('/category-service/update/{serviceCategory}', [ServiceCategoryController::class, 'update']);
        // Route::post('/category-service', [ServiceCategoryController::class, 'store']);
        // Route::get('/category-service/disable/{serviceCategory}', [ServiceCategoryController::class, 'disable']);

        // Service
        // Route::post('/service', [ServiceController::class, 'store']);
        Route::get('/service/edit/{service}', [ServiceController::class, 'edit']);
        Route::post('/service/update/{service}', [ServiceController::class, 'update']);
        Route::post('/auto-price', [ServiceController::class, 'autoPrice']);

        // Upgrade Level
        Route::get('/admin-upgrade-level-request', [UpgradeLevelController::class, 'listRequest']);
        Route::put('/admin-upgrade-level-request', [UpgradeLevelController::class, 'update']);
        // Upgrade Level Promo
        Route::get('/admin-upgrade-level-promo', [UpgradeLevelPromoController::class, 'index']);
        Route::post('/admin-upgrade-level-promo', [UpgradeLevelPromoController::class, 'store']);
        Route::delete('/admin-upgrade-level-promo/{upgradeLevelPromo}', [UpgradeLevelPromoController::class, 'destroy']);

        // Bank
        Route::get('/admin-bank', [BankController::class, 'index']);
        Route::post('/admin-bank', [BankController::class, 'store']);
        Route::get('/admin-bank/edit/{bank}', [BankController::class, 'edit']);
        Route::get('/admin-bank/disable/{bank}', [BankController::class, 'disable']);
        Route::get('/admin-bank/enable/{bank}', [BankController::class, 'enable']);
        Route::get('/admin-bank/delete/{bank}', [BankController::class, 'delete']);
        Route::put('/admin-bank/update/{bank}', [BankController::class, 'update']);

        // Admin Users
        Route::get('/admin-users', [UserController::class, 'users']);
        Route::get('/admin-user-edit/{user}', [UserController::class, 'userEditAdmin']);

        // Discount
        Route::get('/admin-discount', [DiscountController::class, 'index']);
        Route::post('/admin-discount', [DiscountController::class, 'store']);
        Route::delete('/admin-discount/{discount}', [DiscountController::class, 'destroy']);

        // Finance
        Route::get('/admin-finance', [FinanceController::class, 'index']);

        // WA Broadcast
        Route::get('/wa-broadcast', [WaBroadcastController::class, 'index']);
        Route::post('/wa-broadcast', [WaBroadcastController::class, 'store']);
        Route::put('/wa-broadcast/{waBroadcast}', [WaBroadcastController::class, 'stopBroadcast']);

        // Reset Password
        Route::get('/reset-password-manual', [ResetPasswordManualController::class, 'index']);
        Route::post('/reset-password-manual', [ResetPasswordManualController::class, 'store']);

        // Register OTP
        Route::get('/register-otp', [RegisterOtpController::class, 'index']);

        // Mutasi Check
        Route::post('/mutasi-check', [TopupController::class, 'mutasiCheck']);

        // Referral
        Route::get('/user-referral', [UserReferralController::class, 'userReferral']);
        Route::post('/action-bank-account/{userReferral}', [UserReferralController::class, 'actionBankAccount']);

        // Withdrawal
        Route::post('/accept-withdrawal/{withdrawal}', [WithdrawalController::class, 'acceptWithdrawal']);

        // Configuration
        Route::get('/configuration', [AppConfigController::class, 'index']);
        Route::post('/websiteInfoStore', [AppConfigController::class, 'websiteInfoStore']);
        Route::post('/contactInfoStore', [AppConfigController::class, 'contactInfoStore']);
        Route::post('/levelInfoStore', [AppConfigController::class, 'levelInfoStore']);
        Route::post('/deviceWaStore', [AppConfigController::class, 'deviceWaStore']);
        Route::post('/slider-active', [AppConfigController::class, 'sliderActive']);

        // Special Price
        Route::get('/admin-special-price', [SpecialPriceController::class, 'index']);
        Route::post('/admin-special-price', [SpecialPriceController::class, 'store']);
        Route::get('/admin-special-price/edit/{specialPrice}', [SpecialPriceController::class, 'edit']);
        Route::put('/admin-special-price/{specialPrice}', [SpecialPriceController::class, 'update']);

        // Yajra Datatables Admin
        Route::get('/users-list/yajra', [YajraController::class, 'usersList']);
    });

    // Pages Controller
    Route::get('/dashboard', [PagesController::class, 'dashboard'])->name('dashboard');
    Route::get('/info', [PagesController::class, 'info']);

    // Saldo
    Route::get('/tambah-saldo', [TopupController::class, 'index']);
    Route::post('/tambah-saldo', [TopupController::class, 'store']);
    Route::get('/riwayat-isi-saldo', [TopupController::class, 'history']);
    Route::post('/topup-cancel/{topup}', [TopupController::class, 'cancelTopup']);

    // Service
    Route::get('/service', [ServiceController::class, 'index']);

    // Order
    Route::get('/new-order', [HistoryOrderController::class, 'newOrder']);
    Route::post('/success-order', [HistoryOrderController::class, 'successOrder']);
    Route::get('/history-order', [HistoryOrderController::class, 'historyOrder']);
    Route::get('/history-order/check-user-role', [HistoryOrderController::class, 'historyOrderCheckUserRole']);
    Route::post('/new-order', [HistoryOrderController::class, 'store']); // refill masuk ke sini juga
    Route::post('/cancel/{historyOrder}', [HistoryOrderController::class, 'cancel']);
    Route::post('/speedup/{historyOrder}', [HistoryOrderController::class, 'speedup']);

    // User
    Route::get('/profile', [UserController::class, 'profile']);
    Route::post('/reset-user-password/{user}', [UserController::class, 'resetPasswordByUser']);
    Route::post('/user-generate-api-key/{user}', [UserController::class, 'generateApiKey']);

    // Log Saldo
    Route::get('/log-saldo', [LogSaldoController::class, 'index']);

    // Refill
    // Route::get('/history-order-refill', [RefillStatusController::class, 'index']);
    // Route::post('/update-refill-status', [RefillStatusController::class, 'update']);

    // Upgrade Level
    Route::get('/upgrade-level', [UpgradeLevelController::class, 'index']);
    Route::post('/upgrade-level', [UpgradeLevelController::class, 'store']);

    // Midtrans
    Route::post('/midtrans-notification', [MidtransNotificationController::class, 'midtransNotification']);

    // User Referral
    Route::get('/referral', [UserReferralController::class, 'index']);
    Route::get('/tarik-komisi', [UserReferralController::class, 'tarikKomisi']);
    Route::post('/tarik-komisi', [UserReferralController::class, 'dataBankStore']);
    Route::post('/referral-generate-link', [UserReferralController::class, 'generateLink']);
    Route::post('/delete-bank-account', [UserReferralController::class, 'deleteBankAccount']);
    Route::get('/list-downline', [UserReferralController::class, 'listDownline']);

    // Withdrawal
    Route::post('/request-withdrawal', [WithdrawalController::class, 'store']);

    // Documentasi API
    Route::get('/doc-api', [PagesController::class, 'docApi']);

    // Comments Show
    Route::post('/comments-show/{historyOrder}', [HistoryOrderController::class, 'commentShowButton']);

    // Yajra Datatables
    Route::get('/history-order/yajra', [YajraController::class, 'historyOrder']);
});

require __DIR__.'/auth.php';


Route::group(['middleware' => ['guest', 'web_expired']], function () {
    Route::post('/check-otp', [RegisterOtpController::class, 'getOtp']);
    Route::get('/forgot-password', [UserController::class, 'forgotPassword']);
    Route::post('/forgot-password', [UserController::class, 'forgotPasswordPost'])->middleware('throttle:5,1');
    Route::get('/reset-password/{token}', [UserController::class, 'formResetPassword']);
    Route::post('/reset-password/{token}', [UserController::class, 'formResetPasswordPost']);
});

Route::get('/logout', function(){
    return back();
});

// Expired
Route::get('/expired', [PagesController::class, 'expiredWeb'])->name('expired_web');

// Referral link
Route::get('/ref/{refferal}', [UserReferralController::class, 'referralCookie']);
